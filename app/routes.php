<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('press_releases/', 'Press_releasesController@index');
Route::get('press_releases/{id}', 'Press_releasesController@show');

Route::get('press_release/post', 'Press_releasesController@getPost');
Route::post('press_release/posted', array('as'=>'press_release.posted', 'uses'=>'Press_releasesController@postPost'));

Route::get('ideas/', 'IdeasController@index');
Route::get('ideas/{id}', 'IdeasController@show');

Route::get('/profile/view', 'HomeController@viewCompanyProfile' );

Route::resource('articles', 'ArticlesController');
Route::get('articles/category/{category?}', 'ArticlesController@index');

Route::resource('videos', 'VideosController');
Route::get('/logout', 'LoginController@getLogout');
Route::get('/about-us', 'HomeController@about');
Route::get('/corporate-services', 'HomeController@corporate');
Route::get('/corporate', 'HomeController@corporate');

Route::get('/terms-and-conditions', 'HomeController@toc');
Route::get('/privacy-policy', 'HomeController@privacy');
Route::get('/contact-us', array('as' => 'contact-us', 'uses'=>'HomeController@contact'));
Route::post('/feedback',  array('as' => 'feedback', 'uses'=>'HomeController@feedback'));
Route::get('/join',  array('as' => 'join', 'uses'=>'HomeController@join'));
Route::get('/options',  array('as' => 'options', 'uses'=>'HomeController@options'));

Route::get('/sign-in', 'LoginController@getIndex');
Route::post('/sign-in', 'LoginController@postIndex');

Route::get('/forgot-password', 'LoginController@getForgotPassword');
Route::post('/forgot-password', 'LoginController@postForgotPassword');

Route::get('/password/reset/{token}', 'LoginController@getResetPassword');
Route::post('/password/reset/{token}', 'LoginController@postResetPassword');

Route::get('/register', array('as'=>'get-register-basic-member', 'uses'=> 'RegisterController@getIndex'));
Route::post('/register', array('as'=>'post-register-basic-member', 'uses'=> 'RegisterController@postIndex'));

Route::get('/register/basic', array('as'=>'get-register-basic-company', 'uses'=> 'RegisterController@getBasic'));
Route::post('/register/basic', array('as'=>'post-register-basic-company', 'uses'=> 'RegisterController@postBasic'));

Route::get('/register/premium', array('as'=>'get-register-premium-company', 'uses'=> 'RegisterController@getPremium'));
Route::post('/register/premium', array('as'=>'post-register-premium-company', 'uses'=> 'RegisterController@postPremium'));

Route::get('/my-account', 'HomeController@account');

Route::get('/profile/edit',  array('as'=>'get-edit-profile', 'uses'=>'HomeController@getEditProfile'));
Route::post('/profile/update', array('as'=>'post-edit-profile', 'uses'=>'HomeController@postEditProfile'));

Route::get('/profile/edit',  array('as'=>'get-edit-profile', 'uses'=>'HomeController@getEditProfile'));

Route::get('/profile/upload-profile', array('as'=>'get-upload-profile', 'uses'=>'HomeController@getUploadProfile'));
Route::post('/profile/update-profile', array('as'=>'post-upload-profile', 'uses'=>'HomeController@postUploadProfile'));

Route::get('/profile/users', array('as'=>'get-users-profile', 'uses'=>'HomeController@getUsers'));
Route::post('/profile/save-users', array('as'=>'post-users-profile', 'uses'=>'HomeController@postUsers'));
Route::delete('/profile/users/delete/{id}', array('as'=>'profile.users.delete', 'uses'=>'HomeController@deleteUser'));

Route::get('/activate', 'RegisterController@activate');


Route::get('/upgrade', array('as'=>'get-upgrade-user', 'uses'=>'HomeController@getUpgrade'));
Route::post('/upgrade', array('as'=>'post-upgrade-user', 'uses'=>'HomeController@postUpgrade'));
Route::get('/upgrade/success', array('as'=>'get-upgrade-success', 'uses'=>'HomeController@getUpgradeSuccess'));

Route::get('/profile/interviews',  array('as'=>'get-interviews-profile', 'uses'=>'HomeController@getInterviews'));

Route::group(array ('prefix' => 'profile' ), function (){
  Route::resource('press_releases', 'Press_releasesController');
  Route::resource('company_interviews', 'Company_interviewsController');
});



Route::get('/profile/press_releases', array('as'=>'profile-press_releases', 'uses'=>'Press_releasesController@my'));
Route::get('/profile/company_interviews', array('as'=>'profile-company_interviews', 'uses'=>'Company_interviewsController@my'));
Route::post('/profile/company_interviews', array('as'=>'post-interviews-profile', 'uses'=>'HomeController@postInterviews'));
Route::post('/profile/company_interviews/{id}', array('as'=>'profile.company_interviews.update', 'uses'=>'HomeController@putInterviews'));


Route::get('/location/edit',  array('as'=>'get-edit-location', 'uses'=>'HomeController@getEditLocation'));
Route::post('/location/update',  array('as'=>'post-edit-location', 'uses'=>'HomeController@postEditLocation'));
Route::resource('companies', 'CompaniesController');

Route::get('/search', array('as'=>'get-search-jobs', 'uses'=> 'ArticlesController@search'));

Route::get('/login', array('as'=>'login',  'uses'=> 'LoginController@getIndex'));
Route::post('/login', array('as'=>'login',  'uses'=> 'LoginController@postIndex'));// function(){
  //return Redirect::to('admin/login');
//});

Route::get('/api/articles/json', 'Administration\ArticlesController@getDatatable');
Route::get('/api/categories/json', 'Administration\CategoriesController@getDatatable');
Route::get('/api/companies/json', 'Administration\CompaniesController@getDatatable');
Route::get('/api/company_interviews/json', 'Administration\Company_interviewsController@getDatatable');
Route::get('/api/company_reviews/json', 'Administration\Company_reviewsController@getDatatable');
Route::get('/api/complaints/json', 'Administration\ComplaintsController@getDatatable');
Route::get('/api/countries/json', 'Administration\CountriesController@getDatatable');
Route::get('/api/deal_sections/json', 'Administration\Deal_sectionsController@getDatatable');
Route::get('/api/ideas/json', 'Administration\IdeasController@getDatatable');
Route::get('/api/industries/json', 'Administration\IndustriesController@getDatatable');
Route::get('/api/members/json', 'Administration\MembersController@getDatatable');
Route::get('/api/pages/json', 'Administration\PagesController@getDatatable');
Route::get('/api/patent_investments/json', 'Administration\Patent_investmentsController@getDatatable');
Route::get('/api/press_releases/json', 'Administration\Press_releasesController@getDatatable');
Route::get('/api/patents/json', 'Administration\PatentsController@getDatatable');
Route::get('/api/services/json', 'Administration\ServicesController@getDatatable');
Route::get('/api/ad_banners/json', 'Administration\Ad_bannersController@getDatatable');
Route::get('/api/videos/json', 'Administration\VideosController@getDatatable');


Route::get('/api/sections/json', 'Administration\SectionsController@getDatatable');

Route::get('/register/success', array('as'=>'register.success', 'uses'=> 'RegisterController@success'));
Route::group(array ('before' => 'auth', 'prefix' => 'admin' ), function ()
{   
  Route::get('/', 'Administration\PagesController@index' );//'Firalabs\Firadmin\Controllers\DashboardController@getIndex');

  Route::get('articles/category/{category?}', 'Administration\ArticlesController@index');


  Route::put('accounts/{id}/change-password', 'Administration\AccountsController@changePassword');


  Route::get('articles/csv', 'Administration\ArticlesController@getCSV');

  Route::get('categories/csv', 'Administration\CategoriesController@getCSV');
  Route::get('companies/csv', 'Administration\CompaniesController@getCSV');
  Route::get('companies/import', 'Administration\CompaniesController@getImport');
  Route::post('companies/import', 'Administration\CompaniesController@postImport');

  Route::get('company_interviews/csv', 'Administration\Company_interviewsController@getCSV');
  Route::get('company_reviews/csv', 'Administration\Company_reviewsController@getCSV');
  Route::get('complaints/csv', 'Administration\ComplaintsController@getCSV');
  Route::get('countries/csv', 'Administration\CountriesController@getCSV');
  Route::get('deal_sections/csv', 'Administration\Deal_sectionsController@getCSV');
  Route::get('ideas/csv', 'Administration\IdeasController@getCSV');
  Route::get('industries/csv', 'Administration\IndustriesController@getCSV');
  Route::get('members/csv', 'Administration\MembersController@getCSV');
  Route::get('services/csv', 'Administration\ServicesController@getCSV');  
  Route::get('pages/csv', 'Administration\PagesController@getCSV');
  Route::get('patents/csv', 'Administration\PatentsController@getCSV');
  Route::get('press_releases/csv', 'Administration\Press_releasesController@getCSV');
  Route::get('patent_investments/csv', 'Administration\Patent_investmentsController@getCSV');
  Route::get('videos/csv', 'Administration\VideosController@getCSV');
  Route::get('sections/csv', 'Administration\SectionsController@getCSV');
  Route::get('photos/csv', 'PhotosController@getCSV');
  Route::get('sections/csv', 'Administration\SectionsController@getCSV');


  Route::resource('accounts', 'Administration\AccountsController');
  Route::resource('articles', 'Administration\ArticlesController');
  Route::resource('categories', 'Administration\CategoriesController');
  Route::resource('companies', 'Administration\CompaniesController');
  Route::resource('company_interviews', 'Administration\Company_interviewsController');
  Route::resource('company_reviews', 'Administration\Company_reviewsController');
  Route::resource('complaints', 'Administration\ComplaintsController');
  Route::resource('countries', 'Administration\CountriesController');
  Route::resource('deal_sections', 'Administration\Deal_sectionsController');
  Route::resource('ideas', 'Administration\IdeasController');

  Route::resource('industries', 'Administration\IndustriesController');
  Route::resource('jobs', 'Administration\JobsController');
  
  Route::resource('members', 'Administration\MembersController');
  Route::resource('services', 'Administration\ServicesController');  
  
  Route::resource('pages', 'Administration\PagesController');
  Route::resource('patents', 'Administration\PatentsController');
  Route::resource('press_releases', 'Administration\Press_releasesController');
  Route::get('press_releases/{id}/notify', array('as'=>'admin.press_releases.notify', 'uses'=> 'Administration\Press_releasesController@getNotify'));
  Route::post('press_releases/{id}/notify', 'Administration\Press_releasesController@postNotify');

  Route::resource('patent_investments', 'Administration\Patent_investmentsController');
  Route::resource('videos', 'Administration\VideosController');
  Route::resource('sections', 'Administration\SectionsController');
  Route::resource('photos', 'PhotosController');
  Route::resource('sections', 'Administration\SectionsController');
  Route::resource('membership_options', 'Administration\Membership_optionsController');
  Route::resource('ad_settings', 'Administration\Ad_settingsController');

  Route::resource('user', 'Administration\UserController');
  Route::put('user/{id}/change-password', 'Administration\UserController@changePassword');
  Route::resource('ad_banners', 'Administration\Ad_bannersController');
  Route::resource('red_carpets', 'Administration\Red_carpetsController');
  Route::resource('news_headers', 'Administration\MarquesController');
  Route::resource('transactions', 'Administration\TransactionsController');
  Route::resource('video_sections', 'Administration\Video_sectionsController');

  Route::get('videos/section/{section?}', 'Administration\VideosController@index');
  Route::get('videos/section/{section?}/create', 'Administration\VideosController@create');
  Route::resource('auto_emails', 'Administration\Auto_emailsController');
  Route::resource('privileges', 'Administration\PrivilegesController');
});

Route::resource('jobs', 'JobsController');