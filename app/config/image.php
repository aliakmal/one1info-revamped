<?php

return array(
    'library'     => 'gd',
    'upload_dir'  => 'uploads',
    'assets_dir' =>'/uploads/images/',
    'upload_path' => public_path() . '/uploads/images/',
    'default_video_image' => public_path() . '/assets/images/video.jpg',
    'quality'     => 85,
    'dimensions' => array(
        'thumb'  => array(100, 100, true,  80),
        'thumb-med'  => array(120, 90, true,  80),
        'thumb-mini'  => array(70, 50, true,  80),
        'medium' => array(600, 400, true, 90),
        'slide' => array(385, 385, true, 90),
        'main' => array(620, 475, true, 90),
    ),
);
