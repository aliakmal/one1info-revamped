<?php

function getRolesSettings(){
  $a = json_decode(file_get_contents(app_path().'/storage/meta/setting.json'), true);

  $a['roles']['basic-member'] = false;
  $a['roles']['basic-company'] = false;  
  $a['roles']['premium-company'] = false;

  return $a['roles'];
}

return array(
    /*
      |--------------------------------------------------------------------------
      | The view used as a layout for the admin panel
      |--------------------------------------------------------------------------
     */
    'layout' => 'layouts.admin',
    /*
      |--------------------------------------------------------------------------
      | Navigation array us to generate the nav in admin panel
      |
      | 'navigation' => array(
      |	'uri' => 'title' //The key is use to generate the Url
      | )
      |--------------------------------------------------------------------------
     */
    'navigation' => array(
      //'admin' => 'Dashboard',
      'admin/pages'=>'Company Pages',
      'admin/auto_emails'=>'Auto Emails',

      'admin/accounts' => 'Accounts',
      'admin/sections' => 'Media/News Sections',
      'admin/press_releases' => 'Press Releases',
      'admin/categories' => 'Business Categories',
      'admin/articles' => 'Articles',
      'admin/industries' => 'Industries',
      'admin/companies' => 'Companies',
      'admin/membership_options' => 'Membership Options',
      'admin/company_interviews' => 'Company Interviews',
      'admin/company_reviews' => 'Company Reviews',
      'admin/complaints' => 'Complaints',
      'admin/services' => 'Corporate Services',
      'admin/jobs' => 'Jobs',
      'admin/countries' => 'Countries',
      'admin/deal_sections' => 'Deal Sections',
      'admin/transactions' => 'Transactions',
      'admin/ideas' => 'Ideas',
      'admin/members' => 'Members',
      'admin/pages' => 'Pages',
      'admin/patent_investments' => 'Patent Investments',
      'admin/patents' => 'Patents',
      //'admin/videos' => 'Videos',
      'admin/video_sections' => 'Video Gallery',
      'admin/videos/section/youtube' => 'YouTube Videos',

      //'Administration' => '-',

      'admin/news_headers' => 'News Headers',
      'admin/red_carpets' => 'Red Carpet Services',
      'admin/ad_settings' => 'Ad Settings',
      'admin/ad_banners' => 'Ad Banners',
      'admin/user' => 'Admin Users',
      'admin/privileges' => 'User Privileges',
    ),
    /*
      |--------------------------------------------------------------------------
      | You can inverse the color of the twitter bootstrap nav bar.
      | You jsut need to set to TRUE
      |--------------------------------------------------------------------------
     */
    'navigation_inverse' => true,
    /*
      |--------------------------------------------------------------------------
      | The name of the project that would be displayed as "brand" in the layout
      |--------------------------------------------------------------------------
     */
    'title' => 'One1Info - Admin panel',
    /*
      |--------------------------------------------------------------------------
      | The project base URL
      |--------------------------------------------------------------------------
     */
    'project_url' => '#',
    /*
      |--------------------------------------------------------------------------
      | Title of the application display in the browser
      |--------------------------------------------------------------------------
     */
    'title' => 'One1Info - Admin panel',
    /*
      |--------------------------------------------------------------------------
      | Enabled the default routing provided by the package
      |--------------------------------------------------------------------------
     */
    //'default_routing' => true,
    /*
      |--------------------------------------------------------------------------
      | If you want to use custom routing but you want to use the default package controller,
      | You must change the URI of the route in this configurations to redirect controller properly
      |--------------------------------------------------------------------------
     */
    'route' => array(
        'login' => 'admin/login',
        'user' => 'admin/user',
        'logout' => 'admin/logout'
    ),
    /*
      |--------------------------------------------------------------------------
      | The default number of items you wish to display per page. Currently use by the paginator
      |--------------------------------------------------------------------------
     */
    'paginate' => 10,
    /*
      |--------------------------------------------------------------------------
      | Assets you want to integrate in the layout
      |--------------------------------------------------------------------------
     */
    'assets' => array(
        'css' => array(
            '//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css'
        ),
        'js' => array(
            '//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js'
        )
    ),
    /*
      |--------------------------------------------------------------------------
      | ACL available resources list
      |--------------------------------------------------------------------------
     */
    'resources' => array( 'accounts', 'articles', 'categories', 'companies', 'company_interviews', 'company_reviews', 'complaints', 'ad_banners',
                          'countries', 'deal_sections', 'ideas', 'industries', 'jobs', 'members', 'services', 'pages', 'ad_settings', 'user',
                          'patents', 'press_releases', 'patent_investments', 'videos', 'sections', 'membership_options',
                          'red_carpets', 'news_headers','transactions', 'video_sections', 'auto_emails', 'privileges'
    ),
    /*
      |--------------------------------------------------------------------------
      | ACL available roles list
      |
      | If you don't want to handle permissions in your application, use only the administrator roles.
      | Also, you can add custom roles for your application and custom resources.
      |
      |--------------------------------------------------------------------------
     */
    'roles' => getRolesSettings(),//$setting->get('roles'), //Transaction::roles()//eval('Privilege::rolesAsArray();')
);