<?php 

class StringHelper{
	public static function trimIt($str = '', $length = 10, $append = '...'){
		if($str == ''){
			return $str;
		}

		if(strlen(trim($str))>=$length){
			$str = substr($str, 0, ($length-3)).$append;
		}

		return $str;
	}

	public static function youtube($id = '', $width="400", $height = "300"){
		if($id == ''){
			return $id;
		}

		if(strstr($id, 'youtube.com/watch?v=')){
			$arr = explode('v=', $id);
			$id = $arr[1];
		}

		$str = '<iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';

		return $str;
	}

	public static function thumbYoutube($id = ''){
		if($id == ''){
			return $id;
		}

		if(strstr($id, 'youtube.com/watch?v=')){
			$arr = explode('v=', $id);
			$id = $arr[1];
		}

	    return 'http://img.youtube.com/vi/'.$id.'/0.jpg';
	}


}