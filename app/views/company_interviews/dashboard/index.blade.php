@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">
        @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')


      <?php if(!User::find(Auth::user()->id)->hasRole('premium-company')):?>
        <div class="s-content">
          <span class="highlight">Upgrade your account to premium to be able to add company interviews and videos.</span>
          <p><a href="/upgrade" class="btn btn-info">Upgrade</a></p>
        </div>
      <?php else:?>

        <h2>Add / Delete Media</h2>
        <p>You have added <?php echo $company_interviews->count();?> out of 5 media</p>

        <div class="s-content">
    <?php foreach ($company_interviews as $ii=>$company_interview):?>
      
            <div class="inline-holder">
              <img class="pull-left" style="padding-right:10px;" width="200" height="200" src="<?php echo StringHelper::thumbYoutube($company_interview->youtube_url); ?>"  />
              <p>{{ StringHelper::trimIt($company_interview->title, 60) }}</p>
            <a href="/profile/company_interviews/{{$company_interview->id}}/edit" class="margin-x s-link" >Edit</a>
            {{ Form::open(array('method' => 'DELETE', 'class'=>'inline-block', 'route' => array('profile.company_interviews.destroy', $company_interview->id))) }}
              <a href="javascript::void(0)" onclick="$(this).parents('form').submit();" class=" s-link red">Delete</a>
            {{ Form::close() }}

            </div>
      

    <?php endforeach;?>

@if($company_interviews->count()!=5)
  <p>
    <a href="/profile/company_interviews/create" class="btn">New company interview</a>
  </p>
@endif



      </div><?php endif;?>
  </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>


@stop