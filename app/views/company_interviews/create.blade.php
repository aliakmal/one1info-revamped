@extends('layouts.master')
@section('head')
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">
        @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')
@if ($errors->any())
<ul>
  {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</ul>
@endif

    {{ Form::open(array('route' => 'profile.company_interviews.store', 'enctype'=>"multipart/form-data")) }}
    <ul>
      <li>
        {{ Form::label('title', 'Title:') }}
        {{ Form::text('title', '', array('class'=>'input-xxlarge')) }}
      </li>
      <li>
          {{ Form::label('youtube_url', 'Youtube_url:') }}
          {{ Form::text('youtube_url') }}
      </li>

      <li>
          {{ Form::label('dated', 'Dated:') }}
          {{ Form::text('dated', '', array('class'=>'datebox')) }}
      </li>
      <li>
        {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
      </li>
    </ul>
    {{ Form::close() }}
  </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>


@stop
<script >
$(function(){
    CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});

  $('#link-add-image').click(function(){
    
    $('#image-upload-holder').append('<li>'+$('#image-holder-template').html()+'</li>');
    $(document).on('click', '.lnk-remove-image', function(){
      $(this).parents('li').first().remove();
    });
  });
});
</script>
  
@stop