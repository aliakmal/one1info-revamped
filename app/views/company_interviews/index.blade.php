@extends('layouts.scaffold')

@section('main')

<h1>All Company_interviews</h1>

<p>{{ link_to_route('admin.company_interviews.create', 'Add company interview', array(), array('class'=>'btn')) }} {{ link_to('admin/company_interviews/csv', 'Export CSV', array('class'=>'btn')) }}</p>

@if ($company_interviews->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Title</th>
				<th>Company_id</th>
				<th>Youtube_url</th>
				<th>Body</th>
				<th>Dated</th>
				<th>Is_published</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($company_interviews as $company_interview)
				<tr>
					<td>{{{ $company_interview->title }}}</td>
					<td>{{{ $company_interview->company_id }}}</td>
					<td>{{{ $company_interview->youtube_url }}}</td>
					<td>{{{ $company_interview->body }}}</td>
					<td>{{{ $company_interview->dated }}}</td>
					<td>{{{ $company_interview->is_published }}}</td>
                    <td>{{ link_to_route('admin.company_interviews.edit', 'Edit', array($company_interview->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.company_interviews.destroy', $company_interview->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no company_interviews
@endif

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'title'=>'Title', 'company_id'=>'Company ID', 'youtube_url' =>'Youtube url', 'dated'=>'Date', 'is_published'=> 'Published');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/company_interviews/json"
    } );
} );
</script>



@stop
