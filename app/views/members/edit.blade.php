@extends('layouts.scaffold')

@section('main')

<h1>Edit Member</h1>
{{ Form::model($member, array('method' => 'PATCH', 'route' => array('admin.members.update', $member->id))) }}
	<ul>
        <li>
            {{ Form::label('first_name', 'First_name:') }}
            {{ Form::text('first_name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Last_name:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('gender', 'Gender:') }}
            {{ Form::text('gender') }}
        </li>

        <li>
            {{ Form::label('mobile', 'Mobile:') }}
            {{ Form::text('mobile') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::textarea('address') }}
        </li>

        <li>
            {{ Form::label('state', 'State:') }}
            {{ Form::text('state') }}
        </li>

        <li>
            {{ Form::label('country', 'Country:') }}
            {{ Form::select('country_id', Country::lists('name','id')) }}
        </li>

        <li>
            {{ Form::label('dob', 'Dob:') }}
            {{ Form::text('dob') }}
        </li>

        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('profession', 'Profession:') }}
            {{ Form::text('profession') }}
        </li>

        <li>
            {{ Form::label('interests', 'Interests:') }}
            {{ Form::textarea('interests') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('members.show', 'Cancel', $member->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
