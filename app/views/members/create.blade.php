@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">

    <h1>Create Member</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'admin.members.store')) }}
	<ul>
        <li>
            {{ Form::label('first_name', 'First_name:') }}
            {{ Form::text('first_name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Last_name:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('gender', 'Gender:') }}
            {{ Form::text('gender') }}
        </li>

        <li>
            {{ Form::label('mobile', 'Mobile:') }}
            {{ Form::text('mobile') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::textarea('address') }}
        </li>

        <li>
            {{ Form::label('state', 'State:') }}
            {{ Form::text('state') }}
        </li>

        <li>
            {{ Form::label('country', 'Country:') }}
            {{ Form::select('country_id', Country::lists('name','id')) }}
        </li>

        <li>
            {{ Form::label('dob', 'Dob:') }}
            {{ Form::text('dob') }}
        </li>

        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('profession', 'Profession:') }}
            {{ Form::text('profession') }}
        </li>

        <li>
            {{ Form::label('interests', 'Interests:') }}
            {{ Form::textarea('interests') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


