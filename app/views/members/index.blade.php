@extends('layouts.scaffold')

@section('main')

<h1>All Members</h1>

<p>{{ link_to_route('admin.members.create', 'Add new member') }}</p>

@if ($members->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>First_name</th>
				<th>Last_name</th>
				<th>Email</th>
				<th>Gender</th>
				<th>Mobile</th>
				<th>Address</th>
				<th>State</th>
				<th>Country</th>
				<th>Dob</th>
				<th>Title</th>
				<th>Profession</th>
				<th>Interests</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($members as $member)
				<tr>
					<td>{{{ $member->first_name }}}</td>
					<td>{{{ $member->last_name }}}</td>
					<td>{{{ $member->email }}}</td>
					<td>{{{ $member->gender }}}</td>
					<td>{{{ $member->mobile }}}</td>
					<td>{{{ $member->address }}}</td>
					<td>{{{ $member->state }}}</td>
					<td>{{{ $member->country }}}</td>
					<td>{{{ $member->dob }}}</td>
					<td>{{{ $member->title }}}</td>
					<td>{{{ $member->profession }}}</td>
					<td>{{{ $member->interests }}}</td>
                    <td>{{ link_to_route('members.edit', 'Edit', array($member->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('members.destroy', $member->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>

    {{ $members->links() }}

@else
	There are no members
@endif

@stop
