@extends('layouts.master')

@section('main')
<div class="row" style="margin-bottom:0px; padding-bottom:0px;">
  <div class="col-md-12">

  	  			<div id="search-tab-holder">
  			<ul  class="tab-nav">
  				<li ref="by-keyword"><a>By Keyword</a></li><li ref="by-business"><a>By Business</a></li><li ref="by-address"><a>By Address</a></li>
  			</ul>
  			<div id="by-keyword" class="tab-content">

		    	{{ Form::open(array('url'=>'/companies', 'method'=>'get')) }}
		    	<table cellpadding="0" border="0" >
		    		<tr>
		    			<td><span class="label">Keyword:</span>{{ Form::text('search', Input::get('search'), array('style'=>'width:400px', 'placeholder'=>'Keyword / business / company / service / product')) }}&nbsp;
		    			<span class="label">Country:</span>{{ Form::select('country', (array(''=>'All Countries')+array(''=>'Select')+Country::lists('name', 'id')) , Input::get('country'), array('style'=>'width:130px')) }}
		    			&nbsp;

								{{ Form::submit('Find', array('class' => 'small-btn ', 'style'=>'display:inline-block;padding:5px;margin:0px; right:0px;')) }}

		    			</td>
		    		</tr>
	    		</table>

			    {{ Form::close() }}
			</div>
  			<div id="by-business" class="tab-content">
		    	{{ Form::open(array('url'=>'/companies', 'method'=>'get')) }}
		    	<table cellpadding="2" border="0" >
		    		<tr>
		    			<td colspan="3"><span class="label">Keyword:</span>{{ Form::text('search', Input::get('search'), array('style'=>'width:700px', 'placeholder'=>'Keyword / business / company / service / product')) }}</td>
	    			</tr>
	    			<tr>
	    				<td><span class="label">Category:</span>{{ Form::select('category', array(''=>'Select Business')+Category::lists('name', 'id'), Input::get('category'), array('id'=>'category', 'style'=>'width:240px')) }}</td>
		    			<td><span class="label">Country:</span>{{ Form::select('country', array(''=>'Select Country') + Country::lists('name', 'id'), Input::get('country'), array('id'=>'country', 'style'=>'width:240px')) }}</td>
		    			<td><div style="text-align:right;">
		    				{{ Form::hidden('sort', 'asc', array('id'=>'sort')) }}
					{{ Form::submit('Find', array('class' => 'small-btn ', 'style'=>'display:inline-block;padding:5px;margin:0px; right:0px;')) }}
				</div></td>
		    		</tr>
	    		</table>
	    		

			    {{ Form::close() }}
			</div>
  			<div id="by-address" class="tab-content">
		    	{{ Form::open(array('url'=>'/companies', 'method'=>'get')) }}
		    	<table cellpadding="2" border="0">
		    		<tr>
		    			<td >
		    				<span class="label">Keyword:</span><br/>{{ Form::text('search', Input::get('search'), array('style'=>'width:750px; margin-left:10px;', 'placeholder'=>'Keyword / business / company / service / product')) }}
		    				<p>
		    					<span class="label" style="padding-bottom:0px;">Location:</span>
		    				</p>
		    			</td>
	    			</tr>
	    			<tr>
	    				<td style="padding-left:10px;"><b>Address:</b>&nbsp;{{ Form::text('address', Input::get('address'), array('style'=>'width:150px')) }}
		    			<span style="display:inline-block;padding-left:10px;"><b>City:</b>&nbsp;{{ Form::text('city', Input::get('city'), array('style'=>'width:150px')) }}</span>
		    			<span style="display:inline-block;padding-left:10px;"><b>Country:</b>&nbsp;{{ Form::select('country', array(''=>'Select')+Country::lists('name', 'id'), Input::get('country'), array('style'=>'width:180px')) }}
		    				&nbsp;&nbsp;{{ Form::submit('Find', array('class' => 'small-btn ', 'style'=>'display:inline-block;padding:5px;margin:0px; right:0px;')) }}
						</span>
		    			</td>
		    		</tr>
	    		</table>
	    		<div style="text-align:right;padding-top:10px;">
					
				</div>

			    {{ Form::close() }}
			</div>
			

  		</div>
  </div>
</div>

<div class="row">
  <div class="col-md-8">
  	<div class="page">
		<div class="filter-box">
			<a href="/companies" class="blue-link">Business Directory ></a>
			@if(is_null($category))
				<a class="blue-link" href="/companies">All Businesses ></a>
			@endif
			@if(is_null($country))
				<a class="blue-link" href="/companies">All Countries ></a>
			@endif

			@if(!is_null($category))
				<a class="blue-link" href="/companies?category={{$category->id}}">{{$category->name}}</a>
			@endif
			@if(!is_null($country))
				<a class="blue-link" href="/companies?country={{$country->id}}">{{$country->name}}</a>
			@endif
		</div>

  		<div class="filter-box ">
  			<div class="inline-block">
  				<div><b><small>Refine By</small></b></div>
  				{{ Form::select('_category', array(''=>'Business') + Category::lists('name', 'id'), Input::get('category'), array('id'=>'_category', 'style'=>'width:200px')) }}
  			</div>
  			<div class="inline-block">
  				<div><b><small>Refine By</small></b></div>
  				{{ Form::select('_country', array(''=>'Country') + Country::lists('name', 'id'), Input::get('country'), array('id'=>'_country', 'style'=>'width:200px')) }}
  			</div>
  			<div class="inline-block float-right">
  				<div class="text-right"><b><small>Sort By</small></b></div>
  				{{ Form::select('_sort', array(''=>'Relevance', 'asc'=>'A - Z', 'desc'=>'Z - A'), Input::get('sort'), array('id'=>'_sort', 'style'=>'width:100px')) }}
  			</div>
		</div>
		@if ($companies->count())
			@foreach ($companies as $company)
				<div class="single-company">
					<h1>{{ $company->company_name }}</h1>
			        
						@if(strlen(trim($company->address . $company->city . $company->postal_code . $company->country_id)) > 0)
							<p>
								{{ $company->address }}
								{{ $company->city }}
								{{ $company->postal_code }}
								{{ !is_null($company->country()->first()) ? $company->country()->first()->name : '' }}
							</p>
						@else
							No more details for you
						@endif
					
				</div>
			@endforeach
		@else
			<p>
				There are no companies
			</p>
		@endif
		{{ $companies->links() }}
	



  </div>


  </div>
  <div class="col-md-4">
	@include('partials.box-companies-classifications')
	@include('partials.box-get-listed')
    <img src="/assets/images/tall-01.jpg" />

  </div>
</div>

<script type="text/javascript">
$(function(){
	$('#_category').change(function(){
		$('#category').val($(this).val());
		$('#country').val($('#_country').val());
		$('#sort').val($('#_sort').val());
		$('#country').parents('form').first().submit();
		

	})


	$('#_country').change(function(){
		$('#category').val($('#_category').val());
		$('#country').val($(this).val());
		$('#sort').val($('#_sort').val());
		$('#country').parents('form').first().submit();
	})

	$('#_sort').change(function(){
		$('#category').val($('#_category').val());
		$('#country').val($('#_country').val());
		$('#sort').val($(this).val());
		$('#country').parents('form').first().submit();
	})

	
	$('#search-tab-holder .tab-content').hide();
	$('#search-tab-holder .tab-content:first').show();
	$('#search-tab-holder ul > li:first').addClass('selected');
	$('#search-tab-holder ul > li').click(function(){
		$('#search-tab-holder .tab-content').hide();
		$('#search-tab-holder ul > li').removeClass('selected');
		$(this).addClass('selected');

		$('#search-tab-holder div.tab-content[id="'+$(this).attr('ref')+'"]').show();
	});

})
</script>
@stop
