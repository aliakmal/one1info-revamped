@extends('layouts.scaffold')

@section('main')

<h1>Show Company</h1>

<p>{{ link_to_route('admin.companies.index', 'Return to all companies') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Company_name</th>
				<th>Known_as</th>
				<th>Contact_person</th>
				<th>Email</th>
				<th>Website</th>
				<th>Phone</th>
				<th>Mobile</th>
				<th>Fax</th>
				<th>Country_id</th>
				<th>Origin_country_id</th>
				<th>Licence_type</th>
				<th>Established</th>
				<th>Num_employees</th>
				<th>Address</th>
				<th>City</th>
				<th>Postal_code</th>
				<th>Industry_id</th>
				<th>Products</th>
				<th>Services</th>
				<th>Profile</th>
				<th>Subscription</th>
				<th>Publish</th>
				<th>Order</th>
				<th>Location_x</th>
				<th>Location_y</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $company->company_name }}}</td>
					<td>{{{ $company->known_as }}}</td>
					<td>{{{ $company->contact_person }}}</td>
					<td>{{{ $company->email }}}</td>
					<td>{{{ $company->website }}}</td>
					<td>{{{ $company->phone }}}</td>
					<td>{{{ $company->mobile }}}</td>
					<td>{{{ $company->fax }}}</td>
					<td>{{{ $company->country_id }}}</td>
					<td>{{{ $company->origin_country_id }}}</td>
					<td>{{{ $company->licence_type }}}</td>
					<td>{{{ $company->established }}}</td>
					<td>{{{ $company->num_employees }}}</td>
					<td>{{{ $company->address }}}</td>
					<td>{{{ $company->city }}}</td>
					<td>{{{ $company->postal_code }}}</td>
					<td>{{{ $company->industry_id }}}</td>
					<td>{{{ $company->products }}}</td>
					<td>{{{ $company->services }}}</td>
					<td>{{{ $company->profile }}}</td>
					<td>{{{ $company->subscription }}}</td>
					<td>{{{ $company->publish }}}</td>
					<td>{{{ $company->order }}}</td>
					<td>{{{ $company->location_x }}}</td>
					<td>{{{ $company->location_y }}}</td>
                    <td>{{ link_to_route('companies.edit', 'Edit', array($company->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('companies.destroy', $company->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
