@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
  	<p>Your account has been created - however you need to activate it before logging in.</p>
  	<p>An email has been sent to your email address with a link to activate your account.</p>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop  	