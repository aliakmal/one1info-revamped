@extends('layouts.master')
@section('head')
    <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    
    
    {{ Form::open(array('route' => 'post-register-basic-member', 'class'=>'form', 'enctype'=>"multipart/form-data")) }}
      <div class="well">
        <h1>Create Account</h1>
        <div class="s-content">
        
      <ul>
        <li>
          <div class="label-span">{{ Form::label('title', 'Title') }}</div>
          {{ Form::select('title', array('Mr'=>'Mr', 'Mrs'=>'Mrs', 'Miss'=>'Miss', 'Dr'=>'Dr')) }}
          <span class="errors">{{ $errors->first('title') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('first_name', 'First Name') }}<span class="required">*</span></div>
          {{ Form::text('first_name', '', array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('first_name') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('last_name', 'Last Name') }}<span class="required">*</span></div>
          {{ Form::text('last_name', '', array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('last_name') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('dob', 'Date of Birth') }}<span class="required">*</span></div>
          {{ Form::text('dob', '', array('class'=>'input-large datebox')) }}
          <span class="errors">{{ $errors->first('dob') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('gender', 'Gender') }}</div>
          {{ Form::select('gender', array('male'=>'Male', 'female'=>'Female')) }}
          <span class="errors">{{ $errors->first('gender') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('phone', 'Phone') }}<span class="required">*</span></div>
          {{ Form::text('phone', '', array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('phone') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('address', 'Address') }}<span class="required">*</span></div>
          {{ Form::text('address', '', array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('address') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('city', 'City') }}</div>
          {{ Form::text('city', '', array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('city') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('state', 'State') }}<span class="required">*</span></div>
          {{ Form::text('state', '', array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('state') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('country', 'Country') }}<span class="required">*</span></div>
          {{ Form::select('country_id', (array('0'=>'Select')+ Country::lists('name', 'id')) ) }}
          <span class="errors">{{ $errors->first('country') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('occupation', 'Occupation') }}<span class="required">*</span></div>
          {{ 
            Form::select('occupation', array( 'professional'=>'Professional', 
                                                      'high school'=>'High School', 
                                                      'college'=>'College', 
                                                      'university'=>'University', 
                                                      'higher studies'=>'Higher Studies'))
           }}
          <span class="errors">{{ $errors->first('occupation') }}</span>           
        </li>
      </ul>
      <ul>
        <li>
          <div class="label-span">{{ Form::label('username', 'Username') }}<span class="required">*</span></div>
          {{ Form::text('username', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('username') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('password', 'Password') }}<span class="required">*</span></div>
          {{ Form::password('password', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('password') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('password_confirmation', 'Confirm Password') }}<span class="required">*</span></div>
          {{ Form::password('password_confirmation', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('password_confirmation') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('email', 'Email') }}</div>
          {{ Form::text('email', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('email') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('captcha', 'Enter the letters') }}</div>
          <div style="display:inline-block;">{{ HTML::image(Captcha::img(), 'Captcha image') }}<p>
          {{ Form::text('captcha') }}</p></div>
          <span class="errors">{{ $errors->first('captcha') }}</span>
        </li>
        <li>
          {{ Form::submit('Register', array('class' => 'btn btn-info', 'style'=>'width:120px;')) }}
        </li>
      </ul>
    </div>
</div>
    </form>

  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
$(function(){
  //$('.datebox').datepicker();
})
</script>
@stop