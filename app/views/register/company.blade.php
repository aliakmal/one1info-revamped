@extends('layouts.master')
@section('head')
    <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    {{ Form::open(array('enctype'=>"multipart/form-data")) }}

    <div class="well">

    <h2>Registered Members</h2>
    <div class="s-content">
      <ul>
        <li>
            <div class="label-span">{{ Form::label('company_name', 'Company name:') }}<span class="required">*</span></div>
            {{ Form::text('company_name', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('company_name') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('known_as', 'Doing business as:') }}</div>
            {{ Form::text('known_as', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('known_as') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('logo', 'Logo:') }}</div>
            {{ Form::file('logo') }}
        </li>
        <li>
          <div class="label-span">{{ Form::label('username', 'Username') }}<span class="required">*</span></div>
          {{ Form::text('username', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('username') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('password', 'Password') }}<span class="required">*</span></div>
          {{ Form::password('password', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('password') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('password_confirmation', 'Confirm Password') }}<span class="required">*</span></div>
          {{ Form::password('password_confirmation', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('password_confirmation') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('email', 'Email') }}<span class="required">*</span></div>
          {{ Form::text('email', '', array('class'=>'input-large')) }}
          <span class="errors">{{ $errors->first('email') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('contact_person', 'Contact Person:') }}</div>
            {{ Form::text('contact_person', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('contact_person') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('website', 'Website:') }}</div>
            {{ Form::text('website', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('website') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('phone', 'Phone:') }}<span class="required">*</span></div>
            {{ Form::text('phone', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('phone') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('fax', 'Fax:') }}</div>
            {{ Form::text('fax', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('fax') }}</span>
        </li>

        <li>
            <div class="label-span">{{ Form::label('licence_type', 'Licence Type:') }}</div>
            {{ Form::select('licence_type', array('None'=>'None',
                                                  'Chamber of commerce'=>'Chamber of commerce',
                                                  'Department of Economic Development'=>'Department of Economic Development',
                                                  'Free Zone Authority'=>'Free Zone Authority',
                                                  'Municipality'=>'Municipality',
                                                  'Others'=>'Others'), array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('licence_type') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('established', 'Established:') }}</div>
            {{ Form::text('established', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('established') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('num_employees', 'Number Employees:') }}</div>
            {{ Form::input('number', 'num_employees') }}
            <span class="errors">{{ $errors->first('num_employees') }}</span>            
        </li>

        <li>
            <div class="label-span">{{ Form::label('address', 'Address:') }}<span class="required">*</span></div>
            {{ Form::textarea('address', '', array('class'=>'input-large', 'style'=>'width:306px')) }}
            <span class="errors">{{ $errors->first('address') }}</span>
        </li>

        <li>
            <div class="label-span">{{ Form::label('city', 'City:') }}<span class="required">*</span></div>
            {{ Form::text('city', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('city') }}</span>

        </li>
        <li>
            <div class="label-span">{{ Form::label('country_id', 'Country:') }}<span class="required">*</span></div>
            {{ Form::select('country_id', (array('0'=>'Select')+ Country::lists('name', 'id')) ) }}
            <span class="errors">{{ $errors->first('country_id') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('postal_code', 'Postal Code:') }}</div>
            {{ Form::text('postal_code', '', array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('postal_code') }}</span>

        </li>
        <li>
            <div class="label-span">{{ Form::label('categories', 'Business Categories:') }}<span class="required">*</span></div>
            {{ Form::select('categories[]', Category::lists('name','id'), '', array('multiple')) }}
            <span class="errors">{{ $errors->first('categories') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('industry_id', 'Industry:') }}</div>
            {{ Form::select('industry_id', Industry::lists('name','id')) }}
            <span class="errors">{{ $errors->first('industry_id') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('products', 'Products:') }}</div>
            {{ Form::textarea('products', '', array('class'=>'input-large', 'style'=>'width:306px')) }}
            <span class="errors">{{ $errors->first('products') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('services', 'Services:') }}</div>
            {{ Form::textarea('services', '', array('class'=>'input-large', 'style'=>'width:306px')) }}
            <span class="errors">{{ $errors->first('services') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('profile', 'Profile:') }}</div>
            {{ Form::textarea('profile', '', array('class'=>'input-large', 'style'=>'width:306px')) }}
            <span class="errors">{{ $errors->first('profile') }}</span>

        </li>
        <li>
          <div class="label-span">{{ Form::label('captcha', 'Enter the letters') }}</div>
          <div style="display:inline-block;">{{ HTML::image(Captcha::img(), 'Captcha image') }}<p>
          {{ Form::text('captcha') }}</p></div>
          <span class="errors">{{ $errors->first('captcha') }}</span>
        </li>

        <li>
          {{ Form::submit('Register', array('class' => 'btn btn-info', 'style'=>'width:120px;')) }}
        </li>
      </ul>
</div>
    </form>
  
    </div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
$(function(){
  //$('.datebox').datepicker();
})
</script>
@stop