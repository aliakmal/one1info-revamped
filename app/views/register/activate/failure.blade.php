@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
  	<h3><span class="highlight">Error - account could not be activated.</span></h3>
  	<p>Your account cannot be activated. Please check the url to make sure it is correct or contact one1info support for assistance.</p>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop  	