@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
  	<h3>Congratulations.</h3>
  	<p>Your account has been activated. You can now login using the email and password you had set up earlier.</p>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop  	