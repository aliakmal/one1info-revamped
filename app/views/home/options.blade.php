@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-12">
	<div class="page">
		<div class="well col-md-5 " style="margin:10px 40px 30px 30px;">
			<h1>Registered members</h1>
			<div class="content">
				<i>
				    Member in this category will have the rights to POST, and/or ACCESS the following about themselves and other members:
				</i>
			    <ul>
					<li>- Name of company</li>
					<li>- Address</li>
					<li>- Company phone number</li>
					<li>- Website</li>
					<li>- Contact person</li>
					<li>- Industry type</li>
					<li>- Type of Business (Classification)</li>
					<li>- License type</li>
					<li>- Years in Business</li>
					<li>- Number Of Employees</li>
					<li>- Location map</li>
					<li>- Brief company profile</li>
					<li>- Business activity</li>
					<li>- Media Blog (Video profile of the company)</li>
					<li>- Daily newsletter</li>
					<li>- Weekly newsletter</li>
					<li>- One1info-Discounts or Special Offerings</li>
					<li>- Job Site</li>
					<li>- 5 Free Business Consultancies</li>
					<li>- Directory Search Optimization</li>
					<li>- 2 * Free E-marketing Campaign</li>
					<li>- International Business Search</li>
					<li>- Magazine Advert</li>
				</ul>
				<p class="text-center">
			    <a href="/register/premium" class="btn btn-info" >REGISTER NOW</a>
			</p>
			</div>
		</div>
		<div class="well col-md-5 " style="margin:10px 50px 30px 30px;">
			<h1>Listed Member</h1>
			<div class="content">
			        <i>Member in this category will have the rights to POST, and/or ACCESS the following about themselves and other members:</i>
				<ul> 
					<li>- Name of Company</li>
					<li>- Address</li>
					<li>- Company phone number</li>
					<li>- Industry type</li>
					<li>- Type of Business</li>
					<li>- Years in Business</li>
					<li>- Daily newsletter</li>
					<li>- Access to Corporate Services</li>
				</ul>
				<div class="jumbo">
					FREE
				</div>
				<p class="text-center">
			    <a href="/register/basic" class="btn btn-info" >REGISTER NOW</a>
				</p>

			</div>
		</div>
	</div>
  </div>
</div>

@stop		