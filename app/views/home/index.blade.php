@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
    <div id="featured" >  
        <ul class="ui-tabs-nav">  
          <?php $flag = 0; ?>
          @foreach ($featured_articles as $article)
          <?php 
            if(count($article->photos) == 0)
              continue;
          ?>
            <li class="ui-tabs-nav-item" id="nav-fragment-{{$article->id}}"><a href="#fragment-{{$article->id}}"><span>{{ StringHelper::trimIt($article->title, 55) }}</span></a></li>  
              <?php $flag = 1;?>
            </li>
          @endforeach
        </ul>
        @foreach ($featured_articles as $article)
          <?php 
            if(count($article->photos) == 0)
              continue;
          ?>
          <div id="fragment-{{$article->id}}" class="ui-tabs-panel" style="">  
            <img src="<?php echo $article->photos->first()->getImageUrl('385','385'); ?>"  />
            <div class="info">
            <p>{{StringHelper::trimIt($article->caption, 245)}}<a class="blue-link" href="{{ url('articles', array('id'=>$article->id)) }}" >read more</a></p>  
            </div>
          </div>
        @endforeach
    </div>

    <?php echo '<div class="">';?>
    @foreach ($sections as $ii=>$section)
      <?php 
      if (($ii)%2 == 0):
        echo '</div>';
        echo '<div class="row clearfix">';
      endif;
      ?>
      <div class="niblet col-md-4">
        <h2>{{ link_to('articles/category/'.$section->name, $section->name, array('section'=>$section->id), $secure = null); }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles->take(4) as $article)
        <?php if ($flag == 0): ?>
        <?php if(count($article->photos) != 0):?>
        <a href="{{URL::to('articles', array($article->id))}}">
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl('120', '90'); ?>"  />
        </a>
      <?php endif;?>
          <h3 style="margin-bottom:3px;">{{ link_to_route('articles.show', StringHelper::trimIt($article->title, 70), array($article->id)) }}</h3>
            <p><?php // substr($article->caption, 0, 305)}}...?></p>
          
          <div class="clearfix" style="margin-bottom:5px;"></div>
          <?php $flag = 1; ?>
        <?php endif; ?>
        <div class="single-element"><span>■ </span>{{ link_to_route('articles.show', StringHelper::trimIt($article->title, 48), array($article->id)) }}</div>

        @endforeach
        <div class="single-element blue-link">{{ link_to('articles/category/'.$section->name, 'view more', array('section'=>$section->id, 'class'=>'blue-link'), $secure = null); }}</div>

      </div>
    @endforeach
    {{ '</div>'}}

    <div class="row">
      <div class="col-md-8">

    <h2>{{ link_to('press_releases', 'Press Releases'); }}</h2>
    @foreach ($press_releases as $ii=>$press_release)
      <a href="{{URL::to('press_releases', array($press_release->id))}}">
            <div class="inline-holder">
              <?php if(count( $press_release->photos)>0):?>
                <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $press_release->photos->first()->getImageUrl('385', '385'); ?>"  />
              <?php elseif (strlen($press_release->youtube_url)>0): ?>
                <img class="pull-left" style="padding-right:10px;" width="200" height="200" src="<?php echo StringHelper::thumbYoutube($press_release->youtube_url); ?>"  />
              <?php endif;?>

              <p>{{ StringHelper::trimIt($press_release->title, 60) }}</p>
            </div>
        </a>

    @endforeach
    </div>
  </div>



    <div class="row">
      <div class="col-md-8">

    <h2>{{ link_to('ideas', 'New Ideas'); }}</h2>
    @foreach ($ideas as $ii=>$idea)
      <a href="{{URL::to('ideas', array($idea->id))}}">
            <div class="inline-holder">
              <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $idea->photos->first()->getImageUrl('385', '385'); ?>"  />
              <p>{{ StringHelper::trimIt($idea->title, 60) }}</p>
            </div>
        </a>

    @endforeach
    </div>
  </div>

    <div class="row">
      <div class="col-md-8">
        <h2>{{ link_to('videos/', 'Videos'); }}</h2>
         @foreach($videos->take(8) as $video)
         <a href="{{URL::to('videos', array($video->id))}}">
            <div class="video-holder">
              <div style="position:absolute; top:25px;left:40px;"><img src="/assets/images/play-video.png" /></div>

              <img class="pull-left" style="padding-right:10px;" src="<?php echo $video->getThumbnailUrl(); ?>" height="110" width="145" />
              <p style="margin-bottom:3px;">{{ StringHelper::trimIt($video->title, 50) }}</p>
            </div>
          </a>
        @endforeach
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
      <img src="/assets/images/box-1.jpg" />
      <img src="/assets/images/box-2.jpg" />
    </div>
    </div>


  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    @include('partials.box-search-companies')
    <img src="/assets/images/tall-01.jpg" />
    @foreach ($sections->take(2) as $section)
    <div class="row side-niblets">
      <div class="col-md-12">
        <h4> Latest From {{ $section->name }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles as $article)
          <div class="clearfix" style=" <?php echo $flag==0?'':'border-top:1px solid #cccccc; padding-top:10px;';?>"></div>
          <a href="{{URL::to('articles', array($article->id))}}">
            <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl('70','50'); ?>"  />
          <div><b style="margin-bottom:3px;">{{ $article->title}}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          </a>
          <?php $flag = 1;?>
 
        @endforeach
          <div class="clearfix" style="margin-bottom:5px;"></div>
      </div>
    </div>
    @endforeach
  </div>
</div>
<!-- Jumbotron -->
<script>
  $(function() {
    $("#featured").tabs({hide:{effect: "slide"}}).tabs("rotate", 5000, true);
  })
</script>

@stop
