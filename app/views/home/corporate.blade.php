@extends('layouts.master')
@section('head')
    <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
@stop
@section('main')
<div class="row">
  <div class="col-md-12">
	<div class="page">
		<h1>Corporate Services</h1>
		

		<div id="va-accordion" class="va-container">
			@foreach($services as $vv)
			<h3 class="va-title">{{ $vv->name }}</h3>
			<div class="va-content">
				{{ $vv->body }}
			</div>
			@endforeach
		</div>		

		<p>&nbsp;</p>



	</div>
  </div>	

</div>
<script type="text/javascript">
	$(function() {
		$('#va-accordion').accordion({
				visibleSlices : 5,
				expandedHeight : 250,
				animOpacity : 0.1,
				contentAnimSpeed: 100
		});
	}); 
</script>
@stop