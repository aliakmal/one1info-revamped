@extends('layouts.master')
@section('main')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKcl8AF6bZbUKLNCE9P676PIphV_IliEc&sensor=false"></script>
<script type="text/javascript" src="http://www.map-generator.org/map/iframejs/75743e6b-a325-4a82-a2aa-991c2e9f242e?key=AIzaSyCKcl8AF6bZbUKLNCE9P676PIphV_IliEc&width=300px&height=400px"></script>
<div class="row">
  <div class="col-md-8">
	<div class="page">
		<h1>Contact Us</h1>
		 <blockquote>
		 	<p>Al Buraq Building,<br/>
			Floor 1, Office 12<br/>
			P.O. Box No.10044<br/>
			Ras Al Khaimah, UAE<br/><br/>

			Call-Free : 600-577775 [UAE]<br/><br/>

			Phone : +971 7 2282555<br/>
			Fax : +971 7 2260527<br/>
			Email : info@one1info.com</p>
		</blockquote>

	<h1>Email Us</h1>

	{{ Form::open(array('route' => 'feedback')) }}
	<ul>
        <li>
            {{ Form::label('Name', 'Name:') }}
            {{ Form::text('name', '', array('class'=>'input-xxlarge')) }}
            <span class="errors">{{ $errors->first('name') }}</span>
        </li>
        <li>
            {{ Form::label('Email', 'Email:') }}
            {{ Form::text('email', '', array('class'=>'input-xxlarge')) }}
            <span class="errors">{{ $errors->first('email') }}</span>
        </li>
        <li>
            {{ Form::label('Phone', 'Phone:') }}
            {{ Form::text('phone', '', array('class'=>'input-xxlarge')) }}
            <span class="errors">{{ $errors->first('phone') }}</span>
        </li>
        <li>
            {{ Form::label('country', 'Country:') }}
            {{ Form::select('country', Country::lists('name','name')) }}
        </li>
        <li>
            {{ Form::label('message', 'Message:') }}
            {{ Form::textarea('message', '', array('class'=>'input-xxlarge')) }}
            <span class="errors">{{ $errors->first('message') }}</span>
        </li>
    	<li>		
			{{ Form::submit('Send', array('class' => 'btn btn-info')) }}
    	</li>
	</ul>

	{{ Form::close() }}

	</div>
  </div>	

  <div class="col-md-4">
  <div class="well">
      <h2>Our Location</h2>
      <div class="content">
        	<div id ="mapid-75743e6b-a325-4a82-a2aa-991c2e9f242e"></div>
	<br />
	<a href="http://www.map-generator.org/75743e6b-a325-4a82-a2aa-991c2e9f242e/large-map.aspx">Enlarge Map</a>
</div>
</div>
    @include('partials.box-get-listed')
  </div>
</div>

@stop