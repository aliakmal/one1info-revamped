@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">
        @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')


<?php
  

 ?>

      <?php if(!User::find(Auth::user()->id)->hasRole('premium-company')):?>
        <div class="s-content">
          <span class="highlight">Upgrade your account to premium to be able to add location.</span>
          <p><a href="/upgrade" class="btn btn-info">Upgrade</a></p>
        </div>
      <?php else:?>

      {{ Form::model($company, array('method' => 'POST', 'action'=>'HomeController@postEditLocation', 'enctype'=>"multipart/form-data")) }}
        <h2>SET YOUR LOCATION ON THE MAP</h2>
          <div id="map"></div>
        <ul>
          <li>
            {{ Form::hidden('location_x') }}
            {{ Form::hidden('location_y') }}
          </li>
          <li>
            {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
          </li>
        </ul>

      </form>
    <?php endif;?>

  </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
  $(function(){
    var myOptions = {
      zoom: 6,
      center: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
      disableDefaultUI: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      draggableCursor:'pointer',
      draggingCursor: 'move'
  }

  var map = new google.maps.Map(document.getElementById("map"), myOptions);

  var marker;

  marker = new google.maps.Marker({
        position: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
        map: map
  });

  function placeMarker(location) {
    
    if ( marker ) {
      marker.setPosition(location);
    } else {
      marker = new google.maps.Marker({
        position: location,
        map: map
      });
    }
    
  }

  google.maps.event.addListener(map, 'click', function(event) {
    $('input[name="location_x"]').val(event.latLng.lat());
    $('input[name="location_y"]').val(event.latLng.lng());
    placeMarker(event.latLng);
  });
})
</script>
@stop