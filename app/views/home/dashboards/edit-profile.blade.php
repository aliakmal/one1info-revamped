@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
	<div class="page">
		<div class="well">
		@include('partials.profile-menu')
		
		<div class="s-content">
			<p>Copy and paste your profile in the area below</p>
			<div class="s-content">
				<h2>Company Profile</h2>
			    {{ Form::model($company, array('method' => 'POST', 'action'=>'HomeController@postUploadProfile', 'enctype'=>"multipart/form-data")) }}
			      <ul>
			        <li>
			          {{ Form::textarea('profile', $company->profile, array('class'=>'big-textarea')) }}
			        </li>
			        <li>
			          {{ Form::submit('Save Profile', array('class' => 'btn btn-info')) }}
			        </li>
			      </ul>

			    </form>

			</div>

		</div>
		</div>
	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>

@stop