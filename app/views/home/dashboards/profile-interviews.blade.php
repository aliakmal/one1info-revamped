@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">
        @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')

      <h2>Add/Delete Users</h2>
        You have added <?php count($company->company_interviews());?> out of 5 users
        <a href="/profile/interviews/create" class="btn">Add Interview</a>


        <div class="s-content">
        <h3>Add New Interview</h3>
        {{ Form::open(array('method' => 'POST', 'action'=>'HomeController@postInterviews', 'enctype'=>"multipart/form-data")) }}
          <ul>
            <li>
              <div class="label-span">{{ Form::label('title', 'Title') }}</div>
              {{ Form::text('title', '', array('class'=>'input-large')) }}
              <span class="errors">{{ $errors->first('title') }}</span>
            </li>
            <li>
              <div class="label-span">{{ Form::label('youtube_url', 'Youtube url') }}</div>
              {{ Form::text('youtube_url', '', array('class'=>'input-large')) }}
              <span class="errors">{{ $errors->first('youtube_url') }}</span>
            </li>
            <li>
              <div class="label-span">{{ Form::label('dated', 'Date') }}</div>
              {{ Form::text('dated', '', array('class'=>'input-large')) }}
              <span class="errors">{{ $errors->first('password') }}</span>
            </li>
            <li>
              {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
            </li>
          </ul>

        </form>
        </div>
  </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
  $(function(){
    var myOptions = {
      zoom: 6,
      center: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
      disableDefaultUI: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      draggableCursor:'pointer',
      draggingCursor: 'move'
  }

  var map = new google.maps.Map(document.getElementById("map"), myOptions);

  var marker;

  marker = new google.maps.Marker({
        position: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
        map: map
  });

  function placeMarker(location) {
    
    if ( marker ) {
      marker.setPosition(location);
    } else {
      marker = new google.maps.Marker({
        position: location,
        map: map
      });
    }
    
  }

  google.maps.event.addListener(map, 'click', function(event) {
    $('input[name="location_x"]').val(event.latLng.nb);
    $('input[name="location_y"]').val(event.latLng.ob);
    placeMarker(event.latLng);
  });
})
</script>
@stop