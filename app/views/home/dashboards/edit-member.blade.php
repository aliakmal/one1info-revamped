@extends('layouts.master')
@section('head')
    <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
@stop
@section('main')

<style type="text/css">
form > ul > li > select {
    display: inline-block;
    width: 340px;
}

</style>

<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="sublime">
    <h2>Update Profile</h2>
    <p>
      Update your profile below then click save.<br/>
      * Required fields
    </p>
    <h2>Account Details</h2>
    
      <div class="sublime">
      {{ Form::open(array('action'=>'HomeController@postEditProfile', 'enctype'=>"multipart/form-data")) }}  
      <ul>
        <li>
          <div class="label-span">{{ Form::label('username', 'Username') }}</div>
          <b>{{$member->user()->first()->username}}</b>
          <span class="errors">{{ $errors->first('username') }}</span>
        </li>
         <li>
            <a href="javascript:void(0)" id="btn-show-password" class="btn" onclick="$(this).parents('ul').first().append($('#password-container').html());$(this).parents('li').hide();">Change Password?</a>
        </li>
      </ul>
      <ul>
        <li>
          <div class="label-span">{{ Form::label('email', 'Email') }}</div>
          {{ Form::text('email', $member->user()->first()->email, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('email') }}</span>
        </li>
        
        <li>
          <div class="label-span">{{ Form::label('title', 'Title') }}</div>
          {{ Form::select('title', array('Mr'=>'Mr', 'Mrs'=>'Mrs', 'Miss'=>'Miss', 'Dr'=>'Dr')) }}
          <span class="errors">{{ $errors->first('title') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('first_name', 'First Name') }}<span class="required">*</span></div>
          {{ Form::text('first_name', $member->first_name, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('first_name') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('last_name', 'Last Name') }}<span class="required">*</span></div>
          {{ Form::text('last_name', $member->last_name, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('last_name') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('dob', 'Date of Birth') }}<span class="required">*</span></div>
          {{ Form::text('dob', $member->dob, array('class'=>'input-xxlarge datebox')) }}
          <span class="errors">{{ $errors->first('dob') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('gender', 'Gender') }}</div>
          {{ Form::select('gender', array('male'=>'Male', 'female'=>'Female')) }}
          <span class="errors">{{ $errors->first('gender') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('phone', 'Phone') }}<span class="required">*</span></div>
          {{ Form::text('phone', $member->phone, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('phone') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('address', 'Address') }}</div>
          {{ Form::text('address', $member->address, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('address') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('city', 'City') }}</div>
          {{ Form::text('city', $member->city, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('city') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('state', 'State') }}</div>
          {{ Form::text('state', $member->state, array('class'=>'input-xxlarge')) }}
          <span class="errors">{{ $errors->first('state') }}</span>
        </li>
        <li>
          <div class="label-span">{{ Form::label('country', 'Country') }}<span class="required">*</span></div>
          {{ Form::select('country_id', Country::lists('name', 'id'), $member->country_id) }}
          <span class="errors">{{ $errors->first('country') }}</span>
        </li>
        <li>
<div class="label-span">          {{ Form::label('occupation', 'Occupation') }}<span class="required">*</span></div>
          {{ 
            Form::select('occupation', array( 'professional'=>'Professional', 
                                                      'high school'=>'High School', 
                                                      'college'=>'College', 
                                                      'university'=>'University', 
                                                      'higher studies'=>'Higher Studies'))
           }}
          <span class="errors">{{ $errors->first('occupation') }}</span>           
        </li>
        <li>
          {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
        </li>
      </ul>
      
      
    </form>
    </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<div id="password-container" style="display:none;">
    <li>
        <div class="label-span">{{ Form::label('password', 'Password:') }}</div>
        {{ Form::password('password') }}
        <span class="errors errors-password">{{ $errors->first('password') }}</span>
    </li>
    <li>
        <div class="label-span">{{ Form::label('password_confirmation', 'Confirm Password:') }}</div>
        {{ Form::password('password_confirmation') }}
        <span class="errors">{{ $errors->first('password_confirmation') }}</span>
        <a href="javascript:void(0)" class="btn" onclick="li = $(this).parents('li'); $(li).prevAll('li').first().remove();$(li).prevAll('li').first().show();$(li).remove();" >Do Not Change Password</a>
    </li>
</div>
<script>
  $(function(){
    <?php if( strlen($errors->first('password').$errors->first('password_confirmation'))>0):?>
      $('#btn-show-password').parents('ul').first().append($('#password-container').html());$('#btn-show-password').parents('li').hide();
    <?php endif;?>
  });

</script>
@stop