@extends('layouts.master')
@section('head')
    <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">

            @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')

    {{ Form::model($company, array('method' => 'POST', 'action'=>'HomeController@postEditProfile', 'enctype'=>"multipart/form-data")) }}
      <h2>Account Details</h2>
      <div class="s-content">
      <ul>
        <li>
            <div class="label-span">{{ Form::label('company_name', 'Company name:') }}</div>
            {{ Form::text('company_name') }}
            <span class="errors">{{ $errors->first('company_name') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('known_as', 'Known s:') }}</div>
            {{ Form::text('known_as') }}
            <span class="errors">{{ $errors->first('known_as') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('contact_person', 'Contact Person:') }}</div>
            {{ Form::text('contact_person') }}
            <span class="errors">{{ $errors->first('contact_person') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('username', 'Username:') }}</div>
            {{ $company->users()->first()->username }}
        </li>
        <li>
            <a href="javascript:void(0)" class="btn" onclick="$(this).parents('ul').first().append($('#password-container').html());$(this).parents('li').hide();">Change Password?</a>
        </li>
      </ul>
      </div>
    <h2>Contact Details</h2>
    <div class="s-content">
    
    <ul>
        <li>
            <div class="label-span">{{ Form::label('email', 'Email:') }}</div>
            {{ Form::text('email', $company->users()->first()->email) }}
            <span class="errors">{{ $errors->first('email') }}</span>

        </li>
        <li>
            <div class="label-span">{{ Form::label('website', 'Website:') }}</div>
            {{ Form::text('website') }}
            <span class="errors">{{ $errors->first('website') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('phone', 'Phone:') }}</div>
            {{ Form::text('phone') }}
            <span class="errors">{{ $errors->first('phone') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('fax', 'Fax:') }}</div>
            {{ Form::text('fax') }}
            <span class="errors">{{ $errors->first('fax') }}</span>
        </li>
    </ul>
    </div>
    <h2>Company Address</h2>
    <div class="s-content">
    <ul>
        <li>
            <div class="label-span">{{ Form::label('country_id', 'Country:') }}</div>
            {{ Form::select('country_id', Country::lists('name','id')) }}
            <span class="errors">{{ $errors->first('country_id') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('city', 'City:') }}</div>
            {{ Form::text('city') }}
            <span class="errors">{{ $errors->first('city') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('address', 'Address:') }}</div>
            {{ Form::textarea('address') }}
            <span class="errors">{{ $errors->first('address') }}</span>
        </li>
        <li>
            <div class="label-span">{{ Form::label('postal_code', 'Postal_code:') }}</div>
            {{ Form::text('postal_code') }}
            <span class="errors">{{ $errors->first('postal_code') }}</span>
        </li>
    </ul>
    </div>
    <h2>Company Profile</h2>
    
    <div class="s-content">
    
    <ul>
        <li>
            <div class="label-span">{{ Form::label('categories', 'Categories:') }}</div>
            {{ Form::select('categories[]', Category::lists('name','id'), '', array('multiple')) }}
            <span class="errors">{{ $errors->first('categories') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('industry_id', 'Industry:') }}</div>
            {{ Form::select('industry_id', Industry::lists('name','id')) }}
            <span class="errors">{{ $errors->first('industry_id') }}</span>

        </li>
        <li>
            <div class="label-span">{{ Form::label('products', 'Products:') }}</div>
            {{ Form::textarea('products') }}
            <span class="errors">{{ $errors->first('products') }}</span>
        </li>

        <li>
            <div class="label-span">{{ Form::label('services', 'Services:') }}</div>
            {{ Form::textarea('services') }}
            <span class="errors">{{ $errors->first('services') }}</span>

        </li>
        <li>
            <div class="label-span">{{ Form::label('licence_type', 'Licence Type:') }}</div>
            {{ Form::select('licence_type', array('None'=>'None',
                                                  'Chamber of commerce'=>'Chamber of commerce',
                                                  'Department of Economic Development'=>'Department of Economic Development',
                                                  'Free Zone Authority'=>'Free Zone Authority',
                                                  'Municipality'=>'Municipality',
                                                  'Others'=>'Others'), array('class'=>'input-large')) }}
            <span class="errors">{{ $errors->first('licence_type') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('established', 'Established:') }}</div>
            {{ Form::text('established') }}
            <span class="errors">{{ $errors->first('established') }}</span>

        </li>

        <li>
            <div class="label-span">{{ Form::label('num_employees', 'Number Employees:') }}</div>
            {{ Form::input('number', 'num_employees') }}
            <span class="errors">{{ $errors->first('num_employees') }}</span>            
        </li>

        <li>
            <div class="label-span">{{ Form::label('logo', 'Logo:') }}</div>
            {{ Form::file('logo') }}
        </li>


        <li>
            <div class="label-span">{{ Form::label('profile', 'Profile:') }}</div>
            {{ Form::textarea('profile') }}
            <span class="errors">{{ $errors->first('profile') }}</span>
        </li>
        <li>
          {{ Form::submit('Save', array('class' => 'btn btn-info', 'style'=>'width:100px')) }}
        </li>
      </ul>
</div>

    </form>
</div>
</div>
<div id="password-container" style="display:none;">
    <li>
        <div class="label-span">{{ Form::label('password', 'Password:') }}</div>
        {{ Form::password('password') }}
        <span class="errors">{{ $errors->first('password') }}</span>
    </li>
    <li>
        <div class="label-span">{{ Form::label('password_confirmation', 'Confirm Password:') }}</div>
        {{ Form::password('password_confirmation') }}
        <span class="errors">{{ $errors->first('password_confirmation') }}</span>
        <a href="javascript:void(0)" class="btn" onclick="li = $(this).parents('li'); $(li).prevAll('li').first().remove();$(li).prevAll('li').first().show();$(li).remove();" >Do Not Change Password</a>
    </li>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
$(function(){
  //$('.datebox').datepicker();
})
</script>
@stop