@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
  
@stop
@section('main')
<div class="row">
  <div class="col-md-8">
	<div class="page">
		<div class="well">
		@include('partials.profile-menu')
		
		<div class="s-content">
			@include('partials.profile-mini')
			<div class="s-content">
				<h2>Company Profile</h2>
				
				@if($company->logo()->count() != 0)
					<div style="float:right; margin-top:20px;">{{Photo::image_tag($company->logo()->first())}}</div>
				@endif
				<h3>{{ $company->company_name}}</h3>
				<div>Known As {{ $company->known_as }}</div>
				<div>{{ $company->address }}</div>
				<div>{{ $company->city }}</div>
				<div>{{ $company->country->name }}</div>
				<?php
					$map = array( 'phone'=>'Phone',
								  'email'=>'Email',
								  'website'=>'Website');
				?>
				@foreach($map as $ii=>$vv)
					@if(strlen(trim($company->$ii))>0)
						<div>{{ $vv }}: {{ $company->$ii }}</div>
					@endif
				@endforeach
				
 				<div>Business Category(s):
  					<?php foreach($company->categories() as $one_category):?>
  						<?php echo $one_category->name;?>,
  					<?php endforeach;?>
				</div>
				<?php
					$map = array( 'products'=>'Products',
								  'services'=>'Services',
								  'established'=>'Established since',
								  'num_employees'=>'Number of Employees');
				?>
				@foreach($map as $ii=>$vv)
					@if(strlen(trim($company->$ii))>0)
						<div>{{ $vv }}: {{ $company->$ii }}</div>
					@endif
				@endforeach
			</div>

			<?php if(!User::find(Auth::user()->id)->hasRole('premium-company')):?>
				<div class="s-content">
					<span class="highlight">Upgrade your account to premium to be able to add location and add a profile</span>
					<p><a href="/upgrade" class="btn btn-info">Upgrade</a></p>
				</div>
			<?php else:?>
				<?php if(($company->location_x + $company->location_y)==0):?>
					<a href="/location/edit" class="btn">Update Location Map</a>
				<?php else:?>
					<div class="s-content">
						<h2>Location Map</h2>
						<div id="map"></div>
					</div>

					<?php if(strlen($company->profile)==0):?>
						<a href="/profile/upload-profile" class="btn">Upload Profile</a>
					<?php else:?>
						<div class="s-content">
							<h2>Company Profile</h2>
							 {{nl2br($company->profile)}}
						</div>

					<?php endif;?>
				<?php endif;?>
			<?php endif;?>
		</div>
		</div>
	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
  $(function(){
    var myOptions = {
      zoom: 6,
      center: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
      disableDefaultUI: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      } 
  }

  var map = new google.maps.Map(document.getElementById("map"), myOptions);

  var marker;

  marker = new google.maps.Marker({
        position: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
        map: map
  });


})
</script>
@stop