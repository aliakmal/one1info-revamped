@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">
        @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')

      <?php if(!User::find(Auth::user()->id)->hasRole('premium-company')):?>
        <div class="s-content">
          <span class="highlight">Upgrade your account to premium to be able to add users.</span>
          <p><a href="/upgrade" class="btn btn-info">Upgrade</a></p>
        </div>
      <?php else:?>
      <h2>Add/Delete Users</h2>
        <p>You have added <span class="highlight"><?php echo count($users);?></span> out of <span class="highlight">2</span> users</p>

        <table class="striped-table">
          <thead>
            <tr>
              <th>Email</th>
              <th>Username</th>
              <th colspan="2">User Level</th>
            </tr>
          </thead>

        <?php 



        foreach($users as $ii=>$one_user):?>
        <tr>
          <td>{{$one_user->email}}</td>
          <td>{{$one_user->username}}</td>
          <td>{{$one_user->level == 'user'? 'user':'admin'}}</td>
          <td>
                @if($one_user->level=='user')
                  @if(User::find(Auth::user()->id)->level!='user')
                    {{ Form::open(array('method' => 'DELETE', 'class'=>'inline-block', 'route' => array('profile.users.delete', $one_user->id))) }}
                          <a href="javascript::void(0)" onclick="$(this).parents('form').submit();" class=" s-link red">Delete</a>
                    {{ Form::close() }}
                  @endif  
                @endif
          </td>

        </tr>

      <?php endforeach;?>
        </table>
        <?php if(count($users)!=2):?>
          <div class="s-content">
          <h3>Add New User</h3>
          {{ Form::open(array('method' => 'POST', 'action'=>'HomeController@postUsers', 'enctype'=>"multipart/form-data")) }}
            <ul>
              <li>
                <div class="label-span">{{ Form::label('email', 'Email') }}</div>
                {{ Form::text('email', '', array('class'=>'input-large')) }}
                <span class="errors">{{ $errors->first('email') }}</span>
              </li>
              <li>
                <div class="label-span">{{ Form::label('username', 'Username') }}</div>
                {{ Form::text('username', '', array('class'=>'input-large')) }}
                <span class="errors">{{ $errors->first('username') }}</span>
              </li>
              <li>
                <div class="label-span">{{ Form::label('password', 'Password') }}</div>
                {{ Form::password('password', '', array('class'=>'input-large')) }}
                <span class="errors">{{ $errors->first('password') }}</span>
              </li>
              <li>
                {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
              </li>
            </ul>

          </form>
          </div>
        <?php endif;?>
      <?php endif;?>
  </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
<script type="text/javascript">
  $(function(){
    var myOptions = {
      zoom: 6,
      center: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
      disableDefaultUI: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      draggableCursor:'pointer',
      draggingCursor: 'move'
  }

  var map = new google.maps.Map(document.getElementById("map"), myOptions);

  var marker;

  marker = new google.maps.Marker({
        position: new google.maps.LatLng({{ $company->location_x }}, {{ $company->location_y }}),
        map: map
  });

  function placeMarker(location) {
    
    if ( marker ) {
      marker.setPosition(location);
    } else {
      marker = new google.maps.Marker({
        position: location,
        map: map
      });
    }
    
  }

  google.maps.event.addListener(map, 'click', function(event) {
    $('input[name="location_x"]').val(event.latLng.nb);
    $('input[name="location_y"]').val(event.latLng.ob);
    placeMarker(event.latLng);
  });
})
</script>
@stop