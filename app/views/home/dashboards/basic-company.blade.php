@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
	<div class="page">
		<h1>Welcome To One1Info</h1>
		<ul class="button-bar">
			<li>
				<a href="/profile/edit" onclick="alert('Work in Progress');">Your Jobs</a>
			</li>
			<li>
				<a href="/profile/edit">Edit Profile</a>
			</li>
			<li>
				<a href="/location/edit">Edit Location</a>
			</li>
			<li>
				<a href="/profile/password" onclick="alert('Work in Progress');">Change Password</a>
			</li>
			<li>
				<a href="/logout">Logout</a>
			</li>
		</ul>
	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>

@stop