@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-12">
	<div class="page">
		<div class="well col-md-5 " style="margin:10px 40px 30px 30px;">
			<h1>Company account</h1>
			<div class="content">
			    <ul>
					<li>- Appear on our business directory</li>
					<li>- Corporate services</li>
					<li>- Upload company profile and contact details</li>
					<li>- Weekly and daily newsletter</li>
					<li>- Ads banners</li>
					<li>- Post jobs</li>
					<li>- Post media, interview and press releases</li>
					<li>- Others</li>
				</ul>
				<p class="text-center">
			    	<a href="/options" class="btn btn-info" >REGISTER NOW</a>
				</p>
			</div>
		</div>

		<div class="well col-md-5 " style="margin:10px 50px 30px 30px;">
			<h1>Member Account</h1>
			<div class="content">
			    <ul>
					<li>- Benefit from our free services</li>
			    	<li>- Limited access to business directory</li>
			    	<li>- Search and apply for jobs</li>
			    	<li>- Recieve newsletter and updates</li>
			    	<li>- Others</li>
			    </ul>
			    <p class="text-center">
			    <a href="/register" class="btn btn-info" >REGISTER NOW</a>
			</p>

			</div>
		</div>
	</div>
  </div>
</div>

@stop		