@extends('layouts.master')
@section('main')
<style type="text/css">
	h3.title {
	    font-family: 'Open Sans Condensed',sans-serif;
	    font-size: 30px;
	    text-shadow: 0 0 1px #FFFFFF;
	    line-height: 35px;
	}

	blockquote.title {
	    font-family: 'Open Sans Condensed',sans-serif;
	    font-size: 50px;
	    text-shadow: 0 0 1px #FFFFFF;
	    color: #CCCCCC;
	    line-height: 50px;
	    text-align: center;
	}
</style>
<div class="row">
  <div class="col-md-8">
	<div class="page">
		<h1>About Us</h1>
		<p>One1info is a membership based organization that provides complete business information directory but more importantly has been proactively assisting the UAE small and medium businesses to gain access to actual economic opportunities and advancement.</p>
		<p>Through its activities, One1info continues to build a strong business relationship with its members. One1info makes every effort to develop and provide members with current and up to date information on businesses, events, opportunities and outreach programs with major corporations and public agencies in the UAE and across the globe.</p>
		<blockquote class="title">That is why One1info is truly the first and foremost point of call for all your business enquiries.....</blockquote>
		<h3 class="title">Why is One1info so DIFFERENT from the other listing directory for businesses in the United Arab Emirates?</h3>
		<p> Firstly, It provides a more comprehensive information listing than any other business directory, however, what makes One1info unique, is that it offers benefits and advantages by pro-actively marketing, promoting and seeking genuine business opportunities that translate in to profit for its members.</p>
		<h3 class="title">HOW DOES One1info HELP IT's MEMBERS?</h3>
		<p>One1info takes the time to consult with all its members, understand the nature of their business (no matter how big or small), the industry they're in and the underlying objectives for growth and profit. It also identifies the strengths and weaknesses in the members business and then refers them to its specialist team of experts and consultants from over 10 different sectors. The member gets to select 5 different experts, who then visit the member to discuss and implement a strategy to accomplish their goals effectively and efficiently. This service and all the other extensive services are completely free to all our members. </p>


	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
    @include('partials.box-search-companies')
    <img src="/assets/images/tall-01.jpg" />
    
  </div>
</div>

@stop