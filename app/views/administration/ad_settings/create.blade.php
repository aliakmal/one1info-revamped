@extends('layouts.scaffold')

@section('main')

<h1>Create Ad_setting</h1>

{{ Form::open(array('route' => 'ad_settings.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('duration', 'Duration:') }}
            {{ Form::input('number', 'duration') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


