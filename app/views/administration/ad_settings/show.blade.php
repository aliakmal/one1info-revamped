@extends('layouts.scaffold')

@section('main')

<h1>Show Ads setting</h1>

<p>{{ link_to_route('admin.ad_settings.index', 'Return to ad settings') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
				<th>Duration</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $ad_setting->name }}}</td>
					<td>{{{ $ad_setting->duration }}}</td>
                    <td>{{ link_to_route('admin.ad_settings.edit', 'Edit', array($ad_setting->id), array('class' => 'btn btn-info')) }}</td>
		</tr>
	</tbody>
</table>

@stop
