@extends('layouts.scaffold')

@section('main')

<h1>Ad Settings</h1>

@if ($ad_settings->count())
	<table class="table table-striped ">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Duration</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($ad_settings as $ad_setting)
				<tr>
					<td>{{{ $ad_setting->id }}}</td>
					<td>{{{ $ad_setting->name }}}</td>
					<td>{{{ $ad_setting->duration }}}</td>
                    <td>{{ link_to_route('admin.ad_settings.edit', 'Edit', array($ad_setting->id), array('class' => 'btn btn-info')) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no ad_settings
@endif

@stop
