@extends('layouts.scaffold')

@section('main')

<h1>Edit Ad_setting</h1>
{{ Form::model($ad_setting, array('method' => 'PATCH', 'route' => array('admin.ad_settings.update', $ad_setting->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('duration', 'Duration:') }}
            {{ Form::input('number', 'duration') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.ad_settings.show', 'Cancel', $ad_setting->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
