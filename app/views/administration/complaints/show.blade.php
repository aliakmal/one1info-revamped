@extends('layouts.scaffold')

@section('main')

<h1>Show Complaint</h1>

<p>{{ link_to_route('admin.complaints.index', 'Return to all complaints') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>First_name</th>
				<th>Last_name</th>
				<th>Gender</th>
				<th>Company</th>
				<th>Position</th>
				<th>Address</th>
				<th>City</th>
				<th>Country</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Phone</th>
				<th>Fax</th>
				<th>Complaint_against</th>
				<th>Compaint_type</th>
				<th>Product</th>
				<th>Invoice_no</th>
				<th>Amount_in_dispute</th>
				<th>Problem_date</th>
				<th>Complaint_title</th>
				<th>Complain_details</th>
				<th>Expected_resolution</th>
				<th>Dated</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{{ $complaint->first_name }}}</td>
					<td>{{{ $complaint->last_name }}}</td>
					<td>{{{ $complaint->gender }}}</td>
					<td>{{{ $complaint->company }}}</td>
					<td>{{{ $complaint->position }}}</td>
					<td>{{{ $complaint->address }}}</td>
					<td>{{{ $complaint->city }}}</td>
					<td>{{{ $complaint->country }}}</td>
					<td>{{{ $complaint->email }}}</td>
					<td>{{{ $complaint->mobile }}}</td>
					<td>{{{ $complaint->phone }}}</td>
					<td>{{{ $complaint->fax }}}</td>
					<td>{{{ $complaint->complaint_against }}}</td>
					<td>{{{ $complaint->compaint_type }}}</td>
					<td>{{{ $complaint->product }}}</td>
					<td>{{{ $complaint->invoice_no }}}</td>
					<td>{{{ $complaint->amount_in_dispute }}}</td>
					<td>{{{ $complaint->problem_date }}}</td>
					<td>{{{ $complaint->complaint_title }}}</td>
					<td>{{{ $complaint->complain_details }}}</td>
					<td>{{{ $complaint->expected_resolution }}}</td>
					<td>{{{ $complaint->dated }}}</td>
                    <td>{{ link_to_route('admin.complaints.edit', 'Edit', array($complaint->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.complaints.destroy', $complaint->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
