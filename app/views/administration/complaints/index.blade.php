@extends('layouts.scaffold')

@section('main')

<h1>All Complaints</h1>

<p>{{ link_to_route('admin.complaints.create', 'Add complaint', array(), array('class'=>'btn')) }} {{ link_to('admin/complaints/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('first_name'=>'First_name',	'last_name'=>'Last_name',	'gender'=>'Gender',	'company'=>'Company',	'position'=>'Position',	'address'=>'Address',	'city'=>'City',	'country'=>'Country',	'email'=>'Email',	'mobile'=>'Mobile',	'phone'=>'Phone',	'fax'=>'Fax',	'complaint_against'=>'Complaint_against',	'compaint_type'=>'Compaint_type',	'product'=>'Product',	'invoice_no'=>'Invoice_no',	'amount_in_dispute'=>'Amount_in_dispute',	'problem_date'=>'Problem_date',	'complaint_title'=>'Complaint_title',	'complain_details'=>'Complain_details',	'expected_resolution'=>'Expected_resolution',	'dated'=>'Dated');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/complaints/json"
    } );
} );
</script>
@stop
