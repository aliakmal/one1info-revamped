@extends('layouts.scaffold')

@section('main')

<h1>Edit Complaint</h1>
{{ Form::model($complaint, array('method' => 'PATCH', 'route' => array('admin.complaints.update', $complaint->id))) }}
	<ul>
        <li>
            {{ Form::label('first_name', 'First_name:') }}
            {{ Form::text('first_name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Last_name:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('gender', 'Gender:') }}
            {{ Form::text('gender') }}
        </li>

        <li>
            {{ Form::label('company', 'Company:') }}
            {{ Form::text('company') }}
        </li>

        <li>
            {{ Form::label('position', 'Position:') }}
            {{ Form::text('position') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::text('address') }}
        </li>

        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('country', 'Country:') }}
            {{ Form::select('country', Country::lists('name','name')) }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('mobile', 'Mobile:') }}
            {{ Form::text('mobile') }}
        </li>

        <li>
            {{ Form::label('phone', 'Phone:') }}
            {{ Form::text('phone') }}
        </li>

        <li>
            {{ Form::label('fax', 'Fax:') }}
            {{ Form::text('fax') }}
        </li>

        <li>
            {{ Form::label('complaint_against', 'Complaint_against:') }}
            {{ Form::select('complaint_against', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('compaint_type', 'Compaint_type:') }}
            {{ Form::text('compaint_type') }}
        </li>

        <li>
            {{ Form::label('product', 'Product:') }}
            {{ Form::text('product') }}
        </li>

        <li>
            {{ Form::label('invoice_no', 'Invoice_no:') }}
            {{ Form::text('invoice_no') }}
        </li>

        <li>
            {{ Form::label('amount_in_dispute', 'Amount_in_dispute:') }}
            {{ Form::text('amount_in_dispute') }}
        </li>

        <li>
            {{ Form::label('problem_date', 'Problem_date:') }}
            {{ Form::text('problem_date') }}
        </li>

        <li>
            {{ Form::label('complaint_title', 'Complaint_title:') }}
            {{ Form::text('complaint_title') }}
        </li>

        <li>
            {{ Form::label('complain_details', 'Complain_details:') }}
            {{ Form::textarea('complain_details') }}
        </li>

        <li>
            {{ Form::label('expected_resolution', 'Expected_resolution:') }}
            {{ Form::textarea('expected_resolution') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.complaints.show', 'Cancel', $complaint->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
