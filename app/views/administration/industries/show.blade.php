@extends('layouts.scaffold')

@section('main')

<h1>Show Industry</h1>

<p>{{ link_to_route('industries.index', 'Return to all industries') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $industry->name }}}</td>
                    <td>{{ link_to_route('industries.edit', 'Edit', array($industry->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('industries.destroy', $industry->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
