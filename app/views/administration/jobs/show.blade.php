@extends('layouts.scaffold')

@section('main')

<h1>Show Job</h1>

<p>{{ link_to_route('jobs.index', 'Return to all jobs') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Company_id</th>
				<th>Address</th>
				<th>City</th>
				<th>State</th>
				<th>Country_id</th>
				<th>Title</th>
				<th>Category</th>
				<th>Experience</th>
				<th>Education</th>
				<th>Commitment</th>
				<th>Compensation</th>
				<th>Details</th>
				<th>Show_company</th>
				<th>Published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $job->company_id }}}</td>
					<td>{{{ $job->address }}}</td>
					<td>{{{ $job->city }}}</td>
					<td>{{{ $job->state }}}</td>
					<td>{{{ $job->country_id }}}</td>
					<td>{{{ $job->title }}}</td>
					<td>{{{ $job->category }}}</td>
					<td>{{{ $job->experience }}}</td>
					<td>{{{ $job->education }}}</td>
					<td>{{{ $job->commitment }}}</td>
					<td>{{{ $job->compensation }}}</td>
					<td>{{{ $job->details }}}</td>
					<td>{{{ $job->show_company }}}</td>
					<td>{{{ $job->published }}}</td>
                    <td>{{ link_to_route('jobs.edit', 'Edit', array($job->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('jobs.destroy', $job->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
