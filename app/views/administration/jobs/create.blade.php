@extends('layouts.scaffold')

@section('main')

<h1>Create Job</h1>

{{ Form::open(array('route' => 'jobs.store')) }}
	<ul>
        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::select('company_id', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::text('address') }}
        </li>

        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('state', 'State:') }}
            {{ Form::text('state') }}
        </li>

        <li>
            {{ Form::label('country_id', 'Country_id:') }}
            {{ Form::select('country_id', Country::lists('name','id')) }}
        </li>

        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('category', 'Category:') }}
            {{ Form::text('category') }}
        </li>

        <li>
            {{ Form::label('experience', 'Experience:') }}
            {{ Form::text('experience') }}
        </li>

        <li>
            {{ Form::label('education', 'Education:') }}
            {{ Form::text('education') }}
        </li>

        <li>
            {{ Form::label('commitment', 'Commitment:') }}
            {{ Form::text('commitment') }}
        </li>

        <li>
            {{ Form::label('compensation', 'Compensation:') }}
            {{ Form::text('compensation') }}
        </li>

        <li>
            {{ Form::label('details', 'Details:') }}
            {{ Form::textarea('details') }}
        </li>

        <li>
            {{ Form::label('show_company', 'Show_company:') }}
            {{ Form::select('published', array('0'=>'No', '1'=>'Yes' )) }}
        </li>
        <li>
            {{ Form::label('published', 'Published:') }}
            {{ Form::select('published', array('0'=>'No', '1'=>'Yes' )) }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


