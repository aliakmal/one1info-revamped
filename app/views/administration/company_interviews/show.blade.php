@extends('layouts.scaffold')

@section('main')

<h1>Show Company_interview</h1>

<p>{{ link_to_route('admin.company_interviews.index', 'Return to all company_interviews') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Title</th>
				<th>Company_id</th>
				<th>Youtube_url</th>
				<th>Body</th>
				<th>Dated</th>
				<th>Is_published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $company_interview->title }}}</td>
			<td>{{{ $company_interview->company_id }}}</td>
			<td>{{{ $company_interview->youtube_url }}}</td>
			<td>{{{ $company_interview->body }}}</td>
			<td>{{{ $company_interview->dated }}}</td>
			<td>{{{ $company_interview->is_published }}}</td>
            <td>{{ link_to_route('admin.company_interviews.edit', 'Edit', array($company_interview->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.company_interviews.destroy', $company_interview->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
