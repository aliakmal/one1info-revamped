@extends('layouts.scaffold')

@section('main')

<h1>Create Company_interview</h1>

{{ Form::open(array('route' => 'admin.company_interviews.store')) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::select('company_id', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('youtube_url', 'Youtube_url:') }}
            {{ Form::text('youtube_url') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


