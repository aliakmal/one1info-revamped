@extends('layouts.scaffold')

@section('main')

<h1>Edit Ad banner</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($ad_banner, array('method' => 'PATCH',  'class'=>"form-horizontal",  'enctype'=>"multipart/form-data", 'route' => array('admin.ad_banners.update', $ad_banner->id))) }}
<div class="control-group">
    {{ Form::label('image', 'Banner Image:') }}
    <div class="controls">
        {{ Form::file('image') }}
        @if($ad_banner->photo()->count() > 0):
            <div class="row">
                <div class="span3">
                    <div class="well ">
                        {{ Photo::image_tag( $ad_banner->photo()->first() ) }}<br/>
                        <small>Select an image to replace current image, else leave empty to keep current image.</small>
                    </div>
                </div>
            </div>
        @else:
            <i> No Image </i>
        @endif
    </div>
</div>
<div class="control-group">
    {{ Form::label('page', 'Show on Page(s):') }}
    <div class="controls">
        <label class="checkbox">
            <?php echo Form::checkbox('page[all_pages]', '1', (isset($ad_banner['page']['all_pages'])), array('class'=>'togglr'));?> All Pages
        </label>
        <?php $pages = array( 'about-us','articles','business-directory', 'career-center','companies-page', 'contact-us', 
                            'corporate-services','hot-deals','home','ideas-and-concepts','match-making', 'news-sections',
                            'patents', 'press-releases', 'red-carpet', 'video-gallery' );?>
        @foreach($pages as $page)
            <label class="checkbox" style="">
                <?php echo Form::checkbox('page['.$page.']', '1', (isset($ad_banner['page'][$page])), array('class'=>'toggl-check'));?> {{ ucwords(str_replace('-', ' ', $page)) }}
            </label>
        @endforeach
    </div>
</div>
<div class="control-group">
    {{ Form::label('location_id', 'Location:') }}
    <div class="controls">
        {{ Form::select('location_id', Ad_setting::lists('name', 'id')) }}
    </div>
</div>
<div class="control-group">
    {{ Form::label('link', 'Link:') }}
    <div class="controls">{{ Form::text('link') }}</div>
</div>
<div class="control-group">
    {{ Form::label('is_published', 'Published:') }}
    <div class="controls">{{ Form::select('is_published', array('0'=>'No', '1'=>'Yes' )) }}</div>
</div>

{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
{{ link_to_route('admin.ad_banners.show', 'Cancel', $ad_banner->id, array('class' => 'btn')) }}

{{ Form::close() }}


<script type="text/javascript">
$(function(){
    $('.control-group > label:first-child').addClass("control-label");
    $('.togglr').change(function(){

        if($(this).is(':checked')){
            $('.toggl-check').attr('checked', false);
            $('.toggl-check').attr('disabled', true);
        }else{
            $('.toggl-check').attr('disabled', false);
        }
    })
});
</script>
@stop
