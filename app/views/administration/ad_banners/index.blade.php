@extends('layouts.scaffold')

@section('main')

<h1>All Ad Banners</h1>

<p>{{ link_to_route('admin.ad_banners.create', 'Add new ad_banner') }}</p>

<table class="table table-striped "  id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Page</th>
			<th>Location</th>
			<th>Banner</th>
			<th>Link</th>
			<th>Published</th>
			<th></th>
		</tr>
	</thead>

	<td colspan="7" class="dataTables_empty">Loading data from server</td>
</table>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/ad_banners/json"
    } );
} );
</script>


@stop
