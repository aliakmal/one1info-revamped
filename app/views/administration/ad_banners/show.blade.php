@extends('layouts.scaffold')

@section('main')

<h1>Show Ad banner</h1>

<p>{{ link_to_route('admin.ad_banners.index', 'Return to all ad banners') }}

{{ Form::open(array('method' => 'DELETE', 'route' => array('admin.ad_banners.destroy', $ad_banner->id))) }}
<div class="btn-group">
	<a href="{{ URL::route( 'admin.ad_banners.edit', array( $ad_banner->id )) }}" class="btn ">Edit</a>
	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn ">Delete</a>
</div>
{{ Form::close() }}

</p>

<table class="table table-striped ">
	
		<tr>
			<th>ID</th><td>{{{ $ad_banner->id }}}</td>
		</tr><tr>
			<th>Pages</th>
			<td>
				@foreach(unserialize($ad_banner->page) as $pname => $pvalue)
					<span style="display:inline-block;" class="label label-info">{{ ucwords(str_replace(array('-', '_'), ' ', $pname)) }}</span>
				@endforeach
			</td>
		</tr><tr>
			<th>Banner</th><td>{{ Photo::image_tag( $ad_banner->photo()->first() ) }}</td>
		</tr><tr>
			<th>Location</th><td>{{{ $ad_banner->Location()->first()->name }}}</td>
		</tr><tr>
			<th>Link</th><td>{{{ $ad_banner->link }}}</td>
		</tr>
		<tr>
			<th>Is published</th>
			<td>{{ $ad_banner->is_published=="0"? '<span class="badge badge-important">NOT PUBLISHED</span>' : '<span class="badge badge-success">PUBLISHED</span>' }}</td>
		</tr>
</table>

@stop
