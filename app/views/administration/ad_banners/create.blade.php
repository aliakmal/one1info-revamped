@extends('layouts.scaffold')

@section('main')

<h1>Create Ad banner</h1>

{{ Form::open(array('route' => 'admin.ad_banners.store',  'class'=>"form-horizontal",  'enctype'=>"multipart/form-data")) }}
	
        <div class="control-group">
            {{ Form::label('image', 'Banner Image:') }}
            <div class="controls">
                {{ Form::file('image') }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('page', 'Show on Page(s):') }}
              <div class="controls">
                  <label class="checkbox">
                    <?php echo Form::checkbox('page[all_pages]', '1', '', array('class'=>'togglr'));?> All Pages
                  </label>
                  <?php $pages = array( 'about-us','articles','business-directory', 'career-center','companies-page', 'contact-us', 
                                        'corporate-services','hot-deals','home','ideas-and-concepts','match-making', 'news-sections',
                                        'patents', 'press-releases', 'red-carpet', 'video-gallery' );?>
                   @foreach($pages as $page)
                    <label class="checkbox" style="">
                        <?php echo Form::checkbox('page['.$page.']', '1', '', array('class'=>'toggl-check'));?> {{ ucwords(str_replace('-', ' ', $page)) }}
                    </label>
                   @endforeach
               </div>
        </div>
        <div class="control-group">
            {{ Form::label('location_id', 'Location:') }}
            <div class="controls">
                {{ Form::select('location_id', Ad_setting::lists('name', 'id')) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('link', 'Link:') }}
            <div class="controls">{{ Form::text('link') }}</div>
        </div>
        <div class="control-group">
            {{ Form::label('is_published', 'Published:') }}
            <div class="controls">{{ Form::select('is_published', array('0'=>'No', '1'=>'Yes' )) }}</div>
        </div>

			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<script type="text/javascript">
$(function(){
    $('.control-group > label:first-child').addClass("control-label");
    $('.togglr').change(function(){

        if($(this).is(':checked')){
            $('.toggl-check').attr('checked', false);
            $('.toggl-check').attr('disabled', true);
        }else{
            $('.toggl-check').attr('disabled', false);
        }
    })
});
</script>
@stop


