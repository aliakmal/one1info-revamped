@extends('layouts.scaffold')

@section('main')

<h1>Show Patent</h1>

<p>{{ link_to_route('admin.patents.index', 'Return to all patents') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>First_name</th>
				<th>Last_name</th>
				<th>Address</th>
				<th>City</th>
				<th>Country</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Fax</th>
				<th>Inv_name</th>
				<th>Inv_field</th>
				<th>Inv_details</th>
				<th>Has_patent</th>
				<th>Need_register</th>
				<th>Patent_area</th>
				<th>Need_publish</th>
				<th>Need_investor</th>
				<th>Dated</th>
				<th>Is_published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $patent->first_name }}}</td>
					<td>{{{ $patent->last_name }}}</td>
					<td>{{{ $patent->address }}}</td>
					<td>{{{ $patent->city }}}</td>
					<td>{{{ $patent->country }}}</td>
					<td>{{{ $patent->email }}}</td>
					<td>{{{ $patent->phone }}}</td>
					<td>{{{ $patent->fax }}}</td>
					<td>{{{ $patent->inv_name }}}</td>
					<td>{{{ $patent->inv_field }}}</td>
					<td>{{{ $patent->inv_details }}}</td>
					<td>{{{ $patent->has_patent }}}</td>
					<td>{{{ $patent->need_register }}}</td>
					<td>{{{ $patent->patent_area }}}</td>
					<td>{{{ $patent->need_publish }}}</td>
					<td>{{{ $patent->need_investor }}}</td>
					<td>{{{ $patent->dated }}}</td>
					<td>{{{ $patent->is_published }}}</td>
                    <td>{{ link_to_route('admin.patents.edit', 'Edit', array($patent->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.patents.destroy', $patent->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
