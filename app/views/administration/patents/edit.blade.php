@extends('layouts.scaffold')

@section('main')

<h1>Edit Patent</h1>
{{ Form::model($patent, array('method' => 'PATCH', 'route' => array('admin.patents.update', $patent->id))) }}
	<ul>
        <li>
            {{ Form::label('first_name', 'First_name:') }}
            {{ Form::text('first_name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Last_name:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::text('address') }}
        </li>

        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('country', 'Country:') }}
            {{ Form::select('country', Country::lists('name','name')) }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('phone', 'Phone:') }}
            {{ Form::text('phone') }}
        </li>

        <li>
            {{ Form::label('fax', 'Fax:') }}
            {{ Form::text('fax') }}
        </li>

        <li>
            {{ Form::label('inv_name', 'Inv_name:') }}
            {{ Form::text('inv_name') }}
        </li>

        <li>
            {{ Form::label('inv_field', 'Inv_field:') }}
            {{ Form::text('inv_field') }}
        </li>

        <li>
            {{ Form::label('inv_details', 'Inv_details:') }}
            {{ Form::textarea('inv_details') }}
        </li>

        <li>
            {{ Form::label('has_patent', 'Has_patent:') }}
            {{ Form::text('has_patent') }}
        </li>

        <li>
            {{ Form::label('need_register', 'Need_register:') }}
            {{ Form::text('need_register') }}
        </li>

        <li>
            {{ Form::label('patent_area', 'Patent_area:') }}
            {{ Form::text('patent_area') }}
        </li>

        <li>
            {{ Form::label('need_publish', 'Need_publish:') }}
            {{ Form::text('need_publish') }}
        </li>

        <li>
            {{ Form::label('need_investor', 'Need_investor:') }}
            {{ Form::text('need_investor') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.patents.show', 'Cancel', $patent->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
