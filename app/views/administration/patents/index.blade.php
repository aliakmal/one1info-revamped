@extends('layouts.scaffold')

@section('main')

<h1>All Patents</h1>

<p>{{ link_to_route('admin.patents.create', 'Add patents', array(), array('class'=>'btn')) }} {{ link_to('admin/patents/csv', 'Export CSV', array('class'=>'btn')) }}</p>


<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID','first_name'=>'First_name','last_name'=>'Last_name','address'=>'Address','city'=>'City','country'=>'Country','email'=>'Email','phone'=>'Phone','fax'=>'Fax','inv_name'=>'Inv_name','inv_field'=>'Inv_field','inv_details'=>'Inv_details','has_patent'=>'Has_patent','need_register'=>'Need_register','patent_area'=>'Patent_area','need_publish'=>'Need_publish','need_investor'=>'Need_investor','dated'=>'Dated','is_published'=>'Is_published');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/patents/json"
    } );
} );
</script>
@stop
