@extends('layouts.scaffold')

@section('main')
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>
<div class="row-fluid">
  <div class="span12">

    <h1>Edit Article</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span8">
{{ Form::model($article, array('method' => 'PATCH', 'enctype'=>"multipart/form-data", 'route' => array('admin.articles.update', $article->id))) }}
	<ul>
    <li>
      {{ Form::label('title', 'Title:') }}
      {{ Form::text('title', $article->title, array('class'=>'input-xxlarge')) }}
    </li>
    <li>
      {{ Form::label('author', 'Author:') }}
      {{ Form::text('author') }}
    </li>
    <li>
      {{ Form::label('caption', 'Caption:') }}
      {{ Form::textarea('caption', $article->caption, array('class'=>'input-xxlarge')) }}
    </li>
    <li>
      {{ Form::label('images', 'Images:') }}
      <a href="javascript:void(0)" id="link-add-image" class="btn btn-small">ADD IMAGE</a>
      <div class="clearfix"></div>
      <ul id="image-upload-holder">
      </ul>
      <p>
      @foreach($article->photos()->get() as $one_photo)
          <br/><img src="{{$one_photo->getImageUrl('100', '100')}}" >
          <input type="checkbox" name="delete_current_images[]" value="{{ $one_photo->id }}" />
          Delete?
          <br/>
      @endforeach
      </p>
    </li>
    <li>
      {{ Form::label('section_id', 'Section:') }}
      {{ Form::select('section_id', Section::lists('name','id')) }}
    </li>
    <li>
      {{ Form::label('body', 'Body:') }}
      {{ Form::textarea('body' ) }}        
    </li>
    <li>
      {{ Form::label('is_published', 'Publish to site:') }}
      {{ Form::select('is_published', array('0'=>'No', '1'=>'Yes' )) }}
    </li>
    <li>
      {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
    </li>
		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.articles.show', 'Cancel', $article->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}
</div>
</div>
<div id="image-holder-template" style="display:none;">
  <div class="well">
    <input name="photos[]" type="file" />
    {{ Form::text('captions[]') }}

    <a href="javascript:void(0)" class="btn btn-small lnk-remove-image"><i class="icon-cross"/></a>
  </div>
</div>
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<script >
$(function(){
  CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});
  $('#link-add-image').click(function(){
    
    $('#image-upload-holder').append('<li>'+$('#image-holder-template').html()+'</li>');
    $(document).on('click', '.lnk-remove-image', function(){
      $(this).parents('li').first().remove();
    });
  });
});
</script>
  
@stop
