@extends('layouts.scaffold')

@section('main')
@if($category):
	<h1>Articles For Section {{ $category->name }}
 	{{ link_to('admin/sections', 'Back to Sections', array('class'=>'btn ')) }}
	</h1>

@else
	<h1>All Articles</h1>
@endif

<p>{{ link_to_route('admin.articles.create', 'Add new article', array(), array('class'=>'btn')) }} {{ link_to('admin/articles/csv', 'Export CSV', array('class'=>'btn')) }}</p>
<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'title'=>'Title', 'author'=>'Author', 'caption'=>'Caption', 'is_published'=>'Published?');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/articles/json",
        "fnServerParams": function ( aoData ) {
         	<?php echo $category!=null?' aoData.push( { "name": "category", "value": "'.$category->name.'" } );':''; ?>            
        }
    } );
} );
</script>
@stop
