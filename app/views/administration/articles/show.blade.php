@extends('layouts.scaffold')

@section('main')

<h1>Show Article</h1>

<p>{{ link_to_route('admin.articles.index', 'Return to all articles') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Title</th>
				<th>Author</th>
				<th>Caption</th>
				<th>Body</th>
				<th>Is_published</th>
				<th>Created_by</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $article->title }}}</td>
					<td>{{{ $article->author }}}</td>
					<td>{{{ $article->caption }}}</td>
					<td>{{{ $article->body }}}</td>
					<td>{{{ $article->is_published }}}</td>
					<td>{{{ $article->created_by }}}</td>
                    <td>{{ link_to_route('admin.articles.edit', 'Edit', array($article->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.articles.destroy', $article->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
