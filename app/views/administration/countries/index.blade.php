@extends('layouts.scaffold')

@section('main')
<row>
  <span12>
    

<h1>All Countries</h1>

<p> {{ link_to('admin/countries/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'name'=>'Name');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/countries/json"
    } );
} );
</script>
  </span12>  
</row>

@stop
