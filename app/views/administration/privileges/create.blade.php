@extends('layouts.scaffold')

@section('main')

<h1>Create Privilege</h1>

{{ Form::open(array('route' => 'admin.privileges.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>
        <li>
            {{ Form::label('permissions', 'Permissions:') }}
            @foreach (Config::get('firadmin::resources') as $resource)
                <b>{{ ucfirst(str_replace('_', ' ', $resource)) }}</b>
                <p>
                @foreach(array('create', 'read', 'update', 'delete') as $action)
                    <label class="checkbox" style="display:inline-block;width:200px;">
                    <?php echo Form::checkbox('permissions['.$resource.'][]', $action  );?>{{ ucfirst($action) }} {{ ucfirst(str_replace('_', ' ', $resource)) }}
                    </label>
                @endforeach
                </p>
            @endforeach
        </li>
		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


