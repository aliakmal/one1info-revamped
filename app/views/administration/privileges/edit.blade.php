@extends('layouts.scaffold')

@section('main')

<h1>Edit Privilege</h1>
{{ Form::model($privilege, array('method' => 'PATCH', 'route' => array('admin.privileges.update', $privilege->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>
        <li>
            {{ Form::label('permissions', 'Permissions:') }}
            @foreach (Config::get('firadmin::resources') as $resource)
                <b>{{ ucfirst(str_replace('_', ' ', $resource)) }}</b>
                <p>
                @foreach(array('create', 'read', 'update', 'delete') as $action)
                    <label class="checkbox" style="display:inline-block;width:200px;">
                    <?php echo Form::checkbox('permissions['.$resource.'][]', $action, (in_array($action, $privilege->permissions[$resource]))  );?>{{ ucfirst($action) }} {{ ucfirst(str_replace('_', ' ', $resource)) }}
                    </label>
                @endforeach
                </p>
            @endforeach
        </li>
		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.privileges.show', 'Cancel', $privilege->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
