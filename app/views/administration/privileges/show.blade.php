@extends('layouts.scaffold')

@section('main')

<h1>Show Privilege</h1>

<p>{{ link_to_route('admin.privileges.index', 'Return to all privileges') }}</p>

@if(!($privilege->admin == 1))
	{{ Form::open(array('method' => 'DELETE', 'route' => array('admin.privileges.destroy', $privilege->id))) }}
	<div class="btn-group">
		{{ link_to_route('admin.privileges.edit', 'Edit', array($privilege->id), array('class' => 'btn ')) }}</td>
		<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn ">Delete</a>
    </div>
    {{ Form::close() }}

@endif


<table class="table table-striped table-bordered">
		<tr>
			<th>Name</th><td>{{ $privilege->name }}</td>
		</tr>
		<tr>
			<th>Permissions</th>
			<td>
				@foreach($privilege->permissions as $resource=>$permissions)
					<div style="display:inline-block;width:200px; margin:0px 10px 10px 0px">
						<b>{{ ucwords(str_replace('_', ' ', $resource)) }}</b><br />
						@foreach($permissions as $permission)
							<span class="label label-info">{{ $permission }}</span> 
						@endforeach
					</div>
				@endforeach
			</td>
		</tr>
</table>

@stop
