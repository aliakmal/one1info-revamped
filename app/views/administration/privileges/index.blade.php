@extends('layouts.scaffold')

@section('main')

<h1>All Privileges</h1>

<p>{{ link_to_route('admin.privileges.create', 'Add new privilege') }}</p>

@if ($privileges->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($privileges as $privilege)
				<tr>
					<td>{{{ $privilege->name }}}</td>
                    <td>
@if(!($privilege->admin == 1))
	{{ Form::open(array('method' => 'DELETE', 'route' => array('admin.privileges.destroy', $privilege->id))) }}
	<div class="btn-group">
		{{ link_to_route('admin.privileges.edit', 'Edit', array($privilege->id), array('class' => 'btn ')) }}</td>
		<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn ">Delete</a>
    </div>
    {{ Form::close() }}

@endif
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no privileges
@endif

@stop
