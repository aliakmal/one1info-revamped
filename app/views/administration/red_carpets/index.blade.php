@extends('layouts.scaffold')

@section('main')

<h1>All Red carpet services</h1>

<p>{{ link_to_route('admin.red_carpets.create', 'Add new red carpet service') }}</p>

@if ($red_carpets->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($red_carpets as $red_carpet)
				<tr>
					<td>{{{ $red_carpet->name }}}</td>
                    <td>{{ link_to_route('admin.red_carpets.edit', 'Edit', array($red_carpet->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.red_carpets.destroy', $red_carpet->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no red_carpets
@endif

@stop
