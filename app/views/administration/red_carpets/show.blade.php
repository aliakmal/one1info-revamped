@extends('layouts.scaffold')

@section('main')

<h1>Show Red_carpet</h1>

<p>{{ link_to_route('red_carpets.index', 'Return to all red_carpets') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $red_carpet->name }}}</td>
                    <td>{{ link_to_route('red_carpets.edit', 'Edit', array($red_carpet->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('red_carpets.destroy', $red_carpet->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
