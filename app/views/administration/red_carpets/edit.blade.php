@extends('layouts.scaffold')

@section('main')

<h1>Edit Red_carpet</h1>
{{ Form::model($red_carpet, array('method' => 'PATCH', 'route' => array('admin.red_carpets.update', $red_carpet->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.red_carpets.show', 'Cancel', $red_carpet->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
