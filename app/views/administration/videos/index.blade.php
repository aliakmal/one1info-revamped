@extends('layouts.scaffold')

@section('main')
@if($video_section):
	<h1>
	{{ $video_section->name }} Videos

 	{{ link_to('admin/video_sections', 'Back to video sections', array('class'=>'btn ')) }}
	</h1>
	<p>{{ link_to('admin/videos/section/'.$video_section->name.'/create', 'Add videos to '.$video_section->name, array('class'=>'btn')) }} {{ link_to('admin/videos/csv', 'Export CSV', array('class'=>'btn')) }}</p>
@else
	<h1>All Videos</h1>
	<p>{{ link_to_route('admin.videos.create', 'Add videos', array('class'=>'btn')) }} {{ link_to('admin/videos/csv', 'Export CSV', array('class'=>'btn')) }}</p>
@endif




<table class="table table-striped " id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Providor</th>
			<th>Thumbnail</th>
			<th>Url</th>
			<th>section</th>
			<th>Is published</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="7" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/videos/json",
        "fnServerParams": function ( aoData ) {
         	<?php echo $video_section!=null?' aoData.push( { "name": "section", "value": "'.$video_section->name.'" } );':''; ?>            
        }
    } );
} );
</script>
@stop
