@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">

    <h1>Create Video</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'admin.videos.store')) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title', '', array('class'=>'input-xxlarge')) }}
        </li>
        <li>
            {{ Form::hidden('providor', 'youtube') }}
            {{ Form::label('url', 'Url/Video ID:') }}
            {{ Form::text('url', '', array('class'=>'input-xxlarge')) }}
            <small>Enter the ID of the video as on youtube</small>
        </li>
          <li>
            {{ Form::label('section_id', 'Section:') }}
            @if(!is_null($section))
                <b>{{$section->name}}</b>
                {{ Form::hidden('section_id', $section->id) }}
            @else
                {{ Form::select('section_id', Section::lists('name','id')) }}
            @endif
          </li>
        
        <li>
            {{ Form::label('caption', 'Caption:') }}
            {{ Form::textarea('caption', '', array('class'=>'input-xxlarge')) }}
        </li>
        <li>
            {{ Form::label('is_published', 'Is Published:') }}
            {{ Form::select('is_published', array(0=>'Do Not Publish', 1=>'Published')) }}
        </li>

    	<li>
    		{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
    	</li>
	</ul>
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


