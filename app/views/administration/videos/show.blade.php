@extends('layouts.scaffold')

@section('main')

<h1>Show Video</h1>

<p>{{ link_to_route('videos.index', 'Return to all videos') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Title</th>
				<th>Providor</th>
				<th>Url</th>
				<th>Caption</th>
				<th>Is_published</th>
				<th>Created_by</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $video->title }}}</td>
					<td>{{{ $video->providor }}}</td>
					<td>{{{ $video->url }}}</td>
					<td>{{{ $video->caption }}}</td>
					<td>{{{ $video->is_published }}}</td>
					<td>{{{ $video->created_by }}}</td>
                    <td>{{ link_to_route('videos.edit', 'Edit', array($video->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('videos.destroy', $video->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
