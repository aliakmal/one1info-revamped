@extends('layouts.scaffold')

@section('main')
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

<h1>Edit Idea</h1>
{{ Form::model($idea, array('method' => 'PATCH', 'enctype'=>"multipart/form-data", 'route' => array('admin.ideas.update', $idea->id))) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('youtube_url', 'Youtube_url:') }}
            {{ Form::text('youtube_url') }}
        </li>
    <li>
        {{ Form::label('images', 'Images:') }}
        <a href="javascript:void(0)" id="link-add-image" class="btn btn-small">ADD IMAGE</a>
        <div class="clearfix"></div>
        <ul id="image-upload-holder">
        </ul>
        <p>
        @foreach($article->photos()->get() as $one_photo)
            <br/><img src="{{$one_photo->getImageUrl('100', '100')}}" >
            <input type="checkbox" name="delete_current_images[]" value="{{ $one_photo->id }}" />
            Delete?
            <br/>
        @endforeach
        </p>
      </li>
        <li>
            {{ Form::label('media_title', 'Media_title:') }}
            {{ Form::text('media_title') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

        <li>
            {{ Form::label('sort', 'Sort:') }}
            {{ Form::input('number', 'sort') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.ideas.show', 'Cancel', $idea->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<div id="image-holder-template" style="display:none;">
  <div class="well">
    <input name="photos[]" type="file" />
    {{ Form::text('captions[]') }}

    <a href="javascript:void(0)" class="btn btn-small lnk-remove-image"><i class="icon-cross"/></a>
  </div>
</div>

<script >
$(function(){
  CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});
  $('#link-add-image').click(function(){
    
    $('#image-upload-holder').append('<li>'+$('#image-holder-template').html()+'</li>');
    $(document).on('click', '.lnk-remove-image', function(){
      $(this).parents('li').first().remove();
    });
  });
});
</script>
@stop
