@extends('layouts.scaffold')

@section('main')

<h1>All Ideas</h1>

<p>{{ link_to_route('admin.ideas.create', 'Add ideas', array(), array('class'=>'btn')) }} {{ link_to('admin/ideas/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'title'=>'Title','body'=>'Body','youtube_url'=>'Youtube_url','media_title'=>'Media_title','dated'=>'Dated','sort'=>'Sort','is_published'=>'Is_published');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/ideas/json"
    } );
} );
</script>
@stop
