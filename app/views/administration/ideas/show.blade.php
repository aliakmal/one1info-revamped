@extends('layouts.scaffold')

@section('main')

<h1>Show Idea</h1>

<p>{{ link_to_route('ideas.index', 'Return to all ideas') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Title</th>
				<th>Body</th>
				<th>Youtube_url</th>
				<th>Media_title</th>
				<th>Dated</th>
				<th>Sort</th>
				<th>Is_published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $idea->title }}}</td>
					<td>{{{ $idea->body }}}</td>
					<td>{{{ $idea->youtube_url }}}</td>
					<td>{{{ $idea->media_title }}}</td>
					<td>{{{ $idea->dated }}}</td>
					<td>{{{ $idea->sort }}}</td>
					<td>{{{ $idea->is_published }}}</td>
                    <td>{{ link_to_route('admin.ideas.edit', 'Edit', array($idea->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.ideas.destroy', $idea->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
