@extends('layouts.scaffold')

@section('main')

<h1>All Company_reviews</h1>

<p>{{ link_to_route('admin.company_reviews.create', 'Add company review', array(), array('class'=>'btn')) }} {{ link_to('admin/company_reviews/csv', 'Export CSV', array('class'=>'btn')) }}</p>

	
<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('company_reviews.id'=>'ID','company_reviews.title'=>'Title','company_reviews.company_id'=>'Company_id','company_reviews.user_id'=>'User_id','company_reviews.rate'=>'Rate','company_reviews.body'=>'Body','company_reviews.dated'=>'Dated','company_reviews.is_published'=>'Is_published'
);
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/company_reviews/json"
    } );
} );
</script>
@stop
