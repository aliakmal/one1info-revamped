@extends('layouts.scaffold')

@section('main')

<h1>Show Company_review</h1>

<p>{{ link_to_route('admin.company_reviews.index', 'Return to all company_reviews') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Title</th>
				<th>Company_id</th>
				<th>User_id</th>
				<th>Rate</th>
				<th>Body</th>
				<th>Dated</th>
				<th>Is_published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $company_review->title }}}</td>
					<td>{{{ $company_review->company_id }}}</td>
					<td>{{{ $company_review->user_id }}}</td>
					<td>{{{ $company_review->rate }}}</td>
					<td>{{{ $company_review->body }}}</td>
					<td>{{{ $company_review->dated }}}</td>
					<td>{{{ $company_review->is_published }}}</td>
                    <td>{{ link_to_route('company_reviews.edit', 'Edit', array($company_review->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('company_reviews.destroy', $company_review->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
