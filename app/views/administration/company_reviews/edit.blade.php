@extends('layouts.scaffold')

@section('main')

<h1>Edit Company_review</h1>
{{ Form::model($company_review, array('method' => 'PATCH', 'route' => array('admin.company_reviews.update', $company_review->id))) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::select('company_id', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('user_id', 'User_id:') }}
            {{ Form::select('user_id', User::lists('username','id')) }}
        </li>

        <li>
            {{ Form::label('rate', 'Rate:') }}
            {{ Form::input('number', 'rate') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.company_reviews.show', 'Cancel', $company_review->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
