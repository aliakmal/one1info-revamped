@extends('layouts.scaffold')

@section('main')

<h1>All Members</h1>

<p>{{ link_to_route('admin.members.create', 'Add members', array(), array('class'=>'btn')) }} {{ link_to('admin/members/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'first_name'=>'First_name','last_name'=>'Last_name','email'=>'Email','gender'=>'Gender','mobile'=>'Mobile','address'=>'Address','state'=>'State','country_id'=>'Country','dob'=>'Dob','title'=>'Title','profession'=>'Profession','interests'=>'Interests');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/members/json"
    } );
} );
</script>



@stop
