@extends('layouts.scaffold')

@section('main')

<h1>Show Deal_section</h1>

<p>{{ link_to_route('admin.deal_sections.index', 'Return to all deal_sections') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
				<th>Is_published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $deal_section->name }}}</td>
					<td>{{{ $deal_section->is_published }}}</td>
                    <td>{{ link_to_route('deal_sections.edit', 'Edit', array($deal_section->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('deal_sections.destroy', $deal_section->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
