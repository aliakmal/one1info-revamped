@extends('layouts.scaffold')

@section('main')

<h1>Edit Deal_section</h1>
{{ Form::model($deal_section, array('method' => 'PATCH', 'route' => array('admin.deal_sections.update', $deal_section->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.deal_sections.show', 'Cancel', $deal_section->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
