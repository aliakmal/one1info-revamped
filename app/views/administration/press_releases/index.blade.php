@extends('layouts.scaffold')

@section('main')

<h1>All Press Releases</h1>

<p>{{ link_to_route('admin.press_releases.create', 'Add press releases', array(), array('class'=>'btn')) }} {{ link_to('admin/press_releases/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array(	'id'=>'ID','title'=>'Title','company_name'=>'Company', 'image'=>'Image',
								'youtube_url'=>'Youtube_url','dated'=>'Dated','type'=>'Type','is_published'=>'Is_published');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th width="180"></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/press_releases/json"
    } );
} );
</script>


@stop
