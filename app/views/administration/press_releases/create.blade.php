@extends('layouts.scaffold')

@section('main')

<h1>Create Press_release</h1>
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

{{ Form::open(array('route' => 'admin.press_releases.store', 'enctype'=>"multipart/form-data")) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::select('company_id', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('youtube_url', 'Youtube_url:') }}
            {{ Form::text('youtube_url') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

        <li>
            {{ Form::label('type', 'Type:') }}
            {{ Form::select('is_published', array('private'=>'Private', 'public'=>'Public' )) }}
        </li>
        <li>
        {{ Form::label('images', 'Images:') }}
        <a href="javascript:void(0)" id="link-add-image" class="btn btn-small">ADD IMAGE</a>
        <div class="clearfix"></div>
        <ul id="image-upload-holder">
        </ul>
      </li>
        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::select('is_published', array('0'=>'Draft', '1'=>'Published' )) }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
<div id="image-holder-template" style="display:none;">
  <div class="well">
    <input name="photos[]" type="file" />
    {{ Form::text('captions[]') }}

    <a href="javascript:void(0)" class="btn btn-small lnk-remove-image"><i class="icon-cross"/></a>
  </div>
</div>
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
<script >
$(function(){
  CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});
  $('#link-add-image').click(function(){
    
    $('#image-upload-holder').append('<li>'+$('#image-holder-template').html()+'</li>');
    $(document).on('click', '.lnk-remove-image', function(){
      $(this).parents('li').first().remove();
    });
  });
});
</script>
@stop


