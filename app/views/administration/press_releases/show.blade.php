@extends('layouts.scaffold')

@section('main')

<p>{{ link_to_route('admin.press_releases.index', 'Return to all press releases') }}
    {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.press_releases.destroy', $press_release->id))) }}
		<div class="btn-group">
		{{ link_to_route('admin.press_releases.edit', 'Edit', array($press_release->id), array('class' => 'btn')) }}
		<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn ">Delete</a>
		</div>
    {{ Form::close() }}

</p>
<small>ID</small>
<p class="lead">{{{ $press_release->id }}}</p>
<small>Company</small>
<p class="lead">
	{{ link_to_route('admin.companies.show', $press_release->company()->first()->company_name, array('id'=>$press_release->company_id)) }}
</p>


<small>Title</small>
<p class="lead">{{{ $press_release->title }}}</p>

<small>Photos</small>
<p class="lead">
	@foreach($press_release->photos()->get() as $one_photo)
	    {{ Photo::image_tag($one_photo, 'thumb-med', array('class'=>'img-polaroid'))  }}
	@endforeach
</p>


<small>Body</small>
{{ ($press_release->body) }}

<small>Youtube Url</small>
<p>{{ link_to($press_release->youtube_url, $press_release->youtube_url) }}</p>
<p>{{ StringHelper::youtube($press_release->youtube_url)}}</p>

<small>Date</small>
<p class="lead">{{{ $press_release->dated }}}</p>
<small>Type</small>
<p>
	@if ($press_release->type == 'private'):
		<span class="label label-important">PRIVATE</span>
	@else
		@if ($press_release->type == 'public'):
			<span class="label label-success">PUBLIC</span>
		@endif
	@endif

</p>
<small>Is Published</small>
<p>
	@if ($press_release->is_published == '0'):
		<span class="label label-important">NOT PUBLISHED</span>
	@else
		<span class="label label-success">PUBLISHED</span>
	@endif

</p>



@stop
