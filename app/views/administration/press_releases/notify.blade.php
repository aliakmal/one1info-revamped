@extends('layouts.scaffold')

@section('main')

<h1>Notify {{ $company->company_name }}</h1>
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>
@if ($errors->any())
    <ul class="alert alert-danger errors">
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif
{{ Form::open(array('route' => array('admin.press_releases.notify', $press_release->id))) }}
	<ul>
        <li>
            {{ Form::label('company', 'Company:') }}
            <b>{{ $press_release->company()->first()->company_name }}</b>
        </li>
        <li>
            {{ Form::label('email', 'Email:') }}
            <b>{{ $press_release->company()->first()->email }}</b>
        </li>
        <li>
            {{ Form::label('mobile', 'Mobile:') }}
            <b>{{ $press_release->company()->first()->mobile }}</b>
        </li>
        <li>
            {{ Form::label('subject', 'Subject:') }}
            {{ Form::text('subject', $press_release->title) }}
        </li>
        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>
		</li>
	</ul>
    {{ Form::submit('Send Notification', array('class' => 'btn btn-info')) }}
    {{ link_to_route('admin.press_releases.show', 'Cancel', $press_release->id, array('class' => 'btn')) }}


{{ Form::close() }}


<script >
$(function(){
  CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});
  $('#link-add-image').click(function(){
    
    $('#image-upload-holder').append('<li>'+$('#image-holder-template').html()+'</li>');
    $(document).on('click', '.lnk-remove-image', function(){
      $(this).parents('li').first().remove();
    });
  });
});
</script>
@stop


