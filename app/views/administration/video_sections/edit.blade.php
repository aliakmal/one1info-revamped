@extends('layouts.scaffold')

@section('main')

<h1>Edit Video_section</h1>
{{ Form::model($video_section, array('method' => 'PATCH', 'route' => array('admin.video_sections.update', $video_section->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('sort', 'Sort:') }}
            {{ Form::input('number', 'sort') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::select('is_published', array('0'=>'Draft', '1'=>'Published' )) }}
        </li>
		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.video_sections.show', 'Cancel', $video_section->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
