@extends('layouts.scaffold')

@section('main')

<h1>All Video_sections</h1>

<p>{{ link_to_route('admin.video_sections.create', 'Add new video_section') }}</p>

@if ($video_sections->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Sort</th>
				<th>Is Published</th>
				<th></th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($video_sections as $video_section)
				<tr>
					<td>{{{ $video_section->id }}}</td>
					<td>{{{ $video_section->name }}}</td>
					<td>{{{ $video_section->sort }}}</td>
					<td>{{ $video_section->is_published=="0" ? '<span class="badge badge-important">NOT PUBLISHED</span>' : '<span class="badge badge-success">PUBLISHED</span>' }}</td>
					<td><a href="/admin/videos/section/{{{ $video_section->name }}}">Videos ({{ $video_section->videos()->count() }})</a></td>
                    <td>

						{{ Form::open(array('method' => 'DELETE', 'route' => array('admin.video_sections.destroy', $video_section->id))) }}
                    	<div class="btn-group">
                    		<a href="/admin/videos/section/{{{ $video_section->name }}}/create" class="btn btn-mini"> Add Videos</a>
                    		{{ link_to_route('admin.video_sections.edit', 'Edit', array($video_section->id), array('class' => 'btn btn-mini')) }}
                           	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn  btn-mini">Delete</a>
                        </div>
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no video_sections
@endif

@stop
