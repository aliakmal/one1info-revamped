@extends('layouts.scaffold')

@section('main')

<h1>Show Video section</h1>

<p>{{ link_to_route('admin.video_sections.index', 'Return to all video_sections') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
				<th>Sort</th>
				<th>Is_published</th>
				<th>System</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $video_section->name }}}</td>
					<td>{{{ $video_section->sort }}}</td>
					<td>{{{ $video_section->is_published }}}</td>
                    <td>{{ link_to_route('admin.video_sections.edit', 'Edit', array($video_section->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.video_sections.destroy', $video_section->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
