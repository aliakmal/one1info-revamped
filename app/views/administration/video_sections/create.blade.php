@extends('layouts.scaffold')

@section('main')

<h1>Create Video_section</h1>

{{ Form::open(array('route' => 'admin.video_sections.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('sort', 'Sort:') }}
            {{ Form::input('number', 'sort') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
        {{ Form::select('is_published', array('0'=>'Draft', '1'=>'Published' )) }}
        </li>
		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


