@extends('layouts.scaffold')

@section('main')

<h1>Edit News Header</h1>
{{ Form::model($marque, array('method' => 'PATCH', 'route' => array('admin.news_headers.update', $marque->id))) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('url', 'Url:') }}
            {{ Form::text('url') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated', $marque->dated, array('class'=>'datepicker')) }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::select('is_published', array('0'=>'No', '1'=>'Yes' )) }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.news_headers.show', 'Cancel', $marque->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
