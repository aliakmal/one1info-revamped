@extends('layouts.scaffold')

@section('main')

<h1>All News Headers</h1>

<p>{{ link_to_route('admin.news_headers.create', 'Add new News Header') }}</p>

@if ($marques->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Title</th>
				<th>Url</th>
				<th>Dated</th>
				<th>Is_published</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($marques as $marque)
				<tr>
					<td>{{{ $marque->title }}}</td>
					<td>{{{ $marque->url }}}</td>
					<td>{{{ $marque->dated }}}</td>
					<td>{{{ $marque->is_published }}}</td>
                    <td>{{ link_to_route('admin.news_headers.edit', 'Edit', array($marque->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.news_headers.destroy', $marque->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no marques
@endif

@stop
