@extends('layouts.scaffold')

@section('main')

<h1>Show News Header</h1>

<p>{{ link_to_route('admin.news_headers.index', 'Return to all News Headers') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Title</th>
				<th>Url</th>
				<th>Dated</th>
				<th>Is_published</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $marque->title }}}</td>
					<td>{{{ $marque->url }}}</td>
					<td>{{{ $marque->dated }}}</td>
					<td>{{{ $marque->is_published }}}</td>
                    <td>{{ link_to_route('admin.news_headers.edit', 'Edit', array($marque->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.news_headers.destroy', $marque->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
