@extends('layouts.scaffold')

@section('main')

<h1>Create News Header</h1>

{{ Form::open(array('route' => 'admin.news_headers.store')) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('url', 'Url:') }}
            {{ Form::text('url') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated', '', array('class'=>'datepicker')) }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::select('is_published', array('0'=>'No', '1'=>'Yes' )) }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


