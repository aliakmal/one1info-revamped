@extends('layouts.scaffold')

@section('main')

<h1>All Corporate Services</h1>

<p>{{ link_to_route('admin.services.create', 'Add new corporate service') }}</p>

@if ($corporate_services->count())
<table class="table table-striped " id="datatable">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Sort</th>
				<th>Published</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<td colspan="5" class="dataTables_empty">Loading data from server</td>
		</tbody>
	</table>

    {{ $corporate_services->links() }}

@else
	There are no corporate services
@endif

<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/services/json",
        "fnServerParams": function ( aoData ) {
        }
    } );
} );
</script>

@stop
