@extends('layouts.scaffold')

@section('main')
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>
<div class="row-fluid">
  <div class="span12">

    <h1>Create Corporate Service</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'admin.services.store')) }}
	<ul>
      <li>
        {{ Form::label('name', 'Name:') }}
        {{ Form::text('name') }}
      </li>
      <li>
        {{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body', '') }}        
      </li>
      <li>
        {{ Form::label('sort', 'Sort:') }}
        {{ Form::text('sort') }}
      </li>
      <li>
        {{ Form::label('is_published', 'Publish to site:') }}
        {{ Form::select('is_published', array('0'=>'Draft', '1'=>'Published' )) }}
      </li>
  </ul>

	{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
	
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

<script>
  $(function(){
    CKEDITOR.replace('body', {
      toolbar: 'Basic',
      uiColor: '#9AB8F3'
    });
  });
</script>

@stop