@extends('layouts.scaffold')

@section('main')
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

<h1>Edit Corporate Service</h1>
{{ Form::model($corporate_service, array('method' => 'PATCH', 'route' => array('admin.services.update', $corporate_service->id))) }}
	<ul>
        <li>
			{{ Form::label('name', 'Name:') }}
	        {{ Form::text('name') }}
        </li>
      <li>
		{{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body' ) }}        
      </li>
      <li>
        {{ Form::label('sort', 'Sort:') }}
        {{ Form::text('sort') }}
      </li>
      <li>
        {{ Form::label('is_published', 'Publish to site:') }}
        {{ Form::select('is_published', array('0'=>'Draft', '1'=>'Published' )) }}
      </li>
	</ul>
	{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
	{{ link_to_route('admin.services.show', 'Cancel', $corporate_service->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
<script>
  $(function(){
    CKEDITOR.replace('body', {
      toolbar: 'Basic',
      uiColor: '#9AB8F3'
    });
  });
</script>
@stop
