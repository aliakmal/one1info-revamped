@extends('layouts.scaffold')

@section('main')

<h1>Edit Auto_email</h1>
{{ Form::model($auto_email, array('method' => 'PATCH', 'route' => array('admin.auto_emails.update', $auto_email->id))) }}
	<ul>
        <li>
            {{ Form::label('service', 'Service:') }}
            {{ Form::text('service') }}
        </li>

        <li>
            {{ Form::label('from', 'From:') }}
            {{ Form::text('from') }}
        </li>

        <li>
            {{ Form::label('subject', 'Subject:') }}
            {{ Form::text('subject') }}
        </li>

        <li>
            {{ Form::label('header', 'Header:') }}
            {{ Form::textarea('header') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('footer', 'Footer:') }}
            {{ Form::textarea('footer') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.auto_emails.show', 'Cancel', $auto_email->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
