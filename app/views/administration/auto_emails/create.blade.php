@extends('layouts.scaffold')

@section('main')

<h1>Create Auto_email</h1>

{{ Form::open(array('route' => 'admin.auto_emails.store')) }}
	<ul>
        <li>
            {{ Form::label('service', 'Service:') }}
            {{ Form::text('service') }}
        </li>

        <li>
            {{ Form::label('from', 'From:') }}
            {{ Form::text('from') }}
        </li>

        <li>
            {{ Form::label('subject', 'Subject:') }}
            {{ Form::text('subject') }}
        </li>

        <li>
            {{ Form::label('header', 'Header:') }}
            {{ Form::textarea('header') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('footer', 'Footer:') }}
            {{ Form::textarea('footer') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


