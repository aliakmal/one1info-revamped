@extends('layouts.scaffold')

@section('main')

<h1>Show Auto_email</h1>

<p>{{ link_to_route('admin.auto_emails.index', 'Return to all auto_emails') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Service</th>
				<th>From</th>
				<th>Subject</th>
				<th>Header</th>
				<th>Body</th>
				<th>Footer</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $auto_email->service }}}</td>
					<td>{{{ $auto_email->from }}}</td>
					<td>{{{ $auto_email->subject }}}</td>
					<td>{{{ $auto_email->header }}}</td>
					<td>{{{ $auto_email->body }}}</td>
					<td>{{{ $auto_email->footer }}}</td>
                    <td>{{ link_to_route('admin.auto_emails.edit', 'Edit', array($auto_email->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.auto_emails.destroy', $auto_email->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
