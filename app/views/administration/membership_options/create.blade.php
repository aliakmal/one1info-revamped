@extends('layouts.scaffold')

@section('main')

<h1>Create Membership_option</h1>

{{ Form::open(array('route' => 'membership_options.store')) }}
	<ul>
        <li>
            {{ Form::label('membership_title', 'Membership_title:') }}
            {{ Form::text('membership_title') }}
        </li>

        <li>
            {{ Form::label('options', 'Options:') }}
            {{ Form::text('options') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


