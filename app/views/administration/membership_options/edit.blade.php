@extends('layouts.scaffold')

@section('main')

<h1>Edit Membership_option</h1>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
<?php $op = $membership_option->options;?>
{{ Form::model($membership_option, array('method' => 'PATCH', 'class'=>"form-horizontal", 'route' => array('admin.membership_options.update', $membership_option->id))) }}
	<ul >
        <div class="control-group">
            {{ Form::label('membership_title', 'Membership_title:', array('class'=>"control-label")) }}
            <div class="controls">{{ Form::text('membership_title') }}</div>
        </div>
        <div class="control-group">
            {{ Form::label('options[price]', 'Price:', array('class'=>"control-label")) }}
            <div class="controls">{{ Form::text('options[price]', $membership_option->options['price']) }}</div>
        </div>
        <div class="control-group">
            {{ Form::label('options[user_access]', 'User Access:', array('class'=>"control-label")) }}
            <div class="controls">{{ Form::text('options[user_access]', $membership_option->options['user_access']) }}</div>
        </div>
        <?php $vbs = array('telephone'=>'Telephone', 'mobile'=>'Mobile', 'fax'=>'Fax', 'pobox'=>'P.O.Box', 'website'=>'Website',
        					'contact_person'=>'Contact Person', 'business_activity'=>'Business activity', 'license_type'=>'License type',
        					'established_since'=>'Established Since', 'num_of_employees'=>'No. of employees', 'location_map'=>'Location map',
        					'profile'=>'Profile'
        					); ?>
		@foreach($vbs as $name=>$label)
	        <div class="control-group">
	            {{ Form::label("options[$name]", $label.':', array('class'=>"control-label")) }}
	            <div class="controls">{{ Form::radio("options[$name]", 1, ($op[$name]==1?true:false) ) }} Enable
	            {{ Form::radio("options[$name]", 0, ($op[$name]==0?true:false)) }} Disable</div>
	        </div>
		@endforeach
        <div class="control-group">
            {{ Form::label('options[media]', 'Media:', array('class'=>"control-label")) }}
            <div class="controls">{{ Form::text('options[media]', $op['media']) }}</div>
        </div>
        <div class="control-group">
            {{ Form::label('options[press_releases]', 'Press releases:', array('class'=>"control-label")) }}
            <div class="controls">{{ Form::text('options[press_releases]', $op['press_releases']) }}</div>
        </div>
        <?php $vbs = array('daily_newsletter'=>'Daily newsletter', 'weekly_newsletter'=>'Weekly newsletter', 'match_making'=>'Match making', 'red_carpet'=>'Red carpet',
        					'corporate_services'=>'Corporate services', 'post_jobs'=>'Post jobs', 'patent_investment'=>'Patent investment'
        					); ?>
		@foreach($vbs as $name=>$label)
	        <div class="control-group">
	            {{ Form::label("options[$name]", $label.':', array('class'=>"control-label")) }}
	            <div class="controls">{{ Form::radio("options[$name]", 1, ($op[$name]==1?true:false) ) }} Enable
	            {{ Form::radio("options[$name]", 0, ($op[$name]==0?true:false)) }} Disable</div>
	        </div>
		@endforeach


        <div class="control-group">
            {{ Form::label('options[ads_banner]', 'Ads banner:', array('class'=>"control-label")) }}
            <div class="controls">{{ Form::text('options[ads_banner]', $op['ads_banner']) }}</div>
        </div>

		<div class="control-group">
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.membership_options.show', 'Cancel', $membership_option->id, array('class' => 'btn')) }}
		</div>
	</ul>
{{ Form::close() }}

@stop