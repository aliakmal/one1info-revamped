@extends('layouts.scaffold')

@section('main')

<h1>{{ $membership_option->membership_title }}</h1>

<p>{{ link_to_route('admin.membership_options.index', 'Return to all membership options') }}</p>
<?php $op = $membership_option->options;?>
<table class="table table-striped ">
	<thead>
		<tr>
			<th width="10%">Price</th><td>{{$op['price']}}</td>
		</tr>
		<tr>
			<th>User access</th><td>{{$op['user_access']}}</td>
		</tr>
        <?php $vbs = array('telephone'=>'Telephone', 'mobile'=>'Mobile', 'fax'=>'Fax', 'pobox'=>'P.O.Box', 'website'=>'Website',
        					'contact_person'=>'Contact Person', 'business_activity'=>'Business activity', 'license_type'=>'License type',
        					'established_since'=>'Established Since', 'num_of_employees'=>'No. of employees', 'location_map'=>'Location map',
        					'profile'=>'Profile'
        					); ?>
		@foreach($vbs as $name=>$label)
			<tr>
				<th>{{$label}}</th><td>{{($op[$name]==1?'<span class="label label-success">Enabled</span>':'<span class="label label-important">Disabled</span>')}}</td>
			</tr>
		@endforeach
		<tr>
			<th>Media</th><td>{{$op['media']}}</td>
		</tr>
		<tr>
			<th>Press releases</th><td>{{$op['press_releases']}}</td>
		</tr>

        <?php $vbs = array('daily_newsletter'=>'Daily newsletter', 'weekly_newsletter'=>'Weekly newsletter', 'match_making'=>'Match making', 'red_carpet'=>'Red carpet',
        					'corporate_services'=>'Corporate services', 'post_jobs'=>'Post jobs', 'patent_investment'=>'Patent investment'
        					); ?>
		@foreach($vbs as $name=>$label)
			<tr>
				<th>{{$label}}</th><td>{{($op[$name]==1?'<span class="label label-success">Enabled</span>':'<span class="label label-important">Disabled</span>')}}</td>
			</tr>
		@endforeach

		<tr>
			<th>Ads banner</th><td>{{$op['ads_banner']}}</td>
		</tr>

	</thead>

</table>

@stop
