@extends('layouts.scaffold')

@section('main')

<h1>All Membership_options</h1>

@if ($membership_options->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Membership title</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($membership_options as $membership_option)
				<tr>
					<td>{{{ $membership_option->membership_title }}}</td>
                    <td>
						{{ Form::open(array('method' => 'DELETE', 'route' => array('admin.membership_options.destroy', $membership_option->id))) }}
						<div class="button-group">
	                    	{{ link_to_route('admin.membership_options.edit', 'Edit', array($membership_option->id), array('class' => 'btn btn-mini')) }}
	                    	{{ link_to_route('admin.membership_options.show', 'View', array($membership_option->id), array('class' => 'btn btn-mini')) }}
	                    	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn btn-mini">Delete</a>
                    	</div>
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no membership_options
@endif

@stop
