@extends('layouts.scaffold')

@section('main')

<h1>Edit Company</h1>
{{ Form::model($company, array('method' => 'PATCH', 'route' => array('admin.companies.update', $company->id))) }}
	<ul>
        <li>
            {{ Form::label('company_name', 'Company_name:') }}
            {{ Form::text('company_name') }}
        </li>

        <li>
            {{ Form::label('known_as', 'Known_as:') }}
            {{ Form::text('known_as') }}
        </li>

        <li>
            {{ Form::label('contact_person', 'Contact_person:') }}
            {{ Form::text('contact_person') }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('website', 'Website:') }}
            {{ Form::text('website') }}
        </li>

        <li>
            {{ Form::label('phone', 'Phone:') }}
            {{ Form::text('phone') }}
        </li>

        <li>
            {{ Form::label('mobile', 'Mobile:') }}
            {{ Form::text('mobile') }}
        </li>

        <li>
            {{ Form::label('fax', 'Fax:') }}
            {{ Form::text('fax') }}
        </li>

        <li>
            {{ Form::label('country_id', 'Country_id:') }}
            {{ Form::select('country_id', Country::lists('name','id')) }}
        </li>

        <li>
            {{ Form::label('origin_country_id', 'Origin_country_id:') }}
            {{ Form::select('origin_country_id', Country::lists('name','id')) }}
        </li>

        <li>
            {{ Form::label('licence_type', 'Licence_type:') }}
            {{ Form::text('licence_type') }}
        </li>

        <li>
            {{ Form::label('established', 'Established:') }}
            {{ Form::text('established') }}
        </li>

        <li>
            {{ Form::label('num_employees', 'Num_employees:') }}
            {{ Form::input('number', 'num_employees') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::textarea('address') }}
        </li>

        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('postal_code', 'Postal_code:') }}
            {{ Form::text('postal_code') }}
        </li>
        <li>
            {{ Form::label('categories', 'Categories:') }}
            {{ Form::select('categories[]', Category::lists('name','id'), '', array('multiple')) }}
        </li>

        <li>
            {{ Form::label('industry_id', 'Industry_id:') }}
            {{ Form::select('industry_id', Industry::lists('name','id')) }}
        </li>

        <li>
            {{ Form::label('products', 'Products:') }}
            {{ Form::textarea('products') }}
        </li>

        <li>
            {{ Form::label('services', 'Services:') }}
            {{ Form::textarea('services') }}
        </li>

        <li>
            {{ Form::label('profile', 'Profile:') }}
            {{ Form::textarea('profile') }}
        </li>

        <li>
            {{ Form::label('subscription', 'Subscription:') }}
            {{ Form::text('subscription') }}
        </li>

        <li>
            {{ Form::label('publish', 'Publish:') }}
            {{ Form::checkbox('publish') }}
        </li>

        <li>
            {{ Form::label('order', 'Order:') }}
            {{ Form::input('number', 'order') }}
        </li>

        <li>
            {{ Form::label('location_x', 'Location_x:') }}
            {{ Form::text('location_x') }}
        </li>

        <li>
            {{ Form::label('location_y', 'Location_y:') }}
            {{ Form::text('location_y') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('companies.show', 'Cancel', $company->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
