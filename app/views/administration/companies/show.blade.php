@extends('layouts.scaffold')

@section('main')

<h1>Show Company</h1>

<p>{{ link_to_route('admin.companies.index', 'Return to all companies') }}</p>

@if($company->press_releases()->count() > 0)
	{{ link_to_route('admin.press_releases.index','Press releases('.$company->press_releases()->count().')', array('company_id'=>$company->id) ) }}
@else
	<i> No Press Releases </i>
@endif

@if($company->complaints()->count() > 0)
	{{ link_to_route('admin.complaints.index','Complaints('.$company->complaints()->count().')', array('company_id'=>$company->id)  ) }}
@else
	<i> No complaints </i>
@endif

@if($company->company_interviews()->count() > 0)
	{{ link_to_route('admin.company_interviews.index', 'Interviews('.$company->company_interviews()->count().')', array('company_id'=>$company->id)  ) }}
@else
	<i> No company interviews </i>
@endif
@if($company->jobs()->count() > 0)
	{{ link_to_route('admin.jobs.index', 'Jobs('.$company->jobs()->count().')', array('company_id'=>$id)  ) }}
@else
	<i> No Jobs </i>
@endif
@if($company->patent_investments()->count() > 0)
	{{ link_to_route('admin.patent_investments.index', 'Patent investments('.$company->patent_investments()->count().')', array('company_id'=>$company->id)  ) }}
@else
	<i> No patent investments </i>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="10%">ID</th>
			<td>{{{ $company->id }}}</td>
		</tr>
		<tr>
			<th width="10%">Company name</th>
			<td>{{{ $company->company_name }}}</td>
		</tr>
		<tr>
			<th>Known as</th>
			<td>{{{ $company->known_as }}}</td>
		</tr>
		<tr>
			<th>Contact person</th>
			<td>{{{ $company->contact_person }}}</td>
		</tr>
		<tr>
			<th>Email</th>
			<td>{{{ $company->email }}}</td>
		</tr>
		<tr>
			<th>Website</th>
			<td>{{{ $company->website }}}</td>
		</tr>
		<tr>
			<th>Phone</th>
			<td>{{{ $company->phone }}}</td>
		</tr>
		<tr>
			<th>Mobile</th>
			<td>{{{ $company->mobile }}}</td>
		</tr>
		<tr>
			<th>Fax</th>
			<td>{{{ $company->fax }}}</td>
		</tr>
		<tr>
			<th>Country</th>
			<td>{{{ $company->country_name }}}</td>
		</tr>
		<tr>
			<th>Origin country</th>
			<td>{{{ $company->origin_country_name }}}</td>
		</tr>
		<tr>
			<th>Licence type</th>
			<td>{{{ $company->licence_type }}}</td>
		</tr>
		<tr>
			<th>Established</th>
			<td>{{{ $company->established }}}</td>
		</tr>
		<tr>
			<th>No. employees</th>
			<td>{{{ $company->num_employees }}}</td>
		</tr>
		<tr>
			<th>Address</th>
			<td>{{{ $company->address }}}</td>
		</tr>
		<tr>
			<th>City</th>
			<td>{{{ $company->city }}}</td>
		</tr>
		<tr>
			<th>Postal code</th>
			<td>{{{ $company->postal_code }}}</td>
		</tr>
		<tr>
			<th>Industry</th>
			<td>{{{ $company->industry_name }}}</td>
		</tr>
		<tr>
			<th>Products</th>
			<td>{{{ $company->products }}}</td>
		</tr>
		<tr>
			<th>Services</th>
			<td>{{{ $company->services }}}</td>
		</tr>
		<tr>
			<th>Profile</th>
			<td>{{{ $company->profile }}}</td>
		</tr>
		<tr>
			<th>Subscription</th>
			<td>{{{ $company->subscription }}}</td>
		</tr>
		<tr>
			<th>Publish</th>
			<td>{{ $company->published==0 ? '<span class="badge badge-important">NOT PUBLISHED</span>':'<span class="badge badge-success">PUBLISHED</span>' }}</td>
		</tr>
		<tr>
			<th>Order</th>
			<td>{{{ $company->order }}}</td>
		</tr>
		<tr>
			<th>Location(x)</th>
			<td>{{{ $company->location_x }}}</td>
		</tr>
		<tr>
			<th>Location(y)</th>
			<td>{{{ $company->location_y }}}</td>
		</tr>
	</thead>

	<tbody>
		<tr>
                    <td>{{ link_to_route('companies.edit', 'Edit', array($company->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('companies.destroy', $company->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
