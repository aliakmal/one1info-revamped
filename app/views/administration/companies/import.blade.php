@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">
    <h1>Import Companies
      {{ link_to_route('admin.companies.index', 'Return to Companies', array(), array('class'=>'btn pull-right')) }}
    </h1>

  </div>
</div>
<div class="row-fluid">
  <div class="span8">

  {{ Form::open(array('class'=>'form-horizontal', 'enctype'=>"multipart/form-data")) }}
	
  <div class="well" >
    <p>
      {{ Form::label('file', 'Upload File:') }}
      {{ Form::file('file') }}<br/>
      <small>Make sure the file is a CSV file</small>
    </p>
    <p>
      {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
    </p>
	</div>
{{ Form::close() }}
  </div>
  <div class="span4 well">
    <h4>Note:</h4>
    <p>Make sure your csv files first row has some/all of the following field names, order is not important. The company_name field is the minimal required field.</p>
    <table class="table">
      <thead>
        <tr>
          <th>Field name</th>
          <th>Label</th>
        </tr>
      </thead>
      <?php  

      $fields = array('company_name', 'known_as', 'contact_person', 'email', 'website', 'phone', 'mobile', 'fax', 'country_name', 'origin_country_name',
'licence_type', 'established', 'num_employees', 'address', 'city', 'postal_code', 'industry_name', 'products', 'services',
'profile', 'subscription', 'publish', 'order', 'location_x', 'location_y');
      foreach($fields as $field):?>
        <tr>
          <td>{{ $field }}</td>
          <td>{{ ucwords(str_replace('_', ' ', $field)) }}</td>
        </tr>
      <?php endforeach;?>

    </table>


  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


