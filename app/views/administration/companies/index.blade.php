@extends('layouts.scaffold')

@section('main')

<h1>All Companies</h1>

<p>
	{{ link_to_route('admin.companies.create', 'Add company', array(), array('class'=>'btn')) }}
	{{ link_to('admin/companies/csv', 'Export CSV', array('class'=>'btn')) }}
	{{ link_to('admin/companies/import', 'Import Companies', array('class'=>'btn')) }}
</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<th></th>
		<th width="200"></th>
		<th width="200"></th>
		<th width="200"></th>
		<th width="200"></th>
		<th width="200"></th>
		<?php $columns = array(	'id'=>'ID','company_name'=>'Company name','known_as'=>'Known as','contact_person'=>'Contact person',
								'email'=>'Email','website'=>'Website','phone'=>'Phone','mobile'=>'Mobile','fax'=>'Fax',
								'country_id'=>'Country','origin_country_id'=>'Origin country','licence_type'=>'Licence type',
								'established'=>'Established',
								'num_employees'=>'No. employees','address'=>'Address','city'=>'City','postal_code'=>'Postal code',
								'industry_id'=>'Industry','products'=>'Products','services'=>'Services','profile'=>'Profile',
								'subscription'=>'Subscription',
								'publish'=>'Publish',
								'order'=>'Order','location_x'=>'Location(x)','location_y'=>'Location(y)');
		foreach($columns as $column=>$title):
		    echo '<th style="min-width:120px">';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/companies/json"
    } );
} );
</script>
@stop
