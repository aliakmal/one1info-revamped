@extends('layouts.scaffold')

@section('main')

<h1>Show Patent_investment</h1>

<p>{{ link_to_route('admin.patent_investments.index', 'Return to all patent_investments') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Patent_id</th>
				<th>Company_id</th>
				<th>Applicant_name</th>
				<th>Applicant_position</th>
				<th>Address</th>
				<th>City</th>
				<th>Country</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Fax</th>
				<th>Why_invest</th>
				<th>Investment_benefits</th>
				<th>Has_business_same_field</th>
				<th>Need_meeting</th>
				<th>Status</th>
				<th>Dated</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $patent_investment->patent_id }}}</td>
					<td>{{{ $patent_investment->company_id }}}</td>
					<td>{{{ $patent_investment->applicant_name }}}</td>
					<td>{{{ $patent_investment->applicant_position }}}</td>
					<td>{{{ $patent_investment->address }}}</td>
					<td>{{{ $patent_investment->city }}}</td>
					<td>{{{ $patent_investment->country }}}</td>
					<td>{{{ $patent_investment->email }}}</td>
					<td>{{{ $patent_investment->phone }}}</td>
					<td>{{{ $patent_investment->fax }}}</td>
					<td>{{{ $patent_investment->why_invest }}}</td>
					<td>{{{ $patent_investment->investment_benefits }}}</td>
					<td>{{{ $patent_investment->has_business_same_field }}}</td>
					<td>{{{ $patent_investment->need_meeting }}}</td>
					<td>{{{ $patent_investment->status }}}</td>
					<td>{{{ $patent_investment->dated }}}</td>
                    <td>{{ link_to_route('admin.patent_investments.edit', 'Edit', array($patent_investment->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.patent_investments.destroy', $patent_investment->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
