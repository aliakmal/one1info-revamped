@extends('layouts.scaffold')

@section('main')

<h1>Create Patent_investment</h1>

{{ Form::open(array('route' => 'admin.patent_investments.store')) }}
	<ul>
        <li>
            {{ Form::label('patent_id', 'Patent_id:') }}
            {{ Form::input('number', 'patent_id') }}
        </li>

        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::select('company_id', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('applicant_name', 'Applicant_name:') }}
            {{ Form::text('applicant_name') }}
        </li>

        <li>
            {{ Form::label('applicant_position', 'Applicant_position:') }}
            {{ Form::text('applicant_position') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::text('address') }}
        </li>

        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('country', 'Country:') }}
            {{ Form::select('country', Country::lists('name','name')) }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('phone', 'Phone:') }}
            {{ Form::text('phone') }}
        </li>

        <li>
            {{ Form::label('fax', 'Fax:') }}
            {{ Form::text('fax') }}
        </li>

        <li>
            {{ Form::label('why_invest', 'Why_invest:') }}
            {{ Form::textarea('why_invest') }}
        </li>

        <li>
            {{ Form::label('investment_benefits', 'Investment_benefits:') }}
            {{ Form::textarea('investment_benefits') }}
        </li>

        <li>
            {{ Form::label('has_business_same_field', 'Has_business_same_field:') }}
            {{ Form::text('has_business_same_field') }}
        </li>

        <li>
            {{ Form::label('need_meeting', 'Need_meeting:') }}
            {{ Form::text('need_meeting') }}
        </li>

        <li>
            {{ Form::label('status', 'Status:') }}
            {{ Form::text('status') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


