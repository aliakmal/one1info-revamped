@extends('layouts.scaffold')

@section('main')

<h1>All Patent Investments</h1>

<p>{{ link_to_route('admin.patent_investments.create', 'Add patent_investments', array(), array('class'=>'btn')) }} {{ link_to('admin/patent_investments/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'patent_id'=>'Patent_id','company_id'=>'Company_id','applicant_name'=>'Applicant_name','applicant_position'=>'Applicant_position','address'=>'Address','city'=>'City','country'=>'Country','email'=>'Email','phone'=>'Phone','fax'=>'Fax','why_invest'=>'Why_invest','investment_benefits'=>'Investment_benefits','has_business_same_field'=>'Has_business_same_field','need_meeting'=>'Need_meeting','status'=>'Status','dated'=>'Dated');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/patent_investments/json"
    } );
} );
</script>

@stop
