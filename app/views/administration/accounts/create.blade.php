<div class="row-fluid">	
	<div class="span12">
	
		<?php echo View::make('firadmin::partials.form-message')?>			
		<h3><?php echo Lang::get('firadmin::admin.add-user')?></h3>
				
	</div>	
</div>
<div class="row-fluid">
	<div class="span12">
		@if ($errors->any())
			<ul>
				{{ implode('', $errors->all('<li class="error">:message</li>')) }}
			</ul>
		@endif
		<form method="post" action="<?php echo URL::to('/admin/accounts');?>">
		<?php echo Form::token();?>
			<fieldset>
        	    {{ Form::label('company_id', 'Company:') }}
	            {{ Form::select('company_id', Company::lists('company_name','id')) }}

        	    {{ Form::label('level', 'User Level:') }}
	            {{ Form::select('level', array('user'=>'User','admin'=>'Admin')) }}

				<label>Username</label>
				<?php echo Form::text('username', Input::old('username'));?>
				
				<label>Email</label>
				<?php echo Form::text('email', Input::old('email'));?>
        
				<label>Password</label>
				<?php echo Form::password('password');?>
				
				<label>Password Confirmation</label>
				<?php echo Form::password('password_confirmation');?>
				
				<label><?php echo Lang::get('firadmin::admin.roles')?></label>
				<?php //foreach (Config::get('firadmin::roles') as $role => $permissions){?>
				<?php foreach (array('basic-company', 'premium-company') as $role){?>
				<label class="checkbox">
				<?php echo Form::radio('role', $role, ($role == Input::old('role')));?><?php echo ucfirst(str_replace('-', ' ', $role))?>
				</label>
				<?php }?>
				
				<div class="form-actions">
					<?php echo Form::submit(Lang::get('firadmin::admin.store-user'), array('class' => 'btn btn-primary'));?>
					<a href="<?php echo URL::to('/admin/accounts');?>" class="btn"><?php echo Lang::get('firadmin::admin.cancel')?></a>
				</div>
				
			</fieldset>
		</form>
		
	</div>
</div>
