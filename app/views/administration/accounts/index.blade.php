<div class="row-fluid">	
	<div class="span12">		
		<?php echo View::make('firadmin::partials.form-message')?>	
		<h3>Accounts</h3>					
		<a href="<?php echo URL::to('/admin/accounts/create');?>" class="btn ">Add Account</a>

	</div>	

</div>

<div class="row-fluid">
	<div class="span12">		
				
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th><?php echo Lang::get('firadmin::admin.username')?></th>
					<th>Role</th>
					<th><?php echo Lang::get('firadmin::admin.email')?></th>
					<th><?php echo Lang::get('firadmin::admin.updated-at')?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user):?>
				<tr>
					<td><?php echo $user->id?></td>
					<td><a href="<?php echo URL::to('/admin/accounts/' . $user->id);?>"><?php echo $user->username?></a></td>
					<td>
					@if($user->role == 'premium-company')
					<span class="label label-success">{{ucwords(str_replace('-', ' ', $user->role))}}</span>
					@else
					<span class="label label-info">{{ucwords(str_replace('-', ' ', $user->role))}}</span>
					@endif
						
					</td>
					<td><?php echo $user->email?></td>
					<td><?php echo $user->updated_at?></td>
					<td>
						<div class="btn-group">
							<a href="<?php echo URL::to('/admin/accounts/' . $user->id . '/edit');?>" class="btn btn-small"><?php echo Lang::get('firadmin::admin.edit')?></a>
							<a data-toggle="dropdown" class="btn btn-small dropdown-toggle">
								<i class="icon-cog"></i>
							</a>
							<ul class="dropdown-menu pull-right">          
								<li><a href="<?php echo URL::to('/admin/accounts/' . $user->id);?>"><?php echo Lang::get('firadmin::admin.show')?></a></li>
								<li>
									<a href="#" onclick="if(confirm('<?php echo Lang::get('firadmin::admin.delete-confirm')?>')){ return $('form[data-user-id=<?php echo $user->id?>]').submit()}"><?php echo Lang::get('firadmin::admin.delete')?></a>
									<form class="hidden" data-user-id="<?php echo $user->id?>" method="post" action="<?php echo URL::to('/admin/accounts/'.$user->id);?>">
									<?php echo Form::token();?>
									<input type="hidden" name="_method" value="DELETE">
									</form>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		
		
	</div>
</div>



