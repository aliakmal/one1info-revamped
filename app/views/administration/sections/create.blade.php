@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">

    <h1>Create Section</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'admin.sections.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>
        <li>
            {{ Form::label('sort', 'Sort Order:') }}
            {{ Form::text('sort') }}
            <small>Enter a number to indicate the sort order.</small>
        </li>
        <li>
            {{ Form::label('is_published', 'Publish to Site:') }}
            <?php foreach (array('0'=>'Do Not Publish', '1'=>'Publish') as $val => $label){?>
              <label class="checkbox">
                <?php echo Form::radio('is_published', $val, ($val == Input::old('is_published')));?> {{ $label }}
              </label>
            <?php }?>
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


