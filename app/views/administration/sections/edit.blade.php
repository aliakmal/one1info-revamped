@extends('layouts.scaffold')

@section('main')

<h1>Edit Section</h1>
{{ Form::model($section, array('method' => 'PATCH', 'route' => array('admin.sections.update', $section->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>
        <li>
            {{ Form::label('sort', 'Sort Order:') }}
            {{ Form::text('sort') }}
            <small>Enter a number to indicate the sort order.</small>
        </li>
        <li>
            {{ Form::label('is_published', 'Publish to Site:') }}
            <?php foreach (array('0'=>'Do Not Publish', '1'=>'Publish') as $val => $label){?>
              <label class="checkbox">
                <?php echo Form::radio('is_published', $val, ($val == Input::old('is_published')));?> {{ $label }}
              </label>
            <?php }?>
        </li>
		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.sections.show', 'Cancel', $section->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
