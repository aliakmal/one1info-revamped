@extends('layouts.scaffold')

@section('main')

<h1>All Sections</h1>

<p>{{ link_to_route('admin.sections.create', 'Add sections', array(), array('class'=>'btn')) }} {{ link_to('admin/sections/csv', 'Export CSV', array('class'=>'btn')) }}</p>

<table class="table table-striped " id="datatable">
	<thead>
		<tr>
		<?php $columns = array('id'=>'ID', 'name'=>'Name', 'sort'=>'Sort', 'is_published'=>'Published to?', 'num_articles'=>'No. Articles');
		foreach($columns as $column=>$title):
		    echo '<th>';
		    echo $title;
		    echo '</th>';
		endforeach;
		?>
		<th></th>
		</tr>
	</thead>
	<tbody>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tbody>
</table>
<script type="text/javascript">

$(document).ready(function() {
    $('#datatable').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "/api/sections/json",
    } );
} );
</script>
@stop
