@extends('layouts.scaffold')

@section('main')
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

<h1>Edit Page</h1>
{{ Form::model($page, array('method' => 'PATCH', 'route' => array('admin.pages.update', $page->id))) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>
        <li>
            {{ Form::label('image', 'Banner Image:') }}
            {{ Form::file('image') }}
            @if($page->photo()->count() > 0):
            <div class="row">
                <div class="span3">
                    <div class="well ">
                        {{ Photo::image_tag( $page->photo()->first() ) }}<br/>
                        {{ Form::checkbox('deleteme', '1')}}
                        <small>Select an image to replace current image, else leave empty to keep current image.</small>
                    </div>
                </div>
            </div>
            
            @endif
    </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.pages.show', 'Cancel', $page->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
<script >
$(function(){
  CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
    });
  });
</script>

@stop
