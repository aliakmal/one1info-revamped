@extends('layouts.scaffold')

@section('main')
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

<h1>Create Page</h1>

{{ Form::open(array('route' => 'admin.pages.store')) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>
        <li>
            {{ Form::label('image', 'Image:') }}
            {{ Form::file('image') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::select('is_published', array('1'=>'Yes', '0'=>'No')) }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
<script >
$(function(){
  CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});
});
</script>
@stop


