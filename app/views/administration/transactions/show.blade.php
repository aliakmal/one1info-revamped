@extends('layouts.scaffold')

@section('main')

<h1>Show Transaction</h1>

<p>{{ link_to_route('admin.transactions.index', 'Return to all transactions') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Company_id</th>
				<th>Membership</th>
				<th>First_name</th>
				<th>Last_name</th>
				<th>Amount</th>
				<th>Payment_type</th>
				<th>Bill_no</th>
				<th>Dated</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $transaction->company_id }}}</td>
					<td>{{{ $transaction->membership }}}</td>
					<td>{{{ $transaction->first_name }}}</td>
					<td>{{{ $transaction->last_name }}}</td>
					<td>{{{ $transaction->amount }}}</td>
					<td>{{{ $transaction->payment_type }}}</td>
					<td>{{{ $transaction->bill_no }}}</td>
					<td>{{{ $transaction->dated }}}</td>
                    <td>{{ link_to_route('admin.transactions.edit', 'Edit', array($transaction->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.transactions.destroy', $transaction->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
