@extends('layouts.scaffold')

@section('main')

<h1>Edit Transaction</h1>
{{ Form::model($transaction, array('method' => 'PATCH', 'route' => array('admin.transactions.update', $transaction->id))) }}
	<ul>
        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::select('company_id', Company::lists('company_name','id')) }}
        </li>

        <li>
            {{ Form::label('membership', 'Membership:') }}
            {{ Form::text('membership') }}
        </li>

        <li>
            {{ Form::label('first_name', 'First_name:') }}
            {{ Form::text('first_name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Last_name:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('amount', 'Amount:') }}
            {{ Form::input('number', 'amount') }}
        </li>

        <li>
            {{ Form::label('payment_type', 'Payment_type:') }}
            {{ Form::text('payment_type') }}
        </li>

        <li>
            {{ Form::label('bill_no', 'Bill_no:') }}
            {{ Form::text('bill_no') }}
        </li>

        <li>
            {{ Form::label('dated', 'Dated:') }}
            {{ Form::text('dated') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.transactions.show', 'Cancel', $transaction->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
