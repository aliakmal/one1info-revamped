@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">

          <div class="white-search-box">
        {{ Form::open(array('url' => '/search', 'method'=>'get')) }}
          <div class="header">Search</div>
          
            {{ Form::text('search', Input::get('search'), array('style'=>'width:450px;font-size:16px;padding:5px 3px;')) }}
                    {{ Form::submit('Submit', array('class' => 'btn btn-info', 'style'=>'display:inline-block;')) }}

        {{ Form::close() }}
      </div>


    <h1>Search {{Input::get('search')}}</h1>



      <div>
        @foreach($articles as $article)
        <div class="article-element">
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl(); ?>" height="90" />
          <h3 style="margin-bottom:3px;">{{ link_to_route('articles.show', $article->title, array($article->id)) }}</h3>
            <p>{{ substr($article->caption, 0, 305)}}...</p>
          
          <div class="clearfix" style="margin-bottom:5px;"></div>
        </div>
        @endforeach
      </div>






  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    @foreach (Section::all()->take(2) as $section)
    <div class="row">
      <div class="col-md-12">
        <h4> Latest From {{ $section->name }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles as $article)
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl(); ?>" height="50" />
          <div><b style="margin-bottom:3px;">{{ link_to_route('articles.show', $article->title, array($article->id)) }}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>
 
        @endforeach
      </div>
    </div>
    @endforeach
    
    
  </div>
</div>

@stop
