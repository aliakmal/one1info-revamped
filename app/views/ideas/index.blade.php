@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">



    <h1>Press Releases</h1>
    @foreach ($ideas as $ii=>$idea)
      <a href="{{URL::to('ideas', array($idea->id))}}">
            <div class="inline-holder">
              <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $idea->photos->first()->getImageUrl('385', '385'); ?>"  />
              <p>{{ StringHelper::trimIt($idea->title, 60) }}</p>
            </div>
        </a>

    @endforeach

    {{ $ideas->links() }}





  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    @foreach ($sections as $section)
    <div class="row">
      <div class="col-md-12">
        <h4> Latest From {{ $section->name }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles as $article)
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl('70', '50'); ?>" />
          <div><b style="margin-bottom:3px;">{{ link_to_route('articles.show', $article->title, array($article->id)) }}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>
 
        @endforeach
      </div>
    </div>
    @endforeach
    
    
    <div class="well">

    </div>
  </div>
</div>
<!-- Jumbotron -->
<script>
  $(function() {
    $("#featured").tabs({hide:{effect: "fade"}}).tabs("rotate", 5000, true);
  })
</script>

@stop
