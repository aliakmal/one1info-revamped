@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
    <h1>{{$idea->title}}</h1>
    <p>
      <small>Date: {{$idea->created_at}}</small>
    </p>
    <div id="carousel-example-generic" class="carousel  " data-ride="carousel">
      <div class="thumbnail">
        <img width="100%" src="{{$idea->photos()->first()->getImageUrl()}}" >
        <div class="caption">
          {{$idea->caption}}
        </div>
        <div style="padding:10px 0px 10px 0px">
          <span class='st_sharethis_hcount' displayText='ShareThis'></span>
          <span class='st_facebook_hcount' displayText='Facebook'></span>
          <span class='st_twitter_hcount' displayText='Tweet'></span>
          <span class='st_google_hcount' displayText='Google'></span>
          <span class='st_linkedin_hcount' displayText='LinkedIn'></span>
          <span class='st_email_hcount' displayText='Email'></span>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-4" style="width:200px;">
        <h2> {{ link_to('ideas/', 'More Ideas'); }}</h2>
          <?php $flag = 0; ?> 
          @foreach(Press_release::all()->take(5) as $idea)
          <div style="padding:3px;"><b style="margin-bottom:3px;"><a href="{{URL::to('ideas', array($idea->id))}}">{{ StringHelper::trimIt($idea->title, 30) }}</a></b>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>

          @endforeach
          <div class="single-element blue-link" style="border-top:0px;">{{ link_to('ideas/', 'more', array('class'=>'blue-link')); }}</div>

      </div>
      <div class="col-md-5 text-content">
        {{ html_entity_decode($idea->body) }}
        {{ StringHelper::youtube($idea->youtube_url) }}

      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
      <img src="/assets/images/box-1.jpg" />
      <img src="/assets/images/box-2.jpg" />
    </div>
    </div>
    <p>&nbsp;</p><br/>    
  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    <img src="/assets/images/tall-01.jpg" />

    <div class="row">
      <div class="col-md-12">
        <h4> More Ideas</h2>
          <?php $flag = 0; ?> 
          @foreach($sections->first()->articles as $article)
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl(); ?>" height="50" />
          <div><b style="margin-bottom:3px;">{{$article->title}}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>

          @endforeach
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "9693b6f5-c590-4134-a42a-29b6a775ceab", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

@stop
