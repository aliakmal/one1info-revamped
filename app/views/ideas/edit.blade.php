@extends('layouts.scaffold')

@section('main')

<h1>Edit Article</h1>
{{ Form::model($article, array('method' => 'PATCH', 'route' => array('admin.articles.update', $article->id))) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('author', 'Author:') }}
            {{ Form::text('author') }}
        </li>

        <li>
            {{ Form::label('caption', 'Caption:') }}
            {{ Form::textarea('caption') }}
        </li>

        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>

        <li>
            {{ Form::label('created_by', 'Created_by:') }}
            {{ Form::input('number', 'created_by') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('admin.articles.show', 'Cancel', $article->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
