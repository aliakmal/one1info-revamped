@extends('layouts.scaffold')

@section('main')

<h1>All Corporate Services</h1>

<p>{{ link_to_route('admin.services.create', 'Add new corporate service') }}</p>

@if ($corporate_services->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($corporate_services as $corporate_service)
				<tr>
					<td>{{{ $corporate_service->name }}}</td>
                    <td>{{ link_to_route('admin.services.edit', 'Edit', array($corporate_service->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.services.destroy', $corporate_service->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>

    {{ $corporate_services->links() }}

@else
	There are no corporate services
@endif

@stop
