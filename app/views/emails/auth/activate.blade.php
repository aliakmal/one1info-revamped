<p>Hello {{$user->username}}</p>
<p>You need to activate your account before you can log in. To activate your login please click on the below link or paste it in your browsers address bar.</p>

{{ "http://" . $_SERVER['HTTP_HOST'].'/activate?id='.$user->id.'&hash='.$hash }}

<p>Regards</p>
One1info.com Team