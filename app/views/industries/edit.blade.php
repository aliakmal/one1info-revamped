@extends('layouts.scaffold')

@section('main')

<h1>Edit Industry</h1>
{{ Form::model($industry, array('method' => 'PATCH', 'route' => array('admin.industries.update', $industry->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('industries.show', 'Cancel', $industry->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
