@extends('layouts.scaffold')

@section('main')

<h1>All Industries</h1>

<p>{{ link_to_route('admin.industries.create', 'Add new industry') }}</p>

@if ($industries->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($industries as $industry)
				<tr>
					<td>{{{ $industry->name }}}</td>
                    <td>{{ link_to_route('admin.industries.edit', 'Edit', array($industry->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.industries.destroy', $industry->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
    {{ $industries->links() }}

@else
	There are no industries
@endif

@stop
