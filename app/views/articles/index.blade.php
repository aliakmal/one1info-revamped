@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">

    <div id="featured" >  
        <ul class="ui-tabs-nav">  
          <?php $flag = 0; ?>
          @foreach ($featured_articles as $article)
          <?php 
            if(count($article->photos) == 0)
              continue;
          ?>
            <li class="ui-tabs-nav-item" id="nav-fragment-{{$article->id}}"><a href="#fragment-{{$article->id}}"><span>{{ $article->title }}</span></a></li>  
              <?php $flag = 1;?>
            </li>
          @endforeach
        </ul>
        @foreach ($featured_articles as $article)
          <?php 
            if(count($article->photos) == 0)
              continue;
          ?>
          <div id="fragment-{{$article->id}}" class="ui-tabs-panel" style="">  
            <img src="<?php echo $article->photos->first()->getImageUrl('385','385'); ?>"  />
            <div class="info">
            <p>{{substr($article->caption,0, 245)}}....<a class="blue-link" href="{{ url('articles/', array('id'=>$article->id)) }}" >read more</a></p>  
            </div>
          </div>
        @endforeach
    </div>

    <h1>{{ $title }}</h1>
    @foreach ($articles as $ii=>$article)
      <a href="{{URL::to('articles', array($article->id))}}">
        <div class="inline-holder">
          <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $article->photos->first()->getImageUrl('385', '385'); ?>"  />
          <p>{{ StringHelper::trimIt($article->title, 60) }}</p>
        </div>
      </a>


    @endforeach

    {{ $articles->links() }}





  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    @foreach ($sections as $section)
    <div class="row">
      <div class="col-md-12">
        <h4> Latest From {{ $section->name }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles as $article)
          <a href="{{URL::to('articles', array($article->id))}}"><img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl('70', '50'); ?>" /></a>
          <div><b style="margin-bottom:3px;">{{ link_to_route('articles.show', $article->title, array($article->id)) }}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>
 
        @endforeach
      </div>
    </div>
    @endforeach
    
    
  </div>
</div>
<!-- Jumbotron -->
<script>
  $(function() {
    $("#featured").tabs({hide:{effect: "fade"}}).tabs("rotate", 5000, true);
  })
</script>

@stop
