@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
    <h1>{{$article->title}}</h1>
    <p>
      <small>By {{$article->author}}</small><br/>
      <small>Date: {{$article->created_at}}</small>
    </p>
    <div id="carousel-example-generic" class="carousel  " data-ride="carousel">
      <div class="thumbnail">
        <img width="100%" src="{{$article->photos()->first()->getImageUrl()}}" >
        <div class="caption">
          {{$article->caption}}
        </div>
        <div style="padding:10px 0px 10px 0px">
          <span class='st_sharethis_hcount' displayText='ShareThis'></span>
          <span class='st_facebook_hcount' displayText='Facebook'></span>
          <span class='st_twitter_hcount' displayText='Tweet'></span>
          <span class='st_google_hcount' displayText='Google'></span>
          <span class='st_linkedin_hcount' displayText='LinkedIn'></span>
          <span class='st_email_hcount' displayText='Email'></span>
        </div>
      </div>
    </div>
    <div class="row">

      <div class="col-md-4" style="width:200px;">
        <h2> {{ link_to('articles/'.$article->section->name, $article->section->name, array('section'=>$article->section->id), $secure = null); }}</h2>
          <?php $flag = 0; ?> 
          @foreach(Article::all()->take(5) as $article)
          <div style="padding:3px;"><b style="margin-bottom:3px;"><a href="{{URL::to('articles', array($article->id))}}">{{ StringHelper::trimIt($article->title, 30) }}</a></b>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>

          @endforeach
          <div class="single-element blue-link" style="border-top:0px;">{{ link_to('articles/', 'more'); }}</div>

      </div>
      <div class="col-md-5 text-content">{{ html_entity_decode($article->body) }}</div>
    
    </div>
    <p>&nbsp;</p><br/>    
  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    <img src="/assets/images/tall-01.jpg" />

    <div class="row">
      <div class="col-md-12">
        <h4> Recent {{ $sections->first()->name }}</h2>
          <?php $flag = 0; ?> 
          @foreach($sections->first()->articles as $article)
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl(); ?>" height="50" />
          <div><b style="margin-bottom:3px;">{{$article->title}}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>

          @endforeach
      </div>
    </div>
  </div>


  <div class="row clearfix">
    <div class="col-md-12">
    <img src="/assets/images/box-1.jpg" />
    <img src="/assets/images/box-2.jpg" />
    <img src="/assets/images/box-1.jpg" />
  </div>

</div>
</div>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "9693b6f5-c590-4134-a42a-29b6a775ceab", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
@stop
