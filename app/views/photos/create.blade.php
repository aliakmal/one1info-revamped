@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">

    <h1>Create Photo</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'photos.store', 'enctype'=>"multipart/form-data")) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::file('photo') }}
        </li>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('caption', 'Caption:') }}
            {{ Form::textarea('caption') }}
        </li>

        <li>
            {{ Form::label('size', 'Size:') }}
            {{ Form::input('number', 'size') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


