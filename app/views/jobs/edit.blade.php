@extends('layouts.scaffold')

@section('main')

<h1>Edit Job</h1>
{{ Form::model($job, array('method' => 'PATCH', 'route' => array('jobs.update', $job->id))) }}
	<ul>
        <li>
            {{ Form::label('company_id', 'Company_id:') }}
            {{ Form::input('number', 'company_id') }}
        </li>

        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::text('address') }}
        </li>

        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('state', 'State:') }}
            {{ Form::text('state') }}
        </li>

        <li>
            {{ Form::label('country_id', 'Country_id:') }}
            {{ Form::input('number', 'country_id') }}
        </li>

        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('category', 'Category:') }}
            {{ Form::text('category') }}
        </li>

        <li>
            {{ Form::label('experience', 'Experience:') }}
            {{ Form::text('experience') }}
        </li>

        <li>
            {{ Form::label('education', 'Education:') }}
            {{ Form::text('education') }}
        </li>

        <li>
            {{ Form::label('commitment', 'Commitment:') }}
            {{ Form::text('commitment') }}
        </li>

        <li>
            {{ Form::label('compensation', 'Compensation:') }}
            {{ Form::text('compensation') }}
        </li>

        <li>
            {{ Form::label('details', 'Details:') }}
            {{ Form::textarea('details') }}
        </li>

        <li>
            {{ Form::label('show_company', 'Show_company:') }}
            {{ Form::input('number', 'show_company') }}
        </li>

        <li>
            {{ Form::label('published', 'Published:') }}
            {{ Form::input('number', 'published') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('jobs.show', 'Cancel', $job->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
