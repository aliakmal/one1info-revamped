@extends('layouts.master')

@section('main')
<div class="row">
  <div class="col-md-8">
    <div class="page">
    	<div class="white-search-box">
    		{{ Form::open(array('url' => '/jobs', 'method'=>'get')) }}
    			<div class="header">Job Search</div>
    			<div>
    				Keyword:<br/>
    				{{ Form::text('search', Input::get('search')) }}

    			</div>
    			<div>
    				Country<br/>
    				{{ Form::select('country', Country::lists('name', 'id'), Input::get('country')) }}
    			</div>
    			<div>
    				Industry<br/>
            		{{ Form::select('category', array(  'Accounting'=>'Accounting', 'Architecture'=>'Architecture', 'Auto Industry'=>'Auto Industry', 
                                                'Banking/Finance'=>'Banking/Finance', 'Beauty & Fitness'=>'Beauty & Fitness', 
                                                'Business Development'=>'Business Development', 'Construction'=>'Construction', 'Consulting'=>'Consulting', 
                                                'Design'=>'Design', 'Domestic'=>'Domestic', 'Driving'=>'Driving', 'Education'=>'Education', 'Hospitality'=>'Hospitality', 
                                                'HR/Recruiting'=>'HR/Recruiting', 'Human Resources'=>'Human Resources', 'IT/Telecom'=>'IT/Telecom', 
                                                'Managerial & Supervisory'=>'Managerial & Supervisory', 'Marketing/Pr'=>'Marketing/Pr', 'Media'=>'Media', 
                                                'Medical/Health'=>'Medical/Health', 'Retail'=>'Retail', 'Sales'=>'Sales', 'Secretarial/Office'=>'Secretarial/Office', 
                                                'Tailoring'=>'Tailoring', 'Other'=>'Other'), Input::get('category') ) }}
					{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}

    			</div>
    		{{ Form::close() }}
    	</div>

@if ($jobs->count())
	@foreach ($jobs as $job)
		<div class="single-job">
			<h2>{{ link_to_route('jobs.show', $job->title, array($job->id)) }}</h2>
			<div class="small-date">
				{{ 	$job->created_at }}
			</div>
			@if (trim($job->category)!="")
				<div class="nubbins">
					<div class="label">
						Industry
					</div>
					{{ 	$job->category }}
				</div>
			@endif
			@if (trim($job->commitment)!="")
			<div class="nubbins">
				<div class="label">
					Required Commitment
				</div>
				{{ 	$job->commitment }}
			</div>
			@endif
			@if (trim($job->compensation)!="")
			<div class="nubbins">
				<div class="label">
					Compensation
				</div>
				{{ 	$job->compensation }}
			</div>
			@endif
			<div class="breadcrumb">
				{{ $job->country()->name }} > {{ $job->city }} > {{ $job->address }}
			</div>
		</div>
	@endforeach

	{{ $jobs->links() }}

@else
	There are no jobs
@endif
	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop
