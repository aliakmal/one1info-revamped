@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
@stop
@section('main')

<div class="row">
  <div class="col-md-8">
    <div class="page">
    	<div class="single-job">
			<div class="breadcrumb">
				{{ link_to('jobs', 'All Jobs > All Countries > All Categories')}}
			</div>
    		<h2 style="margin-bottom:10px" class="">{{ link_to_route('jobs.show', $job->title, array($job->id)) }}</h2>

			<div style="margin-bottom:10px; border:0px;" class="breadcrumb">
				{{ $job->country()->name }} > {{ $job->city }} > {{ $job->address }}
			</div>
    		<div class="section-header">
				Job Details
    		</div>
    		<div class="section-box">

	    		<h2 class="no-bg-icon">{{ link_to_route('jobs.show', $job->title, array($job->id)) }}</h2>
				<div class="small-date">
					{{ 	$job->created_at }}
				</div>
				@if (trim($job->category)!="")
					<div class="nubbins">
						<div class="label">
							Industry
						</div>
						{{ 	$job->category }}
					</div>
				@endif
				@if (trim($job->commitment)!="")
				<div class="nubbins">
					<div class="label">
						Required Commitment
					</div>
					{{ 	$job->commitment }}
				</div>
				@endif
				@if (trim($job->compensation)!="")
				<div class="nubbins">
					<div class="label">
						Compensation
					</div>
					{{ 	$job->compensation }}
				</div>
				@endif
			</div>

			<div class="section-header">
				Description
    		</div>
    		<div class="section-box">
    			{{ $job->details }}
    		</div>


    		<div class="section-header">
				Location Map
    		</div>
    		<div class="section-box">
    			<div id="map">

    			</div>
    		</div>


    	</div>

	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>

<script type="text/javascript">
  $(function(){
    var myOptions = {
      zoom: 6,
      center: new google.maps.LatLng({{ $job->company()->first()->location_x }}, {{ $job->company()->first()->location_y }}),
      disableDefaultUI: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
  }

  var map = new google.maps.Map(document.getElementById("map"), myOptions);

  var marker;

  marker = new google.maps.Marker({
        position: new google.maps.LatLng({{ $job->company()->first()->location_x }}, {{ $job->company()->first()->location_y }}),
        map: map
  });


})
</script>
@stop
