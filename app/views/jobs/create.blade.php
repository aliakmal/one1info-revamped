@extends('layouts.master')

@section('main')
<div class="row">
  <div class="col-md-8">
    <div class="page">
<h1>Create Job</h1>

{{ Form::open(array('route' => 'jobs.store')) }}
	<ul>
        <li>
            {{ Form::label('address', 'Address:') }}
            {{ Form::text('address') }}
        </li>
        <li>
            {{ Form::label('city', 'City:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('state', 'State:') }}
            {{ Form::text('state') }}
        </li>

        <li>
            {{ Form::label('country_id', 'Country_id:') }}
            {{ Form::select('country_id', Country::lists('name', 'id')) }}
        </li>

        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('category', 'Category:') }}
            {{ Form::select('category', array(  'Accounting'=>'Accounting', 'Architecture'=>'Architecture', 'Auto Industry'=>'Auto Industry', 
                                                'Banking/Finance'=>'Banking/Finance', 'Beauty & Fitness'=>'Beauty & Fitness', 
                                                'Business Development'=>'Business Development', 'Construction'=>'Construction', 'Consulting'=>'Consulting', 
                                                'Design'=>'Design', 'Domestic'=>'Domestic', 'Driving'=>'Driving', 'Education'=>'Education', 'Hospitality'=>'Hospitality', 
                                                'HR/Recruiting'=>'HR/Recruiting', 'Human Resources'=>'Human Resources', 'IT/Telecom'=>'IT/Telecom', 
                                                'Managerial & Supervisory'=>'Managerial & Supervisory', 'Marketing/Pr'=>'Marketing/Pr', 'Media'=>'Media', 
                                                'Medical/Health'=>'Medical/Health', 'Retail'=>'Retail', 'Sales'=>'Sales', 'Secretarial/Office'=>'Secretarial/Office', 
                                                'Tailoring'=>'Tailoring', 'Other'=>'Other')) }}
        </li>
        <li>
            {{ Form::label('experience', 'Experience:') }}
            {{ Form::select('experience', array('0 - 1 year'=>'0 - 1 year',  '1 - 2 years'=>'1 - 2 years', '2 - 5 years'=>'2 - 5 years', '5 -10 years'=>'5 -10 years','10 -15 years'=>'10 -15 years','Over 15'=>'Over 15')) }}
        </li>

        <li>
            {{ Form::label('education', 'Education:') }}
            {{ Form::select('education', array('High school'=>'High school','Bachelors degree'=>'Bachelors degree','Masters degree'=>'Masters degree','PHD'=>'PHD')) }}
        </li>


        <li>
            {{ Form::label('commitment', 'Commitment:') }}
            {{ Form::select('commitment', array('Part time'=>'Part time', 'Full time'=> 'Full time')) }}
        </li>

        <li>
            {{ Form::label('compensation', 'Compensation:') }}
            {{ Form::text('compensation') }}
        </li>
        <li>
            {{ Form::label('details', 'Details:') }}
            {{ Form::textarea('details') }}
        </li>
        <li>
            {{ Form::label('show_company', 'Show company:') }}
            {{ Form::select('show_company', array('1'=>'Yes', '0'=> 'No')) }}
        </li>
        <li>
            {{ Form::label('published', 'Published:') }}
            {{ Form::select('published', array('1'=>'Yes', '0'=> 'No')) }}
        </li>
		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
    </div>
  </div>    
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop


