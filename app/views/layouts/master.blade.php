<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet"> -->
    @yield('head')
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/assets/css/dropit.css" rel="stylesheet">
    <style>
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="/assets/js/extend.tabs.js"></script>
    <script src="/assets/js/jquery.ticker.js"></script>
    <link href="/assets/css/ticker-style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" media="screen" href="/assets/css/superfish/superfish.css">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;v2">
    
    <!-- link to the JavaScript files (hoverIntent is optional) -->
    <!-- if you use hoverIntent, use the updated r7 version (see FAQ) -->
    <script src="/assets/js/superfish/hoverIntent.js"></script>
    <script src="/assets/js/superfish/superfish.js"></script>
    <title>One1info - The Business News Source</title>
  </head>
  <body>
      <!-- mibew button -->
      <div style="position:fixed;bottom:10px;left:30px; z-index:9999;border:1px solid #CCCCCC;">
        <a href="http://demo.one1info.com/mibew/client.php?locale=en&amp;style=original" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open(&#039;/mibew/client.php?locale=en&amp;style=original&amp;url=&#039;+escape(document.location.href)+&#039;&amp;referrer=&#039;+escape(document.referrer), 'mibew', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><img src="/assets/images/chat_pic.jpg" border="0"  alt=""/></a>
      </div>
      <!-- / mibew button -->

    <div class="container">
      <div class="free-call"></div>
      <a href="/"><h3 class="text-muted logo"></h3></a>
      <div class="right-header">
        @if (Auth::check())
          <a href="#" class="btn-gray">
            Welcome 
            <?php 
              $user = User::find(Auth::user()->id);
              $member = $user->member_details()->first();
              echo (Auth::user()->username);
            ?>
          </a>
        <?php 
          switch(Auth::user()->roles()->first()->role):
            case 'basic-member':
            case 'premium-member':
              echo '<a href="/profile/edit" class="btn-gray">Edit Profile</a>';
              echo '<a href="/logout" class="btn-gray">Logout</a>';
              break;
            default:
              echo '<a href="/profile/view" class="btn-gray">Manage Profile</a>';
              echo '<a href="/logout" class="btn-gray">Logout</a>';
              break;
          endswitch;?>
        @else
          <a href="/sign-in" class="btn-gray">Login</a>
          <a href="/join" class="btn-gray">Register</a>
        @endif
        <div class="main-search-holder">
          {{ Form::open(array('url'=>'/search', 'method'=>'get'))}}
            <table cellpadding="0" border="0">
              <tr>
                <td>{{ Form::text('search', ($_SERVER['REQUEST_URI'] == '/companies' ? '' : Input::get('search') ), array('class'=>'blue-search')) }}</td>
                <td><input  style="margin-right:-4px; margin-top:5px;" type="submit" class="search-btn" value="" name="search" /></td>
              </tr>
            </table>
          {{ Form::close()}}
        </div>
      </div>
      <div class="clearfix"></div>
      <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <ul class="nav navbar-nav sf-menu">
            <li class="active"><a href="/">Home</a></li>
            <li>
              <a href="/about-us">About Us</a>
            </li>
            <li><a href="/companies" >Business Directory</a></li>
            <li><a href="/options" >List Your Business</a></li>
            <li><a href="/corporate">Corporate Services</a></li>
            <li><a href="/jobs">Job Site</a>
              <ul>
                  <li><a href="/jobs/create">Post a Job</a></li>
                  <li><a href="/jobs">Find a Job</a></li>
              </ul>
            </li>
            <li><a href="/articles">Media</a>
                <ul>
                    <li style="padding:12px; border-bottom:1px solid #CCCCCC; " class="bigger-text black bolder">Latest News</li>
                  @foreach(Section::all() as $one_section)
                    <li><a href="/articles/category/{{$one_section->name}}">{{$one_section->name}}</a></li>
                  @endforeach
                    <li style="padding:12px; border-bottom:1px solid #CCCCCC; " class="bigger-text black bolder">Press Releases</li>
                    <li><a href="/press_releases">View Press Releases</a></li>
                    <li><a href="/press_release/post">Post Press Releases</a></li>
                </ul>
            </li>
            <li><a href="/videos">Videos</a></li>
            <li><a href="/contact-us">Contact Us</a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="navbar-secondary">
        <div class="hot-news">
          <ul id="js-news" class="js-hidden">
            @foreach(Article::all()->take(10) as $news_article)
              <li class="news-item"><a href="/articles/{{ $news_article->id }}">{{ $news_article->title }}</a></li>
            @endforeach
         </ul>
       </div>
        <div class="social">
          <a href="http://www.youtube.com/user/One1info?feature=mhee" target="_blank"><img border="0" src="/assets/images/youtube.png"></a>
          <a href="http://twitter.com/One_1_info" target="_blank"><img border="0" src="/assets/images/twitter.png"></a>
          <a href="http://www.facebook.com/pages/ONE-1-INFo/446665265382961" target="_blank"><img border="0" src="/assets/images/facebook.png"></a>
          <a href="http://www.linkedin.com/pub/one1info-one1info/62/576/219" target="_blank"><img border="0" src="/assets/images/linkedin.png"></a>
        </div>
      </div>
    @if (!Auth::check())
      <div class="row" style="padding-bottom:0px;" >
        <div style="display:inline-block;">
            <img src="/assets/images/leaderboard.jpg" />
        </div>
        <div style="display:inline-block;float:right;background-color:#EFEFEF; padding:10px;height:70px;">
            <span><b>IS YOUR COMPANY ON ONE1INFO?</b></span>
            <p class="text-center"><a href="/join" class="btn">SIGN UP TODAY</a></p>
          
        </div>
      </div>
    @endif
      <!-- Example row of columns -->
      @if (Session::has('message'))
      <div class="status alert">
        {{ Session::get('message') }}
      </div>
      @endif
      @if (Session::has('success'))
      <div class="status success">
        {{ Session::get('success') }}
      </div>
      @endif
      @yield('main')

      <div class="row" style="clear:both;">
        <div class="col-md-12">&nbsp;</div>
      </div>


      <!-- Site footer -->
      <div class="footer">
        <div class="row">
          <div class="col-3">
            <div class="title">Media center</div>
            <ul class="link-well">
              <li><a href="/companies">Business</a></li>
              <li><a href="/articles">Events</a></li>
              @foreach(Section::all() as $one_section)
                <li><a href="/articles">{{$one_section->name}}</a></li>
              @endforeach

            </ul>
          </div>

          <div class="col-3">
            <div class="title">Business directory</div>
            <ul class="link-well">
              <li><a href="/companies">View business directory</a></li>
              <li><a href="/register">List your business</a></li>
              <li><a href="/contact-us">Make a complaint</a></li>
            </ul>
          </div>

          <div class="col-3">
            <div class="title">Business Services</div>
            <ul class="link-well">
              <li><a href="/corporate">Corporate services</a></li>
              <li><a href="/register">List your business</a></li>
              <li><a href="/contact-us">Make a complaint</a></li>
            </ul>
          </div>

          <div class="col-3">
            <div class="title">Career center</div>
            <ul class="link-well">
              <li><a href="/jobs/create">Post job</a></li>
              <li><a href="/jobs">Find a job</a></li>
            </ul>
          </div>

          <div class="col-3">
            <div class="title">About one1info</div>
            <ul class="link-well">
              <li><a href="/about-us">About us</a></li>
              <li><a href="javascript:alert('Work in Progress!');">Terms &amp; conditions</a></li>
              <li><a href="/privacy-policy">Privacy policy</a></li>
              <li><a href="/contact-us">Contact us</a></li>
            </ul>
          </div>
        </div>

        <div class="copyright clearfix">
          <p>&copy; 2012 -2013 ONE1INFO. All Rights Reserved.</p>
        </div>
      </div>
    </div> <!-- /container -->
  <script>
  $(document).ready(function() {
      
    $('ul.navbar-nav').superfish();
  
  });
    $(function () {
        $('#js-news').ticker();
    });
$(function(){
  $('.datebox').datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: "1900:2012",
    dateFormat: "yy-mm-dd" 
  });
})
</script>

  </body>

</html>