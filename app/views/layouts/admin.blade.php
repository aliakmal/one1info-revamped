<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="../assets/js/html5shiv.js"></script>
	<![endif]-->
	
	<!-- Le javascript
	================================================== -->
	<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.min.js"></script>

	<style>
		body {
			padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}
    </style>

	<!-- Le styles -->
	<?php foreach ($assets['css'] as $css){?>
	<link href="<?php echo $css?>" rel="stylesheet">
	<?php }?>
	<link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/jquery.dataTables_themeroller.min.css" rel="stylesheet" />
	<link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/jquery.dataTables.min.css" rel="stylesheet" />
	<script type="text/javascript">
		function parentFormSubmit(o){
			if(confirm('Are you sure?')){
				$(o).parents('form').first().submit();
			}
		}
	</script>
	<style type="text/css">
		form ul{
			margin-left: 0px;
		}
		form ul li{
			list-style:none;
		}
	</style>
	
</head>

<body>

	<?php if(Request::segment(2) !== 'login'):?>
	<!-- Main navigation -->
	<div class="navbar navbar-fixed-top <?php echo Config::get('firadmin::navigation_inverse')?'navbar-inverse':''?>">
		<div class="navbar-inner">
			<div class="container-fluid">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="brand" href="<?php echo URL::to(Config::get('firadmin::project_url'))?>">One1Info</a>


<div class="nav-collapse collapse">
	<ul class="nav">
				<?php 
					$map = array(	'Company Reviews'=>'Company_review', 
									'Press Releases'=>'Press_release',
									'Patents'=>'Patent',
									//'Patent Investments'=>'Patent_investment',
									//'Companies'=>'Company',
									'Videos'=>'Video'
									);
					foreach($map as $ii =>$vv){
						$class = (($vv::where('is_published', '<>', '1')->count() > 0)?'label-important':'label-warning');
						$html = ' <span class="label '.$class.'">'.$vv::where('is_published', '<>', '1')->count().'</span>';
						echo '<li><a href="/admin/'.strtolower($vv).'s">'.$ii.$html.'</a></li>';
						
					}

					$class = ((Company::where('publish', '<>', '1')->count() > 0)?'label-important':'label-warning');
					$html = '<span class="label '.$class.'">'.Company::where('publish', '<>', '1')->count().'</span>';
					echo '<li><a  href="/admin/companies/">Companies '.$html.'</a></li>';
					
					$class = ((Patent_investment::where('status', '<>', 'completed')->count() > 0)?'label-important':'label-warning');
					$html = '<span class="label '.$class.'">'.Patent_investment::where('status', '<>', 'completed')->count().'</span>';
					echo '<li><a  href="/admin/patent_investments">Patent investments '.$html.'</a></li>';

				?>
</ul>

				</div>

				<div class="nav-collapse collapse">
					<ul class="nav pull-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-user <?php echo Config::get('firadmin::navigation_inverse')?'icon-white':''?>"></span> <?php echo !empty(Auth::user()->username)?Auth::user()->username:''?> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo URL::to(Config::get('firadmin::route.user') . '/' .  (!empty(Auth::user()->id)?Auth::user()->id:''))?>"><?php echo Lang::get('firadmin::admin.profile')?></a></li>
								<li><a href="<?php echo URL::to(Config::get('firadmin::route.logout'));?>"><?php echo Lang::get('firadmin::admin.logout')?></a></li>
               				</ul>
              			</li>
					</ul>
					
					
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	

	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span2">
				<div class="well sidebar-nav">
				<ul class="nav nav-list">
					<?php foreach ($navigation as $uri => $title):?>
						<?php if($title=='-'):?>
							<li class="nav-header"><?php echo $uri;?></li>
						<?php else:?>
							<li <?php echo ($active_menu == $uri)?'class="active"':'';?>>
								<a href="<?php echo URL::to($uri);?>"><?php echo $title;?>
								<?php 
									$map = array(	'Company Reviews'=>'Company_review', 
													'Press Releases'=>'Press_release',
													'Patents'=>'Patent',
													//'Patent Investments'=>'Patent_investment',
													//'Companies'=>'Company',
													'Videos'=>'Video'
													);
									foreach($map as $ii =>$vv){
										if($ii == $title){
											if($vv::where('is_published', '<>', '1')->count() > 0){
												echo '<span class="label label-warning">'.$vv::where('is_published', '<>', '1')->count().'</span>';
											}
										}
									}

									if($title =='Companies'){
										if(Company::where('publish', '<>', '1')->count() > 0){
											echo '<span class="label label-warning">'.Company::where('publish', '<>', '1')->count().'</span>';
										}
									}

									if($title =='Patent_investments'){
										if(Patent_investment::where('status', '<>', 'completed')->count() > 0){
											echo '<span class="label label-warning">'.Patent_investment::where('status', '<>', 'completed')->count().'</span>';
										}
									}

								?>
								</a>
							</li>
						<?php endif;?>
					<?php endforeach;?>
				</ul>
				</div>

			</div>
			<div class="span10">
				
				{{$content}}
			</div>
		</div>

	</div>
<?php else:?>
	<div style="text-align:center; margin:20px;" >
		<img src="/assets/images/logo.png" />
	</div>
	{{$content}}
  <?php endif;?>
 	
	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php foreach ($assets['js'] as $js){?>
	<script src="<?php echo $js?>"></script>
	<?php }?>
  
</body>
</html>