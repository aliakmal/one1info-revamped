@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">

    <h1>Create Corporate Service</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'admin.corporate_services.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


