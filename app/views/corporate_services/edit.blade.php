@extends('layouts.scaffold')

@section('main')

<h1>Edit Corporate Service</h1>
{{ Form::model($corporate_service, array('method' => 'PATCH', 'route' => array('corporate_services.update', $corporate_service->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('corporate_services.show', 'Cancel', $corporate_service->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
