@extends('layouts.scaffold')

@section('main')

<h1>Show Corporate Service</h1>

<p>{{ link_to_route('admin.corporate_services.index', 'Return to all corporate services') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $corporate_service->name }}}</td>
                    <td>{{ link_to_route('corporate_services.edit', 'Edit', array($corporate_service->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('corporate_services.destroy', $corporate_service->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
