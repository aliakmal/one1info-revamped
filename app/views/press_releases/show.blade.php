@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
    <h1>{{$article->title}}</h1>
    <p>
      <small>Date: {{$article->created_at}}</small>
    </p>
    <div id="carousel-example-generic" class="carousel  " data-ride="carousel">
      <div class="thumbnail">
        <?php if(count( $article->photos)>0):?>
          <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $article->photos->first()->getImageUrl('385', '385'); ?>"  />
        <?php endif;?>

        <div class="caption">
          {{$article->caption}}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4" style="width:200px;">
        <h2> {{ link_to('press_releases/', 'More Press Releases'); }}</h2>
          <?php $flag = 0; ?> 
          @foreach(Press_release::all()->take(5) as $article)
          <div style="padding:3px;"><b style="margin-bottom:3px;"><a href="{{URL::to('press_releases', array($article->id))}}">{{ StringHelper::trimIt($article->title, 30) }}</a></b>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>

          @endforeach
          <div class="single-element blue-link" style="border-top:0px;">{{ link_to('articles/', 'more', array('class'=>'blue-link')); }}</div>

      </div>
      <div class="col-md-5 text-content">
        {{ html_entity_decode($article->body) }}
        <p>
        {{ StringHelper::youtube($article->youtube_url) }}
      </p>

      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
      <img src="/assets/images/box-1.jpg" />
      <img src="/assets/images/box-2.jpg" />
    </div>
    </div>
    <p>&nbsp;</p><br/>    
  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    <img src="/assets/images/tall-01.jpg" />

    <div class="row">
      <div class="col-md-12">
        <h4> Latest From {{ $sections->first()->name }}</h2>
          <?php $flag = 0; ?> 
          @foreach($sections->first()->articles as $article)
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl(); ?>" height="50" />
          <div><b style="margin-bottom:3px;">{{$article->title}}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>

          @endforeach
      </div>
    </div>
  </div>
</div>
@stop
