@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">
<script src="/packages/wysiwyg/ckeditor/ckeditor/ckeditor.js?t=C9A85WF" type="text/javascript"></script>

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page">
    <div class="well">
        @include('partials.profile-menu')
    <div class="s-content">
        @include('partials.profile-mini')
@if ($errors->any())
<ul>
  {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</ul>
@endif

    {{ Form::model($press_release, array('route' => array('profile.press_releases.update', $press_release->id), 'enctype'=>"multipart/form-data")) }}
    <ul>
      <li>
        {{ Form::label('title', 'Title:') }}
        {{ Form::text('title', $press_release->title, array('class'=>'input-xxlarge')) }}
      </li>
      <li>
        {{ Form::label('images', 'Images:') }}
        <input name="photos[]" type="file" />
        @foreach($press_release->photos()->get() as $one_photo)
            <br/><img src="{{$one_photo->getImageUrl('100', '100')}}" >
            <input type="checkbox" name="delete_current_images[]" value="{{ $one_photo->id }}" />
            Delete?
            <br/>
        @endforeach

      </li>
      <li>
          {{ Form::label('youtube_url', 'Youtube_url:') }}
          {{ Form::text('youtube_url') }}
      </li>

      <li>
          {{ Form::label('dated', 'Dated:') }}
          {{ Form::text('dated', $press_release->dated, array('class'=>'datebox')) }}
      </li>
      <li>
        {{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body' ) }}        
      </li>
      <li>
        {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
      </li>
    </ul>
    {{ Form::close() }}
  </div>
</div>
  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>


@stop
<script >
$(function(){
    CKEDITOR.replace('body', {
    toolbar: 'Basic',
    uiColor: '#9AB8F3'
});

  $('#link-add-image').click(function(){
    
    $('#image-upload-holder').append('<li>'+$('#image-holder-template').html()+'</li>');
    $(document).on('click', '.lnk-remove-image', function(){
      $(this).parents('li').first().remove();
    });
  });
});
</script>
  
@stop


