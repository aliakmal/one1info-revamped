@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
<h2>
Post a release:
</h2>
<p>Thank you, your request has been received.
Our agents will contact you regarding the press release content</p>
  </div>

<div class="col-md-4">
    @include('partials.box-get-listed')
  </div>  
</div>
@stop
