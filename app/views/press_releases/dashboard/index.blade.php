@extends('layouts.master')
@section('head')
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
  <link rel="stylesheet" media="screen" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css">

@stop
@section('main')
<div class="row">
  <div class="col-md-8">
    <div class="page">
      <div class="well">
        @include('partials.profile-menu')
        <div class="s-content">
            @include('partials.profile-mini')


          <?php if(!User::find(Auth::user()->id)->hasRole('premium-company')):?>
            <div class="s-content">
              <span class="highlight">Upgrade your account to premium to be able to add press releases.</span>
              <p><a href="/upgrade" class="btn btn-info">Upgrade</a></p>
            </div>
          <?php else:?>

              <h2>Add / Delete Press Releases</h2>
              <p>You have added <span class="highlight"><?php echo $press_releases->count();?></span> out of <span class="highlight">5</span> press releases</p>
              <div>
                @foreach ($press_releases as $ii=>$press_release)
                  <a href="{{URL::to('press_releases', array($press_release->id))}}">
                      <div class="inline-holder">
                        <div style="padding:5px; color:#FFFFFF; background-color:{{$press_release->is_published==1?'green':'orange'}}">{{ $press_release->is_published==1 ? 'PUBLISHED' : 'NOT PUBLISHED' }} </div>
                        <?php if(count( $press_release->photos)>0):?>
                          <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $press_release->photos->first()->getImageUrl('385', '385'); ?>"  />
                        <?php elseif (strlen($press_release->youtube_url)>0): ?>
                          <img class="pull-left" style="padding-right:10px;" width="200" height="200" src="<?php echo StringHelper::thumbYoutube($press_release->youtube_url); ?>"  />
                        <?php endif;?>
                        <p>{{ StringHelper::trimIt($press_release->title, 60) }}</p>
                        <a href="/profile/press_releases/{{$press_release->id}}/edit" class="margin-x s-link" >Edit</a>
                        {{ Form::open(array('method' => 'DELETE', 'class'=>'inline-block', 'route' => array('profile.press_releases.destroy', $press_release->id))) }}
                          <a href="javascript::void(0)" onclick="$(this).parents('form').submit();" class=" s-link red">Delete</a>
                        {{ Form::close() }}                      
                      
                      </div>
                  </a>
                @endforeach
                <?php if($press_releases->count() != 5):?>
                <p>
                  <a href="/profile/press_releases/create" class="btn">New Press Release</a>
                </p>
                <?php endif;?>
              </div>
        <?php endif;?>

        </div>
      </div>
    </div>
  </div>
  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>


@stop