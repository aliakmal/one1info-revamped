@extends('layouts.scaffold')

@section('main')
<div class="row">
  <div class="col-md-8">

    <h1> Post a press release:</h1>
    <p>Please submit the details below then click submit. Once we recieve your request, our agents will contact you regarding the press release content.</p>
  

    {{ Form::open(array('route' => 'press_release.posted')) }}
    <ul>
      <li>
        <div class="label-span">{{ Form::label('full_name', 'Full Name:') }}<span class="required">*</span></div>
        {{ Form::text('full_name', '') }}
     
     <span class="errors">{{ $errors->first('full_name') }}</span> </li>

      <li>
        <div class="label-span">{{ Form::label('company', 'Company:') }}<span class="required">*</span></div>
        {{ Form::text('company') }}
        <span class="errors">{{ $errors->first('company') }}</span>
      </li>

      <li>
        <div class="label-span">{{ Form::label('position', 'Your Position:') }}<span class="required">*</span></div>
        {{ Form::text('position') }}
        <span class="errors">{{ $errors->first('position') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('address', 'Address:') }}<span class="required">*</span></div>
        {{ Form::text('address') }}
        <span class="errors">{{ $errors->first('address') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('city', 'City:') }}<span class="required">*</span></div>
        {{ Form::text('city') }}
        <span class="errors">{{ $errors->first('city') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('country', 'Country:') }}<span class="required">*</span></div>
        {{ Form::select('country', Country::lists('name','name')) }}
        <span class="errors">{{ $errors->first('country') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('email', 'Email:') }}<span class="required">*</span></div>
        {{ Form::text('email') }}
        <span class="errors">{{ $errors->first('email') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('phone', 'Phone:') }}<span class="required">*</span></div>
        {{ Form::text('Phone') }}
        <span class="errors">{{ $errors->first('phone') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('fax', 'Fax:') }}</div>
        {{ Form::text('fax') }}
        <span class="errors">{{ $errors->first('fax') }}</span>
      </li>
      <li>
        <div class="label-span">{{ Form::label('mobile', 'Mobile:') }}<span class="required">*</span></div>
        {{ Form::text('mobile') }}
        <span class="errors">{{ $errors->first('mobile') }}</span>
      </li>

      <li>
        <div class="label-span">{{ Form::label('content_types', 'What type of content press release will contain?*:') }}</div>
        {{ Form::checkbox('content_types_1', 'Text', '', array('style'=>'width:20px;')) }} Text
        {{ Form::checkbox('content_types_2', 'Picture', '', array('style'=>'width:20px;')) }} Picture
        {{ Form::checkbox('content_types_3', 'Video', '', array('style'=>'width:20px;')) }} Video
      </li>
      <li>
        <div class="label-span">{{ Form::label('publish_date', 'Publish Date:') }}</div>
        {{ Form::text('publish_date', '', array('class'=>'datebox')) }}
        <span class="errors">{{ $errors->first('publish_date') }}</span>
      </li>


      <li>
        {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
      </li>
    </ul>
    {{ Form::close() }}
  </div>
<div class="col-md-4">
    @include('partials.box-get-listed')
    @foreach (Section::all()->take(2) as $section)
    <div class="row">
      <div class="col-md-12">
        <h4> Latest From {{ $section->name }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles as $article)
          <a href="{{URL::to('articles', array($article->id))}}"><img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl('70', '50'); ?>" /></a>
          <div><b style="margin-bottom:3px;">{{ link_to_route('articles.show', $article->title, array($article->id)) }}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>
 
        @endforeach
      </div>
    </div>
    @endforeach
  </div>  
</div>

  
@stop


