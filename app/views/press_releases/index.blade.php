@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">



    <h1>Press Releases</h1>
    @foreach ($press_releases as $ii=>$press_release)
      <a href="{{URL::to('press_releases', array($press_release->id))}}">
            <div class="inline-holder">
              <?php if(count( $press_release->photos)>0):?>
                <img class="pull-left" style="padding-right:10px;" width="200" src="<?php echo $press_release->photos->first()->getImageUrl('385', '385'); ?>"  />
              <?php elseif (strlen($press_release->youtube_url)>0): ?>
                <img class="pull-left" style="padding-right:10px;" width="200" height="200" src="<?php echo StringHelper::thumbYoutube($press_release->youtube_url); ?>"  />
              <?php endif;?>
              <p>{{ StringHelper::trimIt($press_release->title, 60) }}</p>
            </div>
        </a>

    @endforeach

    {{ $press_releases->links() }}





  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')
    @foreach ($sections as $section)
    <div class="row">
      <div class="col-md-12">
        <h4> Latest From {{ $section->name }}</h2>
        <?php $flag = 0; ?> 
        @foreach($section->articles as $article)
          <img class="pull-left" style="padding-right:10px;" src="<?php echo $article->photos->first()->getImageUrl('70', '50'); ?>" />
          <div><b style="margin-bottom:3px;">{{ link_to_route('articles.show', $article->title, array($article->id)) }}</b>
            <p>{{substr($article->caption, 0, 95)}}...</p>
          </div>
          <div class="clearfix" style="border-top:1px solid #cccccc;margin-bottom:5px;"></div>
 
        @endforeach
      </div>
    </div>
    @endforeach
    
    
    <div class="well">

    </div>
  </div>
</div>
<!-- Jumbotron -->
<script>
  $(function() {
    $("#featured").tabs({hide:{effect: "fade"}}).tabs("rotate", 5000, true);
  })
</script>

@stop
