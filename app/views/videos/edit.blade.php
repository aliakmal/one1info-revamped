@extends('layouts.scaffold')

@section('main')

<h1>Edit Video</h1>
{{ Form::model($video, array('method' => 'PATCH', 'route' => array('admin.videos.update', $video->id))) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>
        <li>
            {{ Form::label('providor', 'Providor:') }}
            {{ Form::select('providor', array('vimeo'=>'Vimeo', 'youtube'=>'Youtube', 'file'=>'Uploaded Video')) }}
        </li>
        <li>
            {{ Form::label('url', 'Url:') }}
            {{ Form::textarea('url') }}
        </li>
        <li>
            {{ Form::label('caption', 'Caption:') }}
            {{ Form::textarea('caption') }}
        </li>
        <li>
            {{ Form::label('is_published', 'Is_published:') }}
            {{ Form::input('number', 'is_published') }}
        </li>
        <li>
            {{ Form::label('created_by', 'Created_by:') }}
            {{ Form::input('number', 'created_by') }}
        </li>
		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('videos.show', 'Cancel', $video->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
