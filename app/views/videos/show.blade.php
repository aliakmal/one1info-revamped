@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
    <h1>{{$video->title}}</h1>
    <p>
      <small>Date: {{$video->created_at}}</small>
    </p>
    <div id="carousel-example-generic" class="carousel  " data-ride="carousel">
      <div class="thumbnail">
        <?php echo $video->getEmbedCode();?>
        <div class="caption">
          
        </div>
      </div>
    </div>
    <div class="row">
      <div class="page col-md-12">{{$video->caption}}</div>
    </div>
  </div>
  <div class="col-md-4">
    @include('partials.box-get-listed')

    
    
  </div>
</div>
@stop
