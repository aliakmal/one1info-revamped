@extends('layouts.scaffold')

@section('main')
<div class="row-fluid">
  <div class="span12">

    <h1>Create Video</h1>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">

{{ Form::open(array('route' => 'admin.videos.store')) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title', '', array('class'=>'input-xxlarge')) }}
        </li>
        <li>
            {{ Form::label('providor', 'Providor:') }}
            {{ Form::select('providor', array('vimeo'=>'Vimeo', 'youtube'=>'Youtube', 'file'=>'Uploaded Video')) }}
        </li>

        <li>
            {{ Form::label('url', 'Url/Video ID:') }}
            {{ Form::text('url', '', array('class'=>'input-xxlarge')) }}
        </li>
        <li>
            {{ Form::label('caption', 'Caption:') }}
            {{ Form::textarea('caption', '', array('class'=>'input-xxlarge')) }}
        </li>
        <li>
            {{ Form::label('is_published', 'Is Published:') }}
            {{ Form::select('is_published', array(0=>'Do Not Publish', 1=>'Published')) }}
        </li>

    	<li>
    		{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
    	</li>
	</ul>
{{ Form::close() }}
  </div>
</div>

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


