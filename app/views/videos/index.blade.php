@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-12">




<div id="featured" >  
    <ul class="ui-tabs-nav">  
      <?php $flag = 0; ?>
      @foreach ($featured_videos as $video)
        <li class="ui-tabs-nav-item" id="nav-fragment-{{$video->id}}"><a href="#fragment-{{$video->id}}"><span>{{$video->title}}</span></a></li>  
          <?php $flag = 1;?>
        </li>
      @endforeach
    </ul>
    @foreach ($featured_videos as $video)

      <div id="fragment-{{$video->id}}" class="ui-tabs-panel" style="">  
        <?php echo $video->getEmbedCode();?>
      </div>
    @endforeach

</div>  

    @foreach ($sections as $ii=>$section)
      <?php 
      ?>
      <div class="niblet ">
        <div class="row">
        <h2>{{ link_to('videos/', $section->name, array('section'=>$section->id), $secure = null); }}</h2>
        @foreach($section->videos->take(5) as $video)
          <div class="video-niblet">
            <a href="{{URL::to('videos', array($video->id))}}">
              <div class="overlay"><img src="/assets/images/play-video.png" /></div>
              <img class="pull-left" style="padding-right:10px;" src="<?php echo $video->getThumbnailUrl(); ?>" height="135" />
            </a>
          <b style="margin-bottom:3px;">{{ link_to_route('videos.show', $video->title, array($video->id)) }}</b>
        </div>
        @endforeach
      </div>
        <div class="single-element blue-link">{{ link_to('videos/', 'more', array('section'=>$section->id), $secure = null); }}</div>

      </div>
    @endforeach




  </div>

</div>
<!-- Jumbotron -->
<script>
  $(function() {
    $("#featured").tabs({hide:{effect: "slide"}});
  })
</script>

@stop


