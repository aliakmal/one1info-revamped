<div class="well">
      <h2>Search Directory</h2>
      <div class="content">
    	{{ Form::open(array('url'=>'/companies', 'method'=>'get', 'class'=>'mini-form')) }}
	    	<p>
				Keyword <br/>{{ Form::text('search', Input::get('search'), array("placeholder" => "Business / Company / Product")) }}</td>
			</p>
			<p>
				Category <br/>{{ Form::select('category', array(''=>'Select Business')+Category::lists('name', 'id'), Input::get('category'), array("placeholder" => "All Categories", 'style'=>'width:260px;', 'width'=>'260')) }}</td>
			</p>
			<p>
				Country <br/>{{ Form::select('country', array(''=>'Select Country')+Country::lists('name', 'id'), Input::get('country'), array("placeholder" => "All Countries")) }}</td>
			</p>
			{{ Form::submit('SEARCH', array('class' => 'btn ')) }}

	    {{ Form::close() }}
	</div>
</div>