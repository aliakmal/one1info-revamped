@if (!Auth::check())
<div class="well">
      <h2>Get Listed</h2>
      <div class="content ">
      	<div class="mini-form">
    Get your business listed at our business directory and benefit from our featured services. </div>
      <p class="text-center"><a class="btn btn-primary" href="/options" role="button">View details &raquo;</a></p>
       <div class="clearfix" style="margin-bottom:5px;"></div>
       <p></p>
   </div>

    </div>
@endif