<div>
	<ul class="horizontal-pill-menu">
		<?php 
			$menu = array(
				//"/profile/view"=>'VIEW PROFILE',
				"/profile/edit"=>'EDIT PROFILE',
				"/location/edit"=>'EDIT MAP',
				"/profile/press_releases"=>'PRESS RELEASES',
				"/profile/company_interviews"=>'MEDIA',
				"/profile/users"=>'USERS',
				//"/logout"=>'LOGOUT'
			);

			foreach($menu as $ii=>$vv):
			echo '<li  '.($current == $ii?' class="current"':'').'>';
				echo '<a href="'.$ii.'">'.$vv.'</a>';
			echo '</li>';

			endforeach;

		?>

	</ul>

</div>