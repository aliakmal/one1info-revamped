<div class="well-square">
		<h2>Classification</h2>
      <div class="content">
      	<?php $categories = Category::all();?>
      	@foreach($categories as $one_category)
      		@if($one_category->companies()->count()>0)
      		  <p>{{ link_to('/companies?category='.$one_category->id, $one_category->name.'('.$one_category->companies()->count().')', array('category'=>$one_category->id) ) }}</p>
      		@endif
      	@endforeach

    </div>
</div>    