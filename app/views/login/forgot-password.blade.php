@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
  <div class="page" >

    <div class=" well-square s-content">

    	<form class="form-signin " method="post">
    		<?php echo Form::token();?>
    		<h3 class="form-signin-heading"><?php echo Lang::get('firadmin::admin.retrieve-password')?></h3>
    		<p>Enter your accounts email address</p>
    		<?php echo View::make('firadmin::partials.form-message')?>
    		<ul>
    		  <li>
            <label>Email</label><input type="text" name="email" class="input-block-level" placeholder="<?php echo Lang::get('firadmin::admin.email')?>"></li>
    		  <li>
            
        		<button class="btn btn-primary" type="submit"><?php echo Lang::get('firadmin::admin.send-reminder')?></button>
            <a href="/sign-in"><?php echo Lang::get('firadmin::admin.cancel')?></a>
          </li>
        </ul>
    	</form>
    	
    </div> 

  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop