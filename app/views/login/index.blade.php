@extends('layouts.master')
@section('main')
<div class="row">
  <div class="col-md-8">
	<div class="page">
    <div class="well-square">
      <div class="s-content">
		<h2>Sign In To Your Account</h2>
    <p>
      <span class="highlight">*Enter username and password</span>
    </p>
    
    <h3 class="form-signin-heading">Please sign in</h3>
    <div class="well-square" style="padding-bottom:0px;">
      <div class="well-square" style="margin-bottom:0px" >
        <form class="form-signin s-content" method="post" >
          <?php echo Form::token();?>
          
          
          <?php echo View::make('firadmin::partials.form-message')?>
          <ul>
            <li>
              <label>Username</label>
              <input type="text" name="username" class="input-block-level" placeholder="Username">
            </li>
            <li>
              <label>Password</label>
              <input type="password" name="password" class="input-block-level" placeholder="Password"></li>
            <li><button class="btn btn-primary" type="submit">Sign In</button>
                <label style="width:200px">
                  <input type="checkbox" style="width:20px" name="remember-me" value="1">Remember Me
                </label>
            </li>
          </ul>
        </form>
        
      </div>
<div class="s-content">
          <a href="/forgot-password" class="blue-link">Forgot Password?</a> - <a href="/join" class="blue-link">Become a Member</a>
        </div>
    </div>



</div>

</div>










	</div>
  </div>	
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop