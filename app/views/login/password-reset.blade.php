@extends('layouts.master')
@section('main')

<style type="text/css">
form > ul > li > label {
    display: inline-block;
    vertical-align: top;
    width: 25%;
}
</style>
<div class="row">

  <div class="col-md-8">
  <div class="page" >

    <div class=" well-square s-content">

    <form class="form-signin" method="post">
  
    <?php echo Form::token();?>
    <input type="hidden" name="token" value="{{ $token }}">
    
    <h3 class="form-signin-heading"><?php echo Lang::get('firadmin::admin.reset-your-password')?></h3>
    
    <?php echo View::make('firadmin::partials.form-message')?>
    
    <ul>
      <li>
        <label><?php echo Lang::get('firadmin::admin.email')?></label>
        <input type="text" name="email" class="input-block-level">    
      </li>
      <li>
        <label><?php echo Lang::get('firadmin::admin.new-password')?></label>
        <input type="password" name="password" class="input-block-level">
      </li>
      <li>
        <label><?php echo Lang::get('firadmin::admin.new-password_confirmation')?></label>    
        <input type="password" name="password_confirmation" class="input-block-level">
      </li>
      <li><button class="btn btn-primary" type="submit"><?php echo Lang::get('firadmin::admin.reset')?></button></li>
    </ul>
  </form>

    </div> 

  </div>
  </div>  
  <div class="col-md-4">
    @include('partials.box-get-listed')
  </div>
</div>
@stop