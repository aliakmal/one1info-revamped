@extends('layouts.scaffold')

@section('main')
<row>
  <span12>
    

<h1>All Countries</h1>


@if ($countries->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($countries as $country)
				<tr>
					<td>{{{ $country->name }}}</td>
                    
				</tr>
			@endforeach
		</tbody>
	</table>

    {{ $countries->links() }}

@else
	There are no countries
@endif
  </span12>  
</row>

@stop
