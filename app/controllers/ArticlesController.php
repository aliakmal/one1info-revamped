<?php
use \BaseController;

class ArticlesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($category = '')
	{
		if($category != ''):
			$section = Section::where('name', '=', $category)->first();

	    	$articles = Article::where('section_id', '=', $section->id)->paginate(15);//->get();

	    	$title = $section->name;
    	else:
    		$articles = Article::paginate(15);
    		$title = 'News';
    	endif;
		$featured_articles = $articles->take(5);

		$sections = Section::all();

		$this->layout->content =  View::make('articles.index', array('sections' => $sections, 'articles' => $articles, 'featured_articles'=>$featured_articles, 'title'=>$title));
	}

	public function search()
	{
		$articles = Article::query();//$this->job->all();
		if( Input::get('search')!=''){
			$articles = $articles->where('title', 'like', Input::get('search').'%');//$this->job->all();
		}

		$articles = $articles->get();

		return View::make('articles.list', compact('articles'));
	}


	public $active_menu = '';
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.master');		
		
		//Set navigation
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		
		//Set application title
		$this->layout->title = 'Test';
		
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
      $article = Article::find($id);
      $sections = Section::all();

      $this->layout->content =  View::make('articles.show', array('article'=>$article, 'sections'=>$sections));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('articles.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
