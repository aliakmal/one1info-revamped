<?php
use \BaseController;


class CompaniesController extends BaseController {

	/**
	 * Company Repository
	 *
	 * @var Company
	 */
	protected $company;
	public $active_menu = 'admin/companies';

	public function __construct(Company $company)
	{
		$this->company = $company;
	}
  
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.master');		
		
		//Set navigation
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		
		//Set application title
		$this->layout->title = 'Test';
		
	}

  
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = Company::query();//$this->company->all();

		$country = null;
		$category = null;

		if( Input::get('search')!=''){
			$companies = $companies->where('company_name', 'like', Input::get('search').'%');
		}

		if(Input::get('category')!=''){
			$category = Category::find(Input::get('category'));
		}

		if(Input::get('country')!=''){
			$companies = $companies->where('country_id', '=', Input::get('country'));//$this->job->all();
			$country = Country::find(Input::get('country'));
		}

		if(Input::get('city')!=''){
			$companies = $companies->where('city', '=', Input::get('city'));//$this->job->all();
		}

		$sort = Input::get('sort')==''?'asc':Input::get('sort');

		$companies = $companies->orderBy('company_name', $sort)->paginate(15);//->get();



		$this->layout->content =  View::make('companies.index', array('companies'=>$companies, 'country'=>$country, 'category'=>$category));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company = $this->company->findOrFail($id);

		$this->layout->content =  View::make('companies.show', compact('company'));
	}


}
