<?php

use \BaseController;

class Company_interviewsController extends BaseController {

	/**
	 * Company_interview Repository
	 *
	 * @var Company_interview
	 */
	protected $company_interview;

	public function __construct(Company_interview $company_interview)
	{
		$this->company_interview = $company_interview;
	}

	public function getCSV(){

		$rows = Company_interview::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


	public $active_menu = 'admin/company_interviews';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.master');		
		//Set navigation
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$company_interviews = $this->company_interview->all();
		$this->layout->content = View::make('administration.company_interviews.index', compact('company_interviews'));
	}

	public function my()
	{
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();

		$company_interviews = Company_interview::where('company_id', '=', $company->id)->get();

		$this->layout->content = View::make('company_interviews.dashboard.index', array('company_interviews' => $company_interviews,'company'=>$company, 'current'=>'/profile/company_interviews'));
	}


	public function getDatatable()
	{
		$posts = $this->company_interview->select(array('company_interviews.id', 'company_interviews.title', 'company_interviews.company_id', 'company_interviews.youtube_url', 'company_interviews.dated', 'company_interviews.is_published'));

		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.company_interviews.edit\', array( $id )) }}" class="btn btn-info">edit</a>
							{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.company_interviews.destroy\', $id))) }}
                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$this->layout->content =  View::make('company_interviews.create', array('company'=>$company, 'current'=>'/profile/company_interviews'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['is_published'] = 0;
		$user = User::find(Auth::user()->id);

		$company = $user->company()->first();
		$input['company_id'] = $company->id;
		$validation = Validator::make($input, Company_interview::$rules);

		if ($validation->passes())
		{
			$this->company_interview->create($input);

			return Redirect::route('profile.company_interviews');
		}

		return Redirect::route('profile.company_interviews.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company_interview = $this->company_interview->findOrFail($id);

		$this->layout->content = View::make('administration.company_interviews.show', compact('company_interview'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company_interview = $this->company_interview->find($id);
		$user = User::find(Auth::user()->id);

		$company = $user->company()->first();

		if (is_null($company_interview))
		{
			return Redirect::route('profile.company_interviews');
		}

		$this->layout->content = View::make('company_interviews.edit', array('company'=>$company, 'company_interview'=>$company_interview, 'current'=>'/profile/company_interviews'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Company_interview::$rules);
		$input['is_published'] = 0;

		if ($validation->passes())
		{
			$company_interview = $this->company_interview->find($id);
			$company_interview->update($input);

			return Redirect::route('profile.company_interviews');
		}

		return Redirect::route('profile.company_interviews.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->company_interview->find($id)->delete();

		return Redirect::route('profile.company_interviews.index');
	}
}
