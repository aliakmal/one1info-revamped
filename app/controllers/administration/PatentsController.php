<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Patent;
use Photo, Datatables, CSV;

class PatentsController extends BaseController {

	/**
	 * Patent Repository
	 *
	 * @var Patent
	 */
	protected $patent;

	public function __construct(Patent $patent)
	{
		$this->patent = $patent;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$patents = $this->patent->all();

		$this->layout->content =  View::make('administration.patents.index', compact('patents'));
	}

	public function getCSV(){

		$rows = Patent::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

	public $active_menu = 'admin/patents';

	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}


	public function getDatatable()
	{

		$posts = $this->patent->select(array('patents.id', 'patents.First_name','patents.Last_name','patents.Address','patents.City','patents.Country','patents.Email','patents.Phone','patents.Fax','patents.Inv_name','patents.Inv_field','patents.Inv_details','patents.Has_patent','patents.Need_register','patents.Patent_area','patents.Need_publish','patents.Need_investor','patents.Dated','patents.Is_published'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.patents.edit\', array( $id )) }}" class="btn btn-info">edit</a>
																{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.patents.destroy\', $id))) }}
									                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
									                        	{{ Form::close() }}')->make();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.patents.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Patent::$rules);

		if ($validation->passes())
		{
			$this->patent->create($input);

			return Redirect::route('admin.patents.index');
		}

		return Redirect::route('admin.patents.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$patent = $this->patent->findOrFail($id);

		$this->layout->content =  View::make('administration.patents.show', compact('patent'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patent = $this->patent->find($id);

		if (is_null($patent))
		{
			return Redirect::route('admin.patents.index');
		}

		$this->layout->content =  View::make('administration.patents.edit', compact('patent'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Patent::$rules);

		if ($validation->passes())
		{
			$patent = $this->patent->find($id);
			$patent->update($input);

			return Redirect::route('admin.patents.show', $id);
		}

		return Redirect::route('admin.patents.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->patent->find($id)->delete();

		return Redirect::route('admin.patents.index');
	}

}
