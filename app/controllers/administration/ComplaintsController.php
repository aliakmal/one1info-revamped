<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Complaint, CSV;
use Photo, Datatables;

class ComplaintsController extends BaseController {

	/**
	 * Complaint Repository
	 *
	 * @var Complaint
	 */
	protected $complaint;

	public function __construct(Complaint $complaint)
	{
		$this->complaint = $complaint;
	}

	public function getCSV(){

		$rows = Complaint::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

	public $active_menu = 'admin/complaints';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$complaints = $this->complaint->all();

		$this->layout->content =  View::make('administration.complaints.index', compact('complaints'));
	}
	public function getDatatable()
	{

		$posts = $this->complaint->select(array('complaints.id','complaints.first_name','complaints.last_name','complaints.gender','complaints.company','complaints.position','complaints.address','complaints.city','complaints.country','complaints.email','complaints.mobile','complaints.phone','complaints.fax','complaints.complaint_against','complaints.compaint_type','complaints.product','complaints.invoice_no','complaints.amount_in_dispute','complaints.problem_date','complaints.complaint_title','complaints.complain_details','complaints.expected_resolution','complaints.dated'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.complaints.edit\', array( $id )) }}" class="btn btn-info">edit</a>

													{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.complaints.destroy\', $id))) }}
						                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
						                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.complaints.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Complaint::$rules);

		if ($validation->passes())
		{
			$this->complaint->create($input);

			return Redirect::route('admin.complaints.index');
		}

		return Redirect::route('admin.complaints.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$complaint = $this->complaint->findOrFail($id);

		$this->layout->content =  View::make('administration.complaints.show', compact('complaint'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$complaint = $this->complaint->find($id);

		if (is_null($complaint))
		{
			return Redirect::route('admin.complaints.index');
		}

		$this->layout->content =  View::make('administration.complaints.edit', compact('complaint'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Complaint::$rules);

		if ($validation->passes())
		{
			$complaint = $this->complaint->find($id);
			$complaint->update($input);

			return Redirect::route('admin.complaints.show', $id);
		}

		return Redirect::route('admin.complaints.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->complaint->find($id)->delete();

		return Redirect::route('admin.complaints.index');
	}

}
