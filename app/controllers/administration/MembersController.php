<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Member, CSV, Datatables;


class MembersController extends BaseController {

	/**
	 * Member Repository
	 *
	 * @var Member
	 */
	protected $member;
	public $active_menu = 'admin/members';

	public function __construct(Member $member)
	{
		$this->member = $member;
	}

	public function getCSV(){

		$rows = Member::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts/admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$members = $this->member->paginate(15);//all();

		$this->layout->content = View::make('administration.members.index', compact('members'));
	}

	public function getDatatable()
	{
		$posts = $this->member->select(array('members.id','members.first_name','members.last_name','members.email','members.gender','members.mobile','members.address','members.state','members.country_id','members.dob','members.title','members.profession','members.interests'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.members.edit\', array( $id )) }}" class="btn btn-info">edit</a>
																{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.members.destroy\', $id))) }}
									                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
									                        	{{ Form::close() }}')->make();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.members.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Member::$rules);

		if ($validation->passes())
		{
			$this->member->create($input);

			return Redirect::route('admin.members.index');
		}

		return Redirect::route('admin.members.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$member = $this->member->findOrFail($id);

		$this->layout->content = View::make('administration.members.show', compact('member'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$member = $this->member->find($id);

		if (is_null($member))
		{
			return Redirect::route('admin.members.index');
		}

		$this->layout->content = View::make('administration.members.edit', compact('member'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Member::$rules);

		if ($validation->passes())
		{
			$member = $this->member->find($id);
			$member->update($input);

			return Redirect::route('admin.members.show', $id);
		}

		return Redirect::route('admin.members.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->member->find($id)->delete();

		return Redirect::route('admin.members.index');
	}

}
