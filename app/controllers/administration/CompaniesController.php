<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Company, CSVHelper, Industry, Country, CSV, Datatables;

class CompaniesController extends BaseController {

	/**
	 * Company Repository
	 *
	 * @var Company
	 */
	protected $company;
	public $active_menu = 'admin/companies';

	public function __construct(Company $company)
	{
		$this->company = $company;
	}
  	public function getCSV(){

		$rows = Company::all()->toArray();  //use eloquent to get array of all users in 'users' table
		$posts = Company::select(array(	'companies.id', 'companies.company_name',	'companies.known_as',	'companies.contact_person',	'companies.email',	
										'companies.website',	'companies.phone',	'companies.mobile',	'companies.fax',	'countries.name as country_name',
										'origin_countries.name as origin_country_name',	'companies.licence_type',	'companies.established',	'companies.num_employees',	
										'companies.address',	'companies.city',	'companies.postal_code',	'industries.name as industries_name',	'companies.products',	
										'companies.services',	'companies.profile',	'companies.subscription',	'companies.publish',	'companies.order',	
										'companies.location_x',	'companies.location_y'))
								->leftJoin('industries', 'industries.id','=','companies.industry_id')
								->leftJoin('countries', 'companies.country_id','=','countries.id')
								->leftJoin('countries as origin_countries', 'companies.origin_country_id','=','origin_countries.id')
								->groupBy('companies.id')->get()->toArray();

		return CSV::fromArray($posts)->render();  //download as csv
	}

  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

  
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = $this->company->paginate(20);//all();
		$this->layout->content =  View::make('administration.companies.index', compact('companies'));
	}
	
	public function getDatatable()
	{
		$posts = $this->company->select(array(	'companies.id', 'companies.company_name',	'companies.known_as',	'companies.contact_person',	'companies.email',	
												'companies.website',	'companies.phone',	'companies.mobile',	'companies.fax',	'countries.name as country_name',
												'origin_countries.name as origin_country_name',	'companies.licence_type',	'companies.established',	'companies.num_employees',	
												'companies.address',	'companies.city',	'companies.postal_code',	'industries.name',	'companies.products',	
												'companies.services',	'companies.profile',	'companies.subscription',	'companies.publish',	'companies.order',	
												'companies.location_x',	'companies.location_y'))
										->leftJoin('industries', 'industries.id','=','companies.industry_id')
										->leftJoin('countries', 'companies.country_id','=','countries.id')
										->leftJoin('countries as origin_countries', 'companies.origin_country_id','=','origin_countries.id')
										->groupBy('companies.id');
		return Datatables::of($posts)->add_column('operations', '

				{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.companies.destroy\', $id))) }}
																<div class="btn-group">
																	<a href="{{ URL::route( \'admin.companies.edit\', array( $id )) }}" class="btn btn-mini">edit</a>
																	<a href="{{ URL::route( \'admin.companies.show\', array( $id )) }}" class="btn btn-mini">View</a>
									                            	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn btn-mini">Delete</a>
								                            	</div>
									                        	{{ Form::close() }}', 0)
										->edit_column('publish', '{{$publish==0 ? \'<span class="badge badge-important">NOT PUBLISHED</span>\':\'<span class="badge badge-success">PUBLISHED</span>\'}}')
										->add_column('press_releases', 
													'@if(Company::find($id)->press_releases()->count() > 0)
														{{ link_to_route(\'admin.press_releases.index\',\'Press releases(\'.Company::find($id)->press_releases()->count().\')\', array(\'company_id\'=>$id)  ) }}
													@else
														<i> No Press Releases </i>
													@endif', 1)
										->add_column('complaints', 
													'@if(Company::find($id)->complaints()->count() > 0)
														{{ link_to_route(\'admin.complaints.index\',\'Complaints(\'.Company::find($id)->complaints()->count().\')\', array(\'company_id\'=>$id)  ) }}
													@else
														<i> No complaints </i>
													@endif', 2)
										->add_column('company_interviews', 
													'@if(Company::find($id)->company_interviews()->count() > 0)
														{{ link_to_route(\'admin.company_interviews.index\',\'Interviews(\'.Company::find($id)->company_interviews()->count().\')\', array(\'company_id\'=>$id)  ) }}
													@else
														<i> No company interviews </i>
													@endif', 3)
										->add_column('jobs', 
													'@if(Company::find($id)->jobs()->count() > 0)
														{{ link_to_route(\'admin.jobs.index\',\'Jobs(\'.Company::find($id)->jobs()->count().\')\', array(\'company_id\'=>$id)  ) }}
													@else
														<i> No Jobs </i>
													@endif', 4)
										->add_column('patent_investments', 
													'@if(Company::find($id)->patent_investments()->count() > 0)
														{{ link_to_route(\'admin.patent_investments.index\',\'Patent investments(\'.Company::find($id)->patent_investments()->count().\')\', array(\'company_id\'=>$id)  ) }}
													@else
														<i> No patent investments </i>
													@endif', 5)
		->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.companies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Company::$rules);

		if ($validation->passes())
		{
			$this->company->create($input);

			return Redirect::route('admin.companies.index');
		}

		return Redirect::route('admin.companies.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company = $this->company->findOrFail($id)->select(array(	'companies.id', 'companies.company_name',	'companies.known_as',	'companies.contact_person',	'companies.email',	
												'companies.website',	'companies.phone',	'companies.mobile',	'companies.fax',	'countries.name as country_name',
												'origin_countries.name as origin_country_name',	'companies.licence_type',	'companies.established',	'companies.num_employees',	
												'companies.address',	'companies.city',	'companies.postal_code',	'industries.name as industry_name',	'companies.products',	
												'companies.services',	'companies.profile',	'companies.subscription',	'companies.publish',	'companies.order',	
												'companies.location_x',	'companies.location_y'))
										->join('industries', 'industries.id','=','companies.industry_id')
										->join('countries', 'companies.country_id','=','countries.id')
										->join('countries as origin_countries', 'companies.origin_country_id','=','origin_countries.id')->get()->first();


		$this->layout->content =  View::make('administration.companies.show', compact('company'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = $this->company->find($id);

		if (is_null($company))
		{
			return Redirect::route('admin.companies.index');
		}

		$this->layout->content =  View::make('administration.companies.edit', compact('company'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Company::$rules);

		if ($validation->passes())
		{
			$company = $this->company->find($id);
			$company->update($input);

			return Redirect::route('admin.companies.show', $id);
		}

		return Redirect::route('admin.companies.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->company->find($id)->delete();

		return Redirect::route('admin.companies.index');
	}





	public function getImport()
	{
		$this->layout->content =  View::make('administration.companies.import');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postImport()
	{
		$input = Input::all();
		$validation = Validator::make($input, array('file'=>'required|between:0,3072'));

		if ($validation->passes())
		{
			$csvFile = new \Keboola\Csv\CsvFile($input['file']);

			$columns = array('company_name', 'known_as', 'contact_person', 'email', 'website', 'phone', 'mobile', 'fax', 'country_name', 'origin_country_name',
'licence_type', 'established', 'num_employees', 'address', 'city', 'postal_code', 'industry_name', 'products', 'services',
'profile', 'subscription', 'publish', 'order', 'location_x', 'location_y');
			$column_map = array();
			$column_row = $csvFile->getHeader();//[0];
			foreach($columns as $ii=>$field){
				$found_index = array_search($field, $column_row);
				if($found_index){
					$column_map[$field] = $found_index;	
				}
			}

			$rules = array('company_name' => 'required');

			$counter_imported = 0;

			foreach($csvFile as $ii=>$row) {
				if($ii==0)
					continue;

				$data = array();
				foreach($column_map as $field=>$index){
					if($field=='country_name'){
						$c = Country::where('name', '=', $row[$index]);
						if($c->count() > 0){
							$data['country_id'] = $c->first()->name;
						};

					}elseif($field=='origin_country_name'){
						$c = Country::where('name', '=', $row[$index]);
						if($c->count() > 0){
							$data['origin_country_id'] = $c->first()->name;
						};

					}elseif($field=='industry_name'){
						$c = Industry::where('name', '=', $row[$index]);
						if($c->count() > 0){
							$data['industry_id'] = $c->first()->name;
						};
					}else{
						$data[$field] = $row[$index];
					}
				}

				$valdation = Validator::make($data, $rules);
				if($valdation->passes()){
					if($this->company->create($data)){
						$counter_imported++;
					}
				}
			}

			return Redirect::to('admin/companies/import')
			->with('success', 'Imported '.$counter_imported.' out of '.$ii.' entries.');
		}

		return Redirect::to('admin/companies/import')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


}
