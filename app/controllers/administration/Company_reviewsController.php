<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Company_review;
use Photo, Datatables, CSV;

class Company_reviewsController extends BaseController {

	/**
	 * Company_review Repository
	 *
	 * @var Company_review
	 */
	protected $company_review;

	public function __construct(Company_review $company_review)
	{
		$this->company_review = $company_review;
	}
	public function getCSV(){

		$rows = Company_review::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$company_reviews = $this->company_review->all();
		$this->layout->content =   View::make('administration.company_reviews.index', compact('company_reviews'));
	}

	public function getDatatable()
	{
		$posts = $this->company_review->select(array('company_reviews.id', 'company_reviews.title',
												'company_reviews.company_id','company_reviews.user_id','company_reviews.rate','company_reviews.body','company_reviews.dated','company_reviews.is_published'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.company_reviews.edit\', array( $id )) }}" class="btn btn-info">edit</a>
													{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.company_reviews.destroy\', $id))) }}
						                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
						                        	{{ Form::close() }}')->make();
	}

	public $active_menu = 'admin/company_reviews';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =   View::make('administration.company_reviews.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Company_review::$rules);

		if ($validation->passes())
		{
			$this->company_review->create($input);

			return Redirect::route('admin.company_reviews.index');
		}

		return Redirect::route('admin.company_reviews.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company_review = $this->company_review->findOrFail($id);

		$this->layout->content =   View::make('administration.company_reviews.show', compact('company_review'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company_review = $this->company_review->find($id);

		if (is_null($company_review))
		{
			return Redirect::route('admin.company_reviews.index');
		}

		$this->layout->content =   View::make('administration.company_reviews.edit', compact('company_review'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Company_review::$rules);

		if ($validation->passes())
		{
			$company_review = $this->company_review->find($id);
			$company_review->update($input);

			return Redirect::route('admin.company_reviews.show', $id);
		}

		return Redirect::route('admin.company_reviews.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->company_review->find($id)->delete();
		return Redirect::route('admin.company_reviews.index');
	}

}
