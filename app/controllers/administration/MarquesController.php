<?php

namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Marque;
use Photo, CSV, Datatables;

class MarquesController extends BaseController {

	/**
	 * Marque Repository
	 *
	 * @var Marque
	 */
	protected $marque;

	protected $active_menu = 'admin/news_headers';

	public function __construct(Marque $marque)
	{
		$this->marque = $marque;
	}

  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');

		//Default layout content is null
		$this->layout->content = '';
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$marques = $this->marque->all();

		$this->layout->content = View::make('administration.marques.index', compact('marques'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.marques.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Marque::$rules);

		if ($validation->passes())
		{
			$this->marque->create($input);

			return Redirect::route('admin.news_headers.index');
		}

		return Redirect::route('admin.news_headers.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$marque = $this->marque->findOrFail($id);

		$this->layout->content =  View::make('administration.marques.show', compact('marque'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$marque = $this->marque->find($id);

		if (is_null($marque))
		{
			return Redirect::route('admin.marques.index');
		}

		$this->layout->content =  View::make('administration.marques.edit', compact('marque'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Marque::$rules);

		if ($validation->passes())
		{
			$marque = $this->marque->find($id);
			$marque->update($input);

			return Redirect::route('admin.news_headers.show', $id);
		}

		return Redirect::route('admin.news_headers.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->marque->find($id)->delete();

		return Redirect::route('admin.news_headers.index');
	}

}
