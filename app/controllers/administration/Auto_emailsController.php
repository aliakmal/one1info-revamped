<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Auto_email;
use Photo, CSV, Datatables;

class Auto_emailsController extends BaseController {

	/**
	 * Auto_email Repository
	 *
	 * @var Auto_email
	 */
	protected $auto_email;

	public function __construct(Auto_email $auto_email)
	{
		$this->auto_email = $auto_email;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$auto_emails = $this->auto_email->all();

		$this->layout->content = View::make('administration.auto_emails.index', compact('auto_emails'));
	}
	public $active_menu = 'admin/auto_emails';
  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');

		//Default layout content is null
		$this->layout->content = '';
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.auto_emails.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Auto_email::$rules);

		if ($validation->passes())
		{
			$this->auto_email->create($input);

			return Redirect::route('admin.auto_emails.index');
		}

		return Redirect::route('admin.auto_emails.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$auto_email = $this->auto_email->findOrFail($id);

		$this->layout->content = View::make('administration.auto_emails.show', compact('auto_email'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$auto_email = $this->auto_email->find($id);

		if (is_null($auto_email))
		{
			return Redirect::route('admin.auto_emails.index');
		}

		$this->layout->content = View::make('administration.auto_emails.edit', compact('auto_email'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Auto_email::$rules);

		if ($validation->passes())
		{
			$auto_email = $this->auto_email->find($id);
			$auto_email->update($input);

			return Redirect::route('admin.auto_emails.show', $id);
		}

		return Redirect::route('admin.auto_emails.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->auto_email->find($id)->delete();

		return Redirect::route('admin.auto_emails.index');
	}

}
