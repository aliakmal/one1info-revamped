<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Service, Corporate_service, CSV, Datatables;



class ServicesController extends BaseController {

	/**
	 * Corporate_service Repository
	 *
	 * @var Corporate_service
	 */
	protected $corporate_service;
  public $active_menu = 'admin/service';
	
  public function __construct(Service $corporate_service)
	{
		$this->corporate_service = $corporate_service;
	}

	public function getCSV(){

		$rows = Service::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

  
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
  
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$corporate_services = $this->corporate_service->paginate(15);//all();

		$this->layout->content = View::make('administration.services.index', compact('corporate_services'));
	}

	public function getDatatable()
	{
		$posts = $this->corporate_service->select(array('services.id', 'services.name','services.sort','services.is_published'));
						//->leftJoin('photos', function($join)
                         //{
                             //$join->on('press_releases.id', '=', 'photos.imageable_id');
                             //$join->on('press_releases.imageable_type', '=', 'Press_release');
                         //});

		return Datatables::of($posts)->edit_column('is_published', '{{$is_published=="0"?\'<span class="badge badge-important">NOT PUBLISHED</span>\':\'<span class="badge badge-success">PUBLISHED</span>\'}}')
										->add_column('operations', '
																{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.press_releases.destroy\', $id))) }}
																<div class="btn-group">
																	<a href="{{ URL::route( \'admin.services.edit\', array( $id )) }}" class="btn btn-mini">Edit</a>
									                            	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn btn-mini">Delete</a>
								                            	</div>
									                        	{{ Form::close() }}')->make();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.services.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Service::$rules);

		if ($validation->passes())
		{
			$this->corporate_service->create($input);

			return Redirect::route('admin.services.index');
		}

		return Redirect::route('admin.services.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$corporate_service = $this->corporate_service->findOrFail($id);

		$this->layout->content = View::make('administration.services.show', compact('corporate_service'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$corporate_service = $this->corporate_service->find($id);

		if (is_null($corporate_service))
		{
			return Redirect::route('admin.services.index');
		}

		$this->layout->content = View::make('administration.services.edit', compact('corporate_service'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Corporate_service::$rules);

		if ($validation->passes())
		{
			$corporate_service = $this->corporate_service->find($id);
			$corporate_service->update($input);

			return Redirect::route('admin.services.show', $id);
		}

		return Redirect::route('admin.services.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->corporate_service->find($id)->delete();

		return Redirect::route('admin.services.index');
	}

}
