<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Industry, CSV, Datatables;

class IndustriesController extends BaseController {

	/**
	 * Industry Repository
	 *
	 * @var Industry
	 */
	protected $industry;
	public $active_menu = 'admin/industries';

	public function __construct(Industry $industry)
	{
		$this->industry = $industry;
	}
  
  	public function getCSV(){

		$rows = Industry::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts/admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$industries = $this->industry->paginate(20);//all();

		$this->layout->content = View::make('administration.industries.index', compact('industries'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.industries.create');
	}

	public function getDatatable()
	{
		$posts = $this->industry->select(array('industries.id','industries.name'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.industries.edit\', array( $id )) }}" class="btn btn-info">edit</a>
													{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.industries.destroy\', $id))) }}
						                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
						                        	{{ Form::close() }}')->make();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Industry::$rules);

		if ($validation->passes())
		{
			$this->industry->create($input);

			return Redirect::route('admin.industries.index');
		}

		return Redirect::route('admin.industries.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$industry = $this->industry->findOrFail($id);

		$this->layout->content = View::make('administration.industries.show', compact('industry'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$industry = $this->industry->find($id);

		if (is_null($industry))
		{
			return Redirect::route('admin.industries.index');
		}

		$this->layout->content = View::make('administration.industries.edit', compact('industry'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Industry::$rules);

		if ($validation->passes())
		{
			$industry = $this->industry->find($id);
			$industry->update($input);

			return Redirect::route('admin.industries.show', $id);
		}

		return Redirect::route('admin.industries.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->industry->find($id)->delete();

		return Redirect::route('admin.industries.index');
	}

}
