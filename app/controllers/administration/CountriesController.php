<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Country, CSV, Datatables;

class CountriesController extends BaseController {

	/**
	 * Country Repository
	 *
	 * @var Country
	 */
	protected $country;
	public $active_menu = 'admin/countries';

	public function __construct(Country $country)
	{
		$this->country = $country;
	}
	

	public function getCSV(){

		$rows = Country::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$countries = $this->country->paginate(20);//all();

		$this->layout->content = View::make('administration.countries.index', compact('countries'));
	}
	public function getDatatable()
	{

		$posts = $this->country->select(array('countries.id','countries.name'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.countries.edit\', array( $id )) }}" class="btn btn-info">edit</a>')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.countries.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Country::$rules);

		if ($validation->passes())
		{
			$this->country->create($input);

			return Redirect::route('admin.countries.index');
		}

		return Redirect::route('admin.countries.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$country = $this->country->findOrFail($id);

		$this->layout->content =  View::make('administration.countries.show', compact('country'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$country = $this->country->find($id);

		if (is_null($country))
		{
			return Redirect::route('admin.countries.index');
		}

		$this->layout->content =  View::make('administration.countries.edit', compact('country'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Country::$rules);

		if ($validation->passes())
		{
			$country = $this->country->find($id);
			$country->update($input);

			return Redirect::route('admin.countries.show', $id);
		}

		return Redirect::route('admin.countries.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->country->find($id)->delete();

		return Redirect::route('admin.countries.index');
	}

}
