<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Press_release;
use Photo, CSV, Datatables;

class Press_releasesController extends BaseController {

	/**
	 * Press_release Repository
	 *
	 * @var Press_release
	 */
	protected $press_release;

	public function __construct(Press_release $press_release)
	{
		$this->press_release = $press_release;
	}

	public function getCSV(){

		$rows = Press_release::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$press_releases = $this->press_release->all();

		$this->layout->content =  View::make('administration.press_releases.index', compact('press_releases'));
	}
	public $active_menu = 'admin/press_releases';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	public function getDatatable()
	{
		$posts = $this->press_release->select(array('press_releases.id', 'press_releases.title','companies.company_name','press_releases.youtube_url','press_releases.dated','press_releases.type','press_releases.is_published'))
									->join('companies', 'companies.id','=','press_releases.company_id');
						//->leftJoin('photos', function($join)
                         //{
                             //$join->on('press_releases.id', '=', 'photos.imageable_id');
                             //$join->on('press_releases.imageable_type', '=', 'Press_release');
                         //});

		return Datatables::of($posts)->edit_column('is_published', '{{$is_published=="0"?\'<span class="badge badge-important">NOT PUBLISHED</span>\':\'<span class="badge badge-success">PUBLISHED</span>\'}}')
									->add_column('image',	'@if(Press_release::find($id)->photos()->count() > 0):
																{{ Photo::image_tag( Press_release::find($id)->photos()->first() ) }}
															@else:
																<i> No Image </i>
															@endif', 3
										
									)->add_column('operations', '
																{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.press_releases.destroy\', $id))) }}
																<div class="btn-group">
																	<a href="{{ URL::route( \'admin.press_releases.edit\', array( $id )) }}" class="btn btn-mini">Edit</a>
																	<a href="{{ URL::route( \'admin.press_releases.show\', array( $id )) }}" class="btn btn-mini">View</a>
									                            	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn btn-mini">Delete</a>
								                            	</div>
								                            	<a href="{{ URL::route( \'admin.press_releases.notify\', array( $id )) }}" class="btn btn-mini"><i class="icon icon-envelope"></i> Notify</a>
									                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.press_releases.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('photos', 'captions');// Input::all();
		$validation = Validator::make($input, Press_release::$rules);
		$photos = Input::file('photos');
		$captions = Input::file('captions');


		//if ($validation->passes())
		{
			$press_release = $this->press_release->create($input);
			foreach($photos as $ii => $one_photo){
				$photo = new Photo();
				$name = $press_release->title . '-' . $ii;
			
				$photo->create_n_upload($one_photo, array(	'name'=>$name, 
															'caption'=>$captions[$ii], 
														'imageable_type'=>'Press_release', 
														'imageable_id'=>$press_release->id ));
			}

			return Redirect::route('admin.press_releases.index');
		}

		return Redirect::route('admin.press_releases.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$press_release = $this->press_release->findOrFail($id);

		$this->layout->content =  View::make('administration.press_releases.show', compact('press_release'));
	}



	public function getNotify($id)
	{
		$press_release = $this->press_release->findOrFail($id);
		$company = Press_release::find($id)->company()->first();

		$this->layout->content =  View::make('administration.press_releases.notify', array('press_release'=>$press_release, 'company'=>$company));
	}


	public function postNotify($id)
	{
		$input = Input::all();
		$company = Press_release::find($id)->company()->first();
		$input['email'] = $company->email;

		$validation = Validator::make($input, array('subject'=>'required', 'email'=>'required|email', 'body'=>'required' ));

		if ($validation->passes())
		{

	        Mail::send('emails.base', $input, function($message) use ($input)
	        {
	            $message->to($input['email'])->subject($input['subject']);
	        });

			return Redirect::route('admin.press_releases.notify', $id)->withInput()->with('success', 'Notification delivered.');
		}

		return Redirect::route('admin.press_releases.notify', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$press_release = $this->press_release->find($id);

		if (is_null($press_release))
		{
			return Redirect::route('admin.press_releases.index');
		}

		$this->layout->content =  View::make('administration.press_releases.edit', compact('press_release'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::except('photos', 'captions', 'delete_current_images'), '_method');//array_except(Input::all(), '_method');
		$validation = Validator::make($input, Press_release::$rules);

		if ($validation->passes())
		{
			$press_release = $this->press_release->find($id);
			$press_release->update($input);


			// deletable images
			$deletables = Input::only('delete_current_images');

			foreach($deletables as $i => $one_deletable):
				$deleteme = Photo::find(array_pop($one_deletable));
			if(!is_null($deleteme))
				$deleteme->delete();
			endforeach;

			$photos = Input::file('photos');
			$captions = Input::file('captions');

			//if($photos)


			if(count($photos) > 0):
				foreach($photos as $ii => $one_photo){
					$photo = new Photo();
					$name = $press_release->title . '-' . $ii;
				
					$photo->create_n_upload($one_photo, array(	'name'=>$name, 
																'caption'=>$captions[$ii], 
															'imageable_type'=>'Press_release', 
															'imageable_id'=>$press_release->id ));
				}
			endif;






			return Redirect::route('admin.press_releases.show', $id);
		}

		return Redirect::route('admin.press_releases.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->press_release->find($id)->delete();

		return Redirect::route('admin.press_releases.index');
	}

}
