<?php
namespace Administration;

use BaseController;
use View;
use Config;

use Validator;
use Redirect;
use Input;

use Red_carpet, CSV;
use Datatables;

class Red_carpetsController extends BaseController {

	/**
	 * Red_carpet Repository
	 *
	 * @var Red_carpet
	 */

	protected $red_carpet;

	public function __construct(Red_carpet $red_carpet){
		$this->red_carpet = $red_carpet;
	}

	public $active_menu = 'admin/red_carpets';

	protected function setupLayout(){
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));

		//Default layout content is null
		$this->layout->content = '';

		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');

		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;

		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');

		//Set application title
		$this->layout->title = Config::get('firadmin::title');

		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index() {
		$red_carpets = $this->red_carpet->all();
		$this->layout->content = View::make('administration.red_carpets.index', compact('red_carpets'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create() {
		$this->layout->content = View::make('administration.red_carpets.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store()	{
		$input = Input::all();
		$validation = Validator::make($input, Red_carpet::$rules);

		if ($validation->passes()) {
			$this->red_carpet->create($input);

			return Redirect::route('admin.red_carpets.index');
		}

		return Redirect::route('admin.red_carpets.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function show($id) {
		$red_carpet = $this->red_carpet->findOrFail($id);
		$this->layout->content = View::make('administration.red_carpets.show', compact('red_carpet'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function edit($id) {
		$red_carpet = $this->red_carpet->find($id);

		if (is_null($red_carpet)) {
			return Redirect::route('admin.red_carpets.index');
		}

		$this->layout->content = View::make('administration.red_carpets.edit', compact('red_carpet'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($id)	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Red_carpet::$rules);

		if ($validation->passes()) {
			$red_carpet = $this->red_carpet->find($id);
			$red_carpet->update($input);
			return Redirect::route('admin.red_carpets.show', $id);
		}

		return Redirect::route('admin.red_carpets.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function destroy($id) {
		$this->red_carpet->find($id)->delete();
		return Redirect::route('admin.red_carpets.index');
	}
}