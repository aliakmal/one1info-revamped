<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Ad_banner;
use Photo, CSV, Datatables;

class Ad_bannersController extends BaseController {

	/**
	 * Ad_banner Repository
	 *
	 * @var Ad_banner
	 */
	protected $ad_banner;
  public $active_menu = 'admin/ad_banners';

	public function __construct(Ad_banner $ad_banner)
	{
		$this->ad_banner = $ad_banner;
	}
  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');

		//Default layout content is null
		$this->layout->content = '';
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ad_banners = $this->ad_banner->all();

		$this->layout->content =  View::make('administration.ad_banners.index', compact('ad_banners'));
	}

	public function getDatatable()
	{

		$posts = $this->ad_banner->select(array('ad_banners.id', 'ad_banners.page',  'ad_settings.name', 'ad_banners.link', 'ad_banners.is_published'))->join('ad_settings', 'ad_settings.id', '=', 'ad_banners.location_id');
		return Datatables::of($posts)->add_column('image',	'@if(Ad_banner::find($id)->photo()->count() > 0):
																{{ Photo::image_tag( Ad_banner::find($id)->photo()->first() ) }}
															@else:
																<i> No Image </i>
															@endif', 3
									)->edit_column('is_published', '{{$is_published=="0"?\'<span class="badge badge-important">NOT PUBLISHED</span>\':\'<span class="badge badge-success">PUBLISHED</span>\'}}')
									->edit_column('page', '
										@foreach(unserialize($page) as $pname => $pvalue)
											<span style="min-height:20px; display:inline-block;" class="label label-info">{{ ucwords(str_replace(array(\'-\', \'_\'), \' \', $pname)) }}</span>
										@endforeach

										')
									->add_column('operations', '
									
							{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.ad_banners.destroy\', $id))) }}
							<div class="btn-group">
								<a href="{{ URL::route( \'admin.ad_banners.edit\', array( $id )) }}" class="btn  btn-mini">Edit</a>
                            	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn  btn-mini ">Delete</a>
							</div>
                        	{{ Form::close() }}')->make();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.ad_banners.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules = array_merge(Ad_banner::$rules, array('image'=>'required|image'));
		$validation = Validator::make($input, $rules);

		if ($validation->passes())
		{
			$input['page'] = serialize($input['page']);
			unset($input['image']);

			$ad_banner = $this->ad_banner->create($input);

	        $img = Input::file('image');
	        
        	$photo = new Photo();
	        $name = 'banner-'.$ad_banner->id;
            
    	    $photo->create_n_upload($img, array(  'name'=>$name, 
                                  'caption'=>'Banner ', 
	                                'imageable_type'=>'Ad_banner', 
	                                'imageable_id'=>$ad_banner->id ));
	        


			return Redirect::route('admin.ad_banners.index');
		}

		return Redirect::route('admin.ad_banners.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ad_banner = $this->ad_banner->findOrFail($id);

		$this->layout->content =  View::make('administration.ad_banners.show', compact('ad_banner'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ad_banner = $this->ad_banner->find($id);

		if (is_null($ad_banner))
		{
			return Redirect::route('admin.ad_banners.index');
		}

		$ad_banner->page = unserialize($ad_banner->page);

		$this->layout->content =  View::make('administration.ad_banners.edit', compact('ad_banner'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$rules = array_merge(Ad_banner::$rules, array('image'=>'image'));

		$validation = Validator::make($input, $rules);

		if ($validation->passes())
		{
			$input['page'] = serialize($input['page']);

			unset($input['image']);

			$ad_banner = $this->ad_banner->find($id);
			$ad_banner->update($input);

			$img =  Input::file('image');


			if(!is_null($img)):

				// delete photo
				if(!is_null($ad_banner->photo()->first())){
					$deleteme = Photo::find($ad_banner->photo()->first()->id);
					$deleteme->delete();
				}
				
	        	$photo = new Photo();
		        $name = 'banner-'.$ad_banner->id;
	            
	    	    $photo->create_n_upload($img, array(  'name'=>$name, 
	                                  'caption'=>'Banner ', 
		                                'imageable_type'=>'Ad_banner', 
		                                'imageable_id'=>$ad_banner->id ));

			endif;


			return Redirect::route('admin.ad_banners.show', $id);
		}

		return Redirect::route('admin.ad_banners.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ad_banner->find($id)->delete();

		return Redirect::route('admin.ad_banners.index');
	}

}
