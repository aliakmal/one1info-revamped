<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Section, DB;
use Datatables, CSV;

class SectionsController extends BaseController {

	/**
	 * Section Repository
	 *
	 * @var Section
	 */
	protected $section;

	public function __construct(Section $section)
	{
		$this->section = $section;
	}
		public $active_menu = 'admin/sections';

	public function getCSV(){

		$rows = Section::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sections = $this->section->paginate(15);//all();
		$posts = $this->section->select(array('sections.id', 'sections.name', 'sections.sort', 'sections.is_published', DB::raw('COUNT(articles.id) as num_articles')))->leftJoin('articles', 'articles.section_id', '=','sections.id')->groupBy('sections.id');
		$this->layout->content =    View::make('administration.sections.index', compact('sections'));
	}

	public function getDatatable()
	{

		$posts = $this->section->select(array('sections.id', 'sections.name', 'sections.sort', 'sections.is_published', DB::raw('COUNT(articles.id) as num_articles')))->leftJoin('articles', 'articles.section_id', '=','sections.id')->groupBy('sections.id');

		return Datatables::of($posts)->edit_column('is_published', '{{$is_published=="0"?\'<span class="badge badge-important">NOT PUBLISHED</span>\':\'<span class="badge badge-success">PUBLISHED</span>\'}}')
									->edit_column('num_articles', '<a href="{{ URL::route( \'admin.articles.index\', array( \'category\' =>$name )) }}" >Articles( {{$num_articles}} )</a>')
									->add_column('operations', '
	{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.articles.destroy\', $id))) }}
	<div class="btn-group">
		<a href="{{ URL::route( \'admin.articles.edit\', array( $id )) }}" class="btn btn-mini">Edit</a>
		<a href="javascript:void(0);" onclick="parentFormSubmit(this);" class="btn btn-mini">Delete</a>
	</div>
	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =    View::make('administration.sections.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Section::$rules);

		if ($validation->passes())
		{
			$this->section->create($input);

			return Redirect::route('admin.sections.index');
		}

		return Redirect::route('admin.sections.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$section = $this->section->findOrFail($id);

		$this->layout->content =    View::make('administration.sections.show', compact('section'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$section = $this->section->find($id);

		if (is_null($section))
		{
			return Redirect::route('admin.sections.index');
		}

		$this->layout->content =    View::make('administration.sections.edit', compact('section'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Section::$rules);

		if ($validation->passes())
		{
			$section = $this->section->find($id);
			$section->update($input);

			return Redirect::route('admin.sections.index');
		}

		return Redirect::route('admin.sections.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->section->find($id)->delete();

		return Redirect::route('admin.sections.index');
	}

}
