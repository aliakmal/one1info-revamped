<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Deal_section, CSV;
use Photo, Datatables;

class Deal_sectionsController extends BaseController {

	/**
	 * Deal_section Repository
	 *
	 * @var Deal_section
	 */
	protected $deal_section;

	public function __construct(Deal_section $deal_section)
	{
		$this->deal_section = $deal_section;
	}
	
	public function getCSV(){

		$rows = Deal_section::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


	public $active_menu = 'admin/deal_sections';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$deal_sections = $this->deal_section->all();

		$this->layout->content =  View::make('administration.deal_sections.index', compact('deal_sections'));
	}

	public function getDatatable()
	{

		$posts = $this->deal_section->select(array('deal_sections.id','deal_sections.name','deal_sections.is_published'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.deal_sections.edit\', array( $id )) }}" class="btn btn-info">edit</a>
													{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.deal_sections.destroy\', $id))) }}
						                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
						                        	{{ Form::close() }}')->make();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.deal_sections.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Deal_section::$rules);

		if ($validation->passes())
		{
			$this->deal_section->create($input);

			return Redirect::route('admin.deal_sections.index');
		}

		return Redirect::route('admin.deal_sections.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$deal_section = $this->deal_section->findOrFail($id);

		$this->layout->content =  View::make('deal_sections.show', compact('deal_section'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$deal_section = $this->deal_section->find($id);

		if (is_null($deal_section))
		{
			return Redirect::route('admin.deal_sections.index');
		}

		$this->layout->content =  View::make('administration.deal_sections.edit', compact('deal_section'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Deal_section::$rules);

		if ($validation->passes())
		{
			$deal_section = $this->deal_section->find($id);
			$deal_section->update($input);

			return Redirect::route('admin.deal_sections.show', $id);
		}

		return Redirect::route('admin.deal_sections.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->deal_section->find($id)->delete();

		return Redirect::route('admin.deal_sections.index');
	}

}
