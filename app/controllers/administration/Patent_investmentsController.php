<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Patent_investment;
use Photo, CSV, Datatables;

class Patent_investmentsController extends BaseController {

	/**
	 * Patent_investment Repository
	 *
	 * @var Patent_investment
	 */
	protected $patent_investment;

	public function __construct(Patent_investment $patent_investment)
	{
		$this->patent_investment = $patent_investment;
	}

	public function getCSV(){

		$rows = Patent_investment::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


	public $active_menu = 'admin/patent_investments';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$patent_investments = $this->patent_investment->all();

		$this->layout->content = View::make('administration.patent_investments.index', compact('patent_investments'));
	}

	public function getDatatable()
	{


		$posts = $this->patent_investment->select(array('patent_investments.id','patent_investments.patent_id','patent_investments.company_id','patent_investments.applicant_name','patent_investments.applicant_position','patent_investments.address','patent_investments.city','patent_investments.country','patent_investments.email','patent_investments.phone','patent_investments.fax','patent_investments.why_invest','patent_investments.investment_benefits','patent_investments.has_business_same_field','patent_investments.need_meeting','patent_investments.status','patent_investments.dated'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.patent_investments.edit\', array( $id )) }}" class="btn btn-info">edit</a>
																{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.patent_investments.destroy\', $id))) }}
									                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
									                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.patent_investments.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Patent_investment::$rules);

		if ($validation->passes())
		{
			$this->patent_investment->create($input);

			return Redirect::route('admin.patent_investments.index');
		}

		return Redirect::route('admin.patent_investments.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$patent_investment = $this->patent_investment->findOrFail($id);

		$this->layout->content =  View::make('administration.patent_investments.show', compact('patent_investment'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patent_investment = $this->patent_investment->find($id);

		if (is_null($patent_investment))
		{
			return Redirect::route('admin.patent_investments.index');
		}

		$this->layout->content =  View::make('administration.patent_investments.edit', compact('patent_investment'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Patent_investment::$rules);

		if ($validation->passes())
		{
			$patent_investment = $this->patent_investment->find($id);
			$patent_investment->update($input);

			return Redirect::route('admin.patent_investments.show', $id);
		}

		return Redirect::route('admin.patent_investments.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->patent_investment->find($id)->delete();

		return Redirect::route('admin.patent_investments.index');
	}

}
