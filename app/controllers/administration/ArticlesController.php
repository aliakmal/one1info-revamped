<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Article;
use Section;
use Photo;
use Datatables, CSV;
class ArticlesController extends BaseController {

	/**
	 * Article Repository
	 *
	 * @var Article
	 */
	protected $article;

	public function __construct(Article $article)
	{
		$this->article = $article;
	}
  
	public $active_menu = 'admin/articles';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sort = Input::get('sort') ? Input::get('sort'):'id';
	    $order = Input::get('order') ? Input::get('order'):'asc';
		$category = null;
	    if($category = Input::get('category')){
			$category = Section::where('name', '=', $category)->first();
			$articles = $this->article->where('section_id', '=', $category->id)->orderby($sort, $order)->paginate(15);
			
	    }else{
			$articles = $this->article->orderby($sort, $order)->paginate(15);//all();
	    }

		$this->layout->content =   View::make('administration.articles.index', array('articles'=>$articles, 'category'=>$category, 'sort'=> $sort, 'order'=>$order));
	}

	public function getCSV(){

		$rows = Article::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

	public function getDatatable()
	{
		if($category = Input::get('category')){
			$category = Section::where('name', '=', $category)->first();
			$posts = $this->article->select(array('articles.id', 'articles.title', 'articles.author', 'articles.caption', 'articles.is_published'))->where('section_id', '=', $category->id);
	    }else{
			$posts = $this->article->select(array('articles.id', 'articles.title', 'articles.author', 'articles.caption', 'articles.is_published'));
		}
		return Datatables::of($posts)->add_column('operations', '
							
								{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.articles.destroy\', $id))) }}
								<div class="btn-group">
									<a href="{{ URL::route( \'admin.articles.edit\', array( $id )) }}" class="btn btn-mini">Edit</a>
	                            	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn  btn-mini">Delete</a>
                            	</div>
	                        	{{ Form::close() }}
                        	')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =   View::make('administration.articles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('photos', 'captions');
		$validation = Validator::make($input, Article::$rules);
		$photos = Input::file('photos');
		$captions = Input::file('captions');

		$image_rules = array('upload' => 'image|max:15500|mimes:jpeg,bmp,png,gif');
		$failed_image_validations = false;

//		if(count($photos)>0):
//			foreach($photos as $one_photo):
//    			$image_validation = Validator::make(array('upload'=>$one_photo), $image_rules);
//    			if(!$image_validation->passes()){
//					$failed_image_validations = $image_validation;
//    			}
//			endforeach;
//		endif;

// && ($failed_image_validations == false)
		if ($validation->passes() ){


			$article = $this->article->create($input);

			foreach($photos as $ii => $one_photo){
				$photo = new Photo();
				$name = $article->title . '-' . $ii;
			
				$photo->create_n_upload($one_photo, array(	'name'=>$name, 
															'caption'=>$captions[$ii], 
														'imageable_type'=>'Article', 
														'imageable_id'=>$article->id ));
			}

			return Redirect::route('admin.articles.index');
		}

		

		return Redirect::route('admin.articles.create')
			->withInput()->withErrors($validation);
			//->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$article = $this->article->findOrFail($id);

		$this->layout->content =   View::make('administration.articles.show', compact('article'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$article = $this->article->find($id);

		if (is_null($article))
		{
			return Redirect::route('articles.index');
		}

		$this->layout->content =   View::make('administration.articles.edit', compact('article'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$input = array_except(Input::except('photos', 'captions', 'delete_current_images'), '_method');
		$validation = Validator::make($input, Article::$rules);

		if ($validation->passes())
		{
			$article = $this->article->find($id);
			$article->update($input);

			// deletable images
			$deletables = Input::only('delete_current_images');

			foreach($deletables as $i => $one_deletable):
				$deleteme = Photo::find(array_pop($one_deletable));
			if(!is_null($deleteme))
				$deleteme->delete();
			endforeach;



			$photos = Input::file('photos');
			$captions = Input::file('captions');

			//if($photos)


			if(count($photos) > 0):
				foreach($photos as $ii => $one_photo){
					$photo = new Photo();
					$name = $article->title . '-' . $ii;
				
					$photo->create_n_upload($one_photo, array(	'name'=>$name, 
																'caption'=>$captions[$ii], 
															'imageable_type'=>'Article', 
															'imageable_id'=>$article->id ));
				}
			endif;




			return Redirect::route('admin.articles.show', $id);
		}

		return Redirect::route('admin.articles.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->article->find($id)->delete();

		return Redirect::route('admin.articles.index');
	}

}
