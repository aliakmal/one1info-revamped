<?php

namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Idea;
use Photo, CSV, Datatables;

class IdeasController extends BaseController {

	/**
	 * Idea Repository
	 *
	 * @var Idea
	 */
	protected $idea;

	public function __construct(Idea $idea)
	{
		$this->idea = $idea;
	}

	public function getCSV(){

		$rows = Idea::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


	public $active_menu = 'admin/ideas';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ideas = $this->idea->all();

		$this->layout->content =  View::make('administration.ideas.index', compact('ideas'));
	}
	public function getDatatable()
	{
		$posts = $this->idea->select(array('ideas.id','ideas.title','ideas.body','ideas.youtube_url','ideas.media_title','ideas.dated','ideas.sort','ideas.is_published'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.ideas.edit\', array( $id )) }}" class="btn btn-info">edit</a>
													{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.ideas.destroy\', $id))) }}
						                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
						                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.ideas.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('photos', 'captions');//Input::all();
		$validation = Validator::make($input, Idea::$rules);
		$photos = Input::file('photos');
		$captions = Input::file('captions');

		if ($validation->passes())
		{
			$idea = $this->idea->create($input);
			foreach($photos as $ii => $one_photo){
				$photo = new Photo();
				$name = $idea->title . '-' . $ii;
			
				$photo->create_n_upload($one_photo, array(	'name'=>$name, 
															'caption'=>$captions[$ii], 
														'imageable_type'=>'Idea', 
														'imageable_id'=>$idea->id ));
			}

			return Redirect::route('admin.ideas.index');
		}

		return Redirect::route('admin.ideas.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$idea = $this->idea->findOrFail($id);

		$this->layout->content =  View::make('administration.ideas.show', compact('idea'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$idea = $this->idea->find($id);

		if (is_null($idea))
		{
			return Redirect::route('admin.ideas.index');
		}

		$this->layout->content =  View::make('administration.ideas.edit', compact('idea'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::except('photos', 'captions', 'delete_current_images'), '_method');//array_except(Input::all(), '_method');
		$validation = Validator::make($input, Idea::$rules);

		if ($validation->passes())
		{
			$idea = $this->idea->find($id);
			$idea->update($input);
			// deletable images
			$deletables = Input::only('delete_current_images');

			foreach($deletables as $i => $one_deletable):
				$deleteme = Photo::find(array_pop($one_deletable));
			if(!is_null($deleteme))
				$deleteme->delete();
			endforeach;



			$photos = Input::file('photos');
			$captions = Input::file('captions');

			//if($photos)


			if(count($photos) > 0):
				foreach($photos as $ii => $one_photo){
					$photo = new Photo();
					$name = $idea->title . '-' . $ii;
				
					$photo->create_n_upload($one_photo, array(	'name'=>$name, 
																'caption'=>$captions[$ii], 
															'imageable_type'=>'Idea', 
															'imageable_id'=>$idea->id ));
				}
			endif;

			return Redirect::route('admin.ideas.show', $id);
		}

		return Redirect::route('admin.ideas.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->idea->find($id)->delete();

		return Redirect::route('admin.ideas.index');
	}

}
