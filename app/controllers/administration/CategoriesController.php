<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Category, CSV;
use Datatables;
//Firalabs\Firadmin\Controllers\

class CategoriesController extends BaseController {

	/**
	 * Category Repository
	 *
	 * @var Category
	 */
	protected $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}
	public $active_menu = 'admin/categories';
	
	public function getCSV(){

		$rows = Category::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

  
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = $this->category->paginate(30);//all();

		$this->layout->content = View::make('administration.categories.index', compact('categories'));
	}

	public function getDatatable()
	{
		$posts = $this->category->select(array('categories.id', 'categories.name'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.categories.edit\', array( $id )) }}" class="btn btn-info">edit</a>
							{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.categories.destroy\', $id))) }}
                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.categories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Category::$rules);

		if ($validation->passes())
		{
			$this->category->create($input);

			return Redirect::route('admin.categories.index');
		}

		return Redirect::route('admin.categories.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = $this->category->findOrFail($id);

		$this->layout->content = View::make('administration.categories.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = $this->category->find($id);

		if (is_null($category))
		{
			return Redirect::route('admin.categories.index');
		}

		$this->layout->content = View::make('administration.categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Category::$rules);

		if ($validation->passes())
		{
			$category = $this->category->find($id);
			$category->update($input);

			return Redirect::route('admin.categories.show', $id);
		}

		return Redirect::route('admin.categories.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->category->find($id)->delete();

		return Redirect::route('admin.categories.index');
	}

}
