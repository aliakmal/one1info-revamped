<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Video, Video_section, Datatables, CSV;


class VideosController extends BaseController {

	/**
	 * Video Repository
	 *
	 * @var Video
	 */
	protected $video;

	public function __construct(Video $video)
	{
		$this->video = $video;
	}
	public $active_menu = 'admin/videos';
	public function getCSV(){

		$rows = Video::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}


  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	

		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	

		//Set application title
		$this->layout->title = Config::get('firadmin::title');

		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}



	public function getDatatable()
	{
		if($section = Input::get('section')){
			$section = Video_section::where('name', '=', $section)->first();
			$posts = $this->video->select(array('videos.id', 'videos.title', 'videos.providor', 'videos.url', 'video_sections.name as section', 'videos.is_published'))
									->join('video_sections', 'video_sections.id', '=', 'videos.section_id')
									->where('section_id', '=', $section->id);
	    }else{
			$posts = $this->video->select(array('videos.id', 'videos.title', 'videos.providor', 'videos.url', 'video_sections.name as section', 'videos.is_published'))
									->join('video_sections', 'video_sections.id', '=', 'videos.section_id');
		}

		return Datatables::of($posts)->edit_column('is_published', '{{$is_published=="0"?\'<span class="badge badge-important">NOT PUBLISHED</span>\':\'<span class="badge badge-success">PUBLISHED</span>\'}}')
									->add_column('thumbnail',	'<img class="pull-left" style="padding-right:10px;" src="{{Video::find($id)->getThumbnailUrl();}}" height="110" width="145" />', 3)
									->add_column('operations', '
								{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.videos.destroy\', $id))) }}
								<div class="btn-group">
									<a href="{{ URL::route( \'admin.videos.edit\', array( $id )) }}" class="btn btn-mini">Edit</a>
	                            	<a href="javascript:void(0);" onclick="parentFormSubmit(this)" class="btn  btn-mini">Delete</a>
                            	</div>
	                        	{{ Form::close() }}
                        	')->make();
	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index($section = null)
	{

		if($section){
			$section = Video_section::where('name', '=', $section)->first();
			$videos = $this->video->where('section_id', '=', $section->id)->paginate(15);//all();
		}else{

			$videos = $this->video->paginate(15);//all();
		}

		$this->layout->content =  View::make('administration.videos.index', array('videos' => $videos, 'video_section' => $section));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create($section = null)
	{
		if($section){
			$section = Video_section::where('name', '=', $section)->first();
		}

		$this->layout->content =  View::make('administration.videos.create', array('section' => $section));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Video::$rules);

		if ($validation->passes())
		{
			$this->video->create($input);

			return Redirect::route('admin.videos.index');
		}

		return Redirect::route('admin.videos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$video = $this->video->findOrFail($id);

		$this->layout->content =  View::make('administration.videos.show', compact('video'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$video = $this->video->find($id);

		if (is_null($video))
		{
			return Redirect::route('admin.videos.index');
		}

		$this->layout->content =  View::make('administration.videos.edit', compact('video'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Video::$rules);

		if ($validation->passes())
		{
			$video = $this->video->find($id);
			$video->update($input);

			return Redirect::route('admin.videos.show', $id);
		}

		return Redirect::route('admin.videos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->video->find($id)->delete();

		return Redirect::route('admin.videos.index');
	}

}
