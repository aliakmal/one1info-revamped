<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

use Page, CSV;
use Photo, Datatables;

class PagesController extends BaseController {

	/**
	 * Page Repository
	 *
	 * @var Page
	 */
	protected $page;

	public function __construct(Page $page)
	{
		$this->page = $page;
	}

	public function getCSV(){

		$rows = Page::all()->toArray();  //use eloquent to get array of all users in 'users' table
		return CSV::fromArray($rows)->render();  //download as csv
	}

	public $active_menu = 'admin/pages';
	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = $this->page->all();

		$this->layout->content =  View::make('administration.pages.index', compact('pages'));
	}

	public function getDatatable()
	{
		$posts = $this->page->select(array('pages.id', 'pages.title', 'pages.body', 'pages.is_published'));
		return Datatables::of($posts)->add_column('operations', '<a href="{{ URL::route( \'admin.pages.edit\', array( $id )) }}" class="btn btn-info">edit</a>
																{{ Form::open(array(\'method\' => \'DELETE\', \'route\' => array(\'admin.pages.destroy\', $id))) }}
									                            	{{ Form::submit(\'Delete\', array(\'class\' => \'btn btn-danger\')) }}
									                        	{{ Form::close() }}')->make();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.pages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Page::$rules);

		if ($validation->passes())
		{
			$page = $this->page->create($input);
	        if($img = Input::file('image')){
            	$photo = new Photo();
    	        $name = 'banner-'.$page->id;
        	    $photo->create_n_upload($img, array(  'name'=>$name, 
                                      'caption'=>' - ', 
    	                                'imageable_type'=>'Page', 
    	                                'imageable_id'=>$page->id ));
    		}

			return Redirect::route('admin.pages.index');
		}

		return Redirect::route('admin.pages.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$page = $this->page->findOrFail($id);

		$this->layout->content =  View::make('administration.pages.show', compact('page'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = $this->page->find($id);

		if (is_null($page))
		{
			return Redirect::route('admin.pages.index');
		}

		$this->layout->content =  View::make('administration.pages.edit', compact('page'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Page::$rules);

		if ($validation->passes())
		{
			$page = $this->page->find($id);
			unset($input['image']);
			$page->update($input);
			$img =  Input::file('image');

			if(!is_null($img) || $input['deleteme']):

				// delete photo
				if(!is_null($page->photo()->first())){
					$deleteme = Photo::find($page->photo()->first()->id);
					$deleteme->delete();
				}

				if(!is_null($img)):

		        	$photo = new Photo();
			        $name = 'banner-'.$page->id;
		            
		    	    $photo->create_n_upload($img, array(  'name'=>$name, 
		                                  'caption'=>' ', 
			                                'imageable_type'=>'Page', 
			                                'imageable_id'=>$page->id ));
	    	    endif;

			endif;

			return Redirect::route('admin.pages.show', $id);
		}

		return Redirect::route('admin.pages.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->page->find($id)->delete();

		return Redirect::route('admin.pages.index');
	}

}
