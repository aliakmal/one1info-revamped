<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Video_section;
use Photo, CSV, Datatables;

class Video_sectionsController extends BaseController {

	/**
	 * Video_section Repository
	 *
	 * @var Video_section
	 */
	protected $video_section;

	public function __construct(Video_section $video_section)
	{
		$this->video_section = $video_section;
	}
	
	public $active_menu = 'admin/video_sections';

  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');

		//Default layout content is null
		$this->layout->content = '';
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$video_sections = $this->video_section->where('system', '<>', '1')->get();

		$this->layout->content = View::make('administration.video_sections.index', compact('video_sections'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.video_sections.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Video_section::$rules);

		if ($validation->passes())
		{
			$this->video_section->create($input);

			return Redirect::route('admin.video_sections.index');
		}

		return Redirect::route('admin.video_sections.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$video_section = $this->video_section->findOrFail($id);

		$this->layout->content = View::make('administration.video_sections.show', compact('video_section'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$video_section = $this->video_section->find($id);

		if (is_null($video_section))
		{
			return Redirect::route('admin.video_sections.index');
		}

		return View::make('administration.video_sections.edit', compact('video_section'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Video_section::$rules);

		if ($validation->passes())
		{
			$video_section = $this->video_section->find($id);
			$video_section->update($input);

			return Redirect::route('admin.video_sections.show', $id);
		}

		return Redirect::route('admin.video_sections.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->video_section->find($id)->delete();

		return Redirect::route('admin.video_sections.index');
	}

}
