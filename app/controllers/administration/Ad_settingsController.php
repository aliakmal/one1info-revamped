<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Ad_setting;
use Photo, CSV, Datatables;

class Ad_settingsController extends BaseController {

	/**
	 * Ad_setting Repository
	 *
	 * @var Ad_setting
	 */
	protected $ad_setting;

	public function __construct(Ad_setting $ad_setting)
	{
		$this->ad_setting = $ad_setting;
	}

	public $active_menu = 'admin/ad_settings';

  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');

		//Default layout content is null
		$this->layout->content = '';
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ad_settings = $this->ad_setting->all();

		$this->layout->content =  View::make('administration.ad_settings.index', compact('ad_settings'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.ad_settings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Ad_setting::$rules);

		if ($validation->passes())
		{
			$this->ad_setting->create($input);

			return Redirect::route('admin.ad_settings.index');
		}

		return Redirect::route('admin.ad_settings.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ad_setting = $this->ad_setting->findOrFail($id);

		$this->layout->content =  View::make('administration.ad_settings.show', compact('ad_setting'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ad_setting = $this->ad_setting->find($id);

		if (is_null($ad_setting))
		{
			return Redirect::route('ad_settings.index');
		}

		$this->layout->content =  View::make('administration.ad_settings.edit', compact('ad_setting'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Ad_setting::$rules);

		if ($validation->passes())
		{
			$ad_setting = $this->ad_setting->find($id);
			$ad_setting->update($input);

			return Redirect::route('admin.ad_settings.show', $id);
		}

		return Redirect::route('admin.ad_settings.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ad_setting->find($id)->delete();

		return Redirect::route('admin.ad_settings.index');
	}

}
