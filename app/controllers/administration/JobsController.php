<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Job;
use Photo, CSV, Datatables;

class JobsController extends BaseController {

	/**
	 * Job Repository
	 *
	 * @var Job
	 */
	protected $job;

	public function __construct(Job $job)
	{
		$this->job = $job;
	}
  public $active_menu = 'admin/jobs';
	

  
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$jobs = Job::query();//$this->job->all();
		if( Input::get('search')!=''){
			$jobs = $jobs->where('title', 'like', Input::get('search').'%');//$this->job->all();
		}

		if(Input::get('country')!=''){
			$jobs = $jobs->where('country_id', '=', Input::get('country'));//$this->job->all();
		}

		if(Input::get('category')!=''){
			$jobs = $jobs->where('category', '=', Input::get('category'));//$this->job->all();
		}

		$jobs = $jobs->paginate(15);//get();

		$this->layout->content =  View::make('administration.jobs.index', compact('jobs'));
	}

	public function search()
	{
		$jobs = $this->job->all();

		$this->layout->content =  View::make('administration.jobs.index', compact('jobs'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.jobs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Job::$rules);

		if ($validation->passes())
		{
			$this->job->create($input);

			return Redirect::route('admin.jobs.index');
		}

		return Redirect::route('jobs.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$job = $this->job->findOrFail($id);

		$this->layout->content =  View::make('administration.jobs.show', compact('job'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$job = $this->job->find($id);

		if (is_null($job))
		{
			return Redirect::route('admin.jobs.index');
		}

		$this->layout->content =  View::make('administration.jobs.edit', compact('job'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Job::$rules);

		if ($validation->passes())
		{
			$job = $this->job->find($id);
			$job->update($input);

			return Redirect::route('admin.jobs.show', $id);
		}

		return Redirect::route('admin.jobs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->job->find($id)->delete();

		return Redirect::route('admin.jobs.index');
	}

}
