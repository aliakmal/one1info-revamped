<?php namespace Firalabs\Firadmin\Controllers;
namespace Administration;


use BaseController, Validator;


use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Firalabs\Firadmin\Facades\AjaxRequest;
use Firalabs\Firadmin\Facades\Permissions;
use Firalabs\Firadmin\Repository\UserRepositoryInterface;
use Firalabs\Firadmin\Repository\UserRoleRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use User;
/**
 * Controller used for users managment
 * 
 * @author maxime.beaudoin
 */
class AccountsController extends BaseController {

/**
	 * The current active menu URI
	 * 
	 * @var string
	 */
	public $active_menu = 'admin/accounts';
	
	/**
	 * User repository
	 * 
	 * @var UserRepositoryInterface
	 */
	protected $_users;
	
	/**
	 * Constructor
	 */
	public function __construct(User $user)//User $user)
    {    	    	
    	//Add csrf filter when posting forms
    	$this->beforeFilter('csrf', array('on' => array('post', 'put', 'delete')));
    	//Create user object
    	$this->_users = $user;
    }
protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	

		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	

		//Set application title
		$this->layout->title = Config::get('firadmin::title');

		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{		

		
		//Define the layout content
		$this->layout->content = View::make('administration.accounts.index', array(
			'users' => $this->_users->select("users.username", "users.id", "users.email", "users.updated_at", "users.id", "roles.role")
				->join("users_roles AS roles", "roles.user_id", "=", "users.id")->whereIn('roles.role', array('basic-company', 'premium-company'))
				->groupBy('users.id')->get()
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{		
		//Check permission
		if(Permissions::isAllowed(Auth::user(), 'user', 'create') !== true){
			return Redirect::route('get admin/login')
				->with('reason', Lang::get('firadmin::admin.messages.insufficient-permission') . '<br>')
				->with('error', 1);
		}

		//If we have roles set from input
		Input::old('roles')
			? $selected_roles = Input::old('roles') //select them from input
			: $selected_roles = array(); //No roles selected, so just set a empty array
		
		//Define the layout content
		$this->layout->content = View::make('administration.accounts.create', array('selected_roles' => $selected_roles));
	}
  private function createUser($data){
      //Create user object
      $user = app()->make('Firalabs\Firadmin\Repository\UserRepositoryInterface');
      $user->username = $data['username'];
      $user->email    = $data['email'];
      $user->password = $data['password'];
      $user->password_confirmation = $data['password_confirmation'];
      $user->save();

      $u = User::find($user->id);
      $u->company_id = $data['company_id'];
      $u->level = $data['level'];
      $u->save();


      return $user;
  }

  private function assignRole($user, $role){
    $user->roles()->delete();
    $roles = app()->make('Firalabs\Firadmin\Repository\UserRoleRepositoryInterface');   
    $roles->role = $role;
    //Save the user role
    $user->roles()->save($roles); 
  }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{			
		
		$input = Input::except('role', '_token');// Input::all();
		$validation = Validator::make($input, User::$rules);

		//var_dump($input);exit;

		if ($validation->passes())
		{
			$user = $this->createUser($input);
			$this->assignRole($user, Input::get('role'));

			//Returns a response in JSON format if it's an Ajax request
			if(AjaxRequest::isAjax()){
				return Response::json(array ('success' => Lang::get('firadmin::admin.store-success') ));
			}
	
			//Redirect with a success message
			return Redirect::to('/admin/accounts')
				->with('success', Lang::get('firadmin::admin.store-success'));
			
		//Else, fail to save the user
		} else {	

			//Flash input to repopulate them in the form
			Input::flash();
			
			
			//Redirect to the form with errors 
			return Redirect::to('/admin/accounts/create')
				->withErrors($validation)
				->with('error', 1);
			
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id)
	{			
		//Check permission
		if(Permissions::isAllowed(Auth::user(), 'user', 'read') !== true){
			return Redirect::route('get admin/login')
				->with('reason', Lang::get('firadmin::admin.messages.insufficient-permission') . '<br>')
				->with('error', 1);
		}
		
		//Get the user in database
		$user = $this->_users->find($id);
		
		//If the user don't exist
		if(!$user){
			
			//Returns a response in JSON format if it's an Ajax request
			if(AjaxRequest::isAjax()){
				return Response::json(array(
					'error' => 1, 
					'reason' => Lang::get('firadmin::admin.messages.user-not-found') 
				));
			}
				
			//Redirect to the user index with errors
			return Redirect::to(Config::get('firadmin::route.user'))
				->with('reason', Lang::get('firadmin::admin.messages.user-not-found'))
				->with('error', 1);
			
		//Else, great the user exist !
		} else {
		
			//Check if it's a ajax request
			if(AjaxRequest::isAjax()){
				
				//Simply return the user data in JSON
				return Response::json($user->toArray());
			}
					
			//Define the layout content
			$this->layout->content = View::make('administration.accounts.show', array(
				'user' => $user
			));
			
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		//Check permission
		if(Permissions::isAllowed(Auth::user(), 'user', 'update') !== true){
			return Redirect::route('get admin/login')
				->with('reason', Lang::get('firadmin::admin.messages.insufficient-permission') . '<br>')
				->with('error', 1);
		}	
		
		//Get the user data
		$user = $this->_users->find($id);
		
		//If the user don't exist
		if(!$user){
			
			//Returns a response in JSON format if it's an Ajax request
			if(AjaxRequest::isAjax()){
				return Response::json(array(
					'error' => 1, 
					'reason' => Lang::get('firadmin::admin.messages.user-not-found') 
				));
			}
				
			//Redirect to users index with errors
			return Redirect::to('/admin/accounts')
				->with('reason', Lang::get('firadmin::admin.messages.user-not-found'))
				->with('error', 1);
			
		//Else the user exist great !
		} else {
		
			//If we have roles set from input
			Input::old('roles')
				? $selected_roles = Input::old('roles') //select them from input
				: $selected_roles = $user->getRoles(); //No roles selected, get them from the model			
			
			//Define the layout content
			$this->layout->content = View::make('administration.accounts.edit', array(
				'user' => $user,
				'selected_roles' => $selected_roles
			));
			
		}
	}

	/**
	 * Update the specified resource in storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		//Check permission
		if(Permissions::isAllowed(Auth::user(), 'user', 'update') !== true){
			return Redirect::route('get admin/login')
				->with('reason', Lang::get('firadmin::admin.messages.insufficient-permission') . '<br>')
				->with('error', 1);
		}
			
		$user = User::find($id);
		
		//If the user don't exist
		if(!$user){
			
			//Returns a response in JSON format if it's an Ajax request
			if(AjaxRequest::isAjax()){
				return Response::json(array(
					'error' => 1, 
					'reason' => Lang::get('firadmin::admin.messages.user-not-found') 
				));
			}
				
			//Redirect to user index with errors
			return Redirect::to('/admin/accounts')
				->with('reason', Lang::get('firadmin::admin.messages.user-not-found'))
				->with('error', 1);
			
		//Else, great the user exist !!
		} else {
			
			//Define custom validation rules because we don't want to validate same field then user store
			$rules = array(
			    'username' => 'required|min:5|unique:users',
			    'email' => 'required|email|unique:users',
		    );
			
			//Check if the username have changed
			if($user->username == Input::get ('username')){
				
				//The username still the same so replace the validation rules
				$rules['username'] = 'required|min:5';
			}
			
			//Check if the email have changed
			if($user->email == Input::get ('email')){
				//The email still the same so replace the validation rules
				$rules['email'] = 'required|email';
			}

			//Update user data
			$user->username = Input::get('username');
			$user->email = Input::get('email');
			
			//Just before save, we don't want to auto hash the existing password, replace this later if possible
			//$user->autoHashPasswordAttributes = false;		
				
			//Try to save the user
			if($user->save($rules)){			
				$this->assignRole($user, Input::get('role'));
				$u = User::find($user->id);
				$u->company_id =  Input::get('company_id');
				$u->level =  Input::get('level');
				$u->save();

				//Returns a response in JSON format if it's an Ajax request
				if(AjaxRequest::isAjax()){
					return Response::json(array ('success' => Lang::get('firadmin::admin.update-success') ));
				}
		
				//Redirect to index with success message
				return Redirect::to('/admin/accounts')
					->with('success', Lang::get('firadmin::admin.update-success'));

			//Else, save validation fail
			} else {
	
				//Flash input to repopulate them in the form
				Input::flash();	

				//Returns a response in JSON format if it's an Ajax request
				if(AjaxRequest::isAjax()){
					return Response::json(array(
						'error' => 1, 
						'reason' => $user->errors()->all(':message') 
					));
				}
			
				//Redirect to the form with errors
				return Redirect::to( '/admin/accounts/' . $id . '/edit')
					->with('reason', $user->errors()->all(':message<br>'))
					->with('error', 1);
				
			}
		}
	}

	/**
	 * Change the user password
	 *
	 * @param int $id
	 * @return Response
	 */
	public function changePassword($id)
	{
		//Check permission
		if(Permissions::isAllowed(Auth::user(), 'user', 'update') !== true){
			return Redirect::route('get admin/login')
				->with('reason', Lang::get('firadmin::admin.messages.insufficient-permission') . '<br>')
				->with('error', 1);
		}
			
		//Get the user in database
		$user = User::find($id);
		
		//If the user don't exist
		if(!$user){
			
			//Returns a response in JSON format if it's an Ajax request
			if(AjaxRequest::isAjax()){
				return Response::json(array(
					'error' => 1, 
					'reason' => Lang::get('firadmin::admin.messages.user-not-found') 
				));
			}
				
			//Redirect to user index with error
			return Redirect::to('/admin/accounts')
				->with('reason', Lang::get('firadmin::admin.messages.user-not-found'))
				->with('error', 1);
			
		// Else, great the user exist !!!
		} else {
		
			//Define custom validation rules because we don't want to validate same field then user store
			$rules = array(
		    	'password' => 'required|min:5',
		    	'password_confirmation' => 'required|min:5|same:password',
		    );
				
			//Update user
			$user->password = Input::get('password');
			$user->password_confirmation = Input::get('password_confirmation');
			
			//Try to save the user
			if($user->save($rules)){
				
				//Returns a response in JSON format if it's an Ajax request
				if(AjaxRequest::isAjax()){
					return Response::json(array ('success' => Lang::get('firadmin::admin.update-password-success') ));
				}
		
				//Redirect to user index with success message
				return Redirect::to('/admin/accounts')
					->with('success', Lang::get('firadmin::admin.update-password-success'));
				
			//Else, save validation fail
			} else {
	
				//Flash input to repopulate them in the form
				Input::flash();
				
				//Returns a response in JSON format if it's an Ajax request
				if( AjaxRequest::isAjax()){
					return Response::json(array(
						'error' => 1, 
						'reason' => $user->errors()->all(':message') 
					));
				}
				
				//Redirect to the form with errors
				return Redirect::to('/admin/accounts/' . $id . '/edit#change-password')
					->with('reason', $user->errors()->all(':message<br>'))
					->with('error', 1);
				
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//Check permission
		if(Permissions::isAllowed(Auth::user(), 'user', 'delete') !== true){
			return Redirect::route('get admin/login')
				->with('reason', Lang::get('firadmin::admin.messages.insufficient-permission') . '<br>')
				->with('error', 1);
		}
		
		//Get the user in database
		$user = $this->_users->find($id);
		
		//If the user don't exist
		if(!$user){
				
			//Returns a response in JSON format if it's an Ajax request
			if( AjaxRequest::isAjax() ){
				return Response::json(array (
					'error' => 1, 
					'reason' => Lang::get('firadmin::admin.destroy-fail') 
				));
			}
				
			//Redirect to user index with errors
			return Redirect::to('/admin/accounts')
				->with('reason', Lang::get('firadmin::admin.destroy-fail'))
				->with('error', 1);
			
		//Else, great the user exist !!
		} else {		
			
			//Delete the user roles
			$user->roles()->delete();
			
			//Delete the user
			$user->delete($id);	

			//Returns a response in JSON format if it's an Ajax request
			if( AjaxRequest::isAjax()){
				return Response::json(array ('success' => Lang::get('firadmin::admin.destroy-success') ));
			}
			
			//Redirect to the user index with success message
			return Redirect::to('/admin/accounts')
				->with('success', Lang::get('firadmin::admin.destroy-success'));
		}
	}
}