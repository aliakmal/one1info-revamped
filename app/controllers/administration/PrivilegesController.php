<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Privilege;
use Photo, CSV, Datatables;

class PrivilegesController extends BaseController {

	/**
	 * Privilege Repository
	 *
	 * @var Privilege
	 */
	protected $privilege;
  	public $active_menu = 'admin/privileges';

	public function __construct(Privilege $privilege)
	{
		$this->privilege = $privilege;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$privileges = $this->privilege->all();

		$this->layout->content = View::make('administration.privileges.index', compact('privileges'));
	}

  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');

		//Default layout content is null
		$this->layout->content = '';

		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('administration.privileges.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Privilege::$rules);

		if ($validation->passes()){
			$input['permissions'] = serialize($input['permissions']);
			$this->privilege->create($input);

			Privilege::resetSettings();

			return Redirect::route('admin.privileges.index');
		}

		return Redirect::route('admin.privileges.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$privilege = $this->privilege->findOrFail($id);
		$privilege->permissions = unserialize($privilege->permissions);
		$this->layout->content = View::make('administration.privileges.show', compact('privilege'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$privilege = $this->privilege->find($id);

		if (is_null($privilege))
		{
			return Redirect::route('admin.privileges.index');
		}

		$privilege->permissions = unserialize($privilege->permissions);

		$this->layout->content = View::make('administration.privileges.edit', compact('privilege'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Privilege::getRulesForUpdating($id));

		if ($validation->passes())
		{
			$input['permissions'] = serialize($input['permissions']);

			$privilege = $this->privilege->find($id);
			$privilege->update($input);
			Privilege::resetSettings();
			return Redirect::route('admin.privileges.show', $id);
		}

		return Redirect::route('admin.privileges.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->privilege->find($id)->delete();

		return Redirect::route('admin.privileges.index');
	}

}
