<?php
namespace Administration;

use BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input, Mail;

use Membership_option;
use Photo, CSV, Datatables;

class Membership_optionsController extends BaseController {

	/**
	 * Membership_option Repository
	 *
	 * @var Membership_option
	 */
	protected $membership_option;

	public function __construct(Membership_option $membership_option)
	{
		$this->membership_option = $membership_option;
	}

	public $active_menu = 'admin/membership_options';

	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.admin');
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$membership_options = $this->membership_option->all();

		$this->layout->content =  View::make('administration.membership_options.index', compact('membership_options'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('administration.membership_options.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Membership_option::$rules);

		if ($validation->passes())
		{
			$this->membership_option->create($input);

			return Redirect::route('admin.membership_options.index');
		}

		return Redirect::route('admin.membership_options.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$membership_option = $this->membership_option->findOrFail($id);
		$membership_option->options = unserialize($membership_option->options);

		$this->layout->content =  View::make('administration.membership_options.show', compact('membership_option'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$membership_option = $this->membership_option->find($id);

		if (is_null($membership_option))
		{
			return Redirect::route('admin.membership_options.index');
		}

		$membership_option->options = unserialize($membership_option->options);

		$this->layout->content =  View::make('administration.membership_options.edit', compact('membership_option'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::except('_method');
		$validation = Validator::make($input, Membership_option::$rules);

		if ($validation->passes())
		{
			$membership_option = $this->membership_option->find($id);
			$input['options'] = serialize($input['options']);
			$membership_option->update($input);

			return Redirect::route('admin.membership_options.show', $id);
		}

		return Redirect::route('admin.membership_options.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->membership_option->find($id)->delete();

		return Redirect::route('admin.membership_options.index');
	}

}
