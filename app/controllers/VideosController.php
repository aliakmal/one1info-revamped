<?php
use \BaseController;



class VideosController extends BaseController {

	/**
	 * Video Repository
	 *
	 * @var Video
	 */
	protected $video;

	public function __construct(Video $video)
	{
		$this->video = $video;
	}
	public $active_menu = 'videos';


  protected function setupLayout()
	{
		//Set layout view
		//Set layout view
		$this->layout = View::make('layouts.master');		
		
		//Set navigation
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		
		//Set application title
		$this->layout->title = 'Test';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
    	$sections = Section::all();
    	
		$featured_videos = Video::all()->take(6);
		$videos = Video::all();

		$this->layout->content =  View::make('videos.index', array('videos'=>$videos, 'featured_videos'=>$featured_videos, 'sections'=>$sections));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$video = $this->video->findOrFail($id);

		$this->layout->content =  View::make('videos.show', compact('video'));
	}
}
