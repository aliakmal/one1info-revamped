<?php

class IndustriesController extends BaseController {

	/**
	 * Industry Repository
	 *
	 * @var Industry
	 */
	protected $industry;
	public $active_menu = 'admin/industries';

	public function __construct(Industry $industry)
	{
		$this->industry = $industry;
	}
  
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$industries = $this->industry->all();

		$this->layout->content = View::make('industries.index', compact('industries'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('industries.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Industry::$rules);

		if ($validation->passes())
		{
			$this->industry->create($input);

			return Redirect::route('industries.index');
		}

		return Redirect::route('industries.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$industry = $this->industry->findOrFail($id);

		$this->layout->content = View::make('industries.show', compact('industry'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$industry = $this->industry->find($id);

		if (is_null($industry))
		{
			return Redirect::route('industries.index');
		}

		$this->layout->content = View::make('industries.edit', compact('industry'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Industry::$rules);

		if ($validation->passes())
		{
			$industry = $this->industry->find($id);
			$industry->update($input);

			return Redirect::route('industries.show', $id);
		}

		return Redirect::route('industries.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->industry->find($id)->delete();

		return Redirect::route('industries.index');
	}

}
