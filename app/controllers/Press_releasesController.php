<?php

class Press_releasesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$entries = Press_release::paginate(15);
		$sections = Section::all();

		$this->layout->content =  View::make('press_releases.index', array('sections' => $sections, 'press_releases' => $entries));
	}

	public function my()
	{
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();

		$entries = Press_release::where('company_id', '=', $user->company_id)->get();
		$sections = Section::all();
		
		$this->layout->content =  View::make('press_releases.dashboard.index', array('sections' => $sections,'company'=>$company, 'current'=>'/profile/press_releases', 'press_releases' => $entries));
	}


	public $active_menu = '';
  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.master');		
		
		//Set navigation
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		
		//Set application title
		
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

      	$press_release = Press_release::find($id);
	    $sections = Section::all();

		$this->layout->content =  View::make('press_releases.show', array('article'=>$press_release, 'sections'=>$sections));
	}



	public function create(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$this->layout->content =  View::make('press_releases.create', array('company'=>$company, 'current'=>'/profile/press_releases'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::except('photos', 'captions', '_token');// Input::all();
		$input['is_published'] = 0;
		$user = User::find(Auth::user()->id);

		$company = $user->company()->first();
		$input['company_id'] = $company->id;

		$validation = Validator::make($input, Press_release::$rules);
		$photos = Input::file('photos');
		$captions = Input::file('captions');


		if ($validation->passes())
		{
			$press_release = Press_release::create($input);
			if(count($photos)>0):
			foreach($photos as $ii => $one_photo){
				$photo = new Photo();
				$name = $press_release->title . '-' . $ii;
			
				$photo->create_n_upload($one_photo, array(	'name'=>$name, 
															'caption'=>$captions[$ii], 
														'imageable_type'=>'Press_release', 
														'imageable_id'=>$press_release->id ));
			}
			endif;

			return Redirect::route('profile.press_releases.index');
		}

		return Redirect::route('profile.press_releases.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$press_release = Press_release::find($id);
		$user = User::find(Auth::user()->id);

		$company = $user->company()->first();

		if (is_null($press_release))
		{
			return Redirect::route('profile.press_releases.index');
		}

		$this->layout->content =  View::make('press_releases.edit', array('press_release'=>$press_release, 'company'=>$company, 'current'=>'/profile/press_releases'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::except('photos', 'captions', 'delete_current_images'), '_method');//array_except(Input::all(), '_method');
		$validation = Validator::make($input, Press_release::$rules);

		if ($validation->passes())
		{
			$press_release = Press_release::find($id);
			$input['is_published'] = 0;
			$press_release->update($input);

			// deletable images
			$deletables = Input::only('delete_current_images');

			foreach($deletables as $i => $one_deletable):
				$deleteme = Photo::find(array_pop($one_deletable));
			if(!is_null($deleteme))
				$deleteme->delete();
			endforeach;

			$photos = Input::file('photos');
			$captions = Input::file('captions');



			if(count($photos) > 0):
				foreach($photos as $ii => $one_photo){
					$photo = new Photo();
					$name = $press_release->title . '-' . $ii;
				
					$photo->create_n_upload($one_photo, array(	'name'=>$name, 
																'caption'=>$captions[$ii], 
															'imageable_type'=>'Press_release', 
															'imageable_id'=>$press_release->id ));
				}
			endif;






			return Redirect::route('profile.press_releases');
		}

		return Redirect::route('profile.press_releases.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	public function getPost(){
		$this->layout->content =  View::make('press_releases.post');
	}

	public function postPost(){
		$input = Input::all();
		$rules = array(
		'full_name' => 'required',
		'company' => 'required',
		'position' => 'required',
		'address' => 'required',
		'city' => 'required',
		'country' => 'required',
		'email' => 'required',
		'phone' => 'required',
		'publish_date' => 'required',
		'mobile' => 'required',
		);

		$validation = Validator::make($input, $rules);

		if($validation->passes())
		{
			//$this->industry->create($input);
			// email to addresss here

			$this->layout->content =  View::make('press_releases.posted');
		
		}else{

		return Redirect::to('press_release/post')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Press_release::find($id)->delete();

		return Redirect::route('profile.press_releases.index');
	}


}