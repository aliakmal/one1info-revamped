<?php
use \BaseController;

 

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
  
	public $active_menu = '';
  	protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make('layouts.master');		
		
		//Set navigation
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		
		//Set application title
		$this->layout->title = 'Test';
		
	}

	public function feedback(){
		$input = Input::all();
		$validation = Validator::make($input, array('name'=>'required', 'email'=>'required', 'phone'=>'required', 'message'=>'required'));

		if ($validation->passes())
		{
			// send email code here
			$data = array('enquiry'=>$input);

			Mail::send('emails.contact-us', $data, function($message) use ($data)
			{
			    $message->to('sales@one1info.com', 'One1Info')->subject('You have an Enquiry');
			});

			return Redirect::route('contact-us')->with('success', 'Thank you for your message.');
;
		}

		return Redirect::route('contact-us')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(){
		$featured_articles = Article::all()->take(5);
   		$videos = Video::all()->take(10);
	    $sections = Section::all();
    	$articles = Article::all();
    	$press_releases = Press_release::all()->take(3);
    	$ideas = Idea::all()->take(3);
		$this->layout->content =  View::make('home.index', array(	'sections' => $sections, 
																	'articles' => $articles, 
																	'videos' => $videos, 
																	'featured_articles' => $featured_articles, 
																	'press_releases' => $press_releases, 
																	'ideas' => $ideas));
	}

	public function about(){
		$this->layout->content = View::make('home.about');
	}

	public function toc(){
		$this->layout->content = View::make('home.toc');
	}

	public function join(){
		$this->layout->content = View::make('home.join');
	}

	public function corporate(){
		$services = Service::orderBy('sort', 'ASC')->get();
		$this->layout->content = View::make('home.corporate', array('services'=>$services));
	}

	public function privacy(){
		$this->layout->content = View::make('home.privacy');
	}

	public function contact(){
		$this->layout->content = View::make('home.contact-us');
	}

	public function options(){
		$this->layout->content = View::make('home.options');
	}

	public function showWelcome(){
		return View::make('hello');
	}

	public function account(){
	  if(Role::hasRole(Auth::user(),'basic-member')){
	    $this->basicMember();
	  }elseif(Role::hasRole(Auth::user(),'basic-company')){
	    $this->basicCompany();
	  }elseif(Role::hasRole(Auth::user(), 'premium-company')){
	    $this->premiumCompany();
	  }elseif(Role::hasRole(Auth::user(), 'administrator')){
	  	return Redirect::to('admin/');
	  }
	}

	public function getUpgrade(){
		$this->layout->content = View::make('home.upgrade');
	}

	public function postUpgrade(){
    	$user = User::find(Auth::user()->id);
		$member = $user->company()->first();
		$member = Company::find($member->id);

		$data = array('user'=>$user, 'company'=>$member);

		Mail::send('emails.upgrade.user', $data, function($message) use ($data)
		{
		    $message->to('sales@one1info.com', 'One1Info')->subject('Request for Upgrade');
		});

        return Redirect::to('/upgrade/success')->with('success');
	}

	public function getUpgradeSuccess(){
		$this->layout->content = View::make('home.upgrade-success');
	}

	public function basicMember(){
		$this->layout->content = View::make('home.dashboards.basic-member');
	}

  	public function basicCompany(){
		$this->layout->content = View::make('home.dashboards.basic-company');
  	}

	public function premiumCompany(){
		$this->layout->content = View::make('home.dashboards.premium-company');
	}

	public function getEditProfile(){
	  if(Role::hasRole(Auth::user(),'basic-member')){
	    return $this->getEditBasicMember();
	  }elseif(Role::hasRole(Auth::user(),'premium-member')){
	    return $this->getEditBasicMember();
	  }elseif(Role::hasRole(Auth::user(),'basic-company')){
	    return $this->getEditCompany();
	  }elseif(Role::hasRole(Auth::user(), 'premium-company')){
	    return $this->getEditCompany();//return $this->getEditPremiumCompany();
	  }
	}

	public function postEditProfile(){
	  if(Role::hasRole(Auth::user(),'basic-member')){
	    return $this->postEditBasicMember();
	  }elseif(Role::hasRole(Auth::user(),'premium-member')){
	    return $this->postEditBasicMember();
	  }elseif(Role::hasRole(Auth::user(),'basic-company')){
	    return $this->postEditCompany();
	  }elseif(Role::hasRole(Auth::user(), 'premium-company')){
	    return $this->postEditCompany();
	  }
	}

	public function getUploadProfile(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$this->layout->content = View::make('home.dashboards.edit-profile', array('company'=>$company, 'current'=>'/profile/view'));
	}

	public function postUploadProfile(){
	    $member_data = Input::all();

    	$user = User::find(Auth::user()->id);
		$member = $user->company()->first();
		$member = Company::find($member->id);
    	$member->update($member_data);

        return Redirect::to('/profile/view')->with('success');
	}

	public function getEditBasicMember(){
		$user = User::find(Auth::user()->id);
		$member = $user->member_details()->first();
		$this->layout->content = View::make('home.dashboards.edit-member', array('member'=>$member));
	}

	public function postEditBasicMember()
  	{
  		$cc = Config::get('auth.model');
		$user = $cc::find(Auth::user()->id);
		//$member_data = Input::all();

	    $user_data = Input::only('email', 'password', 'password_confirmation');
	    $member_data = Input::except('email', 'password', 'password_confirmation');

    	$user_rules = (isset($user_data['password']) && (strlen($user_data['password'].$user_data['password_confirmation']) > 0) ) ? User::getRulesForUpdating($user->id):User::getRulesForUpdatingNoPassword($user->id);

	    $validation = Validator::make(array_merge($member_data, $user_data), array_merge($user_rules, Member::$rules));


	    if($validation->passes()){
			$member = Member::find(User::find($user->id)->member_details()->first()->id);
	    	$member->update($member_data);

	    	if((isset($user_data['password']) && (strlen($user_data['password'].$user_data['password_confirmation']) > 0) ))
		    	$user->password = $user_data['password'];
    		$user->email = $user_data['email'];
	    	$user->forceSave();
	        return Redirect::to('/profile/edit')->with('success', 'Congratulations! You updated your details');
	    }else{
	      //Flash input to repopulate them in the form
	      Input::flash();
	      //Redirect to the form with errors 
	      return Redirect::to('/profile/edit')->withInput()->withErrors($validation)
	        ->with('reason', $validation->errors()->all(':message<br>'))
	        ->with('error', 1);
	    }
	}

	public function viewCompanyProfile(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();

		$this->layout->content = View::make('home.dashboards.view-company-profile', array('company'=>$company, 'current'=>"/profile/view") );
	}

	public function getUsers(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$users = User::where('company_id', '=', $company->id)->get();
		$this->layout->content = View::make('home.dashboards.profile-users', array('company'=>$company, 'users'=>$users, 'current'=>'/profile/users'));
	}

	public function postUsers()
  	{
	    $data = Input::all();
    	$current_user = User::find(Auth::user()->id);
		$company = $current_user->company()->first();
		$user = app()->make('Firalabs\Firadmin\Repository\UserRepositoryInterface');
      	
      	$user->username = $data['username'];
      	$user->email    = $data['email'];
	    $user->password = $data['password'];
      	$user->password_confirmation = $data['password'];
      	$user->company_id = $company->id;
      	$user->level = 'user';
      	$user->save();
      	if((int)$user->id > 0){

	    	$roles = app()->make('Firalabs\Firadmin\Repository\UserRoleRepositoryInterface');   
	        $roles->role = 'basic-member';
	        //Save the user role
	        $user->roles()->save($roles);

	        return Redirect::to('/profile/users')->with('success', Lang::get('firadmin::admin.store-success'));
	    }else{
	    	return Redirect::to('/profile/users')->withInput()->withErrors($user->errors())->with('reason', $user->errors()->all(':message<br>'))->with('error', 1);
	    }

	}

	public function deleteUser($id)
	{
		User::find($id)->delete();
		return Redirect::to('/profile/users');

	}

	public function getInterviews(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$this->layout->content = View::make('home.dashboards.profile-interviews', array('company'=>$company, 'current'=>'/profile/users'));
	}

	public function postInterviews()
  	{
	    $input = Input::except('_token');
    	$current_user = User::find(Auth::user()->id);
		$company = $current_user->company()->first();

	    $input['is_published'] = 0;
		$input['company_id'] = $company->id;

		$validation = Validator::make($input, Company_interview::$rules);

		if ($validation->passes()){
			Company_interview::create($input);
	        return Redirect::to('/profile/company_interviews')->with('success', 'Interview saved');
	    }else{
	    	return Redirect::to('/profile/company_interviews/create')->withInput()->withErrors($validation)
        ->with('message', 'There were errors')->with('error', 1);
	    }

	}


	public function putInterviews($id)
  	{
	    $input = Input::except('_token');
    	$current_user = User::find(Auth::user()->id);
		$company = $current_user->company()->first();
	    $input['is_published'] = 0;
		$input['company_id'] = $company->id;


		$validation = Validator::make($input, Company_interview::$rules);

		if ($validation->passes()){
			$company_interview = Company_interview::find($id);
			$company_interview->update($input);
	        return Redirect::to('/profile/company_interviews')->with('success', 'Interview saved');
	    }else{
	    	return Redirect::to('/profile/company_interviews/create')->withInput()->withErrors($validation)
        ->with('message', 'There were errors')->with('error', 1);
	    }

	}


	public function getEditLocation(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$this->layout->content = View::make('home.dashboards.edit-map-location', array('company'=>$company, 'current'=>'/location/edit'));

	}

	public function postEditLocation()
  	{
	    $member_data = Input::only('location_x','location_y');

    	$user = User::find(Auth::user()->id);
		$member = $user->company()->first();
		$member = Company::find($member->id);
    	$member->update($member_data);

        return Redirect::to('/my-account')->with('success', 'Location has been saved.');
	}

	public function getEditCompany(){
		$user = User::find(Auth::user()->id);
		$company = $user->company()->first();
		$this->layout->content = View::make('home.dashboards.edit-company-member', array('company'=>$company, 'current'=>'/profile/edit'));
	}

	public function postEditCompany()
  	{
	    $member_data = Input::except('_token','logo', 'categories', 'email', 'password', 'password_confirmation');
	    $user_data = Input::only( 'email', 'password', 'password_confirmation');

  		$cc = Config::get('auth.model');
		$user = $cc::find(Auth::user()->id);
    	//$user = User::find(Auth::user()->id);

    	$user_rules = (isset($user_data['password']) || strlen($user_data['password']) == 0) ? User::getRulesForUpdatingNoPassword($user->id):User::getRulesForUpdating($user->id);

	    $validation = Validator::make(array_merge($member_data, $user_data), array_merge($user_rules, Company::$rules));

	    if($validation->passes()){
			$member = User::find($user->id)->company()->first();
			$member = Company::find($member->id);
	    	$member->update($member_data);

	    	if((isset($user_data['password']) && (strlen($user_data['password'].$user_data['password_confirmation']) > 0) )){

		    	$user->password = $user_data['password'];
		    	$user->password_confirmation = $user_data['password'];

	    	}

	    	$user->email = $user_data['email'];
	    	$user->forceSave();

	        $categories = Input::only('categories');

	        if(count($categories['categories'])>0)
	        	$member->categories()->sync(array_values($categories['categories']));
			
			$logo = Input::file('logo');  
	        if(!is_null($logo)){

	          $member->logo()->delete();
	          $photo = new Photo();
	          $name = 'logo-'.$member->id;
	            
	          $photo->create_n_upload($logo, array(  'name'=>$name, 
	                                  'caption'=>'Logo for '.$member->company_name, 
	                                'imageable_type'=>'Company', 
	                                'imageable_id'=>$member->id ));
	        }

	        return Redirect::to('/profile/view')->with('success', 'Details have been saved');

	    }else {
	      //Flash input to repopulate them in the form
	      Input::flash();
	      //Redirect to the form with errors 
	      return Redirect::to('/profile/edit')->withInput()->withErrors($validation)
	        ->with('reason', $validation->errors()->all(':message<br>'))
	        ->with('error', 1);
	    
	    }
	}




}