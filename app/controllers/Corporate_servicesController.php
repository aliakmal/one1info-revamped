<?php

class Corporate_servicesController extends BaseController {

	/**
	 * Corporate_service Repository
	 *
	 * @var Corporate_service
	 */
	protected $corporate_service;
  public $active_menu = 'admin/corporate_service';
	
  public function __construct(Corporate_service $corporate_service)
	{
		$this->corporate_service = $corporate_service;
	}

  
  protected function setupLayout()
	{
		//Set layout view
		$this->layout = View::make(Config::get('firadmin::layout'));
		
		//Default layout content is null
		$this->layout->content = '';		
		
		//Set navigation
		$this->layout->navigation = Config::get('firadmin::navigation');
		
		//Default we don't have active menu 
		$this->layout->active_menu = $this->active_menu;	
		
		//Set javascript assets
		$this->layout->assets = Config::get('firadmin::assets');	
		
		//Set application title
		$this->layout->title = Config::get('firadmin::title');
		
		//Set project name
		$this->layout->project_name = Config::get('firadmin::project_name');
	}
  
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$corporate_services = $this->corporate_service->all();

		$this->layout->content = View::make('corporate_services.index', compact('corporate_services'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('corporate_services.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Corporate_service::$rules);

		if ($validation->passes())
		{
			$this->corporate_service->create($input);

			return Redirect::route('corporate_services.index');
		}

		return Redirect::route('corporate_services.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$corporate_service = $this->corporate_service->findOrFail($id);

		$this->layout->content = View::make('corporate_services.show', compact('corporate_service'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$corporate_service = $this->corporate_service->find($id);

		if (is_null($corporate_service))
		{
			return Redirect::route('corporate_services.index');
		}

		$this->layout->content = View::make('corporate_services.edit', compact('corporate_service'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Corporate_service::$rules);

		if ($validation->passes())
		{
			$corporate_service = $this->corporate_service->find($id);
			$corporate_service->update($input);

			return Redirect::route('corporate_services.show', $id);
		}

		return Redirect::route('corporate_services.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->corporate_service->find($id)->delete();

		return Redirect::route('corporate_services.index');
	}

}
