<?php 


class LoginController extends \Firalabs\Firadmin\Controllers\LoginController{
  
  public $active_menu = '';
    
    protected function setupLayout()
  {
    //Set layout view
    $this->layout = View::make('layouts.master');   
    
    //Set navigation
    //Default we don't have active menu 
    $this->layout->active_menu = $this->active_menu;  
    
    //Set javascript assets
    
    //Set application title
    $this->layout->title = 'Test';
    
  }

  public function getIndex()
  {
    $this->layout->content = View::make('login.index');
  }






  public function postIndex()
  {
    // Define user creadentials
    $credentials = array(
      'username' => Input::get('username'),
      'password' => Input::get('password'),
      'active' => 1
    );
    
    //Want to be remember ?
    if(Input::get('remember-me')){
      $remember = true;
    } else {
      $remember = false;
    }
    
    //If the login attempt fail
    if ( !Auth::attempt($credentials, $remember) ){

      //Redirect to login page
      return Redirect::to('/sign-in')
              ->with('reason', Lang::get('firadmin::admin.messages.attempt-fail'))
              ->with('error', 1);
            
    //Else attempt succed
    } else {
      //Redirect to dashboard
      return Redirect::to('/');
    }        
  }
        
  /**
   * Logout the user
   *
   * @return Redirect
   */
  public function getLogout()
  {                
    //Logout the user
    Auth::logout();
    
    //Redirect to login page
    return Redirect::to('/sign-in')
            ->with('success', Lang::get('firadmin::admin.messages.logout-success'));
  }
        
  /**
   * Get the forgot password form
   */
  public function getForgotPassword()
  {
    $this->layout->content = View::make('login.forgot-password');
  }
  
  /**
   * Post the forgot password form
   */
  public function postForgotPassword()
  {
    $result = Password::remind(array('email' => Input::get('email')), function($message){
      $message->subject('Password Reset Request');
    });
    $this->layout->content = View::make('login.password-reminder');

  }
  
  /**
   * Reset the password
   */
  public function getResetPassword($token)
  {
    $this->layout->content = View::make('login.password-reset')->with('token', $token);
  }
  
  /**
   * Reset the password
   */
  public function postResetPassword($token)
  {

    $response =  Password::reset(Input::except('_token'), function($user, $password){
        $user->password = ($password);
        $user->forceSave();
     
      //
    });


    switch ($response)
    {
      case Password::INVALID_PASSWORD:
      case Password::INVALID_TOKEN:
      case Password::INVALID_USER:
        return Redirect::back()->with('message', Lang::get($response));

      case Password::PASSWORD_RESET:
        return Redirect::to('/')->with('success', 'Password has been changed');
    }


  }


}