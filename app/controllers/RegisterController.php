<?php 


class RegisterController extends \Firalabs\Firadmin\Controllers\BaseController{
  
  public $active_menu = '';

  protected $_users;
  
  /**
   * Constructor
   */

    
    protected function setupLayout()
  {
    //Set layout view
    $this->layout = View::make('layouts.master');   
    
    //Set navigation
    //Default we don't have active menu 
    $this->layout->active_menu = $this->active_menu;  
    
    //Set javascript assets
    
    //Set application title
    $this->layout->title = 'Test';
    
  }

  public function getIndex()
  {
    $this->layout->content = View::make('register.index');
  }


  public function success(){
    $this->layout->content = View::make('register.success');
  }

  public function getBasic()
  {
    $this->layout->content = View::make('register.company');
  }
  public function getPremium()
  {
    $this->layout->content = View::make('register.company');
  }
  private function createUser($data){
      //Create user object
      $user = app()->make('Firalabs\Firadmin\Repository\UserRepositoryInterface');
      $user->username = $data['username'];
      $user->email    = $data['email'];
      $user->password = $data['password'];
      $user->password_confirmation = $data['password_confirmation'];
      $user->save();
      return $user;
  }

  private function assignRole($user, $role){
    $roles = app()->make('Firalabs\Firadmin\Repository\UserRoleRepositoryInterface');   
    $roles->role = $role;
    //Save the user role
    $user->roles()->save($roles); 
  }

  public function activate(){
      $id = Input::get('id');
      $hash = Input::get('hash');

      if(md5($id) == $hash){
        $user = User::find($id);
        $user->active = 1;
        $user->save();
        $this->layout->content = View::make('register.activate.success');
      }else{
        $this->layout->content = View::make('register.activate.failure');
      }

  }


  public function postIndex()
  {
    $user_data = Input::only('username', 'email', 'password', 'password_confirmation');
    $member_data = Input::except('username', 'email', 'password', 'password_confirmation');

    $captcha_rules = array('captcha' => array('required', 'captcha'));

    $error_messages = array('captcha'=>'The captcha was invalid - please enter the above characters.');

    $validation = Validator::make($member_data, array_merge(Member::$rules, $captcha_rules), $error_messages);

    if($validation->passes()){
      $user = $this->createUser($user_data);
      if((int)$user->id > 0){
        //Create role
        $this->assignRole($user, 'basic-member');
        $member_data['user_id'] = $user->id;
        unset($member_data['captcha']);

        Member::create($member_data);


        $data = array('hash' => md5($user->id), 'user'=>$user);
        Mail::send('emails.auth.activate', $data, function($message) use ($data)
        {
            $message->to($data['user']->email)->subject('Please activate your account on One1Info!');
        });
  
        //Redirect with a success message
        return Redirect::to('register/success')->with('success', Lang::get('firadmin::admin.store-success'));
      
        //Else, fail to save the user
      } else {  

      //Flash input to repopulate them in the form
      Input::flash();
      //Redirect to the form with errors 
      return Redirect::to('register')->withInput()->withErrors($user->errors())
        ->with('reason', $user->errors()->all(':message<br>'))->with('error', 1);
      
      }
    }else {  
      //Flash input to repopulate them in the form
      Input::flash();

      //Redirect to the form with errors 
      return Redirect::to('register')->withInput()->withErrors($validation)
        ->with('reason', $validation->errors()->all(':message<br>'))
        ->with('error', 1);
    
    }
  }




  public function postBasic()
  {
    $user_data = Input::only('username', 'email', 'password', 'password_confirmation');
    $member_data = Input::except('username', 'logo', 'categories', 'email', 'password', 'password_confirmation');

    $captcha_rules = array('captcha' => array('required', 'captcha'));
    $error_messages = array('captcha'=>'The captcha was invalid - please enter the above characters.');

    $validation = Validator::make($member_data, array_merge(Company::$rules, $captcha_rules), $error_messages);

    if($validation->passes()){
      $user = $this->createUser($user_data);
      if((int)$user->id > 0){
        //Create role
        $this->assignRole($user, 'basic-company');
        unset($member_data['captcha']);

        $company = Company::create($member_data);   
        
        $user = User::find($user->id);
        $user->company_id = $company->id;
        $user->level = 'admin';
        $user->save();
        $categories = Input::only('categories');
        
        $company->categories()->sync(array_values($categories['categories']));
  
        if($logo = Input::file('logo'))
        {
          $photo = new Photo();
          $name = 'logo-'.$company->id;
            
          $photo->create_n_upload($logo, array(  'name'=>$name, 
                                  'caption'=>'Logo for '.$company->company_name, 
                                'imageable_type'=>'Company', 
                                'imageable_id'=>$company->id ));
        }


        $data = array('hash' => md5($user->id), 'user'=>$user);
        Mail::send('emails.auth.activate', $data, function($message) use ($data)
        {
            $message->to($data['user']->email)->subject('Please activate your account on One1Info!');
        });

       
        //Redirect with a success message
        return Redirect::to('register/success')->with('success', Lang::get('firadmin::admin.store-success'));
      
        //Else, fail to save the user
      } else {  

      //Flash input to repopulate them in the form
      Input::flash();
      //Redirect to the form with errors 
      return Redirect::to('register/basic')->withInput()->withErrors($user->errors())
        ->with('reason', $user->errors()->all(':message<br>'))->with('error', 1);
      
      }
    }else {  
      //Flash input to repopulate them in the form
      Input::flash();

      //Redirect to the form with errors 
      return Redirect::to('register/basic')->withInput()->withErrors($validation)
        ->with('reason', $validation->errors()->all(':message<br>'))
        ->with('error', 1);
    
    }
  }

  public function postPremium()
  {
    $user_data = Input::only('username', 'email', 'password', 'password_confirmation');
    $member_data = Input::except('username', 'logo', 'email', '_token', 'categories', 'password', 'password_confirmation');

    $captcha_rules = array('captcha' => array('required', 'captcha'));
    $error_messages = array('captcha'=>'The captcha was invalid - please enter the above characters.');

    $validation = Validator::make($member_data, array_merge(Company::$rules, $captcha_rules), $error_messages);

    if($validation->passes()){
      $user = $this->createUser($user_data);
      if((int)$user->id > 0){
        //Create role
        $this->assignRole($user, 'basic-company');
        unset($member_data['captcha']);

        $company = Company::create($member_data);   

        $categories = Input::only('categories');
        
        $company->categories()->sync(array_values($categories['categories']));
  
        if($logo = Input::file('logo'))
        {
          $photo = new Photo();
          $name = 'logo-'.$company->id;
            
          $photo->create_n_upload($logo, array(  'name'=>$name, 
                                  'caption'=>'Logo for '.$company->company_name, 
                                'imageable_type'=>'Company', 
                                'imageable_id'=>$company->id ));
        }

        $user = User::find($user->id);
        $user->company_id = $company->id;
        $user->level = 'admin';
        $user->save();

        $data = array('hash' => md5($user->id), 'user'=>$user);
        Mail::send('emails.auth.activate', $data, function($message) use ($data)
        {
            $message->to($data['user']->email)->subject('Please activate your account on One1Info!');
        });


        //Redirect with a success message
        return Redirect::to('register/success')->with('success', Lang::get('firadmin::admin.store-success'));
      
        //Else, fail to save the user
      } else {  

      //Flash input to repopulate them in the form
      Input::flash();
      //Redirect to the form with errors 
      return Redirect::to('register/premium')->withInput()->withErrors($user->errors())
        ->with('reason', $user->errors()->all(':message<br>'))->with('error', 1);
      
      }
    }else {  
      //Flash input to repopulate them in the form
      Input::flash();

      //Redirect to the form with errors 
      return Redirect::to('register/premium')->withInput()->withErrors($validation)
        ->with('reason', $validation->errors()->all(':message<br>'))
        ->with('error', 1);
    
    }
  }


        
  /**
   * Logout the user
   *
   * @return Redirect
   */
        
}