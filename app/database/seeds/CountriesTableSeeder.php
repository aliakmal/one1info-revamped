<?php

class CountriesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('countries')->truncate();



// `one1info`.`countries`
$countries = array(
  array('id' => '1','name' => 'Afghanistan'),
  array('id' => '2','name' => 'Albania'),
  array('id' => '3','name' => 'Algeria'),
  array('id' => '4','name' => 'American Samoa'),
  array('id' => '5','name' => 'Andorra'),
  array('id' => '6','name' => 'Angola'),
  array('id' => '7','name' => 'Anguilla'),
  array('id' => '8','name' => 'Antarctica'),
  array('id' => '9','name' => 'Antigua and Barbuda'),
  array('id' => '10','name' => 'Argentina'),
  array('id' => '11','name' => 'Armenia'),
  array('id' => '12','name' => 'Aruba'),
  array('id' => '13','name' => 'Australia'),
  array('id' => '14','name' => 'Austria'),
  array('id' => '15','name' => 'Azerbaijan'),
  array('id' => '16','name' => 'Bahamas'),
  array('id' => '17','name' => 'Bahrain'),
  array('id' => '18','name' => 'Bangladesh'),
  array('id' => '19','name' => 'Barbados'),
  array('id' => '20','name' => 'Belarus'),
  array('id' => '21','name' => 'Belgium'),
  array('id' => '22','name' => 'Belize'),
  array('id' => '23','name' => 'Benin'),
  array('id' => '24','name' => 'Bermuda'),
  array('id' => '25','name' => 'Bhutan'),
  array('id' => '26','name' => 'Bolivia'),
  array('id' => '27','name' => 'Bosnia and Herzegovina'),
  array('id' => '28','name' => 'Botswana'),
  array('id' => '29','name' => 'Bouvet Island'),
  array('id' => '30','name' => 'Brazil'),
  array('id' => '31','name' => 'British Indian Ocean Territory'),
  array('id' => '32','name' => 'Brunei Darussalam'),
  array('id' => '33','name' => 'Bulgaria'),
  array('id' => '34','name' => 'Burkina Faso'),
  array('id' => '35','name' => 'Burundi'),
  array('id' => '36','name' => 'Cambodia'),
  array('id' => '37','name' => 'Cameroon'),
  array('id' => '38','name' => 'Canada'),
  array('id' => '39','name' => 'Cape Verde'),
  array('id' => '40','name' => 'Cayman Islands'),
  array('id' => '41','name' => 'Central African Republic'),
  array('id' => '42','name' => 'Chad'),
  array('id' => '43','name' => 'Chile'),
  array('id' => '44','name' => 'China'),
  array('id' => '45','name' => 'Christmas Island'),
  array('id' => '46','name' => 'Cocos Islands'),
  array('id' => '47','name' => 'Colombia'),
  array('id' => '48','name' => 'Comoros'),
  array('id' => '49','name' => 'Congo'),
  array('id' => '51','name' => 'Cook Islands'),
  array('id' => '52','name' => 'Costa Rica'),
  array('id' => '53','name' => 'Cote d\'Ivoire'),
  array('id' => '54','name' => 'Croatia'),
  array('id' => '55','name' => 'Cuba'),
  array('id' => '56','name' => 'Cyprus'),
  array('id' => '57','name' => 'Czech Republic'),
  array('id' => '58','name' => 'Denmark'),
  array('id' => '59','name' => 'Djibouti'),
  array('id' => '60','name' => 'Dominica'),
  array('id' => '61','name' => 'Dominican Republic'),
  array('id' => '62','name' => 'Ecuador'),
  array('id' => '63','name' => 'Egypt'),
  array('id' => '64','name' => 'El Salvador'),
  array('id' => '65','name' => 'Equatorial Guinea'),
  array('id' => '66','name' => 'Eritrea'),
  array('id' => '67','name' => 'Estonia'),
  array('id' => '68','name' => 'Ethiopia'),
  array('id' => '69','name' => 'Falkland Islands'),
  array('id' => '70','name' => 'Faroe Islands'),
  array('id' => '71','name' => 'Fiji'),
  array('id' => '72','name' => 'Finland'),
  array('id' => '73','name' => 'France'),
  array('id' => '74','name' => 'French Guiana'),
  array('id' => '75','name' => 'French Polynesia'),
  array('id' => '76','name' => 'Gabon'),
  array('id' => '77','name' => 'Gambia'),
  array('id' => '78','name' => 'Georgia'),
  array('id' => '79','name' => 'Germany'),
  array('id' => '80','name' => 'Ghana'),
  array('id' => '81','name' => 'Gibraltar'),
  array('id' => '82','name' => 'Greece'),
  array('id' => '83','name' => 'Greenland'),
  array('id' => '84','name' => 'Grenada'),
  array('id' => '85','name' => 'Guadeloupe'),
  array('id' => '86','name' => 'Guam'),
  array('id' => '87','name' => 'Guatemala'),
  array('id' => '88','name' => 'Guinea'),
  array('id' => '89','name' => 'Guinea-Bissau'),
  array('id' => '90','name' => 'Guyana'),
  array('id' => '91','name' => 'Haiti'),
  array('id' => '93','name' => 'Honduras'),
  array('id' => '94','name' => 'Hong Kong'),
  array('id' => '95','name' => 'Hungary'),
  array('id' => '96','name' => 'Iceland'),
  array('id' => '97','name' => 'India'),
  array('id' => '98','name' => 'Indonesia'),
  array('id' => '99','name' => 'Iran'),
  array('id' => '100','name' => 'Iraq'),
  array('id' => '101','name' => 'Ireland'),
  array('id' => '102','name' => 'Italy'),
  array('id' => '103','name' => 'Jamaica'),
  array('id' => '104','name' => 'Japan'),
  array('id' => '105','name' => 'Jordan'),
  array('id' => '106','name' => 'Kazakhstan'),
  array('id' => '107','name' => 'Kenya'),
  array('id' => '108','name' => 'Kiribati'),
  array('id' => '109','name' => 'Kuwait'),
  array('id' => '110','name' => 'Kyrgyzstan'),
  array('id' => '111','name' => 'Laos'),
  array('id' => '112','name' => 'Latvia'),
  array('id' => '113','name' => 'Lebanon'),
  array('id' => '114','name' => 'Lesotho'),
  array('id' => '115','name' => 'Liberia'),
  array('id' => '116','name' => 'Libya'),
  array('id' => '117','name' => 'Liechtenstein'),
  array('id' => '118','name' => 'Lithuania'),
  array('id' => '119','name' => 'Luxembourg'),
  array('id' => '120','name' => 'Macao'),
  array('id' => '121','name' => 'Madagascar'),
  array('id' => '122','name' => 'Malawi'),
  array('id' => '123','name' => 'Malaysia'),
  array('id' => '124','name' => 'Maldives'),
  array('id' => '125','name' => 'Mali'),
  array('id' => '126','name' => 'Malta'),
  array('id' => '127','name' => 'Marshall Islands'),
  array('id' => '128','name' => 'Martinique'),
  array('id' => '129','name' => 'Mauritania'),
  array('id' => '130','name' => 'Mauritius'),
  array('id' => '131','name' => 'Mayotte'),
  array('id' => '132','name' => 'Mexico'),
  array('id' => '133','name' => 'Micronesia'),
  array('id' => '134','name' => 'Moldova'),
  array('id' => '135','name' => 'Monaco'),
  array('id' => '136','name' => 'Mongolia'),
  array('id' => '137','name' => 'Montenegro'),
  array('id' => '138','name' => 'Montserrat'),
  array('id' => '139','name' => 'Morocco'),
  array('id' => '140','name' => 'Mozambique'),
  array('id' => '141','name' => 'Myanmar'),
  array('id' => '142','name' => 'Namibia'),
  array('id' => '143','name' => 'Nauru'),
  array('id' => '144','name' => 'Nepal'),
  array('id' => '145','name' => 'Netherlands'),
  array('id' => '146','name' => 'Netherlands Antilles'),
  array('id' => '147','name' => 'New Caledonia'),
  array('id' => '148','name' => 'New Zealand'),
  array('id' => '149','name' => 'Nicaragua'),
  array('id' => '150','name' => 'Niger'),
  array('id' => '151','name' => 'Nigeria'),
  array('id' => '152','name' => 'Norfolk Island'),
  array('id' => '153','name' => 'North Korea'),
  array('id' => '154','name' => 'Norway'),
  array('id' => '155','name' => 'Oman'),
  array('id' => '156','name' => 'Pakistan'),
  array('id' => '157','name' => 'Palau'),
  array('id' => '158','name' => 'Palestine'),
  array('id' => '159','name' => 'Panama'),
  array('id' => '160','name' => 'Papua New Guinea'),
  array('id' => '161','name' => 'Paraguay'),
  array('id' => '162','name' => 'Peru'),
  array('id' => '163','name' => 'Philippines'),
  array('id' => '164','name' => 'Pitcairn'),
  array('id' => '165','name' => 'Poland'),
  array('id' => '166','name' => 'Portugal'),
  array('id' => '167','name' => 'Puerto Rico'),
  array('id' => '168','name' => 'Qatar'),
  array('id' => '169','name' => 'Romania'),
  array('id' => '170','name' => 'Russian Federation'),
  array('id' => '171','name' => 'Rwanda'),
  array('id' => '172','name' => 'Saint Helena'),
  array('id' => '173','name' => 'Saint Kitts and Nevis'),
  array('id' => '174','name' => 'Saint Lucia'),
  array('id' => '175','name' => 'Saint Pierre and Miquelon'),
  array('id' => '177','name' => 'Samoa'),
  array('id' => '178','name' => 'San Marino'),
  array('id' => '179','name' => 'Sao Tome and Principe'),
  array('id' => '180','name' => 'Saudi Arabia'),
  array('id' => '181','name' => 'Senegal'),
  array('id' => '182','name' => 'Serbia'),
  array('id' => '183','name' => 'Seychelles'),
  array('id' => '184','name' => 'Sierra Leone'),
  array('id' => '185','name' => 'Singapore'),
  array('id' => '186','name' => 'Slovakia'),
  array('id' => '187','name' => 'Slovenia'),
  array('id' => '188','name' => 'Solomon Islands'),
  array('id' => '189','name' => 'Somalia'),
  array('id' => '190','name' => 'South Africa'),
  array('id' => '191','name' => 'South Georgia'),
  array('id' => '192','name' => 'South Korea'),
  array('id' => '193','name' => 'Spain'),
  array('id' => '194','name' => 'Sri Lanka'),
  array('id' => '195','name' => 'Sudan'),
  array('id' => '196','name' => 'Suriname'),
  array('id' => '197','name' => 'Svalbard and Jan Mayen'),
  array('id' => '198','name' => 'Swaziland'),
  array('id' => '199','name' => 'Sweden'),
  array('id' => '200','name' => 'Switzerland'),
  array('id' => '201','name' => 'Syrian Arab Republic'),
  array('id' => '202','name' => 'Taiwan'),
  array('id' => '203','name' => 'Tajikistan'),
  array('id' => '204','name' => 'Tanzania'),
  array('id' => '205','name' => 'Thailand'),
  array('id' => '206','name' => 'Republic of Macedonia'),
  array('id' => '207','name' => 'Timor-Leste'),
  array('id' => '208','name' => 'Togo'),
  array('id' => '209','name' => 'Tokelau'),
  array('id' => '210','name' => 'Tonga'),
  array('id' => '211','name' => 'Trinidad and Tobago'),
  array('id' => '212','name' => 'Tunisia'),
  array('id' => '213','name' => 'Turkey'),
  array('id' => '214','name' => 'Turkmenistan'),
  array('id' => '215','name' => 'Tuvalu'),
  array('id' => '216','name' => 'Uganda'),
  array('id' => '217','name' => 'Ukraine'),
  array('id' => '218','name' => 'United Arab Emirates'),
  array('id' => '219','name' => 'United Kingdom'),
  array('id' => '220','name' => 'United States'),
  array('id' => '222','name' => 'Uruguay'),
  array('id' => '223','name' => 'Uzbekistan'),
  array('id' => '224','name' => 'Vanuatu'),
  array('id' => '225','name' => 'Vatican City'),
  array('id' => '226','name' => 'Venezuela'),
  array('id' => '227','name' => 'Vietnam'),
  array('id' => '228','name' => 'Virgin Islands, British'),
  array('id' => '229','name' => 'Virgin Islands, U.S.'),
  array('id' => '230','name' => 'Wallis and Futuna'),
  array('id' => '231','name' => 'Western Sahara'),
  array('id' => '232','name' => 'Yemen'),
  array('id' => '233','name' => 'Zambia'),
  array('id' => '234','name' => 'Zimbabwe')
);


		// Uncomment the below to run the seeder
		//DB::table('countries')->insert($countries);
	}

}
