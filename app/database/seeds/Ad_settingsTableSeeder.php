<?php

class Ad_settingsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('ad_settings')->truncate();

		$ad_settings = array(
			array('name'=>'Leaderboard', 'duration'=>10000),
			array('name'=>'MPU', 'duration'=>7000),
			array('name'=>'Skyscraper', 'duration'=>16000),
			array('name'=>'Overlay 1', 'duration'=>4000),
			array('name'=>'Overlay 2', 'duration'=>6000),
			array('name'=>'Overlay 3', 'duration'=>8000),
		);

		// Uncomment the below to run the seeder

		DB::table('ad_settings')->insert($ad_settings);
	}

}
