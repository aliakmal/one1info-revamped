<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('IndustriesTableSeeder');
		$this->call('CountriesTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('MembersTableSeeder');
		$this->call('Corporate_servicesTableSeeder');
		$this->call('CompaniesTableSeeder');
		$this->call('ArticlesTableSeeder');
		$this->call('VideosTableSeeder');
		$this->call('SectionsTableSeeder');
		$this->call('PhotosTableSeeder');
		$this->call('JobsTableSeeder');
		$this->call('PagesTableSeeder');
		$this->call('Press_releasesTableSeeder');
		$this->call('Company_interviewsTableSeeder');
		$this->call('Company_reviewsTableSeeder');
		$this->call('ComplaintsTableSeeder');
		$this->call('Deal_sectionsTableSeeder');
		$this->call('PatentsTableSeeder');
		$this->call('Patent_investmentsTableSeeder');
		$this->call('IdeasTableSeeder');
		$this->call('Membership_optionsTableSeeder');
		$this->call('Ad_settingsTableSeeder');
		$this->call('Ad_bannersTableSeeder');
		$this->call('Red_carpetsTableSeeder');
		$this->call('MarquesTableSeeder');
		$this->call('TransactionsTableSeeder');
		$this->call('Video_sectionsTableSeeder');
		$this->call('Auto_emailsTableSeeder');
		$this->call('PrivilegesTableSeeder');
	}

}