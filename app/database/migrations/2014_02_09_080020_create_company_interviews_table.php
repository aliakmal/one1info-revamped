<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyInterviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_interviews', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->integer('company_id');
			$table->string('youtube_url');
			$table->text('body');
			$table->date('dated');
			$table->integer('is_published');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_interviews');
	}

}
