<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_reviews', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->integer('company_id');
			$table->integer('user_id');
			$table->integer('rate');
			$table->text('body');
			$table->date('dated');
			$table->integer('is_published');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_reviews');
	}

}
