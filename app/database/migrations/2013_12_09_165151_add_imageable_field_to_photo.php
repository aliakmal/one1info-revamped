<?php

use Illuminate\Database\Migrations\Migration;

class AddImageableFieldToPhoto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('photos', function($t) {
                $t->string('imageable_type', 64);
                $t->integer('imageable_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}