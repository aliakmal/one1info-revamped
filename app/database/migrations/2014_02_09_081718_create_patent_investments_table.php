<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatentInvestmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patent_investments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('patent_id');
			$table->integer('company_id');
			$table->string('applicant_name');
			$table->string('applicant_position');
			$table->string('address');
			$table->string('city');
			$table->string('country');
			$table->string('email');
			$table->string('phone');
			$table->string('fax');
			$table->text('why_invest');
			$table->text('investment_benefits');
			$table->string('has_business_same_field');
			$table->string('need_meeting');
			$table->string('status');
			$table->date('dated');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patent_investments');
	}

}
