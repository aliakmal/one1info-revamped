<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePressReleasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('press_releases', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->integer('company_id');
			$table->string('youtube_url');
			$table->date('dated');
			$table->string('type');
			$table->text('body');
			$table->integer('is_published');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('press_releases');
	}

}
