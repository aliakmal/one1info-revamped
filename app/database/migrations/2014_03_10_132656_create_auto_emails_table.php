<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAutoEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auto_emails', function(Blueprint $table) {
			$table->increments('id');
			$table->string('service');
			$table->string('from');
			$table->string('subject');
			$table->text('header');
			$table->text('body');
			$table->text('footer');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auto_emails');
	}

}
