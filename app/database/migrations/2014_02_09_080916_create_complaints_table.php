<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComplaintsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('complaints', function(Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('gender');
			$table->string('company');
			$table->string('position');
			$table->string('address');
			$table->string('city');
			$table->string('country');
			$table->string('email');
			$table->string('mobile');
			$table->string('phone');
			$table->string('fax');
			$table->integer('complaint_against');
			$table->string('compaint_type');
			$table->string('product');
			$table->string('invoice_no');
			$table->string('amount_in_dispute');
			$table->date('problem_date');
			$table->string('complaint_title');
			$table->text('complain_details');
			$table->text('expected_resolution');
			$table->date('dated');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('complaints');
	}

}
