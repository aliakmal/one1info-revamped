<?php

use Illuminate\Database\Migrations\Migration;

class AddSectionIdToVideos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('videos', function($t) {
            $t->integer('section_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}