<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patents', function(Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('address');
			$table->string('city');
			$table->string('country');
			$table->string('email');
			$table->string('phone');
			$table->string('fax');
			$table->string('inv_name');
			$table->string('inv_field');
			$table->text('inv_details');
			$table->string('has_patent');
			$table->string('need_register');
			$table->string('patent_area');
			$table->string('need_publish');
			$table->string('need_investor');
			$table->date('dated');
			$table->integer('is_published');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patents');
	}

}
