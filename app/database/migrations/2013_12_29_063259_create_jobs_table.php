<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
			$table->string('address');
			$table->string('city');
			$table->string('state');
			$table->integer('country_id');
			$table->string('title');
			$table->string('category');
			$table->string('experience');
			$table->string('education');
			$table->string('commitment');
			$table->string('compensation');
			$table->text('details');
			$table->integer('show_company');
			$table->integer('published');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}
