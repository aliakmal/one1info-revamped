<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
			$table->increments('id');
			$table->string('company_name');
			$table->string('known_as');
			$table->string('contact_person');
			$table->string('email');
			$table->string('website');
			$table->string('phone');
			$table->string('mobile');
			$table->string('fax');
			$table->integer('country_id');
			$table->integer('origin_country_id');
			$table->string('licence_type');
			$table->string('established');
			$table->integer('num_employees');
			$table->text('address');
			$table->string('city');
			$table->string('postal_code');
			$table->integer('industry_id');
			$table->text('products');
			$table->text('services');
			$table->text('profile');
			$table->string('subscription');
			$table->boolean('publish');
			$table->integer('order');
			$table->double('location_x');
			$table->double('location_y');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
