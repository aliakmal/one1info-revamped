<?php

use Illuminate\Database\Migrations\Migration;

class CreateCategoryCompanyTableOnly extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

		Schema::create('category_company', function( $table)
		{
			$table->increments('id');
      		$table->integer('company_id')->unsigned();
		    $table->integer('category_id')->unsigned();
		    $table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories_companies');
	}

}