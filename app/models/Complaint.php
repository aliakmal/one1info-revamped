<?php

class Complaint extends Eloquent {
	protected $guarded = array();

	public function company(){
		return $this->belongsTo('Company', 'complaint_against');
  	}

	public static $rules = array(
		'first_name' => 'required',
		'last_name' => 'required',
		'gender' => 'required',
		'company' => 'required',
		'position' => 'required',
		'address' => 'required',
		'city' => 'required',
		'country' => 'required',
		'email' => 'required',
		'mobile' => 'required',
		'phone' => 'required',
		'fax' => 'required',
		'complaint_against' => 'required',
		'compaint_type' => 'required',
		'product' => 'required',
		'invoice_no' => 'required',
		'amount_in_dispute' => 'required',
		'problem_date' => 'required',
		'complaint_title' => 'required',
		'complain_details' => 'required',
		'expected_resolution' => 'required',
		'dated' => 'required'
	);
}
