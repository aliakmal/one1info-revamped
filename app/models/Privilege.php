<?php

class Privilege extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required|unique:privileges',
		//'permissions' => 'required',
		//'admin' => 'required'
	);

    public static function getRulesForUpdating($id){
    	$rules = self::$rules;
    	$rules['name'].=',id,'.$id;
    	return $rules;
    }


	public static function resetSettings(){
		$roles = Privilege::all();
		$arr = array();
		foreach($roles as $role){
			if($role->admin == 1)
				$arr[$role->name] = true;	
			else
				$arr[$role->name] = unserialize($role->permissions);
		}

		Setting::set('roles', $arr);

		//return $arr;
	}


}
