<?php

class Section extends Eloquent {
	protected $guarded = array();
  
  public function articles(){
    return $this->hasMany('Article');
  }
  public function videos(){
    return $this->hasMany('Video');
  }

	public static $rules = array(
		'name' => 'required'
	);
}
