<?php

class Press_release extends Eloquent {
	protected $guarded = array();
  public function photos()
  {
      return $this->morphMany('Photo', 'imageable');
  }

  	public function company(){
		return $this->belongsTo('Company', 'company_id');
  	}

  	public function getPic(){
  		return 1;
  	}

	public static $rules = array(
		'title' => 'required',
		'company_id' => 'required',
		//'youtube_url' => 'required',
		'dated' => 'required',
		//'type' => 'required',
		'body' => 'required',
		//'is_published' => 'required'
	);
}
