<?php

class Patent extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'first_name' => 'required|alpha|size:10',
		'last_name' => 'required',
		'address' => 'required',
		'city' => 'required',
		'country' => 'required',
		'email' => 'required',
		'phone' => 'required',
		'fax' => 'required',
		'inv_name' => 'required',
		'inv_field' => 'required',
		'inv_details' => 'required',
		'has_patent' => 'required',
		'need_register' => 'required',
		'patent_area' => 'required',
		'need_publish' => 'required',
		'need_investor' => 'required',
		'dated' => 'required',
		'is_published' => 'required'
	);
}
