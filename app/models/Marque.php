<?php

class Marque extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required',
		'url' => 'required',
		'dated' => 'required',
		'is_published' => 'required'
	);
}
