<?php

class Company_review extends Eloquent {
	protected $guarded = array();
	public function company(){
		return $this->belongsTo('Company', 'company_id');
  	}

	public static $rules = array(
		'title' => 'required',
		'company_id' => 'required',
		'user_id' => 'required',
		'rate' => 'required',
		'body' => 'required',
		'dated' => 'required',
		'is_published' => 'required'
	);
}
