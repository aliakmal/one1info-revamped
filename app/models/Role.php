<?php 
class Role extends Eloquent{

	public function __construct(){
		$this->table = 'users_roles';
	}

	public function User(){
		return $this->belongsTo('User');
	}

	public static function getRoles($user){
		$roles = array();

		$all_roles = $user->roles();
		foreach( $all_roles as $role){

			$roles[] = $role->role;
		}



		return $roles;
	}


	public static function hasRole($user, $role){
		$user = User::find($user->id);
		return ($user->roles()->first()->role == $role);
		
	}

	
}