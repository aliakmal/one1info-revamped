<?php

class Ad_banner extends Eloquent {
	protected $guarded = array();

	public function photo(){
      return $this->morphOne('Photo', 'imageable');
	}

	public function Location(){
		return $this->belongsTo('Ad_setting', 'location_id');
	}

	public static $rules = array(
		'page' => 'required',
		'location_id' => 'required',
		'link' => 'required',
		'is_published' => 'required'
	);
}
