<?php

class Membership_option extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'membership_title' => 'required',
		'options' => 'required'
	);
}
