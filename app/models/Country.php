<?php

class Country extends Eloquent {
	protected $guarded = array();

	public function companies(){
		return $this->hasMany('Company');
	}

	public function members(){
		return $this->hasMany('Member');
	}

	public static $rules = array(
		'name' => 'required'
	);
}
