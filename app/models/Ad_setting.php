<?php

class Ad_setting extends Eloquent {
	protected $guarded = array();

	public function banners(){
		return $this->hasMany('Ad_banner', 'location_id');
	}

	public static $rules = array(
		'name' => 'required',
		'duration' => 'required'
	);
}
