<?php

class Video extends Eloquent {
	protected $guarded = array();
  public function section(){
    return $this->belongsTo('Video_section', 'section_id');
  }

	public static $rules = array(
		'title' => 'required',
		'providor' => 'required',
		'url' => 'required',
		'caption' => 'required',
		'is_published' => 'required'
	);


  public function getThumbnailUrl(){
  	if($this->providor == 'youtube'):
	    return 'http://img.youtube.com/vi/'.$this->url.'/0.jpg';
	else:
	    return Config::get('image.default_video_image');
	endif;
  }

  public function getEmbedCode($width="585", $height = "460"){
  	if($this->providor == 'youtube'){
  		return '<iframe width="'.$width.'" height="'.$height.'" src="//www.youtube.com/embed/'.$this->url.'" frameborder="0" allowfullscreen></iframe>';
  	}
  }

}
