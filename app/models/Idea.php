<?php

class Idea extends Eloquent {
	protected $guarded = array();
	
	public function photos()
	{
	  return $this->morphMany('Photo', 'imageable');
	}

	public static $rules = array(
		'title' => 'required',
		'body' => 'required',
		'youtube_url' => 'required',
		'media_title' => 'required',
		'dated' => 'required',
		'sort' => 'required',
		'is_published' => 'required'
	);
}
