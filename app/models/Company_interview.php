<?php

class Company_interview extends Eloquent {
	protected $guarded = array();

	public function company(){
		return $this->belongsTo('Company', 'company_id');
  	}

	public static $rules = array(
		'title' => 'required',
		'company_id' => 'required',
		'youtube_url' => 'required',
		//'body' => 'required',
		'dated' => 'required',
		'is_published' => 'required'
	);
}
