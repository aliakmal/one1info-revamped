<?php

class Transaction extends Eloquent {
	protected $guarded = array();

	public static function roles(){
		return array(
        /*
         * Grant all privileges to the administrator roles.
         */
        'administrator' => true,
        'rocket'=>array(),
        /*
         * Granted basics CRUD privileges to the user administrator role on the user resource.
         */
        //'user_administrator' => array('user' => array('create', 'read', 'update', 'delete')),
        'basic-member' => array('user' => array('create', 'read', 'update', 'delete')),
        'listed-company' => array('user' => array('create', 'read', 'update', 'delete')),
        'registered-company' => array('user' => array('create', 'read', 'update', 'delete'))
        );

    
	}

	public static $rules = array(
		'company_id' => 'required',
		'membership' => 'required',
		'first_name' => 'required',
		'last_name' => 'required',
		'amount' => 'required',
		'payment_type' => 'required',
		'bill_no' => 'required',
		'dated' => 'required'
	);
}
