<?php

class Category extends Eloquent {
	protected $guarded = array();

	public function companies(){
		return $this->belongsToMany('Company');
	}

	public static $rules = array(
		'name' => 'required'
	);
}
