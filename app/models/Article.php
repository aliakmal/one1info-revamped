<?php

class Article extends Eloquent {
	protected $guarded = array();

  public function section(){
    return $this->belongsTo('Section');
  }

  public function beforeCreate() {

    return true;
    //or don't return nothing, since only a boolean false will halt the operation
  }
  
  public function photos()
  {
      return $this->morphMany('Photo', 'imageable');
  }
  
	public static $rules = array(
		'title' => 'required',
		'author' => 'required',
		'caption' => 'required',
		'body' => 'required',
    'section_id'=>'required',
    'dated'=>'',
		'is_published' => 'required',
		'created_by' => ''
	);
}
