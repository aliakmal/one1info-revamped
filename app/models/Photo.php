<?php

class Photo extends Eloquent {

  protected $guarded = array();
  public static $rules = array(
      'name' => 'required',
      'caption' => 'required',
      'imageable_id' => 'required',
      'imageable_type' => 'required',
      'size' => 'required'
  );
  

  public $attributes = array(
      'name' => '',
      'caption' => '',
      'imageable_id' => '',
      'imageable_type' => '',
      'size' => ''
  );

  public function imageable(){
      return $this->morphTo();
  }

  public static function image_tag($img, $size = 'thumb-med', $params = null){
    if(!is_object($img)){
      return '';
    }
    $html = '<img src=';

    $html.='"'.Config::get('image.assets_dir');

    $html.=$img->id.'/';

    if($size=='original'):
      $html.=$img->name.'" ';
    else:

      $dimensions = Config::get('image.dimensions');
      $dimensions = !isset($dimensions[$size]) ? $dimensions['thumb'] : $dimensions[$size];
      $html.=$dimensions[0].'x'.$dimensions[1].($dimensions[2]?'_crop':'').'/'.$img->name.'" ';

    endif;

    if(is_array($params) && (!is_null($params)) ){
      foreach($params as $ii=>$vv){
        $html.=' '.$ii.'="'.$vv.'" ';
      }
    }

    $html.=' />';
    return $html;
  }

  public function create_n_upload($image, $data) {
    $data['size'] = $image->getClientSize();
    $data['name'] = $image->getClientOriginalName();

    $ph = Photo::create($data);
    foreach($data as $ii=>$vv){
      $ph->$ii = $vv;
    }
    $ph->save();


    $url = $image->move(Config::get('image.upload_path').$ph->id, $data['name'])->getRealPath();
    $this->createDimensions($url, $data['name']);
  }

  public function getImageUrl($width = '600', $height = '400', $append = '_crop'){
    return Config::get('image.assets_dir').$this->id.'/'.$width.'x'.$height.$append.'/'.$this->name;
  }

  protected $lab = array('imagine'=>'', 'library'=>'');

  /**
   * Initialize the image service
   * @return void
   */
  public function __construct() {
    if (!$this->lab['imagine']) {
      $this->lab['library'] = Config::get('image.library', 'gd');

      // Now create the instance
      if ($this->lab['library'] == 'imagick')
        $this->lab['imagine'] = new \Imagine\Imagick\Imagine();
      elseif ($this->lab['library'] == 'gmagick')
        $this->imagine = new \Imagine\Gmagick\Imagine();
      elseif ($this->lab['library'] == 'gd')
        $this->lab['imagine'] = new \Imagine\Gd\Imagine();
      else
        $this->lab['imagine'] = new \Imagine\Gd\Imagine();
    }
  }

  public  function createDimensions($url, $filename, $dimensions = array()) {
    // Get default dimensions
    $defaultDimensions = Config::get('image.dimensions');

    if (is_array($defaultDimensions))
      $dimensions = array_merge($defaultDimensions, $dimensions);

    foreach ($dimensions as $dimension) {
      // Get dimmensions and quality
      $width = (int) $dimension[0];
      $height = isset($dimension[1]) ? (int) $dimension[1] : $width;
      $crop = isset($dimension[2]) ? (bool) $dimension[2] : false;
      $quality = isset($dimension[3]) ? (int) $dimension[3] : Config::get('image.quality');

      // Run resizer
      $img = $this->resize($url, $filename, $width, $height, $crop, $quality);
    }
  }

//  public static function resize($url, $width = 100, $height = null, $crop = false, $quality = 90) {
  public function resize($url, $filename, $width = 100, $height = null, $crop = false, $quality = 90) {
    if ($url) {
      // URL info
      $info = pathinfo($url);

      // The size
      if (!$height)
        $height = $width;

      // Quality
      $quality = Config::get('image.quality', $quality);

      // Directories and file names
      $fileName = $filename;  $info['basename'];
      $sourceDirPath = $info['dirname'];
      $sourceFilePath = $sourceDirPath . '\\' .  $info['basename'];
      $targetDirName = $width . 'x' . $height . ($crop ? '_crop' : '');
      $targetDirPath = $sourceDirPath . '\\' . $targetDirName . '\\';
      $targetFilePath = $targetDirPath . $fileName;
      $targetUrl = $targetFilePath;//asset($info['dirname'] . '/' . $targetDirName . '/' . $fileName);
      // Create directory if missing
      try {
        // Create dir if missing
        if (!File::isDirectory($targetDirPath) and $targetDirPath)
          @File::makeDirectory($targetDirPath);

        // Set the size
        $size = new \Imagine\Image\Box($width, $height);

        // Now the mode
        $mode = $crop ? \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND : \Imagine\Image\ImageInterface::THUMBNAIL_INSET;

        if (!File::exists($targetFilePath) or (File::lastModified($targetFilePath) < File::lastModified($sourceFilePath))) {
          $this->lab['imagine']->open($sourceFilePath)
                  ->thumbnail($size, $mode)
                  ->save($targetFilePath, array('quality' => $quality));
        }

      } catch (\Exception $e) {
        Log::error('[IMAGE SERVICE] Failed to resize image "' . $url . '" [' . $e->getMessage() . ']');
      }

      return $targetUrl;
    }
  }

}
