<?php

class Page extends Eloquent {
	protected $guarded = array();

	public function photo()
	{
	  return $this->morphOne('Photo', 'imageable');
	}

	public static $rules = array(
		'title' => 'required',
		'body' => 'required',
		'is_published' => 'required'
	);
}
