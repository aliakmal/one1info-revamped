<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

public static $unguarded = true;
	  public function member_details(){
	    return $this->hasOne('Member');
	  }

	  public function company(){
	  	return $this->belongsTo('Company', 'company_id');
	  }
	public static $rules = array(
    	'username' => 'required|min:5|unique:users',
    	'email' => 'required|email|unique:users',
    	'password' => 'required|min:5',
    	'password_confirmation' => 'required|min:5|same:password',
    );

    public static function getRulesForUpdating($id){
    	$rules = self::$rules;
    	$rules['email'].=',email,'.$id;
    	unset($rules['username']);
    	return $rules;
    }

    public static function getRulesForUpdatingNoPassword($id){
    	
    	return array('email'=>self::$rules['email'].',email,'.$id);
    	
    }


	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function roles(){
		return $this->hasMany('Role', 'user_id');
	}

	public function getRoles(){
		$roles = array();
		$current_roles = User::find($this->id)->roles();

		return array($current_roles->first()->role);

		foreach($current_roles as $ii=>$role){
			

			$roles[] = $role->role;
		}

		return $roles;
	}


	public function hasRole($role){
		
		return in_array($role, $this->getRoles());
	}
public function getRememberToken()
{
    return $this->remember_token;
}

public function setRememberToken($value)
{
    $this->remember_token = $value;
}

public function getRememberTokenName()
{
    return 'remember_token';
}
}