<?php

class Auto_email extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'service' => 'required',
		'from' => 'required',
		'subject' => 'required',
		'header' => 'required',
		'body' => 'required',
		'footer' => 'required'
	);
}
