<?php

class Job extends Eloquent {
	protected $guarded = array();

	public function Country(){
		return $this->belongsTo('Country')->first();
	}

	public function Company(){
		return $this->belongsTo('Company')->first();
	}

	public static $rules = array(
		'company_id' => 'required',
		'address' => 'required',
		'city' => 'required',
		'state' => 'required',
		'country_id' => 'required',
		'title' => 'required',
		'category' => 'required',
		'experience' => 'required',
		'education' => 'required',
		'commitment' => 'required',
		'compensation' => 'required',
		'details' => 'required',
		'show_company' => 'required',
		'published' => 'required'
	);
}
