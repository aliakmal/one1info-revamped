<?php

class Video_section extends Eloquent {
	protected $guarded = array();

	public function videos(){
		return $this->hasMany('Video', 'section_id');
	}

	public static $rules = array(
		'name' => 'required',
		'sort' => 'required',
		'is_published' => 'required',
	);
}
