<?php

class Member extends Eloquent {
	protected $guarded = array();
    
    public function user(){
      return $this->belongsTo('User');
    }

	public static $rules = array(
		'first_name' => 'required',
		'last_name' => 'required',
		'gender' => 'required',
		'address' => 'required',
		'state' => 'required',
		'country_id' => 'required',
		'dob' => 'required',
		'title' => 'required',
		//'profession' => 'required',
		//'interests' => 'required'
	);
  
  public function country(){
    return $this->belongsTo('Country');
  }
}
