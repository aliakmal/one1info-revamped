<?php

class Deal_section extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'is_published' => 'required'
	);
}
