<?php

class Company extends Eloquent {
	protected $guarded = array();
  
  public function categories(){
    return $this->belongsToMany('Category');
  }

	public function users(){
	  return $this->hasMany('User', 'company_id');
	}

	

	public function company_interviews(){
	  return $this->hasMany('Company_interview', 'company_id');
	}
	public function press_releases(){
	  return $this->hasMany('Press_release', 'company_id');
	}

	public function company_reviews(){
	  return $this->hasMany('Company_review', 'company_id');
	}

	public function jobs(){
	  return $this->hasMany('Job', 'company_id');
	}

	public function patent_investments(){
	  return $this->hasMany('Patent_investment', 'company_id');
	}

	public function complaints(){
	  return $this->hasMany('Complaint', 'complaint_against');
	}

	public function country(){
	  return $this->belongsTo('Country');
	}

  public function logo()
  {
      return $this->morphOne('Photo', 'imageable');
  }

	public static $rules = array(
		'company_name' => 'required',
		//'known_as' => 'required',
		//'contact_person' => 'required',
		'address' => 'required',
		'phone' => 'required',
		'city'=>'required',

	);
}
