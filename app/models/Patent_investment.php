<?php

class Patent_investment extends Eloquent {
	protected $guarded = array();
	public function company(){
		return $this->belongsTo('Company', 'company_id');
  	}

	public static $rules = array(
		'patent_id' => 'required',
		'company_id' => 'required',
		'applicant_name' => 'required',
		'applicant_position' => 'required',
		'address' => 'required',
		'city' => 'required',
		'country' => 'required',
		'email' => 'required',
		'phone' => 'required',
		'fax' => 'required',
		'why_invest' => 'required',
		'investment_benefits' => 'required',
		'has_business_same_field' => 'required',
		'need_meeting' => 'required',
		'status' => 'required',
		'dated' => 'required'
	);
}
