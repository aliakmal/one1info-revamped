/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : one2

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2014-03-11 15:24:40
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ad_banners`
-- ----------------------------
DROP TABLE IF EXISTS `ad_banners`;
CREATE TABLE `ad_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` longtext COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(11) NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ad_banners
-- ----------------------------
INSERT INTO `ad_banners` VALUES ('2', 'a:6:{s:8:\"articles\";s:1:\"1\";s:18:\"business-directory\";s:1:\"1\";s:13:\"career-center\";s:1:\"1\";s:18:\"corporate-services\";s:1:\"1\";s:9:\"hot-deals\";s:1:\"1\";s:4:\"home\";s:1:\"1\";}', '1', 'http://www.google.com', '1', '2014-03-10 07:22:39', '2014-03-10 08:31:26');
INSERT INTO `ad_banners` VALUES ('3', 'a:1:{s:9:\"all_pages\";s:1:\"1\";}', '2', 'http://www.yahoo.com', '1', '2014-03-10 07:24:22', '2014-03-10 08:31:51');

-- ----------------------------
-- Table structure for `ad_settings`
-- ----------------------------
DROP TABLE IF EXISTS `ad_settings`;
CREATE TABLE `ad_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ad_settings
-- ----------------------------
INSERT INTO `ad_settings` VALUES ('1', 'Leaderboard', '10000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ad_settings` VALUES ('2', 'MPU', '7000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ad_settings` VALUES ('3', 'Skyscraper', '16000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ad_settings` VALUES ('4', 'Overlay 1', '4000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ad_settings` VALUES ('5', 'Overlay 2', '6000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ad_settings` VALUES ('6', 'Overlay 3', '8000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `section_id` int(11) NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'article',
  `is_published` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('126', 'Dh2.59bn flats and hotel deal for Reem Island', 'By Abbas Rashid', 'Construction is set to start on the second phase of Abu Dhabi’s landmark Gate Towers development on Reem Island following the award of a Dh2.59 billion contract to Arabtec.', '<p>Construction is set to start on the second phase of Abu Dhabi&rsquo;s landmark Gate Towers development on Reem Island following the award of a Dh2.59 billion contract to Arabtec.</p>\r\n\r\n<p>Arabtec Construction won the contract to build the 61-storey block of flats and hotel complex in the Shams area, which makes up the final leg of the master planned four-leg structure.</p>\r\n\r\n<p>&ldquo;We are confident that 2014 will be a special year not only in terms of new awards but also with regard to the progress of implementation of our growth strategy and the striking of more local and international partnerships,&rdquo; said Hasan Abdullah Ismaik, the managing director and chief executive of Arabtec.</p>\r\n\r\n<p>The company said it would build a residential tower of 613 furnished apartments as well as a 15-floor tower, which would comprise a 400-room five-star hotel and 200 serviced apartments.</p>\r\n\r\n<p>It declined to name the developer behind the project.</p>\r\n\r\n<p>Although graphics show that the final residential skyscraper would not be connected to the recently completed three existing Gate Towers, it will be connected to the hotel by a ballroom and a 4,600 square metre shopping area.</p>\r\n\r\n<p>In total the new project will comprise 350,000 sq metres, including 3,275 sq metres of cafes and restaurants and 3,795 sq metres of function space. It will be built on a 32,000 sq metre plot.</p>\r\n\r\n<p>Gate Towers was designed by the Abu Dhabi developer Sorouh before the global financial crisis and before that company merged with rival Aldar last year.</p>\r\n\r\n<p>Sorouh originally planned to build the entire project itself, but when development values tumbled in 2009, the developer took the decision to split the project into two, developing a first phase of 3,533 apartments in three towers connected by the world&rsquo;s highest sky bridge and a lower arc-shaped block.</p>\r\n\r\n<p>In December 2011 Sorouh announced it had sold the second phase of Gate Towers to an unnamed sub-developer. As part of the deal Sorouh also took back from the investor a previously sold marina plot in the Shams area and asked for assurances that it would build the final phase of the development in accordance with Sorouh&rsquo;s master plan.</p>\r\n\r\n<p>Construction work on the project, designed by the Architecture &amp; Planning Group, is scheduled to begin before the end of March and is estimated to take 36 months to complete.</p>\r\n\r\n<p>Arabtec said negotiations were under way with a number of world-leading hospitality brands for the management of the hotel and serviced apartments. The company added that the contract award meant that Arabtec&rsquo;s backlog of schemes it was working on had now crossed Dh33 billion, the highest in the company&rsquo;s 38-year history.</p>\r\n\r\n<p>&ldquo;This is a further indication that development activity on Reem Island is starting to return,&rdquo; said William Neill, the head of Cluttons&rsquo; Abu Dhabi office.</p>\r\n\r\n<p>&ldquo;Over the past few months we have been asked to conduct a number of new development appraisals and property valuations on the island as developers start to look at resuming work on residential and commercial schemes and we expect this trend to continue throughout 2014 and into 2015 as house prices and rents increase.&rdquo;</p>\r\n', '3', '', '', '1', '0', '2014-01-14 14:48:17', '2014-01-14 14:48:17');
INSERT INTO `articles` VALUES ('128', 'Dubai Ruler speaks to BBC about UAE’s legal system', 'By Abbas Rashid', 'Sheikh Mohammed bin Rashid has spoken about the UAE’s legal system and human rights record, together with political issues in the region in a wide-ranging interview with BBC World News.\r\n\r\n', '<p>Sheikh Mohammed bin Rashid has spoken about the UAE&rsquo;s legal system and human rights record, together with political issues in the region in a wide-ranging interview with BBC World News.</p>\r\n\r\n<p><br />\r\nSpeaking to the BBC&rsquo;s Jon Sopel, Sheikh Mohammed, the Prime Minister and Ruler of Dubai, discussed the Syrian conflict and political developments in Egypt.</p>\r\n\r\n<p>Sheikh Mohammed told Sopel in a recorded interview in Dubai on Sunday that the UAE would &ldquo;help but not interfere&rdquo; in the Syrian conflict.</p>\r\n\r\n<p>He had been asked if the UAE would follow the example of Qatar and become involved with resistance groups in Syria.</p>\r\n\r\n<p>&ldquo;We are supporting the people in Jordan or Turkey, we are helping those but the free army you don&rsquo;t know, because there&rsquo;s some extremists, and you don&rsquo;t know how many groups there are. You hear that some groups are fighting each other,&rdquo; said Sheikh Mohammed.</p>\r\n\r\n<p>&ldquo;Maybe Qatar have a reason or a vision, but here, we don&rsquo;t want to interfere with other people, we should help but not interfere.&rdquo;</p>\r\n\r\n<p>He said that while the Syrian president Bashar Al Assad could stay in power for some time, his eventual removal was a certainty.</p>\r\n\r\n<p>&ldquo;Assad will take a long time, but if you kill your people you can&rsquo;t stay... eventually he will go,&rdquo; he said.</p>\r\n\r\n<p>Sheikh Mohammed said the UAE would establish diplomatic relations with Israel if the Israelis reached a peace deal with the Palestinians.</p>\r\n\r\n<p>&ldquo;We will do everything with Israel &ndash; we will trade with them and we will welcome them &ndash; but sign the peace process,&rdquo; he said.</p>\r\n\r\n<p>The future of Egypt was another topic of discussion during the interview.</p>\r\n\r\n<p>Sheikh Mohammed said Egypt was better off without the former president, Mohamed Morsi.</p>\r\n\r\n<p>&ldquo;Much better and I tell you why &hellip; I said they would last only one year then the people and the army would sort that out.&rdquo;</p>\r\n\r\n<p>The Prime Minister also discussed the forthcoming presidential election in Egypt. In remarks later clarified in a statement from his office, he said Gen Abdel Fattah El Sisi, the Egyptian military chief who has hinted that he might run for the presidency, should not run for election while still in the military, but his candidacy as a civilian was a matter for him and the Egyptian people.</p>\r\n\r\n<p>Sopel also asked Sheikh Mohammed about the UAE&rsquo;s human rights record.</p>\r\n\r\n<p>&ldquo;We have a law, like when we cracked on the Muslim Brotherhood, because if they want to live and stay and work, they are OK, if they want to go extremes, we have a law for that,&rdquo; he said.</p>\r\n\r\n<p>On the release last week of the American prisoner who was jailed for a video mocking youth culture, the Dubai Ruler agreed his treatment had not been fair.</p>\r\n\r\n<p>&ldquo;We try to change it. We are not perfect and we try to change it. Any mistakes, we go in and try to change it. We&rsquo;re not perfect, but we are doing our best,&rdquo; said Sheikh Mohammed.</p>\r\n\r\n<p>Another topic was the incident last year at his Godolphin horseracing stables in England, when trainer Mahmood Al Zarooni was banned for eight years by the British Horseracing Authority for doping horses.</p>\r\n\r\n<p>&ldquo;I was shocked,&rdquo; Sheikh Mohammed said. &ldquo;Really &hellip; they gave him eight years and I gave him a lifetime. Finished &hellip; he will never come near my horses &hellip; he doped them not for racing, but for treatment long term.&rdquo;</p>\r\n\r\n<p>In a separate report on the BBC website, Sheikh Mohammed is shown welcoming the TV crew to his ancestral home in Shindagah, which is now a museum. At the time of his childhood, there was no electricity and water in the compound overlooking the Dubai Creek.</p>\r\n\r\n<p>The Dubai Ruler is shown meeting museum visitors.</p>\r\n\r\n<p>While driving in Sheikh Mohammed&rsquo;s Mercedes along Sheikh Zayed Road, Sopel asks if Sheikh Mohammed had a vision for Dubai&rsquo;s transformation from early on in his life.</p>\r\n\r\n<p>&ldquo;I was lucky enough, I travelled with my father to Europe, England,&rdquo; said Sheikh Mohammed. &ldquo;And always my dream saying, they have roads and they have buildings and flags and, to me, that was my dream, when we have that,&rdquo; he said.</p>\r\n\r\n<p>The interview was broadcast on BBC World News on Monday.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1', '', '', '1', '0', '2014-01-14 14:49:18', '2014-01-14 14:49:18');
INSERT INTO `articles` VALUES ('135', ' Dubai Marina still has plots to fill', 'By Abbas Rashid', 'Cluster has one of the highest occupancy levels as investors seek optimum yield', '<p>Drive by the high-rise city that is the Dubai Marina and you would have thought there is hardly any plot left to fit in a new development. Guess you are wrong.</p>\r\n\r\n<p>The Select Group &mdash; which already has eight towers, completed or otherwise, in the freehold cluster &mdash; is adding another one, succinctly named &lsquo;No. 9&rsquo;, a Dh500 million, 35-storey residential project. Unit sales were launched on Monday, with base price set at Dh1.1 million, and construction is scheduled for completion in the second quarter of 2016.</p>\r\n\r\n<p>As with all of Dubai&rsquo;s freehold locations, Dubai Marina too has seen its unit values surge. It currently has one of the highest tenancy levels, which is another huge plus for investors. But isn&rsquo;t Dubai Marina &mdash; given the high density build-up it has already attained &mdash; reaching a sort of saturation level on yields?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Reason for price rise</p>\r\n\r\n<p>Rahail Aslam, CEO of Select Group, begs to differ &mdash; &ldquo;Dubai Marina as a whole is nearing completion, therefore makes it a constrained area with a lack of new quality developments which will continue to push prices up. With the population growing 5 per cent year-on-year since 2011 and house prices rising by 28.5 per cent in 2013, we believe it will continue to be an attractive location to investors and end-users alike.</p>\r\n\r\n<p>&ldquo;Developing property is like any other commodity &mdash; you assess the market, your competitors and ensure your product offering and price tag is what investors are looking for.&rdquo;</p>\r\n\r\n<p>Select Group to date has been anchored to Dubai Marina. It was among the first wave of developers in the early part of the last decade to acquire plots there. To date, it has delivered more than 6 million square feet of residential property there, which makes it the largest private developer at Dubai Marina. Another project, West Avenue, is due for delivery this June. Its completed towers include the Botanica, Torch and Bay Central, all of which are under interim owners&rsquo; association management.</p>\r\n\r\n<p>Good building management</p>\r\n\r\n<p>&ldquo;Occupancy in all of Select Group projects consistently achieve 95 per cent,&rdquo; Aslam said. &ldquo;We believe this is because the right product is being offered with tier-one building management in place that ensures common areas and associated services are maintained to a high standard.&rdquo;</p>\r\n\r\n<p>Annual rental increase was between 12-16 per cent last year at Dubai Marina, according to estimates by Crompton Partners Estate Agents. &ldquo;This compared to increases of close to 20 per cent annually in similar developments such as Jumeirah Lakes Towers (JLT) shows there is still scope for increases in Dubai Marina,&rdquo; Mark Wellman-Riggs, general manager at the estate agency, said. &ldquo;My caveat to this would be these increases should only be expected in high-end buildings with good finishings and in good locations, desirable apartments that tenants want to live in and don&rsquo;t mind paying a little extra for.&rdquo;</p>\r\n\r\n<p>Just days into the new year, the market has already started seeing off-plan project announcements. New supply is getting absorbed as soon as they come online &mdash; at No. 9, more than 20 per cent of pre-launch sales are already committed, according to the developer.</p>\r\n\r\n<p>The market should see a steady supply of new launches as soon as developers clear the regulatory requirements of fully paid up land and 20 per cent completion of construction works for off-plan sales to go ahead.</p>\r\n\r\n<p>Maximising returns</p>\r\n\r\n<p>&ldquo;New properties will always get a premium purely based on the fact that they are new,&rdquo; Wellman-Riggs said. &ldquo;Buyers looking to invest will try and buy off-plan to maximise their returns, both capital growth and rental return, when the property is finished.</p>\r\n\r\n<p>&ldquo;Investors initially look at who the developer is, then follow this up with questions about the location and ask what similar units rent for in the current market. The days of 8-10 per cent rental return have all but disappeared but there are still some who take this long-term, off-plan approach to achieve this.&rdquo;</p>\r\n', '1', '', '', '1', '0', '2014-01-16 05:52:33', '2014-01-16 05:52:33');
INSERT INTO `articles` VALUES ('136', ' Sharjah education show from Jan 29', 'By Abbas Rashid', ' 60 exhibitors to take part in Great India Education Fair to be held alongside the show. ', '<p>Strong growth in higher education will continue to provide students with a bigger choice of courses every year. A prominent fixture on the UAE&rsquo;s education calendar, the International Education Show (IES) in Sharjah will field a larger delegation of universities and schools this year.</p>\r\n\r\n<p>Hosted at the Sharjah Expo Centre from January 29 to February 1, the four-day show has registered a 31 per cent rise in the number of exhibitors with nearly 150 institutions signing up for the event.</p>\r\n\r\n<p>An additional 60 exhibitors from India will also be taking part in the Great India Education Fair which is being held for the second consecutive year featuring international boarding schools, universities and colleges from India.</p>\r\n\r\n<p>With the UAE Cabinet approving 21 per cent of its total budget allocation of Dh46 billion for the education sector in 2014, it becomes a continuing trend where the government is placing high importance on the sector, thus boosting demand for higher education.</p>\r\n\r\n<p>&ldquo;Though we have good local and international institutions in the country and can lay claim to the status of an emerging educational hub, we have to admit that huge gaps exist between workforce supply and demand in a number of industries. Through the high budgetary allocations and proactive strategies implemented by the Ministry of Higher Education and Scientific Research, the UAE government is aiming to plug these gaps,&rdquo; said Saif Mohammed Al Midfa, CEO, Sharjah Expo Centre.</p>\r\n\r\n<p>&ldquo;We are also likely to see more educational institutions coming to the UAE and the existing ones increasing their programmes to meet the rising requirements. The International Education Show will be a dedicated platform for local and international institutions to target the ambitious student population in the region,&rdquo; added Al Midfa.</p>\r\n\r\n<p>Asians constitute the largest student community in the UAE and the Great India Education Fair, India&rsquo;s most popular education event, will target the expat population. The fair will also help parents interact directly with principals from some of India&rsquo;s top boarding schools which are offering spot admissions from Grade 2 onwards.</p>\r\n', '1', '', '', '1', '0', '2014-01-16 05:56:41', '2014-01-16 05:56:41');
INSERT INTO `articles` VALUES ('137', ' Dubai capable of hosting Olympic Games, says sports lawyer', 'By Abbas Rashid', 'Steven Bainbridge says the region was in good standing to make an inaugural Olympics bid before long. ', '<p>Dubai is in a strong position to make a bid for the Olympic Games in the coming years, according to one of the region&rsquo;s top sports lawyers.</p>\r\n\r\n<p>Steven Bainbridge, Head of Sports Law at the Middle East&rsquo;s largest law firm Al Tamami &amp; Company, said the region was in good standing to make an inaugural Olympics bid before long &mdash; especially given Dubai&rsquo;s successful World Expo 2020 bid and Qatar hosting the Football World Cup in 2022.</p>\r\n\r\n<p>&ldquo;That&rsquo;s the elephant in the room. In terms of sports, the World Cup is massive and that&rsquo;s coming to the region ... but I think at some point there will be an Olympic bid in the region ... (Dubai) is the natural choice.&rdquo;</p>\r\n\r\n<p>The Doha-based lawyer, who has previously worked as General Counsel for Yas Marina Circuit, said the World Cup and World Expo would be testers for an Olympic bid.</p>\r\n\r\n<p>&ldquo;It&rsquo;s a continuum of events. If I had to pick anywhere in the world to run an innovative, exciting Olympics, you could do a lot worse than picking somewhere like Dubai.&rdquo;</p>\r\n\r\n<p>But many things would be required to arrange a successful bid, including looking at shifting the timing to the winter months.</p>\r\n\r\n<p>&ldquo;Infrastructure is a big issue but I think all of these elements would be here or will have been ready to be established in a short time.&rdquo;</p>\r\n\r\n<p>Bainbridge also said with the huge increase in all aspects of sports involvement, especially in the Europe, Middle East and Africa market (EMEA) where revenues have grown 4.6 per cent largely due to sponsorship, companies were understanding the importance of sports sponsorship.</p>\r\n\r\n<p>Emirates Airlines were probably the most prolific sports sponsors in the globe which had helped greatly with their brand recognition, he said.</p>\r\n\r\n<p>&ldquo;I can&rsquo;t think of anyone with a pulse who isn&rsquo;t aware of Emirates and certainly there&rsquo;s an element of energy and vibrancy attached to that brand which I think in some respects is linked with sporting sponsorship.&rdquo;</p>\r\n\r\n<p>Smaller companies were also following suit in a big way, he said.</p>\r\n\r\n<p>&ldquo;In the market, I&rsquo;m actually more encouraged by the small companies that don&rsquo;t have millions to spend saying: &lsquo;Actually, it&rsquo;s better for us to spend 100,000, let&rsquo;s do that&rsquo;. They see the benefits that others are getting.&rdquo;</p>\r\n', '2', '', '', '1', '0', '2014-01-16 05:59:19', '2014-01-16 05:59:19');
INSERT INTO `articles` VALUES ('138', ' Excellence awards for Abu Dhabi departments launched', 'By Abbas Rashid', ' The programme will be a parallel for “The Abu Dhabi Award for Excellence in Government Performance” and a contributor to the achievement of its objectives. ', '<p>General Shaikh Mohammed bin Zayed Al Nahyan, Crown Prince of Abu Dhabi and Deputy Supreme Commander of the UAE Armed Forces and Chairman of the Abu Dhabi Executive Council, has directed the launch of a programme of excellence awards within departments affiliated to the Government of Abu Dhabi.</p>\r\n\r\n<p>The programme will be a parallel for &ldquo;The Abu Dhabi Award for Excellence in Government Performance&rdquo; and a contributor to the achievement of its objectives.</p>\r\n\r\n<p>The aim of these awards is to motivate government agencies to work on raising the level of their performance and improving the work mechanisms in its internal sectors and to contribute to raising the awareness on the culture of excellence and quality and transparency, making it a key component in the work environments of various government bodies.</p>\r\n', '2', '', '', '1', '0', '2014-01-16 06:01:28', '2014-01-16 06:01:28');
INSERT INTO `articles` VALUES ('139', ' Financial worries dominate says career survey', 'By Abbas Rashid', ' Career survey finds 44% have perfect job; 59% want promotion ', '<p>Over half the respondents in a career aspirations study say they worry about financial issues in their daily life. However, 44 per cent of working respondents in the UAE think they are in the &lsquo;perfect&rsquo; job, although 59 per cent want a promotion.</p>\r\n\r\n<p>According to the recent Career Aspirations in the Middle East and North Africa survey, conducted by job site Bayt.com, and YouGov, a research and consulting organisation, more than half of the UAE&rsquo;s respondents aspire to be promoted while about a third of them are concerned about losing their job and unemployment.</p>\r\n\r\n<p>Despite the perceived level of contentment, about three in five working respondents claim that there are not many opportunities for career advancement within their current company. Hence, two in five are prepared to move to another area of expertise or department to advance their career, or to move to another industry altogether.</p>\r\n\r\n<p>&ldquo;It&rsquo;s very interesting that 43 per cent of respondents would like to be an expert in their field,&rdquo; said Suhail Masri, VP of Sales, Bayt.com.</p>\r\n\r\n<p>When asked the reasons for accepting their current job offer, 29 per cent claimed that it was the first, or the only one they had received. Almost a quarter, 23 per cent, accepted it because of the salary and benefits offered, and 15 per cent claimed it offered them a chance to work in their dream industry. Interestingly, only one in six were motivated to take the job because of its corporate reputation.</p>\r\n\r\n<p>Salary hike?</p>\r\n\r\n<p>When Khaleej Times caught up with a few residents of the UAE on the survey results, they opined that with Dubai winning the Expo 2020 bid, a career upsurge was expected. However, some of them do suggest that the salary hikes will not be proportionate to the rate of inflation.</p>\r\n\r\n<p>John R, a resident of Dubai said: &ldquo;A lot of new jobs will be created. But I can take advantage of that situation only if I jump jobs, which is something I don&rsquo;t like doing. I don&rsquo;t think salary increases would be proportionate to the rent increase, as well.&rdquo; He added: &ldquo;Take the example of the increase in Salik price, the increase in consumer goods, these are little things, but they make a big difference.&rdquo;</p>\r\n\r\n<p>Sharjah resident Santosh (full name withheld at request) said: &ldquo;I paid for a 25 per cent hike in rent this year. My annual rent now is Dh46,000 and my salary hike for 2013 was Dh1,000. If I want to keep my family here and save money simultaneously, savings are not going to happen.&rdquo; The survey did suggest that on an average, working respondents were able to save about one sixth of their income over the past year.</p>\r\n\r\n<p>The survey by Bayt.com also suggests that just a few respondents have a positive outlook over and above those who have a negative outlook of the current work environment. It is perceived that the government&rsquo;s role should be to increase job opportunities (in particular for the younger respondents), improve labour laws, create avenues for training and career development, as well as increase salary transparency.</p>\r\n\r\n<p>Ideal job for UAE workforce</p>\r\n\r\n<p>Oil and gas emerges as the preferred industry for 14 per cent of the UAE&rsquo;s current and potential workforce. Construction comes in second at eight per cent, followed by banking and finance; advertising, marketing and public relations; tourism, and hospitality.</p>\r\n\r\n<p>&ldquo;The survey, indeed, shows much positivity for the UAE&rsquo;s economy in the years to come,&rdquo; said Masri. &ldquo;All the ingredients are right: employees are happy, motivated, and have for the large part have set goals in place.&rdquo;</p>\r\n\r\n<p>When asked to identify barriers to career growth, respondents specified a lack of growth opportunities (40 per cent) and poor management within their current company (38 per cent) as the top barriers. Overall, respondents are happy with their work environment, with 39 per cent being either &lsquo;somewhat&rsquo; or &lsquo;very happy&rsquo;. However, some believe that &ldquo;the government can help improve the working environment through increased transparency in terms of salaries within companies&rdquo; (62 per cent), by &ldquo;increasing job opportunities&rdquo; (59 per cent), and by &ldquo;further improving labour laws&rdquo; (44 per cent).</p>\r\n', '4', '', '', '1', '0', '2014-01-16 06:14:18', '2014-01-16 06:14:18');
INSERT INTO `articles` VALUES ('140', ' Dubai to be host to all things food and fun', 'By Abbas Rashid', ' Dubai Food Carnival and the Big Grill to promote emirate as foodie destination. ', '<p>It will be a food lover&rsquo;s paradise in Dubai from February 21 to March 15. The city will host two new food events &mdash; the Dubai Food Carnival and the Big Grill to promote itself as a foodie destination.</p>\r\n\r\n<p>Whatever your food palette or cuisine, there will be something for everyone. The festival will showcase diverse food offerings in Dubai, from Michelin starred chefs to street food, drawn from cuisines of more than 200 nationalities.</p>\r\n\r\n<p>Dubai Festivals and Retail Establishment (DFRE), an agency of Dubai Department of Tourism and Commerce Marketing is launching the event and organisers believe the festival will be a good way to promote the city&rsquo;s food offerings.</p>\r\n\r\n<p>&ldquo;Dubai&rsquo;s status as a culinary destination is one that has risen considerably in recent years and the creation of a city-wide food festival will contribute to enhancing this further,&rdquo; said Helal Saeed Almarri, Director-General DTCM.</p>\r\n\r\n<p>The festival will celebrate and enhance Dubai&rsquo;s position as the gastronomy capital of the region with more than 5,300 restaurants and places to eat. Running over 23 days, including four weekends, the festival will feature a wide range of food-related activities, tastings, offers and events.</p>\r\n\r\n<p>&ldquo;The packed festival programme is designed to showcase the wealth of flavours and cuisines on offer in Dubai, and will promote the diversity, creativity and multi-cultural nature of Dubai&rsquo;s culinary offering &mdash; one drawn from traditional Emirati cuisine as well as from the cuisines of the 200 nationalities living in the Emirate today,&rdquo; said Laila Mohammed Suhail, CEO of Dubai Festivals and Retail Establishment, an agency of DTCM.</p>\r\n\r\n<p>Dubai Food Festival will begin with the inaugural Dubai Food Carnival, a celebration of all things food, fun and entertainment, on February 21 and 22 at Dubai Festival City.</p>\r\n\r\n<p>Organised by Dubai World Trade Centre (DWTC), it will feature celebrity chefs conducting master classes, workshops, international food pavilions, mouth-watering food tastings and a multi-cultural feast of music, comedy and family-themed competitions.</p>\r\n\r\n<p>The festival will continue with Gulfood 2014 &mdash; the world&rsquo;s largest annual food and hospitality trade show. Running over five days from February 23 to 27, the show will cover the entire food service sector.</p>\r\n\r\n<p>The last day of Gulfood will coincide with an entirely new event &mdash; The Big Grill. Taking place on February 27 and 28 at Emirates Golf Club, The Big Grill 2014 is a celebration of meat and music; a unique event dedicated to anything grilled.</p>\r\n\r\n<p>Top grill masters, from the high street will serve their delicacies in a vibrant festive and family friendly atmosphere, while a King of the Grill competition aims to seek out and showcase Dubai&rsquo;s own BBQ masters.</p>\r\n\r\n<p>Throughout the Food Festival a range of gourmet experiences and culinary delights will take place at participating restaurants and venues across Dubai before the festival finale, Taste of Dubai.</p>\r\n\r\n<p>Taste of Dubai will take place at Dubai Media City Amphitheatre from March 13 to 15.</p>\r\n', '3', '', '', '1', '0', '2014-01-16 06:16:18', '2014-01-16 06:16:18');
INSERT INTO `articles` VALUES ('141', ' Two-day Gala in Dubai to celebrate science', 'By Abbas Rashid', ' Gala will bring together scientists and children on January 17 and 18 for motivating the latter ', '<p>Science India Gala 2013 will be celebrated over two days on January 17 and 18. The gala is an annual celebration of the Science India Forum-UAE (SIF-UAE) that brings together scientists and children with the purpose of igniting young minds and motivating them in the field of science. SIF-UAE is active with educational institutions in the UAE and organises events to motivate young minds and highlight the pivotal role of science in shaping modern life.</p>\r\n\r\n<p>The gala is being organised by SIF-UAE in association with Vijnana Bharathi of India, Indian Space Research Organisation and supported by the Embassy of India, UAE. This year&rsquo;s gala will felicitate Sastra Prathibhas (young scientists), officials from educational institutions and visionary leaders. The two-day programme will be held in Dubai and Abu Dhabi.</p>\r\n\r\n<p>The first day of the gala will be held at the Dubai Mens College, Academic City on Friday between 4pm and&nbsp; 7.30pm. The 2013 Sastra Prathibhas will be felicitated at the event. Padma Vibhushan Dr Anil Kakodkar, former chairman of the Atomic Energy Commission of India (AECI) and also the secretary to the Government of India, Department of Atomic Energy; Dr Easa Bastaki, president, University of Dubai; A. Jayakumar, secretary general, Vijnana Bharathi, and other dignitaries are expected to attend.</p>\r\n\r\n<p>It is a matter of pride that 19,500 students participated from 56 schools across the UAE for the Sastra Prathibha contest held on June 24 last year. The 16 Sastra Prathibhas and 48 A+ winners are the students who have excelled in the competitive science talent exam &lsquo;Sastra Prathibha Contest 2013&rsquo;. Followed by cultural programmes, the day will also feature the much-awaited release of the Science India Year Book 2013. Thirteen students belonging to four teams in the UAE, who were selected and taken by SIF-UAE to participate at the National Children Science Congress (NCSC) Bhopal, will also be felicitated.</p>\r\n\r\n<p>An interactive session with Dr Kakodkar will take place at the Indian Embassy Auditorium in Abu Dhabi on Saturday,&nbsp; from 9.30 to 11.30am.</p>\r\n\r\n<p>Anil Kakodkar</p>\r\n\r\n<p>SIF-UAE provides a platform for selected students to participate in an interactive session with Dr Kakodkar. The chief mentor will be speaking on &lsquo;Energy, Education and Rural Development&rsquo; and students will be encouraged to ask questions.</p>\r\n\r\n<p>At another event &lsquo;Igniting Minds&rsquo;, school principals will meet Dr Kakodkar at the Indian Embassy Auditorium in Abu Dhabi on Saturday between 12 noon and 1pm.</p>\r\n\r\n<p>Science India Forum, in association with the Cultural Wing of the Embassy of India in Abu Dhabi, is organising an exclusive meet with school principals/head of institutions to further discuss the &rsquo;Igniting Minds&rsquo; project. The interactive meet followed by lunch, will be chaired by Dr Kakodkar and Jayakumar.</p>\r\n', '3', '', '', '1', '0', '2014-01-16 06:17:19', '2014-01-16 06:17:19');
INSERT INTO `articles` VALUES ('142', ' World Bank raises growth outlook', 'By Abbas Rashid', ' Recovery in advanced economies boosts prospects for developing markets’ exports ', '<p>The World Bank raised its global growth forecasts as the easing of austerity policies in advanced economies supports their recovery, boosting prospects for developing markets&rsquo; exports.</p>\r\n\r\n<p>The Washington-based lender sees the world economy expanding 3.2 per cent this year, compared with a June projection of three per cent and up from 2.4 per cent in 2013. The forecast for the richest nations was raised to 2.2 per cent from two per cent. Part of the increase reflects improvement in the 18-country euro area, with the US ahead of developed peers, growing twice as fast as Japan.</p>\r\n\r\n<p>The report by the institution that&rsquo;s trying to eradicate extreme poverty by 2030 indicates a near-doubling of the growth in world trade this year from 2012, as developed economies lift export-reliant emerging nations. At the same time, the withdrawal of monetary stimulus in the US may raise market interest rates, hurting poorer countries as investors return to assets such as Treasuries, according to the bank.</p>\r\n\r\n<p>&ldquo;This strengthening of output among high-income countries marks a significant shift from recent years when developing countries alone pulled the global economy forward,&rdquo; the bank said on Tuesday in its Global Economic Prospects report published twice a year. Import demand from the richest nations &ldquo;should help compensate for the inevitable tightening of global financial conditions that will arise as monetary policy in high- income economies is normalised.&rdquo;</p>\r\n\r\n<p>The bank&rsquo;s forecasts hinge on the orderly unwinding of Federal Reserve stimulus, which is starting this month with the trimming of monthly bond purchases to $75 billion from $85 billion. If investors react abruptly in coming months, as they did in May when the central bank mentioned the possibility of tapering, capital inflows to developing economies could drop again, according to the report. The bank sees a global expansion of 3.4 per cent in 2015, compared with 3.3 per cent predicted in June.</p>\r\n\r\n<p>In the US, where growth is seen accelerating to 2.8 per cent this year, unchanged from the outlook in June, the recent budget compromise in Congress will ease spending cuts previously in place and boost confidence from households and businesses, the bank said.</p>\r\n\r\n<p>The bank held its forecast this year for Japan at 1.4 per cent.&rdquo;</p>\r\n\r\n<p>It raised its prediction for the euro region to 1.1 per cent for this year from 0.9 per cent in June.</p>\r\n\r\n<p>The 2014 forecast for developing markets was cut to 5.3 per cent from 5.6 per cent.</p>\r\n\r\n<p>The bank lowered its forecast for China this year to 7.7 per cent from eight per cent.</p>\r\n', '2', '', '', '1', '0', '2014-01-16 06:19:13', '2014-01-16 06:19:13');
INSERT INTO `articles` VALUES ('143', ' Landmark housing market brings high hopes for 2014', 'By Abbas Rashid', ' Rise in sales and investors’ interests set the stage for thriving demand ', '<p>First of all, I&rsquo;d like to wish you all a great start to the new year and hope that you&rsquo;re enjoying the holidays.</p>\r\n\r\n<p>2013 has no doubt been a big year for the UAE property market. Dubai was recently named the world&rsquo;s strongest housing market in 2013 given the city&rsquo;s status as a safe haven with improved consumer and investor confidence. Home prices have been recorded at the highest levels since the downturn and the market has also gained a more favourable reputation for tighter regulation, given the spate of laws we&rsquo;ve seen come into effect this year.</p>\r\n\r\n<p>So much so that even as 2013 inches to a close, the government has enforced a new rental decree which allows for rents to be increased by five per cent if they are 11 per cent below the market rate for the area rather than 26 per cent, as determined by RERA&rsquo;s rental index. The law will be applicable to private and public sector owned properties in Dubai, as well as those within the free zones.</p>\r\n\r\n<p>With fears of a lurking rental bubble especially post the Expo 2020 win, this latest announcement can be seen as yet another initiative by the government to ensure that the momentum within the real estate sector is managed well and that landlords do not arbitrarily hike rents on renewals on any property within the city. This also makes sense given the fact that the many businesses and professionals expected to come in to the country in the years leading to the Expo could lead to heightened demand for properties, causing landlords to demand higher rents.</p>\r\n\r\n<p>Reactions to the new rental policy, as expected, have been mixed. Whilst some residents consider this a better move than the recent total removal of the rent cap in Abu Dhabi, others worry that a rise in rents too quickly could drive the market into bubble territory. However, we can also look at the situation from the viewpoint of the landlord. For one, since 2008/2009, tenants in Dubai have enjoyed the benefits of relatively lower rents. Hence, landlords could argue that given the rebounding market, they should be able to pick up better returns. This may also work in the favour of tenants, as more landlords content with rental returns in the long run would mean fewer reasons for them to make a quick buck by evicting tenants. Also, given the huge influx of investors to Dubai in view of Expo 2020, it makes sense to realign and adjust the rental index across private and public sector and free zone owned properties in Dubai.</p>\r\n\r\n<p>Amidst all this debate, there are a couple of lessons we need to take away. Whilst landlords should understand their responsibilities and abide by the law, tenants should make sure they understand the Dubai Rent Index that provides average rentals for all key neighbourhoods and use the online Rental Increase Calculator to know the increase their landlords are eligible for.</p>\r\n\r\n<p>The house price boom that preceded the downturn was so remarkable that to most people there seemed only one way for prices to go up. 2013 has been a year of change and growth that many hope will carry into the new year. Whilst the housing market has indeed made great strides showing a marked increase in sales and investor interest and setting the stage for thriving demand with the Expo 2020 win, expecting the price appreciation to continue with the same fervour next year seems to me once again a self-reinforcing cycle of popular belief that prices can only go higher. This, of course, is not realistic and a sign of misplaced optimism, as for the housing market stable rather than accelerated growth is what will make it robust and keep the heat out of housing.</p>\r\n\r\n<p>Ultimately, supply and demand market dynamics will override other variables. With market and economic fundamentals remaining strong, there is little reason at this stage to question that 2014 will also be a strong year for the real estate market in the UAE.</p>\r\n', '4', '', '', '1', '0', '2014-01-16 06:21:26', '2014-01-16 06:21:26');
INSERT INTO `articles` VALUES ('144', 'UAE Pump Plan: Fill your own petrol and get a discounted rate', 'By Abbas Rashid', 'Fuel retailers are considering monetary incentive for customers to promote self-service at petrol stations', '<div class=\"content\">\r\n<div class=\"body html-output\">\r\n<p>UAE&#39;s fuel retailers intend to introduce a new system whereby motorists will be offered petrol and diesel at discounted rates for self-service.</p>\r\n\r\n<p>According to highly placed sources, the plan is currently on the negotiating table and will be implemented once approved by the higher authorities. The scheme will reward customers for using self-service petrol stations.</p>\r\n\r\n<p>Sources told Emirates 24|7 that in the next phase of expanding the self-service, customers are most likely to be rewarded for the self-service with fuel at a rate lower than at manned petrol stations.</p>\r\n\r\n<p>Fuel at discounted rate at self-service petrol stations is offered in some countries around the world and has attracted a large number of cost-conscious customers. Self-service was introduced at fuel stations in Europe, America and Canada to reduce&nbsp; manpower cost and to circumvent fuel station employee strikes.</p>\r\n\r\n<p>Currently, 25 fuel stations are offering self-service &ndash; but not offering discounted rate &ndash; in Dubai on experimental basis.</p>\r\n\r\n<p>The operational cost at a self-service fuel station is considerably less than the cost of manned fuel stations, sources said.</p>\r\n\r\n<p>An official told Emirates 24l7 that the initial response to introducing self-service at selected petrol stations yielded mixed results. Some customers felt it is inconvenient but giving fuel at discounted price may tempt them to get out of their car, make payment at the convenience store and personally fill his/her vehicle&rsquo;s tank.</p>\r\n\r\n<p>&ldquo;It is a novel idea to give a monetary incentive to motorists who&rsquo;re comfortable with&nbsp; self-services. Womens, handicapped people, unwell and old customers are now given special preference and the staff is advised to give help,&rdquo; an official said.</p>\r\n\r\n<p>&ldquo;I will definitely opt for self-service if I can save Dh5 by filling the car&rsquo;s tank. I think everybody in town will opt for self-service. Even if my saving is small per filling, the option will have an impact on my monthly budget,&rdquo; said Mohammed, who spends Dh300 per week on fuel for his four-wheel drive.</p>\r\n\r\n<p>Many customers, especially women, are reluctant to get out of their vehicles to fill petrol particularly in&nbsp; hot and humid weather.</p>\r\n</div>\r\n</div>\r\n', '1', '', 'article', '0', '0', '2014-02-12 06:11:17', '2014-02-12 06:11:17');
INSERT INTO `articles` VALUES ('145', 'CENTRAL AFRICAN REPUBLIC CRISIS EXPLAINED IN 60 SECONDS - BBC NEWS', 'By Abbas Rashid', 'A similar ‘moving-out policy’ has been introduced in another building in Tecom. “We’ve issued eviction notices to people sharing illegally,” said a building manager here.', '<p>Dubai: Building managements are issuing eviction notices to people found sharing apartments in Tecom areas.</p>\r\n\r\n<div class=\"articleBody\">\r\n<p>In a bid to crack down on unauthorised sharing in new Dubai, they have also introduced a rule under which people must seek permission from them before moving out.</p>\r\n</div>\r\n\r\n<div class=\"articleBody\">\r\n<p>&ldquo;The policy is intended to ensure that it&rsquo;s the tenant who is moving out and not anyone else,&rdquo; said Abdul Jalil, leasing manager, Al Abjar Real Estate.</p>\r\n</div>\r\n\r\n<div class=\"articleBody\">\r\n<p>A notice on one of the buildings &mdash; Al Shaiba Tower &mdash; read: &ldquo;Shifting of furniture etc. requires permission from the accounts office and if the move-out falls on a public holiday, you must obtain a permit the day before.&rdquo;</p>\r\n</div>\r\n\r\n<div class=\"articleBody\">\r\n<p>&ldquo;Our building is only for families and we ensure this when they register as tenants. We even take an undertaking from tenants that the lease will only be for family accommodation. Despite this, sometimes tenants bring others to live in. No sooner we identify such a flat, we immediately issue an eviction notice,&rdquo; said Jalil.</p>\r\n</div>\r\n\r\n<div class=\"articleBody\">\r\n<p>A similar &lsquo;moving-out policy&rsquo; has been introduced in another building in Tecom. &ldquo;We&rsquo;ve issued eviction notices to people sharing illegally,&rdquo; said a building manager here.</p>\r\n</div>\r\n\r\n<div class=\"articleBody\">\r\n<p>In an email response, a Tecom spokesperson said: &ldquo;It is the responsibility of building owners what management rules and regulations they put in place in terms of tenants occupying their buildings, but these must be in line with government regulations. Tecom Investments has no responsibility in this regard.&rdquo;</p>\r\n</div>\r\n', '2', '', 'article', '1', '0', '2014-02-13 06:49:58', '2014-02-13 06:49:58');

-- ----------------------------
-- Table structure for `auto_emails`
-- ----------------------------
DROP TABLE IF EXISTS `auto_emails`;
CREATE TABLE `auto_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `footer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auto_emails
-- ----------------------------

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1265 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Abrasives', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('2', 'Access Control Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('3', 'Accountants and Auditors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('4', 'Accounting Software', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('5', 'Acoustical Consultants and Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('6', 'Acrylic Products - Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('7', 'Adhesive Tapes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('8', 'Adhesives and Glues', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('9', 'Advertising - Gift Articles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('10', 'Advertising - Outdoor', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('11', 'Advertising - Print Media', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('12', 'Advertising Agencies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('13', 'Aggregate and Sand Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('14', 'Agricultural and Horticultural Contractors and Eqpt Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('15', 'Air Conditioning - Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('16', 'Air Conditioning Contractors and Rental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('17', 'Air Conditioning Duct Cleaning', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('18', 'Air Conditioning Engineers, Installation and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('19', 'Air Conditioning Equipment and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('20', 'Air Conditioning Grilles and Diffusers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('21', 'Air Conditioning Supplies and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('22', 'Air Duct Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('23', 'Air Freight', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('24', 'Air Purifying and Cleaning Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('25', 'Aircraft Charter, Rental and Leasing Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('26', 'Aircraft Equipment, Parts and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('27', 'Airline and Aircraft Support Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('28', 'Airlines', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('29', 'Airport Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('30', 'Alloy and Alloy Analyzers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('31', 'Aluminium and Aluminium Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('32', 'Aluminium - Cast Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('33', 'Aluminium Composite Panels', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('34', 'Aluminium Extrusions ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('35', 'Aluminium Fabricators ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('36', 'Aluminium Sheet Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('37', 'Ambulance - Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('38', 'Amusement Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('39', 'Amusement Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('40', 'Animal and Poultry Feed Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('41', 'Animal Welfare Organization', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('42', 'Antennas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('43', 'Antiques - Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('44', 'Appliances - Domestic - Sales and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('45', 'Aquariums and Aquarium Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('46', 'Architects', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('47', 'Architectural Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('48', 'Architectural Ironmongers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('49', 'Architectural Structures and Illumination', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('50', 'Armoured Vehicle', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('51', 'Art Galleries and Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('52', 'Artificial Flowers and Plants - Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('53', 'Artists Materials and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('54', 'Asphalt and Asphalt Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('55', 'Audio-Visual Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('56', 'Audio-Visual Equipment - Rental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('57', 'Audio-Visual Equipment Systems and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('58', 'Audio-Visual Mounting Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('59', 'Audio-Visual Production Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('60', 'Automation Systems and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('61', 'Automobile Parts and Supplies (See Car Parts and Accessories)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('62', 'Aviation - Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('63', 'Awnings and Canopies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('64', 'Baby Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('65', 'Badges', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('66', 'Bags and Sacks - Mfrs and Distrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('67', 'Bags - Paper, Plastic and Speciality', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('68', 'Bakeries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('69', 'Bakeries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('70', 'Bakery Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('71', 'Balancing Service - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('72', 'Balloons', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('73', 'Bank Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('74', 'Banks - Commercial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('75', 'Banks - Merchant and Investment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('76', 'Banks - Representative Office', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('77', 'Bar Coding Equipment, Systems and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('78', 'Battery Acid/Water Mfrs and Distributors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('79', 'Battery Charging Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('80', 'Battery Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('81', 'Bearings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('82', 'Bearings - Marine', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('83', 'Beauty Institutes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('84', 'Beauty Salons', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('85', 'Beauty Salons - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('86', 'Belts - Automotive and Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('87', 'Bicycles and Bicycles Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('88', 'Billiard Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('89', 'Binding Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('90', 'Bird Control', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('91', 'Bitumen and Bituminous Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('92', 'Blankets', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('93', 'Blasting and Quarrying Contractors and Eqpt Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('94', 'Blinds and Awnings - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('95', 'Boat Builders and Repairs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('96', 'Boat Dealers, Eqpt Rental and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('97', 'Boiler Distrs and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('98', 'Boiler Rental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('99', 'Boiler Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('100', 'Boiler Supplies and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('101', 'Bolts and Nuts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('102', 'Bookshops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('103', 'Bottles and Bottle Cap Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('104', 'Bouncy Castles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('105', 'Boutiques - General', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('106', 'Boutiques - General', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('107', 'Boutiques - Ladies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('108', 'Boutiques - Ladies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('109', 'Boutiques - Men', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('110', 'Boutiques - Men', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('111', 'Bowling Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('112', 'Bowling Centres - Eqpt and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('113', 'Boxes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('114', 'Brakes - Lining - Mfrs and Distrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('115', 'Brand Name', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('116', 'Broadcasting Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('117', 'Broadcasting Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('118', 'Brushes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('119', 'Building Chemicals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('120', 'Building Cleaning Services - See Building Maintenance, Repairs and Restoration', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('121', 'Building Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('122', 'Building Maintenance, Repairs and Restoration', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('123', 'Building Management Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('124', 'Building Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('125', 'Building Materials - Whol and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('126', 'Buildings - Portable', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('127', 'Bundles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('128', 'Bus Bar', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('129', 'Buses - Charter and Rental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('130', 'Buses - Distrs, Mfrs and Eqpt', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('131', 'Business Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('132', 'Business Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('133', 'Cable and Pipe Laying and Locating Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('134', 'Cable Cover Tiles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('135', 'Cable Installation Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('136', 'Cable Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('137', 'Cable Reel and Drum Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('138', 'Cable Trays - Ladders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('139', 'Cables Support System', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('140', 'Cafeterias', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('141', 'Calibration - Laboratory', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('142', 'Calibration Systems and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('143', 'Call Centre', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('144', 'Cameras Thermal - Infra Red', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('145', 'Camps and Camping Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('146', 'Candles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('147', 'Candy and Confectionery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('148', 'Cans - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('149', 'Car Air Conditioning', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('150', 'Car Care and Tinting Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('151', 'Car Care Products and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('152', 'Car Customizing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('153', 'Car Dealers - New Cars', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('154', 'Car Dealers - Used Cars', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('155', 'Car Finance Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('156', 'Car Hire and Leasing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('157', 'Car Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('158', 'Car Navigation Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('159', 'Car Paint Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('160', 'Car Park - Shades', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('161', 'Car Parks and Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('162', 'Car Parts and Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('163', 'Car Parts and Accessories - Used and Rebuilt', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('164', 'Car Recovery Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('165', 'Car Registration Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('166', 'Car Wash and Polish Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('167', 'Car Washing Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('168', 'Carbon Brushes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('169', 'Cards - Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('170', 'Cargo Services - Air, Land and Sea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('171', 'Carpenters and Joiners', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('172', 'Carpet and Rug Cleaners', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('173', 'Carpet and Rug Distrs and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('174', 'Carpet and Rug Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('175', 'Carpet Tiles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('176', 'Case Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('177', 'Cash Registers and Till Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('178', 'Castings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('179', 'Caterers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('180', 'Caterers Eqpt and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('181', 'Cathodic Protection Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('182', 'CD-DVD Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('183', 'Ceilings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('184', 'Cement Dealers and Stockists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('185', 'Cement Lining', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('186', 'Chains', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('187', 'Chamber of Commerce', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('188', 'Chartered Accountants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('189', 'Chauffeur Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('190', 'Chemicals and Chemical Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('191', 'Chemicals - Cleaning and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('192', 'Chillers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('193', 'Chiropractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('194', 'Chocolate and Cocoa', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('195', 'Churches', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('196', 'Cigar, Cigarette and Tobacco Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('197', 'Cinemas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('198', 'Civil Engineers - Contracting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('199', 'Cladding', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('200', 'Cleaning and Janitorial Services and Contrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('201', 'Cleaning Machinery and Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('202', 'Cleaning Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('203', 'Clearing and Forwarding Companies and Agents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('204', 'Clinics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('205', 'Clocks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('206', 'Closed Circuit TV Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('207', 'Clubs and Associations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('208', 'Clubs and Associations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('209', 'Coating - Protective', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('210', 'Coding and Marking Devices and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('211', 'Coffee Brewing Devices', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('212', 'Coffee Importers, Manufacturers and Merchants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('213', 'Coil Coating', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('214', 'Cold Storage Accessories Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('215', 'Cold Storage Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('216', 'Cold Storage Equipment Suppliers and Installation Contrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('217', 'Cold Storage Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('218', 'Colour Separations - Offset, Photo Engraving Etc', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('219', 'Commercial Vehicles and Hire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('220', 'Commission Agents and Indentors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('221', 'Commission Agents and Indentors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('222', 'Communications Equipment and Systems Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('223', 'Compact Disk Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('224', 'Compressor Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('225', 'Compressors - Hire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('226', 'Computer and Laptop Repairs and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('227', 'Computer - Renting and Leasing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('228', 'Computer Aided Design and Drafting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('229', 'Computer Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('230', 'Computer Data Recovery and Storage Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('231', 'Computer Multimedia Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('232', 'Computer Network Systems and Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('233', 'Computer Power Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('234', 'Computer Recycling', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('235', 'Computer Repairs, Installation and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('236', 'Computer Security Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('237', 'Computer Services, Systems and Eqpt Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('238', 'Computer Software', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('239', 'Computer Stationery Supplies and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('240', 'Computer Supplies and Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('241', 'Computer Training', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('242', 'Concrete - Precast and Post Tensioning', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('243', 'Concrete Blocks Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('244', 'Concrete Drilling and Cutting Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('245', 'Concrete Mixers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('246', 'Concrete Plant Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('247', 'Concrete Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('248', 'Concrete Ready Mixed', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('249', 'Concrete Specialised Applications and Repair Work', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('250', 'Concrete Suppliers and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('251', 'Conference and Seminar Rooms and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('252', 'Construction Claim Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('253', 'Construction Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('254', 'Construction Equipment and Machinery - Hire (See Plant Hire)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('255', 'Construction Equipment and Machinery - Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('256', 'Construction Equipment and Machinery Auctioneers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('257', 'Construction Equipment and Machinery Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('258', 'Construction Equipment - Spare Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('259', 'Construction Equipment - Used', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('260', 'Construction Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('261', 'Consulates', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('262', 'Container - Freight Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('263', 'Containers - Maintenance and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('264', 'Contractors - Electro-Mechanical', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('265', 'Contractors - General', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('266', 'Contractors - Turnkey Projects', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('267', 'Conveyor and Conveyors Belt Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('268', 'Cooling Towers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('269', 'Corrosion Resistant Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('270', 'Cosmetics and Plastic Surgery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('271', 'Cosmetics and Toiletries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('272', 'Costumes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('273', 'Cotton Rags and Waste Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('274', 'Couplings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('275', 'Courier Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('276', 'Covers/Colour Inserts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('277', 'Cradles - Rental and Sales', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('278', 'Crane Hire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('279', 'Cranes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('280', 'Cranes - Accessories and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('281', 'Cruises', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('282', 'Crushing, Screening, Washing Plants and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('283', 'Crystal Products Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('284', 'Crystal Repairs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('285', 'Curtains and Curtain Fixtures', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('286', 'Dairies and Dairy Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('287', 'Dancing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('288', 'Data Processing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('289', 'Dates', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('290', 'Day Care Centres and Nurseries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('291', 'DE - Watering Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('292', 'DE - Watering Pumps - Rental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('293', 'DE - Watering Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('294', 'Debt Adjusters and Collectors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('295', 'Decorating Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('296', 'Decoration Contractors - Party and Theming', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('297', 'Dehumidifying Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('298', 'Delivery Service - Corporate and Residential', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('299', 'Demolition Contractors - Comm, Ind and Residential', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('300', 'Dental Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('301', 'Dental Laboratories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('302', 'Dentists and Dental Clinics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('303', 'Department Stores', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('304', 'Dermatologists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('305', 'Desalination Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('306', 'Desalination Plants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('307', 'Detectable Warning Tapes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('308', 'Diamond Cutting Tools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('309', 'Diamond Jewellers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('310', 'Die Makers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('311', 'Diesel Engines - Parts and Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('312', 'Diesel Engines - Sales and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('313', 'Diesel Fuel', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('314', 'Digital Photographic Services and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('315', 'Digital Printing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('316', 'Digital Voice Recording Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('317', 'Direct Mail Advertising', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('318', 'Display Designers and Producers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('319', 'District Cooling Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('320', 'Divers Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('321', 'Diving Instruction and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('322', 'Doctors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('323', 'Doctors - Cardiology', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('324', 'Doctors - General Surgery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('325', 'Doctors - Gynaecology', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('326', 'Doctors - Homoeopathy', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('327', 'Doctors - Orthopaedics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('328', 'Doctors - Osteopathy', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('329', 'Doctors - Psychiatry and Psychotherapy', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('330', 'Document Clearing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('331', 'Document Shredding', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('332', 'Document Storage Facility', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('333', 'Dome Structures', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('334', 'Doors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('335', 'Doors and Gates - Automatic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('336', 'Doors - Fireproof', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('337', 'Doors - Folding and Sliding', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('338', 'Doors - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('339', 'Doors - Wood', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('340', 'Drainage and Sewerage Systems and Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('341', 'Dredgers and Dredging Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('342', 'Drilling Contractors - Concrete', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('343', 'Drilling Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('344', 'Drilling Equipment - Repair, Service and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('345', 'Driving Instructors and Schools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('346', 'Driving Instructors and Schools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('347', 'Drum - Components and Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('348', 'Drum Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('349', 'Dry Cleaners (See Laundries and Dry Cleaners)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('350', 'Duct - Fire Rated', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('351', 'Ductile Iron - Pipe and Pipe Fittings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('352', 'Ducting Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('353', 'Duty Free Shops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('354', 'Dyes and Dyestuffs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('355', 'E-Business Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('356', 'E-Commerce - Solution Providers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('357', 'Earthing and Lightning Protection Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('358', 'Earthmoving Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('359', 'Earthmoving Equipment and Spare Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('360', 'Educational Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('361', 'Educational Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('362', 'Educational Institutions and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('363', 'Educational Teaching Aids and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('364', 'Electric Busway Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('365', 'Electric Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('366', 'Electric Equipment - Service and Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('367', 'Electric Motor Rewinding Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('368', 'Electric Motors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('369', 'Electrical Contractors and Electricians', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('370', 'Electrical Switchgear', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('371', 'Electrical Traders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('372', 'Electro - Mechanical Consultants and Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('373', 'Electro - Mechanical Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('374', 'Electrolysis', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('375', 'Electronic Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('376', 'Electroplating', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('377', 'Embassies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('378', 'Embassies, Consulates, High Commissions and Legations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('379', 'Embroidery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('380', 'Energy Saving Products and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('381', 'Engineering Machinery Workshops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('382', 'Engineers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('383', 'Engineers - Architectural', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('384', 'Engineers - Consulting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('385', 'Engravers - General', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('386', 'Engraving Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('387', 'Enterprise Resource Planning ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('388', 'Entertainment Agencies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('389', 'Environmental Agencies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('390', 'Environmental Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('391', 'Environmental Control Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('392', 'Environmental Friendly Products and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('393', 'Environmental Monitoring and Testing Equipment and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('394', 'Equestrian Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('395', 'Equestrian Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('396', 'Events - Promotion Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('397', 'Events Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('398', 'Exhibition Management and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('399', 'Exhibition Organizers and Halls', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('400', 'Exhibition Stands and Fittings - Designers and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('401', 'Explosion Proof Components and Junction Boxes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('402', 'Explosives', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('403', 'Fabrics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('404', 'Facility Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('405', 'Fans and Ventilators - Ind and Comm - Sales and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('406', 'Fans - Exhaust', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('407', 'Fans - Manufacturers and Distributors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('408', 'Fasteners - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('409', 'Feasibility Studies Consultancy', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('410', 'Fencing Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('411', 'Fertilisers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('412', 'Fibreglass and Fibreglass Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('413', 'Fibreglass Fabricators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('414', 'Fibreglass Materials', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('415', 'Film and TV Equipment Rental ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('416', 'Film Production and Distribution', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('417', 'Filtering Materials and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('418', 'Filters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('419', 'Finance Companies and Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('420', 'Finger Prints Testing Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('421', 'Fire Alarm Systems - Commercial and Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('422', 'Fire Fighting Equipment - Installation, Maintenance and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('423', 'Fire Fighting Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('424', 'Fireproofing Contractors and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('425', 'Fireworks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('426', 'Fish and Seafood Importers, Exporters and Processors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('427', 'Fishermens Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('428', 'Flag Poles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('429', 'Flags and Banners', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('430', 'Flavouring Extracts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('431', 'Fleet Management Systems and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('432', 'Floor and Ceiling Products - Suppliers and Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('433', 'Floor Decks - Standard and Composite', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('434', 'Floor Laying, Refinishing and Resurfacing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('435', 'Floors - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('436', 'Floors - PVC', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('437', 'Floors - Raised', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('438', 'Floors - Wooden', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('439', 'Florists and Floral Designers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('440', 'Flow-Meters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('441', 'Foam Machinery Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('442', 'Food Colours and Flavourings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('443', 'Food Hygiene Training (HACAPP)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('444', 'Food Importers and Exporters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('445', 'Food Packaging', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('446', 'Food Processing Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('447', 'Food Processors and Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('448', 'Food Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('449', 'Food Safety Consultants and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('450', 'Forklift - Rental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('451', 'Forklift Repairs and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('452', 'Forklift Spare Parts Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('453', 'Forklift Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('454', 'Formwork and Shuttering Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('455', 'Foundries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('456', 'Fountains - Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('457', 'Free Zone Authorities', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('458', 'Free Zone Listing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('459', 'Freight Forwarding', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('460', 'Frozen Foods', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('461', 'Fruit and Vegetable - Brokers, Importers and Exporters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('462', 'Fruit and Vegetable Juices', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('463', 'Fruits and Vegetables', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('464', 'Fruits - Dried', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('465', 'Fuel Handling Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('466', 'Fuel Injection Parts and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('467', 'Fuel Pump Sales and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('468', 'Fume Extractor', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('469', 'Furnished Apartments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('470', 'Furnishers - Contract', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('471', 'Furnishing Fabrics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('472', 'Furniture - Outdoor', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('473', 'Furniture - Supplies and Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('474', 'Furniture Dealers - Whol and Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('475', 'Furniture Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('476', 'Fuses - Electric', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('477', 'Gabions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('478', 'Galvanizing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('479', 'Galvanizing Plant Machinery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('480', 'Garage Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('481', 'Garage Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('482', 'Garbage Disposal Equipment - Household', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('483', 'Garbage Disposal Equipment - Industrial and Commercial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('484', 'Garden and Lawn Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('485', 'Garden Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('486', 'Garment Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('487', 'Garments - Mfrs, Exporters and Importers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('488', 'Garments - Readymade', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('489', 'Gas Central Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('490', 'Gas Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('491', 'Gas Cylinders - Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('492', 'Gas Detection and Monitoring Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('493', 'Gas Eqpt Supplies and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('494', 'Gas Producers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('495', 'Gas Turbine Equipment and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('496', 'Gaskets', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('497', 'Gauges and Gages', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('498', 'Gear Motors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('499', 'Gears', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('500', 'General Traders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('501', 'Generator Repair Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('502', 'Generator Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('503', 'Generators - Hire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('504', 'Geosynthetics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('505', 'Geotextiles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('506', 'Gift and Novelty Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('507', 'Glass and Mirror Merchants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('508', 'Glass - Etched, Designed and Sandblasted', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('509', 'Glass - Stained and Leaded', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('510', 'Glass - Textured', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('511', 'Glass Bending, Drilling, Grinding Etc', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('512', 'Glass Fibre Reinforced Cement Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('513', 'Glass Processors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('514', 'Glassware', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('515', 'Global Positioning Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('516', 'Gold and Silver Refiners', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('517', 'Golf Cars and Carts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('518', 'Golf Club Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('519', 'Golf Courses', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('520', 'Golf Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('521', 'Graphic Designers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('522', 'Grass Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('523', 'Gratings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('524', 'Grease Traps - Sales, Services and Cleaning', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('525', 'Green Buildings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('526', 'Green Roofs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('527', 'Greenhouse Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('528', 'Greeting Cards', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('529', 'Grocers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('530', 'Gunite Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('531', 'Gymnasiums - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('532', 'Gypsum and Gypsum Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('533', 'Gypsum Interior Decorators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('534', 'Hair Care Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('535', 'Hair Ornaments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('536', 'Hair Replacement', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('537', 'Hairdressers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('538', 'Hairdressers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('539', 'Hand Tools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('540', 'Handicrafts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('541', 'Hardware - Whol and Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('542', 'Health Care', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('543', 'Health Clubs and Fitness Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('544', 'Health Food Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('545', 'Hearing Aids and Protectors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('546', 'Heat Exchangers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('547', 'Heat Insulation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('548', 'Heat Treating - Metal - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('549', 'Heaters - Unit - Mfrs and Distrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('550', 'Heavy Transport and Lifting Eqpt (See Lifting Equipment - Heavy  Transport)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('551', 'Helicopter Charter and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('552', 'Hoists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('553', 'Holding Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('554', 'Holography', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('555', 'Home Automation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('556', 'Home Health Care', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('557', 'Home Theatre Installation and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('558', 'Horizontal Directional Drilling', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('559', 'Horse Furnishings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('560', 'Hose Coupling and Fittings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('561', 'Hose Crimping Machine Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('562', 'Hoses and Belting Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('563', 'Hoses - Marine and Offshore', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('564', 'Hospital Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('565', 'Hospital Management and Medical Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('566', 'Hospitals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('567', 'Hotel Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('568', 'Hotel Furniture Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('569', 'Hotel Management Software', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('570', 'Hotels', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('571', 'Hotels - Apartments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('572', 'Household and Kitchen Eqpt Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('573', 'Humanitarian Organizations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('574', 'HVAC Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('575', 'Hydraulic - Repair and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('576', 'Hydraulic Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('577', 'Hydraulic Hoses and Fittings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('578', 'Ice Cream and Frozen Desserts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('579', 'Ice Cream Making Equipment and Machines', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('580', 'Ice Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('581', 'Identification Cards', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('582', 'Immigration and Naturalization Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('583', 'Import and Export Agents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('584', 'Industrial Automation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('585', 'Industrial Computer Integrated Mfrg Automation and Control', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('586', 'Industrial Consultants and Developments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('587', 'Industrial Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('588', 'Industrial Inspection Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('589', 'Information Technology Solution Provider', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('590', 'Inkjet Media', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('591', 'Inks - Printing, Flexographic and Lithographing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('592', 'Inks Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('593', 'Inspection Bureaus, Consultants and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('594', 'Instrument Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('595', 'Instrumentation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('596', 'Insulation Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('597', 'Insulation Materials - Cold and Heat', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('598', 'Insurance Brokers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('599', 'Insurance Companies and Agents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('600', 'Insurance Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('601', 'Interior Decorators and Designers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('602', 'Interior Decorators and Designers Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('603', 'Interior Design Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('604', 'Internet Related Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('605', 'Investment Companies and Advisers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('606', 'Ironmongery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('607', 'Irrigation Contractors and Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('608', 'Irrigation Systems and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('609', 'Jacuzzis', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('610', 'Jewellers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('611', 'Jewellery Mfrs Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('612', 'Karate Clubs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('613', 'Kennels', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('614', 'Keys and Key Cutting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('615', 'Keyword', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('616', 'Kitchen Cabinets and Equipment - Household', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('617', 'Kitchen Equipment - Commercial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('618', 'Kitchen Equipment - Parts and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('619', 'Kitchen Exhaust System - Commercial and Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('620', 'Kitchen Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('621', 'Kitchen Turnkey', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('622', 'Labelling Equipment and Labels', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('623', 'Laboratories - General', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('624', 'Laboratories - Testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('625', 'Laboratory Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('626', 'Laboratory Furniture Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('627', 'Labour Supply Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('628', 'Ladders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('629', 'Landscape Architects', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('630', 'Landscape Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('631', 'Landscaping Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('632', 'Laser Markings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('633', 'Laser Shows', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('634', 'Laundries and Dry Cleaners', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('635', 'Laundry and Dry Cleaning Equipment Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('636', 'Laundry and Dry Cleaning Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('637', 'Laundry Detergents and Chemicals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('638', 'Lawyers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('639', 'Lawyers - Corporate and Commercial Law', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('640', 'Leather Goods', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('641', 'Legal Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('642', 'Leisure Activities', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('643', 'Leisure Parks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('644', 'Libraries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('645', 'Library Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('646', 'Lifting Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('647', 'Lifting Equipment - Heavy Transport', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('648', 'Lifting Sling', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('649', 'Lifting Tackle', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('650', 'Lifts and Escalators Maintenance and Repair', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('651', 'Lifts and Escalators Suppliers and Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('652', 'Light Bulbs and Tubes - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('653', 'Lighting Equipment - Aircraft Warning', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('654', 'Lighting Fixtures - Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('655', 'Lighting Fixtures - Supplies and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('656', 'Lighting Fixtures - Whol and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('657', 'Lighting Poles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('658', 'Lighting Systems and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('659', 'Lime Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('660', 'Limousine Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('661', 'Linen, Cushion and Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('662', 'Lingerie', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('663', 'Locks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('664', 'Locksmiths', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('665', 'Logistics and Distribution', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('666', 'Loss Adjusters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('667', 'Loyalty Programs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('668', 'Loyalty Programs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('669', 'Lubricants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('670', 'Lubrication Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('671', 'Luggage', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('672', 'Machine Shops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('673', 'Machine Shops Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('674', 'Machine Tools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('675', 'Machine Tools - Workshops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('676', 'Machinery - New', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('677', 'Maid Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('678', 'Mailing Machines and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('679', 'Maintenance - General', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('680', 'Maintenance Contractors and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('681', 'Management Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('682', 'Management Training and Development', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('683', 'Manhole Covers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('684', 'Manpower Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('685', 'Maps', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('686', 'Marble and Granite - Grinding and Crystallizing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('687', 'Marble and Granite - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('688', 'Marble Machinery and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('689', 'Marine and Offshore - Bunkering Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('690', 'Marine and Offshore - Charter Operators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('691', 'Marine and Offshore - Coating and Paint Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('692', 'Marine and Offshore - Electrical and Electronic Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('693', 'Marine and Offshore - Engine Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('694', 'Marine and Offshore - Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('695', 'Marine and Offshore - Fire Protection Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('696', 'Marine and Offshore - Maintenance Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('697', 'Marine and Offshore - Surveyors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('698', 'Marine and Offshore Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('699', 'Marine Communications and Navigation Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('700', 'Marine Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('701', 'Marine Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('702', 'Marine Control and Automation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('703', 'Marine Electrical Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('704', 'Marine Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('705', 'Marine Generators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('706', 'Marine Machinery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('707', 'Marine Operators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('708', 'Marine Refrigeration and Air Conditioning Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('709', 'Marine Safety and Survival Eqpt - Sales and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('710', 'Marine Sports Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('711', 'Market Research and Analysis', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('712', 'Marketing Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('713', 'Massage Therapists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('714', 'Material Handling Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('715', 'Mattresses', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('716', 'Mechanical Engineering Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('717', 'Media Representatives', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('718', 'Medical Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('719', 'Medical Goods', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('720', 'Medical Rehabilitation Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('721', 'Meditation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('722', 'Metals - Raw Materials', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('723', 'Military Goods', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('724', 'Mineral Water Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('725', 'Minerals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('726', 'Mining and Quarrying Accessories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('727', 'Mobile Telephones', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('728', 'Model Makers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('729', 'Modelling Agencies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('730', 'Money Changers and Exchange Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('731', 'Mortgages', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('732', 'Motorcycles and Motor Scooters - Renting and Leasing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('733', 'Motorcycles and Motor Scooters - Sales and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('734', 'Motorcycles and Motor Scooters - Supplies and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('735', 'Mould Makers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('736', 'Multimedia Developers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('737', 'Museum - Exhibit Specialist', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('738', 'Museums', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('739', 'Museums', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('740', 'Music Instruction - Instrumental and Vocal', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('741', 'Musical Instruments - Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('742', 'Nails and Tacks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('743', 'Neon Sign Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('744', 'Neon Sign Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('745', 'Networking - Data Communication', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('746', 'Night Clubs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('747', 'Night Clubs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('748', 'No Classiification', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('749', 'Nurseries - Horticultural', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('750', 'Odour Control', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('751', 'Office and Desk Space Rental Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('752', 'Office Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('753', 'Office Furniture - Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('754', 'Office Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('755', 'Offshore Construction and Installation and Contractors and Supplies and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('756', 'Oil and Gas Pipe Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('757', 'Oil Companies - Offshore and Onshore', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('758', 'Oil Pollution Control Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('759', 'Oil Well Drilling Chemical Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('760', 'Oilfield Contractors and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('761', 'Oilfield Equipment - Repairs and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('762', 'Oilfield Equipment Rentals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('763', 'Oilfield Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('764', 'Optical Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('765', 'Opticians and Optical Goods Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('766', 'Packaging Machinery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('767', 'Packaging Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('768', 'Packaging Materials', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('769', 'Packaging Printers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('770', 'Packaging Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('771', 'Paediatricians', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('772', 'Paint - Raw Materials', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('773', 'Paint Merchants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('774', 'Painters and Painting Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('775', 'Painters - Special Finishers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('776', 'Painters Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('777', 'Pallets and Skids', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('778', 'Paper and Paper Products - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('779', 'Paper Distributors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('780', 'Paper Tissues - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('781', 'Paper Tubes and Cores', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('782', 'Parking Area Maintenance and Marking', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('783', 'Parking Meters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('784', 'Parks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('785', 'Partitions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('786', 'Party Planning Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('787', 'Party Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('788', 'Pens and Ball Pens', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('789', 'Perforated Sheet - Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('790', 'Perfumes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('791', 'Pest Control Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('792', 'Pest Control Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('793', 'Pet Grooming and Washing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('794', 'Pet Shops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('795', 'Pet Transporting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('796', 'Petroleum Products Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('797', 'Pharmaceutical Machinery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('798', 'Pharmaceutical Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('799', 'Pharmacies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('800', 'Photocopier - Repairing and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('801', 'Photocopier Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('802', 'Photocopying Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('803', 'Photographers - Advertising and Fashion', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('804', 'Photographers - Commercial and Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('805', 'Photographers - Digital', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('806', 'Photographic Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('807', 'Photographic Equipment - Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('808', 'Physiotherapists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('809', 'Pianos', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('810', 'Picture Frames Eqpt - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('811', 'Piling Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('812', 'Piling Equipment and Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('813', 'Pipe and Pipe Fitting Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('814', 'Pipe - Galvanized', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('815', 'Pipe Cutting and Threading Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('816', 'Pipe Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('817', 'Pipeline Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('818', 'Pipeline Installation Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('819', 'Pipelining and Coating', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('820', 'Pipes - Freezing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('821', 'Pipes - Pre Insulated', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('822', 'Plant Hire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('823', 'Plant Machinery and Heavy Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('824', 'Plants - Indoor', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('825', 'Plaster', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('826', 'Plastic Cards - Printing and Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('827', 'Plastics and Plastic Products Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('828', 'Plastics - Fabrics, Films and Sheets - Producers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('829', 'Plastics - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('830', 'Plastics - Machinery and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('831', 'Plastics - Polymers, Additives, Masterbatches', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('832', 'Plastics - Raw Materials - Powders, Liquids, Resins Etc', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('833', 'Plastics - Rods, Tubes, Sheets Etc - Supply Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('834', 'Playground Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('835', 'Plumbing Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('836', 'Plumbing Equipment and Fixtures Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('837', 'Plywood and Veneers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('838', 'Pneumatic Equipment and Tools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('839', 'Point Of Sale and Information Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('840', 'Poles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('841', 'Police', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('842', 'Polycarbonate Sheets and Rolls', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('843', 'Polypropylene Bags and Sacks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('844', 'Polystyrene Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('845', 'Polyurethane Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('846', 'Post Offices', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('847', 'Post Offices', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('848', 'Poultry Farms', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('849', 'Powder Coating', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('850', 'Powder Coating Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('851', 'Power Generation and Transmission Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('852', 'Power Plant Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('853', 'Power Tools Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('854', 'Pre-Insulated Panels - Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('855', 'Precision Dies and Tools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('856', 'Pressure Gauges', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('857', 'Pressure Vessels', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('858', 'Printers - Commercial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('859', 'Printing Equipment and Material Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('860', 'Process Control Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('861', 'Procurement Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('862', 'Project Design and Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('863', 'Project Management Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('864', 'Projectors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('865', 'Promoters/Instore Promotion and Sales', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('866', 'Promotional Gifts (See Advertising - Gift Articles)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('867', 'Properties - Freehold', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('868', 'Property Companies and Developers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('869', 'Property Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('870', 'Property Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('871', 'Protective Coating Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('872', 'Psychiatrists and Psychologists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('873', 'Public Addressing System', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('874', 'Public Relations Counsellors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('875', 'Publications - Nautical', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('876', 'Publishers - Book', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('877', 'Publishers - Directory and Guide', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('878', 'Pulleys', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('879', 'Pumps', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('880', 'Pumps - Renting and Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('881', 'Quality Assurance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('882', 'Quality Control Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('883', 'Quarries', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('884', 'Queuing Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('885', 'Radiator Coolants - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('886', 'Radiators - Automotive', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('887', 'Radio Communication Equipment and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('888', 'Real Estate', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('889', 'Real Estate - International', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('890', 'Real Estate Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('891', 'Recording Studios', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('892', 'Recruitment Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('893', 'Recycled Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('894', 'Refractories', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('895', 'Refrigerating Equipment - Comm - Sales and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('896', 'Refrigeration Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('897', 'Refrigeration Equipment Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('898', 'Refrigeration Maintenance and Repairs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('899', 'Refrigerators and Freezers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('900', 'Refuse Disposal Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('901', 'Relocation Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('902', 'Removal, Packing and Storage Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('903', 'Research and Development Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('904', 'Resorts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('905', 'Restaurant Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('906', 'Restaurant Furniture', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('907', 'Restaurant Menu', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('908', 'Restaurants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('909', 'Restaurants - African', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('910', 'Restaurants - American', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('911', 'Restaurants - Arabian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('912', 'Restaurants - Argentinean', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('913', 'Restaurants - Asian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('914', 'Restaurants - Australian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('915', 'Restaurants - British', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('916', 'Restaurants - Chinese', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('917', 'Restaurants - Coffee Shops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('918', 'Restaurants - Continental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('919', 'Restaurants - Cuban', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('920', 'Restaurants - English', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('921', 'Restaurants - European', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('922', 'Restaurants - Far Eastern', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('923', 'Restaurants - Fast Food', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('924', 'Restaurants - Filipino', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('925', 'Restaurants - Fish and Chips', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('926', 'Restaurants - French', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('927', 'Restaurants - German', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('928', 'Restaurants - Greek', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('929', 'Restaurants - Health Food', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('930', 'Restaurants - Indian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('931', 'Restaurants - International', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('932', 'Restaurants - Iranian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('933', 'Restaurants - Irish', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('934', 'Restaurants - Italian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('935', 'Restaurants - Japanese', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('936', 'Restaurants - Korean', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('937', 'Restaurants - Latin American', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('938', 'Restaurants - Lebanese', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('939', 'Restaurants - Malaysian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('940', 'Restaurants - Mediterranean', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('941', 'Restaurants - Mexican', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('942', 'Restaurants - Moroccan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('943', 'Restaurants - Oriental', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('944', 'Restaurants - Pakistani', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('945', 'Restaurants - Polynesian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('946', 'Restaurants - Portuguese', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('947', 'Restaurants - Russian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('948', 'Restaurants - Scottish', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('949', 'Restaurants - Sea Food', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('950', 'Restaurants - Singaporean', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('951', 'Restaurants - Spanish', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('952', 'Restaurants - Sri Lankan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('953', 'Restaurants - Steak Houses', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('954', 'Restaurants - Thai', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('955', 'Restaurants - Turkish', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('956', 'Restaurants - Vegetarian', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('957', 'Restaurants - Vietnamese', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('958', 'Reverse Osmosis Units - Supply and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('959', 'Road Building Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('960', 'Road Building Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('961', 'Road Safety Equipment and Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('962', 'Rock Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('963', 'Roofing Materials - Whol and Mfrs and Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('964', 'Rope', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('965', 'Rope Access - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('966', 'Rubber and Rubber Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('967', 'Rubber Linings - Anti - Corrosive', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('968', 'Rubber Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('969', 'Rubber Moulded Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('970', 'Rubber Stamp Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('971', 'Rust Proofing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('972', 'Safes and Vaults - Commercial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('973', 'Safety Consultants and Training', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('974', 'Safety Equipment and Clothing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('975', 'Safety Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('976', 'Salons - Gents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('977', 'Salons - Gents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('978', 'Sandblasting - Commercial and Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('979', 'Sandblasting Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('980', 'Sanitaryware Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('981', 'Satellite Communication Eqpt and Systems Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('982', 'Satellite Receiving Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('983', 'Satellite Telephone Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('984', 'Sauna Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('985', 'Sauna, Steambath, SPA Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('986', 'Sawmills', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('987', 'Scaffolding and Shuttering Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('988', 'Scales and Scales Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('989', 'Scanning Services - Documents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('990', 'School Furniture and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('991', 'School Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('992', 'Schools', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('993', 'Schools - Colleges and Universities', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('994', 'Schools - Elementary and Secondary', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('995', 'Schools - Kindergarten and Primary', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('996', 'Schools - Language', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('997', 'Schools - Nursery', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('998', 'Schools - Special Needs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('999', 'Scrap Merchants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1000', 'Scrap Metals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1001', 'Screen Printing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1002', 'Seals - Hydraulic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1003', 'Seals - Mechanical', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1004', 'Seals - O - Ring', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1005', 'Seals - Security', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1006', 'Seating Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1007', 'Secretarial Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1008', 'Security Consultants - Protective', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1009', 'Security Control Equipment and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1010', 'Security Guard and Patrol Service - Residential', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1011', 'Security Guards and Patrol Services - Comm and Ind', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1012', 'Security Services and Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1013', 'Sewage Treatment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1014', 'Sewage Treatment Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1015', 'Sewage Treatment Plants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1016', 'Sewer Cleaning Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1017', 'Sewing Machines - Sales, Service and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1018', 'Sheet Metal Work', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1019', 'Sheet Metal Working Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1020', 'Shelving and Storage Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1021', 'Shelving - Comm and Ind', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1022', 'Ship Builders and Repairers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1023', 'Ship Chandlers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1024', 'Ship Charterers and Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1025', 'Ship Scrap', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1026', 'Ship Spares', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1027', 'Ship Stores Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1028', 'Ship, Yacht and Boat Brokers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1029', 'Shipping Companies and Agents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1030', 'Shipyards', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1031', 'Shoe Shops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1032', 'Shoe Shops', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1033', 'Shopfitters and Shopfitting Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1034', 'Shopping Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1035', 'Shutters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1036', 'Signmakers and Signwriters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1037', 'Signs - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1038', 'Silica Sand Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1039', 'Silicones', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1040', 'Skin Treatments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1041', 'Soaps and Detergents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1042', 'Soft Drink Mfrs, Bottlers, Canners and Distrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1043', 'Soil Investigation Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1044', 'Soil Testing Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1045', 'Solar Energy - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1046', 'Solar Film Protection', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1047', 'Solar Water Heating Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1048', 'Soldering Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1049', 'Space Frames Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1050', 'Spas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1051', 'Sports Clubs and Associations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1052', 'Sports Clubs and Associations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1053', 'Sports Court Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1054', 'Sports Flooring Equipment and Seating', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1055', 'Sports Goods Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1056', 'Sports Injury Clinics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1057', 'Springs - Coil, Flat Etc - Distributors and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1058', 'Sprockets', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1059', 'Stainless Steel and High Nickel Alloy Bars', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1060', 'Stainless Steel Fabricators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1061', 'Stainless Steel Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1062', 'Stainless Steel Stockists', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1063', 'Stamps - Rubber - Mfrs&quot; Eqpt and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1064', 'Stationery - Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1065', 'Stationery - Whol and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1066', 'Steam Specialties', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1067', 'Steel and Steel Fabricated Product Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1068', 'Steel Buildings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1069', 'Steel Erectors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1070', 'Steel Fabricators and Engineers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1071', 'Steel Manufacturers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1072', 'Steel Mills', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1073', 'Steel Pipe Work Fabricators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1074', 'Steel Profiled Sheeting Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1075', 'Steel Reinforcing - Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1076', 'Steel Stockholders, Distributors and Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1077', 'Stock and Bond Brokers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1078', 'Stone - Crushed', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1079', 'Stone - Natural', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1080', 'Storage - Self Storage', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1081', 'Storage - Tanks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1082', 'Structural Cabling Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1083', 'Stud Bolts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1084', 'Studios - Photo', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1085', 'Sugar Brokers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1086', 'Supermarket Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1087', 'Supermarkets', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1088', 'Supply Chain Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1089', 'Surveying Instruments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1090', 'Surveyors - Chartered', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1091', 'Surveyors - Hydrographic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1092', 'Surveyors - Independent', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1093', 'Surveyors - Insurance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1094', 'Surveyors - Land, Marine and Offshore', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1095', 'Surveyors - Quantity', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1096', 'Sweets - Arabic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1097', 'Swimming Pool Contractors, Installation and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1098', 'Swimming Pool Equipment Mfrs and Distrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1099', 'Swimwear', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1100', 'Switches - Electric', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1101', 'T-Shirt Mfrs, Distributors and Printers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1102', 'Tailors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1103', 'Tailors Trimmings and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1104', 'Tank Cleaning', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1105', 'Tank Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1106', 'Tanker Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1107', 'Tanks - Fibreglass, Plastic, Rubber Etc', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1108', 'Tapes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1109', 'Taxis', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1110', 'Tea Importers and Merchants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1111', 'Telecommunication and Data Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1112', 'Telecommunication Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1113', 'Telecommunication Eqpt, Systems and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1114', 'Telecommunication Network Products and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1115', 'Telecommunication Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1116', 'Telecommunications Organizations', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1117', 'Telecommunications Regulatory Authority', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1118', 'Telephone Answering Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1119', 'Telephone Equipment and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1120', 'Television Programme Producers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1121', 'Tensile Fabric Structures', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1122', 'Tent and Tents PVC Materials', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1123', 'Tents and Tarpaulins', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1124', 'Tents - Renting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1125', 'Testing and Measuring Instruments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1126', 'Textiles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1127', 'Theme Parks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1128', 'Thermaplastic Powder Coating - PVC ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1129', 'Tile - Ceramic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1130', 'Tile - Mosaic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1131', 'Tile - Non-Ceramic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1132', 'Timber Merchants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1133', 'Time Recorders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1134', 'Toilets - Cubicles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1135', 'Toilets - Portable', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1136', 'Tools - Cutting', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1137', 'Touch Screens', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1138', 'Tour Operators', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1139', 'Tourism Promotions Bureaus', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1140', 'Tourist Information', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1141', 'Towing - Automotive', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1142', 'Towing - Marine', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1143', 'Toys', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1144', 'Tracking Device', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1145', 'Trade Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1146', 'Trade Mark Registration', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1147', 'Traffic Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1148', 'Traffic Sign Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1149', 'Traffic Signalling Systems and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1150', 'Trailer Renting and Leasing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1151', 'Trailers - Equipment and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1152', 'Trailers - Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1153', 'Training Aids - Industrial and Educational', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1154', 'Training Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1155', 'Transformer Repair and Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1156', 'Transformers - Mfrs and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1157', 'Translators and Interpreters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1158', 'Translators Systems and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1159', 'Transport Companies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1160', 'Transport Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1161', 'Transportation Consultants', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1162', 'Transportation Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1163', 'Travel Agents', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1164', 'Trench Shoring Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1165', 'Trolleys', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1166', 'Trophies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1167', 'Truck Bodies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1168', 'Truck Dealers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1169', 'Truck Equipment and Parts', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1170', 'Truck Repairing and Servicing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1171', 'Tubes and Tube Fittings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1172', 'Tunnelling Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1173', 'Turbines', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1174', 'Typing Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1175', 'Tyre Dealers and Distributors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1176', 'Tyre Dealers - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1177', 'Tyre Repair', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1178', 'Tyre Repair - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1179', 'Tyre Retreaders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1180', 'Ultraviolet Disinfection', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1181', 'Umbrellas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1182', 'Underwear', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1183', 'Uniforms', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1184', 'Uninterruptible Power Supply', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1185', 'Universities', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1186', 'Upholsterers and Upholstry Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1187', 'Vacuum Cleaners - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1188', 'Vacuum Equipment and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1189', 'Vacuum Packaging Equipment and Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1190', 'Valet Parking Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1191', 'Valve Actuators - Electric', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1192', 'Valves', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1193', 'Valves Repairers and Testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1194', 'Vehicle Tracking Systems and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1195', 'Vehicles Testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1196', 'Vending Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1197', 'Vending Machines - Sales and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1198', 'Venetian Blinds', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1199', 'Ventilating Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1200', 'Veterinarians Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1201', 'Veterinary Clinics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1202', 'Veterinary Medicines', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1203', 'Vibrators - Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1204', 'Video - Production', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1205', 'Video Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1206', 'Video Centres', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1207', 'Video Conferencing Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1208', 'Wallpapers and Wallcoverings', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1209', 'Warehouses', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1210', 'Waste - Paper', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1211', 'Waste Containers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1212', 'Waste Handling Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1213', 'Waste Management and Environmental Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1214', 'Waste Management and Recycling Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1215', 'Waste Management - Medical', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1216', 'Waste Reduction and Disposal Eqpt and Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1217', 'Waste Water Treatment and Recycling Systems', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1218', 'Watches and Watches Repairing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1219', 'Water and Waste Water Rental Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1220', 'Water Central Filtration System', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1221', 'Water Companies - Bottled, Bulk Etc', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1222', 'Water Conservation Devices', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1223', 'Water Coolers and Treatment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1224', 'Water Coolers - Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1225', 'Water Heaters - Whol and Mfrs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1226', 'Water Jet Cutting Equipment and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1227', 'Water Meters', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1228', 'Water Parks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1229', 'Water Purification Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1230', 'Water Softening Equipment, Service and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1231', 'Water Supply', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1232', 'Water Treatment Chemicals', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1233', 'Water Treatment Equipment, Service and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1234', 'Water Well Drilling and Service and Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1235', 'Waterproofing Contractors', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1236', 'Waterproofing Materials', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1237', 'Web Application Developers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1238', 'Web Application Developers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1239', 'Web Designing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1240', 'Wedding Cards - Manufacturers and Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1241', 'Wedding Supplies and Services', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1242', 'Weighbridges', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1243', 'Weighing Equipment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1244', 'Welding Electrodes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1245', 'Welding Equipment and Supplies and Repairs', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1246', 'Welding Equipment - Hire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1247', 'Wheel Alignment, Frame and Axle Servicing - Automotive', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1248', 'Wheel Balancing Equipment Suppliers', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1249', 'Wheels', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1250', 'Wildlife Organisation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1251', 'Winches', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1252', 'Window Cleaning Equipment and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1253', 'Windows', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1254', 'Windows - uPVC', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1255', 'Wire and Wire Products', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1256', 'Wire Mesh - Woven', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1257', 'Wire Rope', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1258', 'Wireless Communication Equipment, Systems and Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1259', 'Woodworkers (See Carpenters and Joiners)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1260', 'Woodworking Machinery, Eqpt and Supplies', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1261', 'Wrought Iron Works', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1262', 'Yoga Instruction', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `categories` VALUES ('1264', 'Others', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `categories_companies`
-- ----------------------------
DROP TABLE IF EXISTS `categories_companies`;
CREATE TABLE `categories_companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categories_companies
-- ----------------------------

-- ----------------------------
-- Table structure for `category_company`
-- ----------------------------
DROP TABLE IF EXISTS `category_company`;
CREATE TABLE `category_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of category_company
-- ----------------------------
INSERT INTO `category_company` VALUES ('1', '9', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('2', '9', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('3', '9', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('4', '9', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('5', '9', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('6', '9', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('7', '9', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('8', '9', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('9', '9', '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('10', '9', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('11', '9', '15', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('12', '9', '16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('13', '9', '17', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('27', '10', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('28', '10', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('29', '11', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('30', '11', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('31', '11', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('32', '12', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('33', '12', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('34', '12', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('35', '13', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('36', '13', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('37', '13', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('38', '14', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('39', '14', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('40', '14', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('41', '15', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('42', '15', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('43', '15', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('44', '18', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('45', '18', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('46', '18', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('47', '19', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('48', '19', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('49', '19', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('50', '20', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('51', '20', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('52', '20', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('53', '20', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `category_company` VALUES ('54', '20', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `companies`
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `known_as` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `origin_country_id` int(11) NOT NULL,
  `licence_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `established` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_employees` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `industry_id` int(11) NOT NULL,
  `products` text COLLATE utf8_unicode_ci NOT NULL,
  `services` text COLLATE utf8_unicode_ci NOT NULL,
  `profile` text COLLATE utf8_unicode_ci NOT NULL,
  `subscription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  `location_x` double NOT NULL,
  `location_y` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', 'Abdullah & Sons', 'Abdullah LLC', 'Mr Abdullah', '', 'www.yahoo.com', '+008 667 233 23', '', '+008 667 233 24', '20', '39', 'None', '1990', '100', '10 Base Road', 'Yolumbo City', '65317', '5', 'Tiles, Polishing', 'Tiles, Polishing', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:06:02', '2013-12-19 10:06:02');
INSERT INTO `companies` VALUES ('2', 'Richardson Tiles', 'Richardson', 'Mr Rocardo Benedetti', '', 'www.yahoo.com', '+67 846 234', '', '+67 846 232', '22', '39', 'None', '1990', '165', '24 Jameel Street', 'Rolfenburg', '30901', '5', 'Papers, Recycling', 'Recycling Services', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:12:01', '2013-12-19 10:12:01');
INSERT INTO `companies` VALUES ('3', 'Khawar Zia Holdings', 'Khawar Co.', 'Mr Zia ur Rehman', '', 'www.ziaurrehma.tk', '+87 667 5651', '', '+87 667 5652', '20', '33', 'None', '1990', '43', '190 A - RIchardson Road', 'Pattersonville', '67822', '5', 'Curds', 'Food industrialists', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:14:26', '2013-12-19 10:14:26');
INSERT INTO `companies` VALUES ('4', 'Yanbu Publications', 'Yanbu Publications', 'Mr Yousuf Islam', '', 'www.yanbu.pub', '+23 367 1451', '', '+23 367 1453', '20', '22', 'None', '1990', '44', 'MArkinsons Palace, avenue 1245', 'Charleston', '86572', '5', 'Textiles, Mass Prints', 'Import and Exports', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:18:45', '2013-12-19 10:18:45');
INSERT INTO `companies` VALUES ('5', 'Rashid Automotives', 'Rashid Autos', 'Mr Rafiq Tejani', '', 'www.automotorsexpert.co', '+2300 1331 643', '', '+2300 1331 642', '20', '44', 'None', '1990', '50', 'Jeffry Street 120', 'Kiev', '22133', '5', 'Tiles, Polishing', 'Tiles, Polishing', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:20:21', '2013-12-19 10:20:21');
INSERT INTO `companies` VALUES ('6', 'United Diesel Purveyors', 'United Diesel', 'Mr. Yim Zuang', '', 'www.united-diesel.cn', '+2300 3245 789', '', '+2300 3245 781', '33', '12', 'None', '1990', '6', '7811 Lincoln Boulevard', 'Mongo', '24322', '5', 'Tiles, Polishing', 'Tiles, Polishing', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:21:04', '2013-12-19 10:21:04');
INSERT INTO `companies` VALUES ('7', 'Gulf Motors', 'Gulf Motors', 'Mr. Asad Abbas', '', 'www.gulf-motors.biz', '+2300 6752 100', '', '+2300 6752 102', '32', '39', 'None', '1990', '23', '678 Victoria Park', 'Rafulia', '44212', '5', 'Textiles', 'Distributors of textiles', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:21:44', '2013-12-19 10:21:44');
INSERT INTO `companies` VALUES ('8', 'Tahir Majeed Petrochemicals', 'Tahir Majid', 'Majid Abdullah', '', 'www.tahir-chem.com', '+2300 6122 121', '', '+2300 6122 124', '23', '39', 'None', '1990', '54', '871 Nicholas  Freid Street', 'Miningoria', '92821', '5', 'Books, Magazines', 'Mass publisters', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:22:16', '2013-12-19 10:22:16');
INSERT INTO `companies` VALUES ('9', 'GoodBooks Readings', 'GoodBooks', 'Miss Julie Sanders', '', 'www.goodbooks.co.uk', '+2110 8765 127', '', '+2110 8765 123', '32', '39', 'None', '1990', '67', '100 Muareed Boulevard', 'Orion City', '65212', '5', 'Fixtures', 'Retailers', 'hkjh', '', '0', '0', '0', '0', '2013-12-19 10:22:44', '2013-12-19 10:22:44');
INSERT INTO `companies` VALUES ('10', 'Kitchenettes and Co.', 'Kitchennettes & Co.', 'Mr. Charlie Dawkins', '', 'www.kitchensrus.com', '+3110 4677 910', '', '+3110 4677 912', '6', '16', 'None', '1990', '35', '1201 Pollocks Road', 'Muscat', '43218', '13', 'Publications', 'fsdfdsfsd', 'sdfsdfsfd', '', '0', '0', '6.914793656671707', '30.322265625', '2013-12-19 10:23:51', '2013-12-24 14:12:37');
INSERT INTO `companies` VALUES ('11', 'Mandrake', 'Madarin', '979798', '', '', '987987987987', '', '', '20', '0', 'Chamber of commerce', '', '0', 'uoiuoiuoi', 'uoiuoioui', '', '1', '', '', 'This is a profile of the person here', '', '0', '0', '0', '0', '2014-02-19 06:57:46', '2014-02-19 08:55:36');
INSERT INTO `companies` VALUES ('12', 'Blue Basics', 'Blue FZC', 'Mr Akmal', '', 'www.yahoo.com', '879798798798', '', '987798987987', '20', '0', 'Others', '1990', '100', 'Test City Road 666', 'Avenue Boulevards', '96555', '3', 'Products 1, and Products 2', '', '', '', '0', '0', '0', '0', '2014-02-19 09:49:32', '2014-02-19 09:49:32');
INSERT INTO `companies` VALUES ('13', 'Blue Basics', 'Blue FZC', 'Mr Akmal', '', 'www.yahoo.com', '879798798798', '', '987798987987', '20', '0', 'Others', '1990', '100', 'Test City Road 666', 'Avenue Boulevards', '96555', '3', 'Products 1, and Products 2', '', '', '', '0', '0', '0', '0', '2014-02-19 09:50:23', '2014-02-19 09:50:23');
INSERT INTO `companies` VALUES ('14', 'Blue Basics', 'Blue FZC', 'Mr Akmal', '', 'www.yahoo.com', '879798798798', '', '987798987987', '20', '0', 'Others', '1990', '100', 'Test City Road 666', 'Avenue Boulevards', '96555', '3', 'Products 1, and Products 2', '', '', '', '0', '0', '0', '0', '2014-02-19 10:01:41', '2014-02-19 10:01:41');
INSERT INTO `companies` VALUES ('15', 'Blue Basics', 'Blue FZC', 'Mr Akmal', '', 'www.yahoo.com', '879798798798', '', '987798987987', '20', '0', 'Others', '1990', '100', 'Test City Road 666', 'Avenue Boulevards', '96555', '3', 'Products 1, and Products 2', '', '', '', '0', '0', '0', '0', '2014-02-19 10:02:15', '2014-02-19 10:02:15');
INSERT INTO `companies` VALUES ('16', 'Mandrake Man', 'Blue FZC', 'Mr Abdullah', '', 'admin.com', '897987987', '', '987987987', '21', '0', 'Department of Economic Development', '1990', '100', 'Test', 'City Boulevard', '98798', '3', 'Test 1 Test 2 Test 2', 'Working 2 3 a', '', '', '0', '0', '0', '0', '2014-02-19 10:49:09', '2014-02-19 10:49:09');
INSERT INTO `companies` VALUES ('17', 'Mandrake Man', 'Blue FZC', 'Mr Abdullah', '', 'admin.com', '897987987', '', '987987987', '21', '0', 'Department of Economic Development', '1990', '100', 'Test', 'City Boulevard', '98798', '3', 'Test 1 Test 2 Test 2', 'Working 2 3 a', '', '', '0', '0', '0', '0', '2014-02-19 10:50:11', '2014-02-19 10:50:11');
INSERT INTO `companies` VALUES ('18', 'Mandrake Man', 'Blue FZC', 'Mr Abdullah', '', 'admin.com', '897987987', '', '987987987', '21', '0', 'None', '1990', '100', 'Test', 'City Boulevard2', '98798', '3', 'Test 1 Test 2 Test 2', 'Working 2 3 a', '', '', '0', '0', '0', '0', '2014-02-19 10:50:40', '2014-02-20 11:31:49');
INSERT INTO `companies` VALUES ('19', 'Blue Basics', 'Blue FZC', 'Mr Blueman', '', '', '87979809', '', '', '218', '0', 'Department of Economic Development', '1990', '8', 'Test City', 'Dubai', '8999', '2', '', '', '', '', '0', '0', '0', '0', '2014-02-23 05:17:15', '2014-02-23 05:17:15');
INSERT INTO `companies` VALUES ('20', 'Grand Company', 'grando', 'Mr Blueman', '', '', '13232123234432', '', '', '14', '0', 'Chamber of commerce', '1990', '8', 'test city', 'Avenue Boulevards', '', '1', 'testing', 'stuff', '', '', '0', '0', '0', '0', '2014-03-02 06:17:57', '2014-03-02 06:17:57');

-- ----------------------------
-- Table structure for `company_interviews`
-- ----------------------------
DROP TABLE IF EXISTS `company_interviews`;
CREATE TABLE `company_interviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of company_interviews
-- ----------------------------
INSERT INTO `company_interviews` VALUES ('1', 'Dh2.59bn flats and hotel deal for Reem Island', '18', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', '', '0000-00-00', '0', '2014-02-20 07:45:06', '2014-02-20 07:45:06');

-- ----------------------------
-- Table structure for `company_reviews`
-- ----------------------------
DROP TABLE IF EXISTS `company_reviews`;
CREATE TABLE `company_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of company_reviews
-- ----------------------------

-- ----------------------------
-- Table structure for `complaints`
-- ----------------------------
DROP TABLE IF EXISTS `complaints`;
CREATE TABLE `complaints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `complaint_against` int(11) NOT NULL,
  `compaint_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount_in_dispute` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `problem_date` date NOT NULL,
  `complaint_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `complain_details` text COLLATE utf8_unicode_ci NOT NULL,
  `expected_resolution` text COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of complaints
-- ----------------------------

-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'Afghanistan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('2', 'Albania', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('3', 'Algeria', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('4', 'American Samoa', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('5', 'Andorra', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('6', 'Angola', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('7', 'Anguilla', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('8', 'Antarctica', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('9', 'Antigua and Barbuda', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('10', 'Argentina', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('11', 'Armenia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('12', 'Aruba', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('13', 'Australia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('14', 'Austria', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('15', 'Azerbaijan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('16', 'Bahamas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('17', 'Bahrain', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('18', 'Bangladesh', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('19', 'Barbados', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('20', 'Belarus', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('21', 'Belgium', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('22', 'Belize', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('23', 'Benin', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('24', 'Bermuda', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('25', 'Bhutan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('26', 'Bolivia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('27', 'Bosnia and Herzegovina', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('28', 'Botswana', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('29', 'Bouvet Island', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('30', 'Brazil', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('31', 'British Indian Ocean Territory', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('32', 'Brunei Darussalam', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('33', 'Bulgaria', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('34', 'Burkina Faso', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('35', 'Burundi', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('36', 'Cambodia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('37', 'Cameroon', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('38', 'Canada', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('39', 'Cape Verde', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('40', 'Cayman Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('41', 'Central African Republic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('42', 'Chad', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('43', 'Chile', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('44', 'China', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('45', 'Christmas Island', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('46', 'Cocos Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('47', 'Colombia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('48', 'Comoros', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('49', 'Congo', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('51', 'Cook Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('52', 'Costa Rica', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('53', 'Cote d\'Ivoire', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('54', 'Croatia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('55', 'Cuba', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('56', 'Cyprus', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('57', 'Czech Republic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('58', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('59', 'Djibouti', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('60', 'Dominica', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('61', 'Dominican Republic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('62', 'Ecuador', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('63', 'Egypt', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('64', 'El Salvador', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('65', 'Equatorial Guinea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('66', 'Eritrea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('67', 'Estonia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('68', 'Ethiopia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('69', 'Falkland Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('70', 'Faroe Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('71', 'Fiji', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('72', 'Finland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('73', 'France', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('74', 'French Guiana', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('75', 'French Polynesia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('76', 'Gabon', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('77', 'Gambia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('78', 'Georgia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('79', 'Germany', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('80', 'Ghana', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('81', 'Gibraltar', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('82', 'Greece', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('83', 'Greenland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('84', 'Grenada', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('85', 'Guadeloupe', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('86', 'Guam', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('87', 'Guatemala', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('88', 'Guinea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('89', 'Guinea-Bissau', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('90', 'Guyana', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('91', 'Haiti', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('93', 'Honduras', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('94', 'Hong Kong', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('95', 'Hungary', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('96', 'Iceland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('97', 'India', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('98', 'Indonesia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('99', 'Iran', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('100', 'Iraq', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('101', 'Ireland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('102', 'Italy', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('103', 'Jamaica', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('104', 'Japan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('105', 'Jordan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('106', 'Kazakhstan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('107', 'Kenya', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('108', 'Kiribati', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('109', 'Kuwait', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('110', 'Kyrgyzstan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('111', 'Laos', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('112', 'Latvia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('113', 'Lebanon', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('114', 'Lesotho', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('115', 'Liberia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('116', 'Libya', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('117', 'Liechtenstein', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('118', 'Lithuania', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('119', 'Luxembourg', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('120', 'Macao', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('121', 'Madagascar', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('122', 'Malawi', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('123', 'Malaysia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('124', 'Maldives', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('125', 'Mali', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('126', 'Malta', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('127', 'Marshall Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('128', 'Martinique', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('129', 'Mauritania', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('130', 'Mauritius', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('131', 'Mayotte', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('132', 'Mexico', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('133', 'Micronesia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('134', 'Moldova', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('135', 'Monaco', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('136', 'Mongolia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('137', 'Montenegro', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('138', 'Montserrat', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('139', 'Morocco', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('140', 'Mozambique', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('141', 'Myanmar', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('142', 'Namibia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('143', 'Nauru', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('144', 'Nepal', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('145', 'Netherlands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('146', 'Netherlands Antilles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('147', 'New Caledonia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('148', 'New Zealand', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('149', 'Nicaragua', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('150', 'Niger', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('151', 'Nigeria', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('152', 'Norfolk Island', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('153', 'North Korea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('154', 'Norway', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('155', 'Oman', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('156', 'Pakistan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('157', 'Palau', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('158', 'Palestine', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('159', 'Panama', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('160', 'Papua New Guinea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('161', 'Paraguay', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('162', 'Peru', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('163', 'Philippines', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('164', 'Pitcairn', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('165', 'Poland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('166', 'Portugal', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('167', 'Puerto Rico', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('168', 'Qatar', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('169', 'Romania', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('170', 'Russian Federation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('171', 'Rwanda', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('172', 'Saint Helena', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('173', 'Saint Kitts and Nevis', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('174', 'Saint Lucia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('175', 'Saint Pierre and Miquelon', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('177', 'Samoa', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('178', 'San Marino', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('179', 'Sao Tome and Principe', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('180', 'Saudi Arabia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('181', 'Senegal', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('182', 'Serbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('183', 'Seychelles', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('184', 'Sierra Leone', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('185', 'Singapore', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('186', 'Slovakia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('187', 'Slovenia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('188', 'Solomon Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('189', 'Somalia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('190', 'South Africa', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('191', 'South Georgia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('192', 'South Korea', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('193', 'Spain', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('194', 'Sri Lanka', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('195', 'Sudan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('196', 'Suriname', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('197', 'Svalbard and Jan Mayen', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('198', 'Swaziland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('199', 'Sweden', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('200', 'Switzerland', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('201', 'Syrian Arab Republic', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('202', 'Taiwan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('203', 'Tajikistan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('204', 'Tanzania', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('205', 'Thailand', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('206', 'Republic of Macedonia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('207', 'Timor-Leste', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('208', 'Togo', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('209', 'Tokelau', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('210', 'Tonga', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('211', 'Trinidad and Tobago', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('212', 'Tunisia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('213', 'Turkey', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('214', 'Turkmenistan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('215', 'Tuvalu', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('216', 'Uganda', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('217', 'Ukraine', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('218', 'United Arab Emirates', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('219', 'United Kingdom', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('220', 'United States', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('222', 'Uruguay', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('223', 'Uzbekistan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('224', 'Vanuatu', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('225', 'Vatican City', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('226', 'Venezuela', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('227', 'Vietnam', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('228', 'Virgin Islands, British', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('229', 'Virgin Islands, U.S.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('230', 'Wallis and Futuna', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('231', 'Western Sahara', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('232', 'Yemen', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('233', 'Zambia', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `countries` VALUES ('234', 'Zimbabwe', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `deal_sections`
-- ----------------------------
DROP TABLE IF EXISTS `deal_sections`;
CREATE TABLE `deal_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of deal_sections
-- ----------------------------

-- ----------------------------
-- Table structure for `ideas`
-- ----------------------------
DROP TABLE IF EXISTS `ideas`;
CREATE TABLE `ideas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `sort` int(11) NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ideas
-- ----------------------------
INSERT INTO `ideas` VALUES ('3', 'Dh2.59bn flats and hotel deal for Reem Island', '<p>The concept is quite a simple one: the app takes in sounds from the user&rsquo;s surroundings using the iPhone&rsquo;s built-in microphone, amplifies it using algorithms tailored to the user, and plays it back through either the loudspeaker or headphones in real-time. Users can configure the app using one of six settings to best suit their condition, which can then be fine tuned even further. Professor Ray Meddis &ndash; emeritus professor of psychology at the University of Essex, who is part of the team at BioAid &ndash; explains more about the project in the video.</p>\r\n\r\n<p>While it&rsquo;s true that the system currently requires the hard of hearing to keep their iPhones on them as well as use headphones, the team hope that &ndash; with advances such as smaller Bluetooth-enabled headphones and smart watches &ndash; that an app such as BioAid could become a feasible and inexpensive way to improve the quality of life of deaf people. For now, a beta version of the app is available for free on the App Store. Investors &ndash; one to get in on early? - See more at: http://one1info.com/demo_2/ideas-App-turns-iPhone-into-a-hearing-aid-91#sthash.yb96zIuw.dpuf</p>\r\n', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', 'This is a title for the media', '0000-00-00', '2', '1', '2014-02-13 10:58:36', '2014-02-13 10:58:36');
INSERT INTO `ideas` VALUES ('4', 'Dh2.59bn flats and hotel deal for Reem Island', '<p>While it&rsquo;s true that the system currently requires the hard of hearing to keep their iPhones on them as well as use headphones, the team hope that &ndash; with advances such as smaller Bluetooth-enabled headphones and smart watches &ndash; that an app such as BioAid could become a feasible and inexpensive way to improve the quality of life of deaf people. For now, a beta version of the app is available for free on the App Store. Investors &ndash; one to get in on early? - See more at: http://one1info.com/demo_2/ideas-App-turns-iPhone-into-a-hearing-aid-91#sthash.yb96zIuw.dpufWhile it&rsquo;s true that the system currently requires the hard of hearing to keep their iPhones on them as well as use headphones, the team hope that &ndash; with advances such as smaller Bluetooth-enabled headphones and smart watches &ndash; that an app such as BioAid could become a feasible and inexpensive way to improve the quality of life of deaf people. For now, a beta version of the app is available for free on the App Store. Investors &ndash; one to get in on early? - See more at: http://one1info.com/demo_2/ideas-App-turns-iPhone-into-a-hearing-aid-91#sthash.yb96zIuw.dpuf</p>\r\n', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', 'This is a title for the media', '0000-00-00', '1', '1', '2014-02-13 10:59:54', '2014-02-13 10:59:54');
INSERT INTO `ideas` VALUES ('5', 'Arabtec wins Dh2.59bn flats and hotel deal for Reem Island’s Stonehenge-style development', '<p>While it&rsquo;s true that the system currently requires the hard of hearing to keep their iPhones on them as well as use headphones, the team hope that &ndash; with advances such as smaller Bluetooth-enabled headphones and smart watches &ndash; that an app such as BioAid could become a feasible and inexpensive way to improve the quality of life of deaf people. For now, a beta version of the app is available for free on the App Store. Investors &ndash; one to get in on early? - See more at: http://one1info.com/demo_2/ideas-App-turns-iPhone-into-a-hearing-aid-91#sthash.yb96zIuw.dpufWhile it&rsquo;s true that the system currently requires the hard of hearing to keep their iPhones on them as well as use headphones, the team hope that &ndash; with advances such as smaller Bluetooth-enabled headphones and smart watches &ndash; that an app such as BioAid could become a feasible and inexpensive way to improve the quality of life of deaf people. For now, a beta version of the app is available for free on the App Store. Investors &ndash; one to get in on early? - See more at: http://one1info.com/demo_2/ideas-App-turns-iPhone-into-a-hearing-aid-91#sthash.yb96zIuw.dpuf</p>\r\n', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', 'This is a title for the media', '0000-00-00', '1', '1', '2014-02-13 11:00:21', '2014-02-13 11:00:21');

-- ----------------------------
-- Table structure for `industries`
-- ----------------------------
DROP TABLE IF EXISTS `industries`;
CREATE TABLE `industries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of industries
-- ----------------------------
INSERT INTO `industries` VALUES ('1', 'Agriculture and Food', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('2', 'Automotive & Marine', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('3', 'Aviation & Airlines', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('4', 'Economy, Financial & Banking', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('5', 'Education', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('6', 'Energy, Oil & Gas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('7', 'Government', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('8', 'Healthcare & Social', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('9', 'Hospitality & Tourism', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('10', 'Industry', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('11', 'Insurance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('12', 'Media', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('13', 'Real Estate & Construction', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('14', 'Services & Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('15', 'Telecoms & IT', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `industries` VALUES ('16', 'Transport & Logistics', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `education` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commitment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `compensation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `show_company` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('1', '10', 'AL Nakheel', 'Ras Al Khaimah', 'Ras Al Khaimah', '218', 'Web Developer ', 'IT/Telecom', '1 - 2 years', 'Bachelors degree', 'Full time', 'AED 4500', 'Need Web developer to work on website in Php, MySQL and javascript. Must have prior experience in working with fully functional websites and collaboration systems.', '1', '1', '2013-12-29 07:29:04', '2013-12-29 07:29:04');

-- ----------------------------
-- Table structure for `marques`
-- ----------------------------
DROP TABLE IF EXISTS `marques`;
CREATE TABLE `marques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of marques
-- ----------------------------

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profession` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `interests` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` int(11) NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', 'Mohammad Ali', 'Sultan', '', 'female', '', 'jhkj 876786', 'GAHS', '3', '0000-00-00', 'Mr', '', '', '2013-12-19 08:52:22', '2013-12-19 08:52:22', '8', '897987987', '0', 'college');
INSERT INTO `members` VALUES ('2', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:10:47', '2014-02-24 07:10:47', '34', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('3', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:11:29', '2014-02-24 07:11:29', '35', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('4', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:11:56', '2014-02-24 07:11:56', '36', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('5', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:12:22', '2014-02-24 07:12:22', '37', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('6', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:14:16', '2014-02-24 07:14:16', '38', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('7', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:15:54', '2014-02-24 07:15:54', '39', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('8', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:22:35', '2014-02-24 07:22:35', '40', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('9', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:29:07', '2014-02-24 07:29:07', '41', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('10', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:57:14', '2014-02-24 07:57:14', '42', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('11', 'Mohammad Ali', 'AKmal', '', 'male', '', 'jhkj 876786', 'DS', '18', '0000-00-00', 'Mr', '', '', '2014-02-24 07:58:23', '2014-02-24 07:58:23', '43', '7788667877', '0', 'college');
INSERT INTO `members` VALUES ('12', 'Mohammad Ali', 'Sultan2', '', 'male', '', 'test', 'DS', '3', '0000-00-00', 'Mr', '', '', '2014-02-24 11:08:39', '2014-02-25 06:36:30', '44', '87979809', '1212', 'professional');

-- ----------------------------
-- Table structure for `membership_options`
-- ----------------------------
DROP TABLE IF EXISTS `membership_options`;
CREATE TABLE `membership_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `membership_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of membership_options
-- ----------------------------
INSERT INTO `membership_options` VALUES ('1', 'Listed Members', 'a:24:{s:5:\"price\";s:3:\"100\";s:11:\"user_access\";s:1:\"1\";s:9:\"telephone\";s:1:\"1\";s:6:\"mobile\";s:1:\"1\";s:3:\"fax\";s:1:\"1\";s:5:\"pobox\";s:1:\"1\";s:7:\"website\";s:1:\"0\";s:14:\"contact_person\";s:1:\"1\";s:17:\"business_activity\";s:1:\"0\";s:12:\"license_type\";s:1:\"0\";s:17:\"established_since\";s:1:\"1\";s:16:\"num_of_employees\";s:1:\"1\";s:12:\"location_map\";s:1:\"1\";s:7:\"profile\";s:1:\"0\";s:5:\"media\";s:1:\"5\";s:14:\"press_releases\";s:1:\"5\";s:16:\"daily_newsletter\";s:1:\"1\";s:17:\"weekly_newsletter\";s:1:\"0\";s:10:\"ads_banner\";s:1:\"3\";s:12:\"match_making\";s:1:\"1\";s:10:\"red_carpet\";s:1:\"1\";s:18:\"corporate_services\";s:1:\"0\";s:9:\"post_jobs\";s:1:\"1\";s:17:\"patent_investment\";s:1:\"0\";}', '0000-00-00 00:00:00', '2014-03-06 12:52:32');
INSERT INTO `membership_options` VALUES ('2', 'Registered Members', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2013_10_28_162316_create_password_reminders_table', '1');
INSERT INTO `migrations` VALUES ('2013_02_15_193634_firalabs_firadmin_create_users_table', '2');
INSERT INTO `migrations` VALUES ('2013_02_27_125051_firalabs_firadmin_create_users_roles_table', '2');
INSERT INTO `migrations` VALUES ('2013_10_28_165659_create_industries_table', '3');
INSERT INTO `migrations` VALUES ('2013_10_28_171735_create_industries_table', '4');
INSERT INTO `migrations` VALUES ('2013_10_28_173430_create_countries_table', '4');
INSERT INTO `migrations` VALUES ('2013_10_28_174620_create_categories_table', '5');
INSERT INTO `migrations` VALUES ('2013_11_10_180149_create_members_table', '6');
INSERT INTO `migrations` VALUES ('2013_11_10_183700_create_corporate_services_table', '7');
INSERT INTO `migrations` VALUES ('2013_11_11_155600_create_companies_table', '8');
INSERT INTO `migrations` VALUES ('2013_11_11_161053_create_category_company', '8');
INSERT INTO `migrations` VALUES ('2013_11_30_114436_create_articles_table', '9');
INSERT INTO `migrations` VALUES ('2013_11_30_114727_create_videos_table', '9');
INSERT INTO `migrations` VALUES ('2013_12_05_180233_create_sections_table', '9');
INSERT INTO `migrations` VALUES ('2013_12_05_180414_add_section_to_articles', '9');
INSERT INTO `migrations` VALUES ('2013_12_05_193458_create_photos_table', '9');
INSERT INTO `migrations` VALUES ('2013_12_09_165151_add_imageable_field_to_photo', '10');
INSERT INTO `migrations` VALUES ('2013_12_16_132254_add_section_id_to_videos', '11');
INSERT INTO `migrations` VALUES ('2013_12_19_063516_add_fields_to_user', '12');
INSERT INTO `migrations` VALUES ('2013_12_19_084922_add_more_fields_to_user', '13');
INSERT INTO `migrations` VALUES ('2013_12_19_085025_add_new_fields_to_user', '14');
INSERT INTO `migrations` VALUES ('2013_12_19_100524_add_user_id_to_company', '15');
INSERT INTO `migrations` VALUES ('2013_12_19_101515_create_category_company_table_only', '16');
INSERT INTO `migrations` VALUES ('2013_12_29_063259_create_jobs_table', '17');
INSERT INTO `migrations` VALUES ('2014_02_09_064705_add_type_to_articles', '18');
INSERT INTO `migrations` VALUES ('2014_02_09_074659_create_pages_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_075749_create_press_releases_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_080020_create_company_interviews_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_080153_create_company_reviews_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_080916_create_complaints_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_081042_create_deal_sections_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_081424_create_patents_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_081718_create_patent_investments_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_09_085838_create_ideas_table', '19');
INSERT INTO `migrations` VALUES ('2014_02_19_093640_add_rmove_company_ids', '20');
INSERT INTO `migrations` VALUES ('2014_02_19_112846_add_levels_to_users', '21');
INSERT INTO `migrations` VALUES ('2014_02_23_130215_add_notifications_active_touser', '22');
INSERT INTO `migrations` VALUES ('2014_03_04_111012_add_fields_to_sections', '23');
INSERT INTO `migrations` VALUES ('2014_03_06_110754_create_membership_options_table', '24');
INSERT INTO `migrations` VALUES ('2014_03_06_132841_add_fields_to_services', '25');
INSERT INTO `migrations` VALUES ('2014_03_09_111147_add_published_to_services', '26');
INSERT INTO `migrations` VALUES ('2014_03_09_130310_create_ad_settings_table', '27');
INSERT INTO `migrations` VALUES ('2014_03_09_132402_create_ad_banners_table', '28');
INSERT INTO `migrations` VALUES ('2014_03_10_084027_create_red_carpets_table', '29');
INSERT INTO `migrations` VALUES ('2014_03_10_104114_create_marques_table', '30');
INSERT INTO `migrations` VALUES ('2014_03_10_105742_create_transactions_table', '31');
INSERT INTO `migrations` VALUES ('2014_03_10_111414_create_video_sections_table', '32');
INSERT INTO `migrations` VALUES ('2014_03_10_132656_create_auto_emails_table', '33');
INSERT INTO `migrations` VALUES ('2014_03_11_054455_create_privileges_table', '34');

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pages
-- ----------------------------

-- ----------------------------
-- Table structure for `password_reminders`
-- ----------------------------
DROP TABLE IF EXISTS `password_reminders`;
CREATE TABLE `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_reminders
-- ----------------------------

-- ----------------------------
-- Table structure for `patent_investments`
-- ----------------------------
DROP TABLE IF EXISTS `patent_investments`;
CREATE TABLE `patent_investments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patent_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `applicant_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applicant_position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `why_invest` text COLLATE utf8_unicode_ci NOT NULL,
  `investment_benefits` text COLLATE utf8_unicode_ci NOT NULL,
  `has_business_same_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `need_meeting` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of patent_investments
-- ----------------------------

-- ----------------------------
-- Table structure for `patents`
-- ----------------------------
DROP TABLE IF EXISTS `patents`;
CREATE TABLE `patents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inv_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inv_details` text COLLATE utf8_unicode_ci NOT NULL,
  `has_patent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `need_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patent_area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `need_publish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `need_investor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of patents
-- ----------------------------

-- ----------------------------
-- Table structure for `photos`
-- ----------------------------
DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `imageable_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `imageable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of photos
-- ----------------------------
INSERT INTO `photos` VALUES ('58', '', '', '0', '2013-12-09 18:52:17', '2013-12-09 18:52:17', '', '0');
INSERT INTO `photos` VALUES ('59', '', '', '0', '2013-12-09 18:52:17', '2013-12-09 18:52:17', '', '0');
INSERT INTO `photos` VALUES ('60', '', '', '0', '2013-12-09 18:53:43', '2013-12-09 18:53:43', '', '0');
INSERT INTO `photos` VALUES ('61', '', '', '0', '2013-12-09 18:53:43', '2013-12-09 18:53:43', '', '0');
INSERT INTO `photos` VALUES ('62', '', '', '0', '2013-12-09 18:55:07', '2013-12-09 18:55:07', '', '0');
INSERT INTO `photos` VALUES ('63', '', '', '0', '2013-12-09 18:55:07', '2013-12-09 18:55:07', '', '0');
INSERT INTO `photos` VALUES ('64', '', '', '0', '2013-12-09 18:58:02', '2013-12-09 18:58:02', '', '0');
INSERT INTO `photos` VALUES ('65', '', '', '0', '2013-12-09 18:59:13', '2013-12-09 18:59:13', '', '0');
INSERT INTO `photos` VALUES ('66', '', '', '0', '2013-12-09 19:00:17', '2013-12-09 19:00:17', '', '0');
INSERT INTO `photos` VALUES ('67', '', '', '0', '2013-12-09 19:03:08', '2013-12-09 19:03:08', '', '0');
INSERT INTO `photos` VALUES ('68', '', '', '0', '2013-12-09 19:07:39', '2013-12-09 19:07:39', '', '0');
INSERT INTO `photos` VALUES ('69', '', '', '0', '2013-12-09 19:08:01', '2013-12-09 19:08:01', '', '0');
INSERT INTO `photos` VALUES ('70', 'Bridal-Dress-For-Mehndi-Function-8.jpg', '', '83480', '2013-12-09 19:08:49', '2013-12-09 19:08:49', 'Article', '85');
INSERT INTO `photos` VALUES ('71', 'Bridal-Dress-For-Mehndi-Function-8.jpg', '', '83480', '2013-12-09 19:08:55', '2013-12-09 19:08:55', 'Article', '86');
INSERT INTO `photos` VALUES ('72', 'Bridal-Dress-For-Mehndi-Function-8.jpg', '', '83480', '2013-12-09 19:09:07', '2013-12-09 19:09:07', 'Article', '87');
INSERT INTO `photos` VALUES ('73', '1.jpg', '', '82838', '2013-12-10 18:02:12', '2013-12-10 18:02:12', 'Article', '88');
INSERT INTO `photos` VALUES ('74', '1.jpg', '', '82838', '2013-12-10 18:03:20', '2013-12-10 18:03:20', 'Article', '89');
INSERT INTO `photos` VALUES ('75', '3.jpg', '', '60458', '2013-12-10 18:04:35', '2013-12-10 18:04:35', 'Article', '90');
INSERT INTO `photos` VALUES ('76', '1.jpg', '', '82838', '2013-12-10 18:05:14', '2013-12-10 18:05:15', 'Article', '91');
INSERT INTO `photos` VALUES ('77', '2.jpg', '', '90408', '2013-12-10 18:06:12', '2013-12-10 18:06:12', 'Article', '92');
INSERT INTO `photos` VALUES ('78', '4.jpg', '', '82716', '2013-12-10 18:07:12', '2013-12-10 18:07:12', 'Article', '93');
INSERT INTO `photos` VALUES ('79', '1.jpg', '', '82838', '2013-12-10 18:07:48', '2013-12-10 18:07:48', 'Article', '94');
INSERT INTO `photos` VALUES ('80', '3.jpg', '', '60458', '2013-12-10 18:08:43', '2013-12-10 18:08:43', 'Article', '95');
INSERT INTO `photos` VALUES ('81', '2.jpg', '', '90408', '2013-12-10 18:09:50', '2013-12-10 18:09:50', 'Article', '96');
INSERT INTO `photos` VALUES ('82', '5.jpg', '', '55188', '2013-12-10 18:10:54', '2013-12-10 18:10:54', 'Article', '97');
INSERT INTO `photos` VALUES ('83', '6.jpg', '', '55486', '2013-12-10 18:11:46', '2013-12-10 18:11:47', 'Article', '98');
INSERT INTO `photos` VALUES ('84', '7.jpg', '', '48365', '2013-12-10 18:12:44', '2013-12-10 18:12:44', 'Article', '99');
INSERT INTO `photos` VALUES ('85', '8.jpg', '', '34620', '2013-12-10 18:13:36', '2013-12-10 18:13:36', 'Article', '101');
INSERT INTO `photos` VALUES ('86', '9.jpg', '', '44033', '2013-12-10 18:14:46', '2013-12-10 18:14:46', 'Article', '102');
INSERT INTO `photos` VALUES ('87', '11.jpg', '', '39683', '2013-12-10 18:16:10', '2013-12-10 18:16:10', 'Article', '103');
INSERT INTO `photos` VALUES ('88', '12.jpg', '', '43288', '2013-12-10 18:17:00', '2013-12-10 18:17:00', 'Article', '104');
INSERT INTO `photos` VALUES ('89', '13.jpg', '', '48679', '2013-12-10 18:17:59', '2013-12-10 18:17:59', 'Article', '105');
INSERT INTO `photos` VALUES ('90', '14.jpg', '', '50987', '2013-12-10 18:18:36', '2013-12-10 18:18:36', 'Article', '106');
INSERT INTO `photos` VALUES ('91', '9.jpg', '', '44033', '2013-12-10 18:19:22', '2013-12-10 18:19:22', 'Article', '107');
INSERT INTO `photos` VALUES ('92', '13.jpg', '', '48679', '2013-12-10 18:20:23', '2013-12-10 18:20:23', 'Article', '108');
INSERT INTO `photos` VALUES ('93', '3.jpg', '', '60458', '2013-12-10 18:21:18', '2013-12-10 18:21:18', 'Article', '109');
INSERT INTO `photos` VALUES ('103', 'Koala.jpg', 'Logo for Mandrake', '780831', '2013-12-24 13:35:26', '2013-12-24 13:35:26', 'Company', '10');
INSERT INTO `photos` VALUES ('104', '1050948617.gif', '', '197644', '2014-01-14 14:14:12', '2014-01-14 14:14:12', 'Article', '110');
INSERT INTO `photos` VALUES ('105', '139.jpg', '', '178727', '2014-01-14 14:18:19', '2014-01-14 14:18:19', 'Article', '111');
INSERT INTO `photos` VALUES ('106', 'Dubai-Marina.jpg', '', '169425', '2014-01-14 14:21:17', '2014-01-14 14:21:17', 'Article', '112');
INSERT INTO `photos` VALUES ('107', '1073190-8-wide.jpg', '', '98783', '2014-01-14 14:25:55', '2014-01-14 14:25:55', 'Article', '113');
INSERT INTO `photos` VALUES ('108', '70914036.JPG', '', '24756', '2014-01-14 14:27:04', '2014-01-14 14:27:04', 'Article', '114');
INSERT INTO `photos` VALUES ('109', 'Dubai-Marina.jpg', '', '169425', '2014-01-14 14:27:38', '2014-01-14 14:27:39', 'Article', '115');
INSERT INTO `photos` VALUES ('110', '1073190-8-wide.jpg', '', '98783', '2014-01-14 14:28:03', '2014-01-14 14:28:03', 'Article', '116');
INSERT INTO `photos` VALUES ('111', '139.jpg', '', '178727', '2014-01-14 14:29:16', '2014-01-14 14:29:16', 'Article', '117');
INSERT INTO `photos` VALUES ('112', '1073190-8-wide.jpg', '', '98783', '2014-01-14 14:29:32', '2014-01-14 14:29:32', 'Article', '118');
INSERT INTO `photos` VALUES ('113', '163.jpg', '', '139081', '2014-01-14 14:31:07', '2014-01-14 14:31:07', 'Article', '119');
INSERT INTO `photos` VALUES ('114', '139.jpg', '', '178727', '2014-01-14 14:31:32', '2014-01-14 14:31:32', 'Article', '120');
INSERT INTO `photos` VALUES ('115', '115.jpg', '', '910795', '2014-01-14 14:32:56', '2014-01-14 14:32:56', 'Article', '121');
INSERT INTO `photos` VALUES ('116', 'dubai-hot-air-balloon-flight-in-dubai-143592.jpg', '', '18527', '2014-01-14 14:33:34', '2014-01-14 14:33:34', 'Article', '122');
INSERT INTO `photos` VALUES ('117', '115.jpg', '', '910795', '2014-01-14 14:34:16', '2014-01-14 14:34:16', 'Article', '123');
INSERT INTO `photos` VALUES ('119', '70914036.JPG', '', '24756', '2014-01-14 14:47:59', '2014-01-14 14:47:59', 'Article', '125');
INSERT INTO `photos` VALUES ('120', '139.jpg', '', '178727', '2014-01-14 14:48:17', '2014-01-14 14:48:17', 'Article', '126');
INSERT INTO `photos` VALUES ('121', 'dubai-hot-air-balloon-flight-in-dubai-143592.jpg', '', '18527', '2014-01-14 14:48:27', '2014-01-14 14:48:27', 'Article', '127');
INSERT INTO `photos` VALUES ('122', '115.jpg', '', '910795', '2014-01-14 14:49:18', '2014-01-14 14:49:18', 'Article', '128');
INSERT INTO `photos` VALUES ('123', '163.jpg', '', '139081', '2014-01-14 14:49:36', '2014-01-14 14:49:36', 'Article', '129');
INSERT INTO `photos` VALUES ('124', 'Dubai-Marina.jpg', '', '169425', '2014-01-14 14:50:04', '2014-01-14 14:50:04', 'Article', '130');
INSERT INTO `photos` VALUES ('125', '139.jpg', '', '178727', '2014-01-14 14:50:18', '2014-01-14 14:50:18', 'Article', '131');
INSERT INTO `photos` VALUES ('126', '1073190-8-wide.jpg', '', '98783', '2014-01-14 14:51:07', '2014-01-14 14:51:07', 'Article', '132');
INSERT INTO `photos` VALUES ('127', '1050948617.gif', '', '197644', '2014-01-14 14:51:21', '2014-01-14 14:51:21', 'Article', '133');
INSERT INTO `photos` VALUES ('128', '115.jpg', '', '910795', '2014-01-14 14:51:33', '2014-01-14 14:51:33', 'Article', '134');
INSERT INTO `photos` VALUES ('129', 'the-torch-dubai-marina-002.jpg', '', '147890', '2014-01-16 05:52:33', '2014-01-16 05:52:33', 'Article', '135');
INSERT INTO `photos` VALUES ('130', '5.jpg', '', '969321', '2014-01-16 05:56:41', '2014-01-16 05:56:41', 'Article', '136');
INSERT INTO `photos` VALUES ('131', '2008_Olympic_Games_Equestrian_Game_Day_Celemony_01.jpg', '', '553809', '2014-01-16 05:59:19', '2014-01-16 05:59:19', 'Article', '137');
INSERT INTO `photos` VALUES ('132', 'Dubai-Trafrfic.jpg', '', '97861', '2014-01-16 06:01:28', '2014-01-16 06:01:28', 'Article', '138');
INSERT INTO `photos` VALUES ('133', 'business_people.jpg', '', '374874', '2014-01-16 06:14:18', '2014-01-16 06:14:18', 'Article', '139');
INSERT INTO `photos` VALUES ('134', 'Tulips.jpg', '', '620888', '2014-01-16 06:16:18', '2014-01-16 06:16:18', 'Article', '140');
INSERT INTO `photos` VALUES ('135', 'Jellyfish.jpg', '', '775702', '2014-01-16 06:17:19', '2014-01-16 06:17:19', 'Article', '141');
INSERT INTO `photos` VALUES ('136', 'website-photo-state-incentive-page.jpg', '', '214407', '2014-01-16 06:19:13', '2014-01-16 06:19:13', 'Article', '142');
INSERT INTO `photos` VALUES ('137', 'fog-over-dubai-3-935x600.jpg', '', '78349', '2014-01-16 06:21:26', '2014-01-16 06:21:26', 'Article', '143');
INSERT INTO `photos` VALUES ('138', 'Desert.jpg', '', '845941', '2014-02-12 06:11:18', '2014-02-12 06:11:18', 'Article', '144');
INSERT INTO `photos` VALUES ('139', 'one2.sql', '', '271769', '2014-02-12 06:11:20', '2014-02-12 06:11:20', 'Article', '144');
INSERT INTO `photos` VALUES ('140', '5.jpg', '', '969321', '2014-02-13 06:49:58', '2014-02-13 06:49:58', 'Article', '145');
INSERT INTO `photos` VALUES ('141', '5.jpg', '', '969321', '2014-02-13 07:21:41', '2014-02-13 07:21:41', 'Press_release', '1');
INSERT INTO `photos` VALUES ('142', 'Koala.jpg', '', '780831', '2014-02-13 07:22:45', '2014-02-13 07:22:45', 'Press_release', '2');
INSERT INTO `photos` VALUES ('143', 'Tulips.jpg', '', '620888', '2014-02-13 07:23:58', '2014-02-13 07:23:58', 'Press_release', '3');
INSERT INTO `photos` VALUES ('144', 'Lighthouse.jpg', '', '561276', '2014-02-13 10:58:36', '2014-02-13 10:58:36', 'Idea', '3');
INSERT INTO `photos` VALUES ('145', '1-auckland-airport.jpg', '', '45427', '2014-02-13 10:59:54', '2014-02-13 10:59:54', 'Idea', '4');
INSERT INTO `photos` VALUES ('146', '163.jpg', '', '139081', '2014-02-13 11:00:22', '2014-02-13 11:00:22', 'Idea', '5');
INSERT INTO `photos` VALUES ('147', '1 (5).jpg', 'Logo for Mandrake', '88552', '2014-02-19 06:57:46', '2014-02-19 06:57:46', 'Company', '11');
INSERT INTO `photos` VALUES ('148', '1 (5).jpg', 'Logo for Blue Basics', '88552', '2014-02-19 09:49:32', '2014-02-19 09:49:32', 'Company', '12');
INSERT INTO `photos` VALUES ('149', '1 (5).jpg', 'Logo for Blue Basics', '88552', '2014-02-19 09:50:24', '2014-02-19 09:50:24', 'Company', '13');
INSERT INTO `photos` VALUES ('150', '1 (5).jpg', 'Logo for Blue Basics', '88552', '2014-02-19 10:01:42', '2014-02-19 10:01:42', 'Company', '14');
INSERT INTO `photos` VALUES ('151', '1 (5).jpg', 'Logo for Blue Basics', '88552', '2014-02-19 10:02:16', '2014-02-19 10:02:16', 'Company', '15');
INSERT INTO `photos` VALUES ('152', '1 (5).jpg', 'Logo for Mandrake Man', '88552', '2014-02-19 10:50:40', '2014-02-19 10:50:40', 'Company', '18');
INSERT INTO `photos` VALUES ('153', '1 (2).jpg', '', '54430', '2014-02-20 06:13:10', '2014-02-20 06:13:10', 'Press_release', '5');
INSERT INTO `photos` VALUES ('156', '1.jpg', 'Banner ', '27616', '2014-03-10 08:31:26', '2014-03-10 08:31:26', 'Ad_banner', '2');
INSERT INTO `photos` VALUES ('157', '02.jpg', 'Banner ', '224209', '2014-03-10 08:31:51', '2014-03-10 08:31:51', 'Ad_banner', '3');

-- ----------------------------
-- Table structure for `press_releases`
-- ----------------------------
DROP TABLE IF EXISTS `press_releases`;
CREATE TABLE `press_releases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of press_releases
-- ----------------------------
INSERT INTO `press_releases` VALUES ('1', '#MyDubai initiative to showcase life in the city receives 75,000 entries', '1', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', '0000-00-00', '', '<p>In such a short span of time WAI-ME has made numerous achievements as well as garnered the trust of al entities who share in the passion that the women of the Middle East also can aim high. With the support of our supporters and renewed interest of all our members and participants alike we are more than confident that WAI-ME would enable women of the middle east to set new milestones in realizing their dreams.<br />\r\nThe changing culture of acceptance for women has indeed allowed women in the middle east push beyond what was regarded as contemporary and it is with the intent to ensure that women of the Middle East are well aware that they too can attain their dreams in the skies, that WAI had established the Middle East Chapter. Our aim is simple, to help support women throughout the Middle East in their passion to be a part of the growing aviation sector. And at the same time render the stereotypical image of the Middle Eastern woman as that of one who is oppressed as a falsification &ndash; as we too can make our dreams come true.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1', '2014-02-13 07:21:41', '2014-02-13 07:21:41');
INSERT INTO `press_releases` VALUES ('2', 'Dubai Ruler speaks to BBC about UAE’s legal system', '3', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', '0000-00-00', 'public', '<p>In such a short span of time WAI-ME has made numerous achievements as well as garnered the trust of al entities who share in the passion that the women of the Middle East also can aim high. With the support of our supporters and renewed interest of all our members and participants alike we are more than confident that WAI-ME would enable women of the middle east to set new milestones in realizing their dreams.<br />\r\nThe changing culture of acceptance for women has indeed allowed women in the middle east push beyond what was regarded as contemporary and it is with the intent to ensure that women of the Middle East are well aware that they too can attain their dreams in the skies, that WAI had established the Middle East Chapter. Our aim is simple, to help support women throughout the Middle East in their passion to be a part of the growing aviation sector. And at the same time render the stereotypical image of the Middle Eastern woman as that of one who is oppressed as a falsification &ndash; as we too can make our dreams come true.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1', '2014-02-13 07:22:45', '2014-02-13 07:22:45');
INSERT INTO `press_releases` VALUES ('3', 'Dh2.59bn flats and hotel deal for Reem Island', '2', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', '0000-00-00', '', '<p>In such a short span of time WAI-ME has made numerous achievements as well as garnered the trust of al entities who share in the passion that the women of the Middle East also can aim high. With the support of our supporters and renewed interest of all our members and participants alike we are more than confident that WAI-ME would enable women of the middle east to set new milestones in realizing their dreams.<br />\r\nThe changing culture of acceptance for women has indeed allowed women in the middle east push beyond what was regarded as contemporary and it is with the intent to ensure that women of the Middle East are well aware that they too can attain their dreams in the skies, that WAI had established the Middle East Chapter. Our aim is simple, to help support women throughout the Middle East in their passion to be a part of the growing aviation sector. And at the same time render the stereotypical image of the Middle Eastern woman as that of one who is oppressed as a falsification &ndash; as we too can make our dreams come true.</p>\r\n\r\n<p>In such a short span of time WAI-ME has made numerous achievements as well as garnered the trust of al entities who share in the passion that the women of the Middle East also can aim high. With the support of our supporters and renewed interest of all our members and participants alike we are more than confident that WAI-ME would enable women of the middle east to set new milestones in realizing their dreams.<br />\r\nThe changing culture of acceptance for women has indeed allowed women in the middle east push beyond what was regarded as contemporary and it is with the intent to ensure that women of the Middle East are well aware that they too can attain their dreams in the skies, that WAI had established the Middle East Chapter. Our aim is simple, to help support women throughout the Middle East in their passion to be a part of the growing aviation sector. And at the same time render the stereotypical image of the Middle Eastern woman as that of one who is oppressed as a falsification &ndash; as we too can make our dreams come true.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1', '2014-02-13 07:23:58', '2014-02-13 07:23:58');
INSERT INTO `press_releases` VALUES ('4', '', '0', '', '0000-00-00', '', '', '0', '2014-02-20 05:48:54', '2014-02-20 05:48:54');
INSERT INTO `press_releases` VALUES ('5', '#MyDubai initiative to showcase life in the city receives 75,000 entries', '18', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', '0000-00-00', '', 'Editor’s Note: The following is a guest post by Farnoosh Brock of Prolific Living. “We should start choosing our thoughts like we choose our clothes for the day.” —Farnoosh Brock Fact: No two physical objects can occupy the same space at the same time. This is just basic physics. We have to choose between this piece […]', '0', '2014-02-20 06:13:10', '2014-02-20 06:13:10');
INSERT INTO `press_releases` VALUES ('8', '#MyDubai initiative to showcase life in the city receives 75,000 entries', '18', 'http://www.youtube.com/watch?v=UyiFBa4LGx4', '0000-00-00', '', 'Editor’s Note: The following is a guest post by Farnoosh Brock of Prolific Living. “We should start choosing our thoughts like we choose ', '0', '2014-02-20 06:31:32', '2014-02-20 06:31:32');

-- ----------------------------
-- Table structure for `privileges`
-- ----------------------------
DROP TABLE IF EXISTS `privileges`;
CREATE TABLE `privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `admin` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of privileges
-- ----------------------------
INSERT INTO `privileges` VALUES ('1', 'administrator', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `privileges` VALUES ('2', 'Test', 'a:30:{s:8:\"accounts\";a:3:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"update\";}s:8:\"articles\";a:1:{i:0;s:6:\"create\";}s:10:\"categories\";a:2:{i:0;s:6:\"create\";i:1;s:6:\"delete\";}s:9:\"companies\";a:3:{i:0;s:4:\"read\";i:1;s:6:\"update\";i:2;s:6:\"delete\";}s:18:\"company_interviews\";a:2:{i:0;s:6:\"create\";i:1;s:6:\"update\";}s:15:\"company_reviews\";a:2:{i:0;s:4:\"read\";i:1;s:6:\"delete\";}s:10:\"complaints\";a:1:{i:0;s:6:\"update\";}s:10:\"ad_banners\";a:1:{i:0;s:4:\"read\";}s:9:\"countries\";a:4:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"update\";i:3;s:6:\"delete\";}s:13:\"deal_sections\";a:1:{i:0;s:6:\"delete\";}s:5:\"ideas\";a:2:{i:0;s:4:\"read\";i:1;s:6:\"update\";}s:10:\"industries\";a:4:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"update\";i:3;s:6:\"delete\";}s:4:\"jobs\";a:3:{i:0;s:4:\"read\";i:1;s:6:\"update\";i:2;s:6:\"delete\";}s:7:\"members\";a:3:{i:0;s:6:\"create\";i:1;s:6:\"update\";i:2;s:6:\"delete\";}s:8:\"services\";a:3:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"update\";}s:5:\"pages\";a:1:{i:0;s:6:\"create\";}s:11:\"ad_settings\";a:2:{i:0;s:6:\"update\";i:1;s:6:\"delete\";}s:4:\"user\";a:1:{i:0;s:6:\"delete\";}s:7:\"patents\";a:2:{i:0;s:6:\"create\";i:1;s:6:\"update\";}s:14:\"press_releases\";a:3:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"delete\";}s:18:\"patent_investments\";a:1:{i:0;s:6:\"update\";}s:6:\"videos\";a:2:{i:0;s:4:\"read\";i:1;s:6:\"update\";}s:8:\"sections\";a:2:{i:0;s:6:\"create\";i:1;s:6:\"delete\";}s:18:\"membership_options\";a:1:{i:0;s:6:\"update\";}s:11:\"red_carpets\";a:2:{i:0;s:6:\"create\";i:1;s:6:\"update\";}s:12:\"news_headers\";a:2:{i:0;s:6:\"create\";i:1;s:6:\"update\";}s:12:\"transactions\";a:3:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"update\";}s:14:\"video_sections\";a:3:{i:0;s:6:\"create\";i:1;s:6:\"update\";i:2;s:6:\"delete\";}s:11:\"auto_emails\";a:3:{i:0;s:6:\"create\";i:1;s:4:\"read\";i:2;s:6:\"update\";}s:10:\"privileges\";a:2:{i:0;s:6:\"create\";i:1;s:4:\"read\";}}', '0', '2014-03-11 08:02:41', '2014-03-11 08:02:41');

-- ----------------------------
-- Table structure for `red_carpets`
-- ----------------------------
DROP TABLE IF EXISTS `red_carpets`;
CREATE TABLE `red_carpets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of red_carpets
-- ----------------------------

-- ----------------------------
-- Table structure for `sections`
-- ----------------------------
DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sort` int(11) NOT NULL,
  `is_published` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sections
-- ----------------------------
INSERT INTO `sections` VALUES ('1', 'News', '2013-12-09 17:25:32', '2014-03-04 13:05:00', '2', '0');
INSERT INTO `sections` VALUES ('2', 'Business', '2013-12-09 17:26:30', '2013-12-09 17:26:30', '0', '0');
INSERT INTO `sections` VALUES ('3', 'Worldwide', '2013-12-09 17:28:17', '2013-12-09 17:28:59', '0', '0');
INSERT INTO `sections` VALUES ('4', 'Opinions', '2013-12-09 17:29:15', '2013-12-09 17:29:15', '0', '0');

-- ----------------------------
-- Table structure for `services`
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `is_published` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES ('1', 'industrial', '2013-12-05 17:58:01', '2013-12-05 17:58:01', '', '0', '0');
INSERT INTO `services` VALUES ('2', 'Real  Estates Services', '2014-03-09 11:23:36', '2014-03-09 11:23:36', '<p>Details go here and here and here and here and here</p>\r\n', '2', '1');

-- ----------------------------
-- Table structure for `transactions`
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `membership` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dated` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of transactions
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `company_id` int(11) NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'admin@one1info.com', '$2y$08$AxQrBjAMnQVhKLs24xyZyejhxenAVwlxwHPY7k1MAnUTMgsp5GgNu', '2013-10-28 16:26:11', '2013-10-28 16:26:11', '0', '', '0');
INSERT INTO `users` VALUES ('29', 'mandrake', 'admin@bobby.com', '$2y$10$EOpi/R8hK.pcuR5q7B9VxODGdAY0M61qKr9Zs6Xck5diQ46p2pLGW', '2014-02-19 10:50:40', '2014-02-19 10:50:40', '18', '', '0');
INSERT INTO `users` VALUES ('31', 'admin1', 'ali@yahoo.com', '$2y$10$50q/kv4lYU1eO3GN2jNf/OsD.RRJZBIzUc0a8LYreARJGQQGEnIbq', '2014-02-19 11:37:07', '2014-02-19 11:37:07', '18', 'user', '0');
INSERT INTO `users` VALUES ('32', 'blubasic', 'blue@yahoo.com', '$2y$10$JGLoUo/tDoACskkLsvCuVeBc4anMtfzdh.EMxwlApaiZRFQUzYIOK', '2014-02-23 05:15:12', '2014-02-23 05:15:12', '0', '', '1');
INSERT INTO `users` VALUES ('33', 'blueman', 'blue1@yahoo.com', '$2y$10$6CdQAEVght5gRuT0/5XjLOdCKg6aVYw/Z3CarxnTXlp2ldj2MGWT.', '2014-02-23 05:17:15', '2014-02-23 05:17:15', '19', '', '0');
INSERT INTO `users` VALUES ('43', 'redadmin', 'ali@ramjet.ae', '$2y$10$9Ht82g4fk/zlLlumGWf2u.gCtbIT2KlBv5NN68BHTQgJXO0no5Nra', '2014-02-24 07:58:23', '2014-02-24 08:14:52', '0', '', '1');
INSERT INTO `users` VALUES ('44', 'member', 'it@ramjet.aero', '$2y$10$Cw4QkGt1/Te9SQ4N.xU3mu5k0q1aN9366U17nMEtg9pmhks9LGNTK', '2014-02-24 11:08:38', '2014-02-24 11:08:38', '0', '', '1');
INSERT INTO `users` VALUES ('45', 'grando', 'it@ramjet.ae', '$2y$10$p/osEyXpUX9d4I93OAJt4Omyr22mIKFjPY4/TqMMGRS9yxJk4vvhe', '2014-03-02 06:17:57', '2014-03-02 06:19:13', '20', '', '1');

-- ----------------------------
-- Table structure for `users_roles`
-- ----------------------------
DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users_roles
-- ----------------------------
INSERT INTO `users_roles` VALUES ('1', '1', 'administrator', '2013-10-28 16:26:11', '2013-10-28 16:26:11');
INSERT INTO `users_roles` VALUES ('2', '4', 'basic-member', '2013-12-19 08:11:42', '2013-12-19 08:11:42');
INSERT INTO `users_roles` VALUES ('5', '7', 'basic-member', '2013-12-19 08:50:16', '2013-12-19 08:50:16');
INSERT INTO `users_roles` VALUES ('6', '8', 'basic-member', '2013-12-19 08:52:22', '2013-12-19 08:52:22');
INSERT INTO `users_roles` VALUES ('10', '12', 'basic-company', '2013-12-19 10:12:01', '2013-12-19 10:12:01');
INSERT INTO `users_roles` VALUES ('14', '16', 'basic-company', '2013-12-19 10:20:21', '2013-12-19 10:20:21');
INSERT INTO `users_roles` VALUES ('18', '20', 'basic-company', '2013-12-19 10:22:44', '2013-12-19 10:22:44');
INSERT INTO `users_roles` VALUES ('19', '21', 'basic-company', '2013-12-19 10:23:51', '2013-12-19 10:23:51');
INSERT INTO `users_roles` VALUES ('20', '22', 'basic-company', '2014-02-19 06:57:46', '2014-02-19 06:57:46');
INSERT INTO `users_roles` VALUES ('21', '23', 'basic-company', '2014-02-19 09:49:32', '2014-02-19 09:49:32');
INSERT INTO `users_roles` VALUES ('22', '24', 'basic-company', '2014-02-19 09:50:23', '2014-02-19 09:50:23');
INSERT INTO `users_roles` VALUES ('23', '25', 'basic-company', '2014-02-19 10:01:41', '2014-02-19 10:01:41');
INSERT INTO `users_roles` VALUES ('24', '26', 'basic-company', '2014-02-19 10:02:15', '2014-02-19 10:02:15');
INSERT INTO `users_roles` VALUES ('25', '27', 'basic-company', '2014-02-19 10:49:09', '2014-02-19 10:49:09');
INSERT INTO `users_roles` VALUES ('26', '28', 'basic-company', '2014-02-19 10:50:11', '2014-02-19 10:50:11');
INSERT INTO `users_roles` VALUES ('27', '29', 'basic-company', '2014-02-19 10:50:40', '2014-02-19 10:50:40');
INSERT INTO `users_roles` VALUES ('28', '31', 'premium-member', '2014-02-19 11:37:07', '2014-02-19 11:37:07');
INSERT INTO `users_roles` VALUES ('29', '32', 'premium-company', '2014-02-23 05:15:12', '2014-02-23 05:15:12');
INSERT INTO `users_roles` VALUES ('30', '33', 'basic-company', '2014-02-23 05:17:15', '2014-02-23 05:17:15');
INSERT INTO `users_roles` VALUES ('31', '34', 'basic-member', '2014-02-24 07:10:47', '2014-02-24 07:10:47');
INSERT INTO `users_roles` VALUES ('32', '35', 'basic-member', '2014-02-24 07:11:29', '2014-02-24 07:11:29');
INSERT INTO `users_roles` VALUES ('33', '36', 'basic-member', '2014-02-24 07:11:56', '2014-02-24 07:11:56');
INSERT INTO `users_roles` VALUES ('34', '37', 'basic-member', '2014-02-24 07:12:22', '2014-02-24 07:12:22');
INSERT INTO `users_roles` VALUES ('35', '38', 'basic-member', '2014-02-24 07:14:16', '2014-02-24 07:14:16');
INSERT INTO `users_roles` VALUES ('36', '39', 'basic-member', '2014-02-24 07:15:54', '2014-02-24 07:15:54');
INSERT INTO `users_roles` VALUES ('37', '40', 'basic-member', '2014-02-24 07:22:35', '2014-02-24 07:22:35');
INSERT INTO `users_roles` VALUES ('38', '41', 'basic-member', '2014-02-24 07:29:07', '2014-02-24 07:29:07');
INSERT INTO `users_roles` VALUES ('39', '42', 'basic-member', '2014-02-24 07:57:14', '2014-02-24 07:57:14');
INSERT INTO `users_roles` VALUES ('40', '43', 'basic-member', '2014-02-24 07:58:23', '2014-02-24 07:58:23');
INSERT INTO `users_roles` VALUES ('41', '44', 'basic-member', '2014-02-24 11:08:38', '2014-02-24 11:08:38');
INSERT INTO `users_roles` VALUES ('42', '45', 'basic-company', '2014-03-02 06:17:57', '2014-03-02 06:17:57');

-- ----------------------------
-- Table structure for `video_sections`
-- ----------------------------
DROP TABLE IF EXISTS `video_sections`;
CREATE TABLE `video_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `is_published` int(11) NOT NULL,
  `system` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of video_sections
-- ----------------------------
INSERT INTO `video_sections` VALUES ('1', 'Business', '1', '1', '0', '2014-03-10 11:48:49', '2014-03-10 11:48:49');
INSERT INTO `video_sections` VALUES ('2', 'Lifestyle', '2', '0', '0', '2014-03-10 11:49:03', '2014-03-10 11:49:03');
INSERT INTO `video_sections` VALUES ('3', 'Entertainment', '3', '1', '0', '2014-03-10 11:49:17', '2014-03-10 11:49:17');
INSERT INTO `video_sections` VALUES ('4', 'youtube', '0', '0', '1', '2014-03-10 16:16:27', '2014-03-10 16:16:31');

-- ----------------------------
-- Table structure for `videos`
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `providor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES ('1', 'CENTRAL AFRICAN REPUBLIC CRISIS EXPLAINED IN 60 SECONDS - BBC NEWS', 'youtube', '-EkRbHzlLxs', 'You want to find our why is there chaos in the Central Africa Republic? Make sure to check our our 60 second explainer.', '1', '0', '2013-12-17 06:04:33', '2013-12-17 06:04:33', '1');
INSERT INTO `videos` VALUES ('2', ' Inside North Korea BBC Panorama News Programme', 'youtube', '2zDYrFE985g', 'North Korean travel experts have slammed the BBC for putting local guides at risk, saying the country would hold them responsible for the actions of the journalists who posed as LSE students on a tour of the country.', '1', '0', '2013-12-17 06:05:28', '2013-12-17 06:05:28', '1');
INSERT INTO `videos` VALUES ('3', 'How Robots Will Change the World - BBC Documentary', 'youtube', '8zP7yP8hdLE', 'Robotics is the art and commerce of robots, their design, manufacture, application, and practical use. Robots will soon be everywhere, in our home and at work. They will change the way we live. This will raise many philosophical, social, and political questions that will have to be answered. In science fiction, robots become so intelligent that they decide to take over the world because humans are deemed inferior. In real life, however, they might not choose to do that. Robots might follow rules such as Asimov\'s Three Laws of Robotics, that will prevent them from doing so. When the Singularity happens, robots will be indistinguishable from human beings and some people may become Cyborgs: half man and half machine. ', '1', '0', '2013-12-17 06:06:31', '2013-12-17 06:06:31', '1');
INSERT INTO `videos` VALUES ('4', ' \'I HAD A BREAKDOWN WHEN I SAW ANGELS\' MANDELA INTERPRETER - BBC NEWS ', 'youtube', '7c33Omwka0A', 'The sign language interpreter accused of using fake hand signals at Nelson Mandela\'s memorial has told the BBC that he had a breakdown when he saw \'angels come from the sky\'', '1', '0', '2013-12-17 06:08:12', '2013-12-17 06:08:12', '1');
INSERT INTO `videos` VALUES ('5', 'The Girl Who Never Ate - Extraordinary People - Documentary', 'youtube', '4vIjIqPPUSU', 'The story of Tia McCarthy, a seven-year-old girl with a rare disorder in which her oesophagus and stomach are unconnected. Despite corrective surgery, she has never eaten a morsel.', '1', '0', '2013-12-17 06:09:31', '2013-12-17 06:09:31', '1');
INSERT INTO `videos` VALUES ('6', 'Conflict in Syria', 'youtube', 'FRMT55jsaIs', 'Given the conflict in Syria, the war in Afghanistan, and negotiations over Iran\'s nuclear program, how do Americans see U.S. foreign policy right now? The Pew Research Center has released a new study today on a whole range of topics and the BBC\'s David Botti takes a look at what they found.', '1', '0', '2013-12-17 06:24:45', '2013-12-17 06:24:45', '1');
INSERT INTO `videos` VALUES ('7', 'WHERE STREETS CARRY MANDELA\'S NAME - BBC NEWS ', 'youtube', '8cTGj0WHbQw', 'Mandela\'s legacy is set to endure for a long time yet - not least because his name is etched on to streets, buildings and monuments around the world.', '1', '0', '2013-12-17 06:25:56', '2013-12-17 06:25:56', '1');
INSERT INTO `videos` VALUES ('43', 'SHEIKH MOHAMMED \'HALTS PLANES\' TO CONDUCT EXCLUSIVE BBC NEWS INTERVIEW ', 'youtube', 'jaTv1vAC3W8', 'Jon Sopel explains how, during the exclusive BBC News interview with Sheikh Mohammed, planes in the sky were halted in order for the interview to take place. ', '1', '0', '2014-01-16 06:25:04', '2014-01-16 06:25:04', '2');
INSERT INTO `videos` VALUES ('44', 'AMR MOUSSA \' NEW EGYPTIAN CONSTITUTION MARKS NEW ERA\' - BBC NEWS ', 'youtube', '4cGoOWBryx8', 'Veteran Egyptian diplomat Amr Moussa, Chairman of the 50-member committee which drafted the constitution, talks to BBC News about how important the referendum for a new consitution is for Egypt.', '1', '0', '2014-01-16 06:30:14', '2014-01-16 06:30:14', '2');
INSERT INTO `videos` VALUES ('45', 'BBC HARDtalk - Amr Moussa - Secretary General of the Arab League (2001-2011) (30/8/13) ', 'youtube', 'olJ4R4mpeXY', 'The Egyptian authorities are drafting a new constitution that would ban religious-based political parties. It\'s prompted a furious reaction from those who support the deposed President, Mohamed Morsi. They say it will incite \"chaos\" within Egypt and opens the door to \"the system which produced pharaohs\". HARDtalk speaks to the country\'s former foreign minister and former head of the Arab League, Amr Moussa. He supports the new military-backed government, but does he trust it to bring true democracy to Egypt?', '1', '0', '2014-01-16 06:31:13', '2014-01-16 06:31:13', '2');
INSERT INTO `videos` VALUES ('46', ' Frost Over the World - Mohamed ElBaradei', 'youtube', 'u0C7YONpgjc', 'A week after the resignation of Hosni Mubarak, Sir David is joined by Nobel Laureate Mohamed ElBaradei, the man many are touting to be the next president of Egypt. Plus, Jeffrey Ghannam on the role of social media in the protests that are sweeping the Middle East.\r\n', '1', '0', '2014-01-16 06:31:48', '2014-01-16 06:31:48', '2');
INSERT INTO `videos` VALUES ('47', ' Frost Over the World - Imran Khan: Pakistan\'s next leader?', 'youtube', 'z8JlW8kwozg', 'Imran Khan, the cricketer-turned-politician discusses the fight against corruption and violence in Pakistan. Also joining Sir David Frost is author Thomas Friedman to talk about the global role of the US.\r\n', '1', '0', '2014-01-16 06:32:44', '2014-01-16 06:32:44', '2');
INSERT INTO `videos` VALUES ('48', 'Murdoch University Dubai on Emirates 247 for The Women\'s Lunchbox 2013 ', 'youtube', '8yXPHhYfayg', 'Farhanah Raza from Murdoch University Dubai discussing Gulf Glass Ceiling\r\n', '1', '0', '2014-01-16 06:33:47', '2014-01-16 06:33:47', '2');
INSERT INTO `videos` VALUES ('49', 'The Palm Island, Dubai UAE - Megastructure Development ', 'youtube', '0BXGh0EYJtE', 'An awe-inspiring engineering feat, Dubai Palm Islands is definitely the largest artificial islands on earth. These Islands are three man made islands in the form of massive palm trees connected to the beaches of Dubai. The first island is the Palm Jumeirah, Jebel Ali Palm second and the third is the Palm Deira which is the biggest of them all. This artificial archipelago located off the coastline of United Arab Emirates in Persian Gulf.\r\n\r\nDubai islands project was commissioned by Sheikh Mohammed in an effort to maximize beachfront property. Dubai\'s palm tree-shaped resort island on land built from the sea will contribute 120 km of beaches. Aside from the construction of the ambitious expansion, there is also the problem of constructing a mini city, which consists of 4,500 high-end residential homes, miles of roads, luxurious hotels and many amenities that will require basic human resources including electricity, safe drinking water and many more. This enormous project will position Dubai as one of the top tourist destinations in the world.', '1', '0', '2014-01-16 06:34:22', '2014-01-16 06:34:22', '2');
INSERT INTO `videos` VALUES ('50', 'City of Arabia, Dubai', 'youtube', '3sosUbZyxTI', 'City of Arabia is a spectacular multibillion US$ retail, residential, commercial and entertainment destination lying at the gateway to Dubailand.\r\n\r\nWith business and administrative offices, schools and clinics, supported by luxury apartments, shops, galleries, restaurants and unique attractions, City of Arabia will become a key destination and urban community within the new Dubai. The project will have a phased opening starting towards the end of 2011.', '1', '0', '2014-01-16 06:35:13', '2014-01-16 06:35:13', '3');
INSERT INTO `videos` VALUES ('51', 'Skydive Dubai - May 2011', 'youtube', 'xFEN7BQ7Zus', 'Music :\r\nAdele - Rolling In The Deep (Moto Blanco Radio Edit)\r\nAlexandra Stan - Get Back (Radio Edit)\r\nZoot vs Zoe Badwi - Freefallin (Radio Edit)\r\n\r\nMusics belong to their respective authors, no copyright infringement intended', '1', '0', '2014-01-16 06:35:53', '2014-01-16 06:35:53', '3');
INSERT INTO `videos` VALUES ('52', 'Ultimate Dubai Airport - Episode 2 - National Geographic', 'youtube', 'hmbovsDhx7k', 'Music :\r\nAdele - Rolling In The Deep (Moto Blanco Radio Edit)\r\nAlexandra Stan - Get Back (Radio Edit)\r\nZoot vs Zoe Badwi - Freefallin (Radio Edit)\r\n\r\nMusics belong to their respective authors, no copyright infringement intended', '1', '0', '2014-01-16 06:36:30', '2014-01-16 06:36:30', '3');
INSERT INTO `videos` VALUES ('53', 'Worlds Largest Aircraft - Even Bigger than a380 - shuttle carrier', 'youtube', '4yqnDOxlNEk', 'The Antonov An-225 Mriya (Ukrainian: Антонов Ан-225 Мрія, Dream, NATO reporting name: \'Cossack\') is a strategic airlift cargo aircraft, designed by the Soviet Union\'s Antonov Design Bureau in the 1980s. The An-225\'s name, Mriya (Мрiя) means \"Dream\" (Inspiration) in Ukrainian. It is the largest airplane in the world; it is the heaviest aircraft with a gross weight of 640 tonnes and the biggest heavier-than-air aircraft in terms of length and wingspan in operational service.', '1', '0', '2014-01-16 06:37:16', '2014-01-16 06:37:16', '3');
INSERT INTO `videos` VALUES ('54', 'THE CONCORDE STORY ', 'youtube', 'b6OCuJZ9bX4', 'The Aérospatiale-BAC Concorde is a turbojet-powered supersonic passenger airliner, a supersonic transport (SST). It was a product of an Anglo-French government treaty, combining the manufacturing efforts of Aérospatiale and the British Aircraft Corporation. First flown in 1969, Concorde entered service in 1976 and continued commercial flights for 27 years.\r\n\r\nAmong other destinations, Concorde flew regular transatlantic flights from London Heathrow (British Airways) and Paris-Charles de Gaulle Airport (Air France) to New York JFK, profitably flying these routes at record speeds, in less than half the time of other airliners.', '1', '0', '2014-01-16 06:38:27', '2014-01-16 06:38:27', '3');
INSERT INTO `videos` VALUES ('55', ' How legally protected are UAE residents in the event of a nuclear accident?', 'youtube', '2zu6yH89Nkc', 'By 2017 the Emirates Nuclear Energy Corporation hopes to have some of the UAE\'s power being produced from its upcoming nuclear plant in Abu Dhabi.\r\n\r\nEmirates 24/7 on Dubai One\'s Priyanka Dutt finds out just how protected UAE residents are in the event of an accident.', '1', '0', '2014-01-16 06:39:35', '2014-01-16 06:39:35', '3');
INSERT INTO `videos` VALUES ('56', 'The UAE\'s Upcoming Credit Rating System', 'youtube', 'hLrzOJyfOQc', 'Next year everyone in the UAE will have a credit rating, helping banks decide the credit-worthiness of borrowers.\r\n\r\nIt\'s a move designed to help banks against loan defaulters and also help borrowers manage their debts and get the best interest rates from banks.', '1', '0', '2014-01-16 06:40:09', '2014-01-16 06:40:09', '3');
INSERT INTO `videos` VALUES ('57', 'How to Stay Out of Debt: Warren Buffett - Financial Future of American Youth (1999) ', 'youtube', 'IvveZr0D_9Y', 'Buffett became a billionaire on paper when Berkshire Hathaway began selling class A shares on May 29, 1990, when the market closed at $7,175 a share. In 1998, in an unusual move, he acquired General Re (Gen Re) for stock. In 2002, Buffett became involved with Maurice R. Greenberg at AIG, with General Re providing reinsurance. On March 15, 2005, AIG\'s board forced Greenberg to resign from his post as Chairman and CEO under the shadow of criticism from Eliot Spitzer, former attorney general of the state of New York. On February 9, 2006, AIG and the New York State Attorney General\'s office agreed to a settlement in which AIG would pay a fine of $1.6 billion. In 2010, the federal government settled with Berkshire Hathaway for $92 million in return for the firm avoiding prosecution in an AIG fraud scheme, and undergoing \'corporate governance concessions\'.', '1', '0', '2014-01-16 06:40:41', '2014-01-16 06:40:41', '4');
INSERT INTO `videos` VALUES ('58', ' \"60 Minutes To Getting Rich.\" Robert Kiyosaki.', 'youtube', 'ehbn-NQX3Jg', 'This is a video from his \"Choose To Be Rich\" package. NO COPYRIGHT INFRINGEMENT INTENDED!\r\n', '1', '0', '2014-01-16 06:41:18', '2014-01-16 06:41:18', '4');
INSERT INTO `videos` VALUES ('59', 'NEW MILLIONAIRES - HOW ARE THEY MAKING THEIR MONEY (Documentary) Finance/Education', 'youtube', 'rGWI7mjmnNk', 'NEW MILLIONAIRES - HOW THEY MAKING THEIR MONEY (Documentary) Finance/Education\r\n', '1', '0', '2014-01-16 06:41:52', '2014-01-16 06:41:52', '4');
INSERT INTO `videos` VALUES ('60', 'An unsuccessful story - from Millionaire to Homeless', 'youtube', 'jasLByiVOWg', 'Comunidade Vida e Paz is a nonprofit organization in Portugal dedicated to providing the poor and homeless of Lisbon with nutritious meals, clothing, and assistance in the transition to employment and life off the streets. Comunidade Vida e Paz operates a daily meal program through the dedication of its volunteers and is not only serving food and clothes but also gives those homeless people the confidence they so desperately need to become productive members of society again and leave life on the streets.\r\n', '1', '0', '2014-01-16 06:42:37', '2014-01-16 06:42:37', '4');
INSERT INTO `videos` VALUES ('61', 'ABC\'s Secret Millionaire Ali Brown goes to Common Ground to help homeless youth. ', 'youtube', 'qK2WbFRja80', 'ABC\'s Secret Millionaire Ali Brown goes undercover to volunteer for the homeless youth program at Common Ground, the Westside HIV Community Center. After working with kids living on the streets of Venice, California -- and hearing about lives being saved and changed -- she reveals her secret.\r\n', '1', '0', '2014-01-16 06:43:13', '2014-01-16 06:43:13', '4');
INSERT INTO `videos` VALUES ('62', ' How Money Is Made - Documentary', 'youtube', 'ZGDwEr0GT10', 'All about currency. How money is printed in the United States mint, the Treasury Department, counterfeiters and thieves are all explained in depth.\r\n', '1', '0', '2014-01-16 06:43:54', '2014-01-16 06:43:54', '4');
INSERT INTO `videos` VALUES ('63', 'Super Rich - The Greed Game (Documentary) ', 'youtube', 'DLIeW9agjoY', 'Finance and money documentary on Super Rich: The Greed Game.\r\n\r\nAs the credit crunch bites and a global economic crisis threatens, Robert Peston reveals how the super-rich have made their fortunes, and the rest of us are picking up the bill.\r\n\r\n', '1', '0', '2014-01-16 06:44:34', '2014-01-16 06:44:34', '4');
